package com.norma.atear_mobile.db;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.authorized.data.AuthorizedInfo;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.project.SharedPref;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by hyojin on 7/8/16.
 */
public class AtEarDB extends SQLiteOpenHelper {
    //db
    private static final String DB_NAME = "atear_DB.db";
    private static final int DATABASE_VERSION = 5;

    //table
    private static final String PACKET_TABLE = "packet_table";
    private static final String CONNECT_AP_TABLE = "connect_aptable";
    private static final String SSID_TABLE = "ssidtable";
    private static final String BSSID_TABLE = "bssidtable";
    private static final String SINGLE_BSSID_TABLE = "single_bssidtable";
    private static final String REPORT_TABLE = "report_table";
    private static final String FILTER_SSID_TABLE = "filter_ssid_table";
    private static final String LICENSE_TABLE = "licenseTable";
    private static final String DICTIONARY_TABLE = "dictionaryTable";
    private static final String OPEN_DNS_TABLE = "openDNS_Table";
    //column
    private static final String PROJECT = "projectName";
    private static final String LOCATION = "location";
    private static final String SAVE_TIME = "save_time";
    private static final String GPS = "gps";

    private static final String BSSID = "bssid";
    private static final String SSID = "ssid";
    private static final String SIGNAL = "signal";
    private static final String CHANNEL = "channel";
    private static final String ENCRYPTION = "encryption";
    private static final String CIPHER = "cipher";
    private static final String AUTH = "auth";
    private static final String SCAN_TIME = "scan_time";
    private static final String APTYPE = "ap_type";
    private static final String OUI = "oui";
    //	private static final String MODEL = "model";
    private static final String COMMENT = "comment";
    private static final String ALERT = "alert";
    private static final String RISK = "risk";
    private static final String WPS = "wps";
    private static final String AUTHORIZED = "isAuthorized";
    private static final String SSID_FILTER = "ssid_filter";
    private static final String BSSID_FILTER = "bssid_filter";
    private static final String POLICY_TYPE = "policy_type";
    private static final String PICTURE_PATH = "picture_path";
    private static final String MEMO = "memo";
    private static final String IS_PENTEST = "isPentest";
    private static final String PENTEST_PWD = "PENTEST_PWD";

    private static final String KEY_ID = "idx";
    private static final String IP = "ip_info";
    private static final String DNS = "dns";
    private static final String DNS1 = "dns1";
    private static final String DNS2 = "dns2";
    private static final String ATTACK_TIYE = "attck_type";
    private static final String ATTACK_IP = "acttck_ip";
    private static final String ATTACK_MAC = "acttck_mac";
    private static final String ATTACK_TIME = "attck_time";

    private static final String LICENSE = "license";
    private static final String END_DATE = "end_date";
    private static final String IS_UPDATE = "is_update";
    private static final String DEVICE_MAC = "device_mac";

    private static final String DICTIONARY = "dictionary";

    private String LICENSE_TABLE_CREATE = "create table " + LICENSE_TABLE + "("
            + LICENSE + " TEXT , " + DEVICE_MAC + " TEXT , " + END_DATE + " integer , " + IS_UPDATE + " integer);";

    //create table
    private String REPORT_TABLE_CREATE = "create table " + REPORT_TABLE + "("
            + PROJECT + " TEXT , " + LOCATION + " TEXT,"
            + SAVE_TIME + " TEXT , " + GPS + " TEXT,"
            + BSSID + " TEXT , " + SSID + " TEXT,"
            + SIGNAL + " integer , " + CHANNEL + " integer,"
            + ENCRYPTION + " TEXT , " + CIPHER + " TEXT,"
            + AUTH + " TEXT , " + SCAN_TIME + " TEXT,"
            + APTYPE + " TEXT , " + OUI + " TEXT,"
            + COMMENT + " TEXT , " + ALERT + " TEXT,"
            + RISK + " integer , " + WPS + " integer,"
            + AUTHORIZED + " integer , " + SSID_FILTER + " integer,"
            + BSSID_FILTER + " integer , " + POLICY_TYPE + " integer,"
            + PICTURE_PATH + " TEXT , " + MEMO + " TEXT,"
            + IS_PENTEST + " integer , " + PENTEST_PWD + " TEXT);";

    private String PACKET_TABLE_CREATE = "create table " + PACKET_TABLE + "("
            + BSSID + " TEXT , " + SSID + " TEXT,"
            + SIGNAL + " integer , " + CHANNEL + " integer,"
            + ENCRYPTION + " TEXT , " + CIPHER + " TEXT,"
            + AUTH + " TEXT , " + SCAN_TIME + " TEXT,"
            + APTYPE + " TEXT , " + OUI + " TEXT,"
            + COMMENT + " TEXT , " + ALERT + " TEXT,"
            + RISK + " integer , " + WPS + " integer,"
            + AUTHORIZED + " integer , " + SSID_FILTER + " integer,"
            + BSSID_FILTER + " integer , " + POLICY_TYPE + " integer,"
            + PICTURE_PATH + " TEXT , " + MEMO + " TEXT,"
            + IS_PENTEST + " integer , " + PENTEST_PWD + " TEXT);";


    private static final String CONNECT_APTABLE_CREATE = "create table " +
            CONNECT_AP_TABLE + " (" + KEY_ID
            + " integer primary key autoincrement, " // id: 0
            + SSID + " TEXT, "
            + BSSID + " TEXT, "
            + IP + " TEXT, "
            + DNS1 + " TEXT, "
            + DNS2 + " TEXT, "
            + ATTACK_TIYE + " integer, "
            + ATTACK_IP + " TEXT, "
            + ATTACK_MAC + " TEXT, "
            + ATTACK_TIME + " TEXT);";

    private static final String FILTER_SSID_TABLE_CREATE = "create table " + FILTER_SSID_TABLE
            + "(" + SSID + " TEXT , " + SAVE_TIME + " TEXT);";


    private static final String SSID_TABLE_CREATE = "create table "
            + SSID_TABLE + "(" + SSID + " TEXT)";

    private static final String BSSID_TABLE_CREATE = "create table "
            + BSSID_TABLE + "( " + LOCATION + "  TEXT, " + BSSID + " TEXT primary key, " + SSID + " TEXT)";

    private static final String SINGLE_BSSID_TABLE_CREATE = "create table "
            + SINGLE_BSSID_TABLE + "(" + BSSID + " TEXT)";

    private static final String DICTIONARY_TABLE_CREATE = "create table "
            + DICTIONARY_TABLE + "(" + DICTIONARY + " TEXT)";

    private static final String OPEN_DNS_TABLE_CREATE = "create table "
            + OPEN_DNS_TABLE + "(" + DNS + " TEXT);";


    private static final String PACKET_TABLE_DROP = "drop table " + PACKET_TABLE;

    private static final String SINGLE_BSSID_TABLE_DROP = "drop table " + SINGLE_BSSID_TABLE;

    private static final String REPORT_TABLE_DROP = "drop table " + REPORT_TABLE;

    private static final String LICENSE_TABLE_DROP = "drop table " + LICENSE_TABLE;


    public AtEarDB(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PACKET_TABLE_CREATE);
        db.execSQL(SSID_TABLE_CREATE);
        db.execSQL(BSSID_TABLE_CREATE);
        db.execSQL(CONNECT_APTABLE_CREATE);
        db.execSQL(REPORT_TABLE_CREATE);
        db.execSQL(FILTER_SSID_TABLE_CREATE);
        db.execSQL(SINGLE_BSSID_TABLE_CREATE);
        db.execSQL(LICENSE_TABLE_CREATE);
        //   db.execSQL(DICTIONARY_TABLE_CREATE); 나중에 필요한거 같애 .. 딕셔너리 테이블
        db.execSQL(OPEN_DNS_TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + PACKET_TABLE);
        db.execSQL("drop table if exists " + SSID_TABLE);
        db.execSQL("drop table if exists " + BSSID_TABLE);
        db.execSQL("drop table if exists " + CONNECT_AP_TABLE);
        db.execSQL("drop table if exists " + REPORT_TABLE);
        db.execSQL("drop table if exists " + FILTER_SSID_TABLE);
        db.execSQL("drop table if exists " + SINGLE_BSSID_TABLE);
        db.execSQL("drop table if exists " + LICENSE_TABLE);
        //     db.execSQL("drop table if exists " + DICTIONARY_TABLE_CREATE);
        db.execSQL("drop table if exists " + OPEN_DNS_TABLE);


        onCreate(db);
    }

    public void LICENSE_TABLE_INSERT(String license, String device_mac, long date, int isUpdate, int reload) {

        SQLiteDatabase db = getWritableDatabase();

        if (reload == 1) {
            LICENSE_TABLE_DROP();
        }

        String sqlSelect = "select " + LICENSE + " from " + LICENSE_TABLE + " where " + LICENSE + " = '" + license + "'";

        Cursor c = db.rawQuery(sqlSelect, null);

        if (c.getCount() != 0) {

        } else {
            String sql = "insert into " + LICENSE_TABLE + " values "
                    + "('" + license + "' , '" + device_mac + "' , '" + date + "' , '" + isUpdate + "');";
            try {
                db.execSQL(sql);
            } catch (Exception e) {
                Log.e("db error ", "license insert");
            }
        }
        c.close();
        db.close();

    }

    public void LICENSE_TABLE_DROP() {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(LICENSE_TABLE_DROP);

        } catch (Exception e) {
        }
        try {
            db.execSQL(LICENSE_TABLE_CREATE);

        } catch (Exception e) {
        }
        db.close();
    }

    public void LICENSE_TABLE_UPDATE(String license, long date) {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + LICENSE_TABLE + " set " + END_DATE + " = " + date + " , "
                + IS_UPDATE + " = '" + 1 + "' where " + LICENSE + " = '" + license + "';";

        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("db error ", "license update");
        }
        db.close();

    }


    public void PACKET_TABLE_INSERT(Ap_info sr) {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "insert into " + PACKET_TABLE + " values "
                + "('" + sr.bssid + "' , '" + sr.ssid + "' , '" + sr.signal + "' , '" + sr.channel + "' , '"
                + sr.encryption + "' , '" + sr.cipher + "' , '" + sr.auth + "' , '" + sr.scan_time + "' , '"
                + sr.ap_type + "' , '" + sr.oui + "' , '" + sr.comment + "' , '" + sr.alert + "' , '" + sr.risk + "' , '" + sr.wps + "' , '"
                + sr.isAuthorized + "' , '" + sr.ssid_filter + "' , '" + sr.bssid_filter + "' , '"
                + sr.policyType + "' , '" + sr.picturePath + "' , '" + sr.memo + "','" + sr.isPentest + "','" + sr.pentest_password + "');";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            //강제 종료.
        }
        db.close();
    }

    public Ap_info PACKET_TABLE_SELECT(String bssid, String ssid) {

        Ap_info info = new Ap_info();

        SQLiteDatabase db = getWritableDatabase();
        //뿌려줄 데이터 ssid, bssid, signal  encyption wps , alert, risk

        String sql = "select " + ALERT + " , " + SSID + ", " + BSSID + " , "
                + APTYPE + " , " + OUI + " , " + ENCRYPTION + " , " + CHANNEL
                + " , " + RISK + " from " + PACKET_TABLE + " where " + BSSID + " = '" + bssid + "' and " + SSID + " = '" + ssid + "';";
        try {
            Cursor c = db.rawQuery(sql, null);

            while (c.moveToNext()) {

                info.alert = c.getString(0);
                info.ssid = c.getString(1);
                info.bssid = c.getString(2);
                info.ap_type = c.getString(3);
                info.oui = c.getString(4);
                info.encryption = c.getString(5);
                info.channel = c.getInt(6);
                info.risk = c.getInt(7);
            }
            c.close();
        } catch (Exception e) {
            Log.e("db error", "PACKET_TABLE_SELECT");
        }
        db.close();

        return info;

    }

    public void PACKET_TABLE_UPDATE_PANTEST(Ap_info sr, int isPentest, String pwd) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "update " + PACKET_TABLE + " set " + IS_PENTEST + " = " + isPentest + " , " + PENTEST_PWD + " = '"
                + pwd + "' where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("packetDB error", "pantesting error");
        }
        db.close();
    }

    public void PACKET_TABLE_UPDATE_AUTHORIZED(Ap_info sr) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "update " + PACKET_TABLE + " set " + AUTHORIZED + " d= 1 where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("packetDB error", "authorized error");
        }
        db.close();
    }

    public void PACKET_TABLE_UPDATE_RISK(Ap_info sr) {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + PACKET_TABLE + " set " + RISK + " = '" + sr.risk + "' where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";

        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("db error", "packet update");
        }
        db.close();
    }

    public void PACKET_TABLE_UPDATE(Ap_info sr) {

        SQLiteDatabase db = getWritableDatabase();

        int old_level = 0;
        int authorized = 0;
        String sql = "select " + SIGNAL + ", " + AUTHORIZED + " from " + PACKET_TABLE + " where "
                + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "'";
        try {
            Cursor c = db.rawQuery(sql, null);

            if (c.getCount() != 0) {

                while (c.moveToNext()) {
                    old_level = c.getInt(0);
                    authorized = c.getInt(1);
                }

                if (((sr.risk & 0x02) == 0x02) && authorized != sr.isAuthorized) {

                    sql = "update " + PACKET_TABLE + " set " + SIGNAL + " = '" + sr.signal + "' , "
                            + RISK + " = '" + sr.risk + "' , " + AUTHORIZED + " = '" + sr.isAuthorized
                            + "' , " + ALERT + " = '" + sr.alert + "' , " + COMMENT + " = '" + sr.comment
                            + "' where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";
                    try {
                        db.execSQL(sql);
                    } catch (Exception e) {
                        Log.e("db error", "packet update");
                    }
                } else if (((sr.risk & 0x02) == 0x02) && authorized == sr.isAuthorized) {
                    sql = "update " + PACKET_TABLE + " set " + SIGNAL + " = '" + sr.signal + "' , "
                            + RISK + " = '" + sr.risk + "' where " + BSSID + " = '" + sr.bssid
                            + "' and " + SSID + " = '" + sr.ssid + "';";
                    try {
                        db.execSQL(sql);
                    } catch (Exception e) {
                        Log.e("db error", "packet update");
                    }
                } else if (authorized != sr.isAuthorized) {
                    sql = "update " + PACKET_TABLE + " set " + SIGNAL + " = '" + sr.signal + "' , "
                            + AUTHORIZED + " = '" + sr.isAuthorized + "' ," + ALERT + " = '" + sr.alert + "' , "
                            + COMMENT + " = '" + sr.comment + "' where " + BSSID + " = '" + sr.bssid
                            + "' and " + SSID + " = '" + sr.ssid + "';";
                    try {
                        db.execSQL(sql);
                    } catch (Exception e) {
                        Log.e("db error", "packet update");
                    }
                } else {
                    if (sr.signal >= old_level) {
                        sql = "update " + PACKET_TABLE + " set " + SIGNAL + " = '" + sr.signal + "' where "
                                + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";
                        try {
                            db.execSQL(sql);
                        } catch (Exception e) {
                            Log.e("db error", "packet update");
                        }
                    }
                }
            } else {
                PACKET_TABLE_INSERT(sr);

            }
            c.close();
        } catch (Exception e) {

        }

        db.close();
    }

    public int PACKET_TABLE_SELECT_SIZE() {

        SQLiteDatabase db = getWritableDatabase();

        int packetSize = 0;

        String sql = "select count(" + BSSID + ") from " + PACKET_TABLE;
        try {
            Cursor c = db.rawQuery(sql, null);

            if (c.getCount() > 0) {
                while (c.moveToNext())
                    packetSize = c.getInt(0);
            }
            c.close();
        } catch (Exception e) {
            Log.e("packetTable", "length select error");
        }

        db.close();

        return packetSize;
    }


    public void PACKET_TABLE_DROP() {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(PACKET_TABLE_DROP);
        } catch (Exception e) {
        }
        try {
            db.execSQL(PACKET_TABLE_CREATE);
        } catch (Exception e) {
        }
        db.close();
    }

    public void PACKET_TABLE_DELETE() {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "Delete from " + PACKET_TABLE + ";";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("db error", "packet delete");
        }
        db.close();
    }

    public void REPORT_TABLE_INSERT(Ap_info sr) {
        //1.1.03 버전 까지 사용
       /* SQLiteDatabase db = getWritableDatabase();

        String oldScanTime = "";
        int oldSignal = 0;

        String selectSql = "select " + SCAN_TIME + " , " + SIGNAL + " from " + REPORT_TABLE + " where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";

        String sql = "insert into " + REPORT_TABLE + " values "
                + "('" + sr.projectName + "' , '" + sr.locationName + "' , '" + sr.save_time + "' , '" + sr.gps + "' , '"
                + sr.bssid + "' , '" + sr.ssid + "' , '" + sr.signal + "' , '" + sr.channel + "' , '"
                + sr.encryption + "' , '" + sr.cipher + "' , '" + sr.auth + "' , '" + sr.scan_time + "' , '"
                + sr.ap_type + "' , '" + sr.oui + "' , '" + sr.comment + "' , '" + sr.alert + "' , '" + sr.risk + "' , '" + sr.wps + "' , '"
                + sr.isAuthorized + "' , '" + sr.ssid_filter + "' , '" + sr.bssid_filter + "' , '"
                + sr.policyType + "','" + sr.picturePath + "','" + sr.memo + "','" + sr.isPentest + "','" + sr.pentest_password + "');";

        String deleteSql = "delete from " + REPORT_TABLE + " where " + BSSID + " = '" + sr.bssid + "'";

        Cursor c = db.rawQuery(selectSql, null);
        if (c.getCount() != 0) {

            while (c.moveToNext()) {
                oldScanTime = c.getString(0);
                oldSignal = c.getInt(1);
            }
           // Log.e("time", String.valueOf(Long.parseLong(oldScanTime.replaceAll("[^0-9]", ""))));

            //1.1.03 버전 부터 날짜가 동일시 시그널 기준
            long oldtempTIme = Long.parseLong(oldScanTime.substring(0, 7).replaceAll("[^0-9]", ""));
            long newtempTime = Long.parseLong(sr.scan_time.substring(0, 7).replaceAll("[^0-9]", ""));


            if (oldtempTIme == newtempTime) {
                //날짜가 동일 할 경우 시그널 기준
                if(oldSignal <= sr.signal ){
                    try {
                        db.execSQL(deleteSql);
                        db.execSQL(sql);
                    } catch (Exception e) {
                        Log.e("error", String.valueOf(e));
                    }
                }
            }else if(oldtempTIme < newtempTime){
                //새로운 날짜가 더 높을 경우
                try {
                    db.execSQL(deleteSql);
                    db.execSQL(sql);
                } catch (Exception e) {
                    Log.e("error", String.valueOf(e));
                }
            }else{
                //기존날짜가 더클경우 no insert
            }
        } else {
            try {
                db.execSQL(sql);
            } catch (Exception e) {
                //강제 종료.
            }
        }
        c.close();
        db.close();*/

        //1.1.04 버전
        SQLiteDatabase db = getWritableDatabase();

        int oldSingal = 0;

        String selectSql = "select " + SIGNAL + " from " + REPORT_TABLE + " where " + BSSID + " = '" + sr.bssid + "' and " + SSID + " = '" + sr.ssid + "';";

        String sql = "insert into " + REPORT_TABLE + " values "
                + "('" + sr.projectName + "' , '" + sr.locationName + "' , '" + sr.save_time + "' , '" + sr.gps + "' , '"
                + sr.bssid + "' , '" + sr.ssid + "' , '" + sr.signal + "' , '" + sr.channel + "' , '"
                + sr.encryption + "' , '" + sr.cipher + "' , '" + sr.auth + "' , '" + sr.scan_time + "' , '"
                + sr.ap_type + "' , '" + sr.oui + "' , '" + sr.comment + "' , '" + sr.alert + "' , '" + sr.risk + "' , '" + sr.wps + "' , '"
                + sr.isAuthorized + "' , '" + sr.ssid_filter + "' , '" + sr.bssid_filter + "' , '"
                + sr.policyType + "','" + sr.picturePath + "','" + sr.memo + "','" + sr.isPentest + "','" + sr.pentest_password + "');";

        Cursor c = db.rawQuery(selectSql, null);
        if (c.getCount() != 0) {

            while (c.moveToNext()) {
                oldSingal = c.getInt(0);
            }
            // Log.e("aaabb", oldSingal + "   " + sr.signal);
            if (oldSingal > sr.signal) {
                //      Log.e("trrr", oldSingal + "   " + sr.signal);
                String deleteSql = "delete from " + REPORT_TABLE + " where " + BSSID + " = '" + sr.bssid + "'";
                try {
                    db.execSQL(deleteSql);
                    db.execSQL(sql);
                } catch (Exception e) {

                }
            } else {
                // Log.e("insert reportTable", "overlap bssid");
            }
        } else {
            try {
                db.execSQL(sql);
            } catch (Exception e) {
                //강제 종료.
            }
        }
        c.close();
        db.close();
    }

    public boolean REPORT_TABLE_SELECT() {
        boolean check = false;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "select count(" + PROJECT + ") from " + REPORT_TABLE;

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() == 0) {
            check = false;
        } else {
            while (c.moveToNext()) {
                if (c.getInt(0) == 0) {
                    check = false;
                } else {
                    check = true;
                }
            }
        }
        c.close();
        db.close();

        return check;
    }


    public void REPORT_TABLE_DROP() {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(REPORT_TABLE_DROP);
        } catch (Exception e) {

        }
        db.execSQL(REPORT_TABLE_CREATE);

        db.close();

    }


    public boolean SSID_TABLE_SELECT(String data) {

        SQLiteDatabase db = getWritableDatabase();

        boolean isData;

        String sql = "select " + SSID + " from " + SSID_TABLE + " where " + SSID + " = '" + data + "'";
        try {
            Cursor c = db.rawQuery(sql, null);

            if (c.getCount() != 0) {
                isData = true;
            } else {
                isData = false;
            }
            c.close();
        } catch (Exception e) {
            isData = false;
        }
        db.close();
        return isData;

    }

    public boolean BSSID_TABLE_SELECT(String data) {

        SQLiteDatabase db = getWritableDatabase();

        boolean isData;
        String sql = "select " + BSSID + " from " + BSSID_TABLE + " where " + BSSID + " = '" + data + "'";
        try {
            Cursor c = db.rawQuery(sql, null);
            if (c.getCount() != 0) {
                isData = true;
            } else {
                isData = false;
            }
            c.close();
        } catch (Exception e) {
            isData = false;
        }

        db.close();
        return isData;
    }

    public ArrayList<AuthorizedInfo> BSSID_TABLE_SELECT() {

        ArrayList<AuthorizedInfo> apList = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "select " + LOCATION + " , " + BSSID + " , " + SSID + " from " + BSSID_TABLE + ";";
        try {
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                //ssid ,bssid
                AuthorizedInfo ap_info = new AuthorizedInfo();
                ap_info.locationName = c.getString(0);
                ap_info.bssid = c.getString(1);
                ap_info.ssid = c.getString(2);
                ap_info.del = false;
                apList.add(ap_info);
            }
            c.close();
        } catch (Exception e) {

        }
        db.close();
        return apList;
    }

    public ArrayList<AuthorizedInfo> BSSID_TABLE_SELECT_LIKE(String text) {

        ArrayList<AuthorizedInfo> apList = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "select " + LOCATION + " , " + BSSID + " , " + SSID + " from " + BSSID_TABLE + " where " + BSSID + " like '%" + text + "%';";
        try {
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                //ssid ,bssid
                AuthorizedInfo ap_info = new AuthorizedInfo();
                ap_info.locationName = c.getString(0);
                ap_info.bssid = c.getString(1);
                ap_info.ssid = c.getString(2);
                ap_info.del = false;
                apList.add(ap_info);
            }
            c.close();
        } catch (Exception e) {

        }

        db.close();
        return apList;
    }

    public void BSSID_TABLE_INSERT(ArrayList<String> bssidList) {

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < bssidList.size(); i++) {
            if (!loadingProgress.isShowing())
                break;
            try {
                db.execSQL("insert into " + BSSID_TABLE + " values('null' , '" + bssidList.get(i) + "', 'null');");
            } catch (Exception e) {
            } finally {
                loadingProgress.setProgress(i);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void BSSID_TABLE_INSERT(String location, String bssid, String ssid, Context mContext) {

        SQLiteDatabase db = getWritableDatabase();
        SharedPref sharedPref = new SharedPref(mContext);
        int language = sharedPref.getValue("language", 0);

        String sqlSelect = "select " + BSSID + " , " + SSID + " , " + LOCATION + "  from " + BSSID_TABLE + " where " + BSSID + " = '" + bssid + "';";
        Cursor c = db.rawQuery(sqlSelect, null);

        if (c.getCount() == 0) {
            String sql = "insert into " + BSSID_TABLE + " values('" + location + "', '" + bssid + "', '" + ssid + "');";
            try {
                db.execSQL(sql);

                String msg = String.format(mContext.getResources().getString(R.string.ssid_auth), ssid);


                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                //   Log.e("bssidTable", "insert error");
            }
        } else {
            while (c.moveToNext()) {
                if (!ssid.equals(c.getString(1)) || !location.equals(c.getString(2))) {
                    String sqlUpdate = "update " + BSSID_TABLE + " set " + SSID + " = '" + ssid
                            + "' , " + LOCATION + "= '" + location + "' where " + BSSID + " = '" + bssid + "';";
                    try {
                        db.execSQL(sqlUpdate);
                    } catch (Exception e) {
                        Log.e("BSSID_TABLE", "update error");
                    }
                }
            }
            Toast.makeText(mContext, mContext.getResources().getString(R.string.its_added), Toast.LENGTH_SHORT).show();
        }
        c.close();
        db.close();
    }

    public void BSSID_TABLE_DELETE(ArrayList<String> bssidlist) {
        if (bssidlist.size() != 0) {
            SQLiteDatabase db = getWritableDatabase();
            db.beginTransaction();

            final boolean[] isThreading = {true};
            try {
                for (int i = 0; i < bssidlist.size(); i++) {
                    //  Log.e("deleteee", bssidlist.get(i));
                    if (!loadingProgress.isShowing())
                        break;
                    db.execSQL("delete from " + BSSID_TABLE + " where " + BSSID + " = '" + bssidlist.get(i) + "';");
                    loadingProgress.setProgress(i);
                    loadingProgress.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isThreading[0] = false;
                        }
                    });
                    if (isThreading[0] == false) {
                        break;
                    }
                }
                db.setTransactionSuccessful();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
                db.close();
            }
            //  db.close();
        }
    }

    public void SINGLE_BSSID_TABLE_INSERT(ArrayList<String> list) {

        SQLiteDatabase db = getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < list.size(); i++) {
                if (!loadingProgress.isShowing())
                    break;
                db.execSQL("insert into " + SINGLE_BSSID_TABLE + " values('" + list.get(i) + "');");
                loadingProgress.setProgress(i);
            }
            db.setTransactionSuccessful();

        } catch (SQLException e) {
            Log.e("single bssid table ", "insert error");
        } finally {
            db.endTransaction();
            db.close();
        }
//        for (String s : list) {
//            String sql = "insert into " + SINGLE_BSSID_TABLE + " values('" + s + "');";
//
//            try {
//                db.execSQL(sql);
//                Log.e("single",s);
//            } catch (Exception e) {
//                Log.e("single bssid table ", "insert error");
//            }
//        }


    }

    public void SINGLE_BSSID_TABLE_DROP() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL(SINGLE_BSSID_TABLE_DROP);

        } catch (Exception e) {

        }
        try {
            db.execSQL(SINGLE_BSSID_TABLE_CREATE);

        } catch (Exception e) {

        }
        db.close();
    }

    public boolean SINGLE_BSSID_TABLE_SELECT(String data) {

        SQLiteDatabase db = getWritableDatabase();

        boolean isData;
        String sql = "select " + BSSID + " from " + SINGLE_BSSID_TABLE + " where " + BSSID + " = '" + data + "'";
        try {
            Cursor c = db.rawQuery(sql, null);


            if (c.getCount() != 0) {
                isData = true;
            } else {
                isData = false;
            }
            c.close();
        } catch (Exception e) {
            isData = false;
        }

        db.close();
        return isData;
    }

    public void PACKET_TABLE_UPDATE_SSIDFITTLER(String bssid) {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + PACKET_TABLE + " set " + SSID_FILTER + " = 1 where " + BSSID + " = '" + bssid + "';";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("db error", "filtering....");
        }
        db.close();
    }

    public int PACKET_TABLE_UPDATE_MEMO(String bssid, String path, String memo) {

        int i = 0;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + PACKET_TABLE + " set " + PICTURE_PATH + " = '"
                + path + "' , " + MEMO + " = '" + memo + "' where " + BSSID + " = '" + bssid + "';";
        try {
            db.execSQL(sql);
            //  Log.e("IS_PENTEST", path);
            i = 1;
        } catch (Exception e) {
            Log.e("db error", "packet table momo ....");
        }
        db.close();
        return i;
    }


    //접속기록검증
    public void CONNECT_AP_TABLE_INSERT(String ssid, String bssid, String ip, String dns1, String dns2) {

        SQLiteDatabase db = getWritableDatabase();

        String sql = "insert into " + CONNECT_AP_TABLE
                + " values(null, '" + ssid + "' , '" + bssid + "' , '" + ip + "' , '" + dns1 + "' , '" + dns2 + "', 0 ,'null' ,'null', 'null');";
        try {
            db.execSQL(sql);

        } catch (Exception e) {
            Log.e("Insert table exception", String.valueOf(e));
        }
        db.close();
    }

    //접속기록검증
    public void CONNECT_AP_TABLE_SELECT(String bssid, String ssid, String getip, String getDns1, String getDns2) {

        // boolean check = false;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "select " + IP + " , " + DNS1 + " , " + DNS2 + ""
                + " from " + CONNECT_AP_TABLE + " where " + BSSID + " ='" + bssid + "'";

        String ip = "";
        String dns1 = "";
        String dns2 = "";
        //Log.e("connectAP",bssid+","+ssid+","+getDns1+","+getDns2);
        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                ip = c.getString(0);
                dns1 = c.getString(1);
                dns2 = c.getString(2);


                if (!getip.equals(ip) || !getDns1.equals(dns1) || !getDns2.equals(dns2)) {
                    //        check = true;
                    CONNECT_AP_TABLE_UPDATE(bssid, 4);
                }
            }
        } else {
            CONNECT_AP_TABLE_INSERT(ssid, bssid, getip, getDns1, getDns2);
            Log.e("hyojin_TAG10", "insert :" + getip + " , " + getDns1 + " , " + getDns2);
        }

        c.close();
        db.close();
        //  return check;
    }

    //접속기록검증 update
    public void CONNECT_AP_TABLE_UPDATE_IP(String bssid, String geteway, String dns1, String dns2, int type) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + CONNECT_AP_TABLE +
                " set " + IP + "='" + geteway + "' , " + DNS1 + "='" + dns1 + "', " +
                "" + DNS2 + "='" + dns2 + "', " + ATTACK_TIYE + "=" + type + " where " + BSSID + " = '" + bssid + "';";
        //  Log.e("hyojin_TAG10", "update :" + geteway + " , " + dns1 + " , " + dns2);
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("UPdate table exception", String.valueOf(e));
        }

        db.close();

    }

    public void CONNECT_AP_TABLE_UPDATE(String bssid, int attack_type) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + CONNECT_AP_TABLE +
                " set " + ATTACK_TIYE + "='" + attack_type + "' where " + BSSID + " = '" + bssid + "';";

        // Log.e("hyojin_TAG10", "update :" + attack_type );
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("UPdate table exception", String.valueOf(e));
        }
        db.close();
    }


    //스푸핑
    public void CONNECT_AP_TABLE_UPDATE_SPOOFING(String bssid, int attack_type, String attack_ip, String attack_mac, String attack_time) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "update " + CONNECT_AP_TABLE +
                " set " + ATTACK_TIYE + "='" + attack_type + "' , " + ATTACK_IP + "='" + attack_ip
                + "', " + ATTACK_MAC + "= '" + attack_mac + "' , " + ATTACK_TIME + "='" + attack_time + "' where " + BSSID + " = '" + bssid + "';";

        Log.e("Connect DB", "update :" + attack_type + " , " + attack_ip + " , " + attack_time);
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("UPdate table exception", String.valueOf(e));
        }
        db.close();
    }

    public int CONNECT_AP_TABLE_SELECT_SPOOFING(String bssid) {
        SQLiteDatabase db = getWritableDatabase();

        int attack_type = 0;

        String sql = "select " + ATTACK_TIYE + " from " + CONNECT_AP_TABLE + " where " + BSSID + " = '" + bssid + "'";

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {
                attack_type = c.getInt(0);
            }
        }
        c.close();
        db.close();
        return attack_type;
    }


    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH) + 1;
        int day = today.get(Calendar.DAY_OF_MONTH);
        int hour = today.get(Calendar.HOUR_OF_DAY);
        int minute = today.get(Calendar.MINUTE);
        int second = today.get(Calendar.SECOND);
        String result = String.valueOf(year) + "-" + String.valueOf(month) + "-"
                + String.valueOf(day) + " " + String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second);

        return result;
    }


    /**
     * 딕셔너리 DB만들어 놨으니까 쓰고 싶으면 갖다 쓰자  !@#!@#!@#$!
     *
     * @param data
     */

    public void DICTIONARY_TABLE_INSERT(String data) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "insert into " + DICTIONARY_TABLE + " values('" + data + "');";
        try {
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("DictonaryTable", "insert error");
        }

        db.close();
    }

    public ArrayList<String> DICTIONARY_TABLE_SELECT() {

        ArrayList<String> dictionaryList = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        String sql = "select " + DICTIONARY + " from " + DICTIONARY_TABLE;

        Cursor c = db.rawQuery(sql, null);
        try {
            if (c.getCount() > 0) {
                while (c.moveToNext()) {
                    String data = c.getString(0);
                    dictionaryList.add(data);
                }
            }
        } catch (Exception e) {
            Log.e("DictonaryTable", "select error");
        }
        c.close();
        db.close();

        return dictionaryList;
    }

    public boolean DICTIONARY_TABLE_SELECT(String data) {

        boolean isSelect = false;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "select " + DICTIONARY + " from " + DICTIONARY_TABLE + " where " + DICTIONARY + " = '" + data + "'";

        Cursor c = db.rawQuery(sql, null);
        try {
            if (c.getCount() > 0) {
                isSelect = true;
            } else {
                isSelect = false;
            }
        } catch (Exception e) {
            Log.e("DictonaryTable", "select data error");
        }
        c.close();
        db.close();

        return isSelect;
    }

    public boolean DICTIONARY_TABLE_DELETE(String data) {

        boolean isDelete = false;
        boolean isData = this.DICTIONARY_TABLE_SELECT(data);

        if (isData == true) {
            String sql = "DELETE from " + DICTIONARY_TABLE + " where " + DICTIONARY + " = '" + data + "';";
            SQLiteDatabase db = getWritableDatabase();
            try {
                db.execSQL(sql);
                isDelete = true;
            } catch (Exception e) {
                Log.e("DictonaryTable", "DELETE data error");
            }
            db.close();
        }
        return isDelete;
    }

    public int OPEN_DNS_TABLE_SELECT() { // 총갯수

        int count = 0;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "select count(*) from " + OPEN_DNS_TABLE;

        Cursor c = db.rawQuery(sql, null);

        try {
            if (c.getCount() > 0) {
                count = c.getInt(0);
            }
        } catch (Exception e) {
            Log.e("openDns table", "select error");
        }
        c.close();
        db.close();
        return count;

    }

    public boolean OPEN_DNS_TABLE_SELECT(String data) {
        boolean isSelect = false;

        SQLiteDatabase db = getWritableDatabase();

        String sql = "select " + DNS + " from " + OPEN_DNS_TABLE + " where " + DNS + " = '" + data + "';";

        Cursor c = db.rawQuery(sql, null);
        try {
            if (c.getCount() > 0) {
                isSelect = true;
            } else {
                isSelect = false;
            }
        } catch (Exception e) {
            Log.e("openDnsTable", "select data error");
        }
        c.close();
        db.close();
        return isSelect;
    }

    public boolean OPEN_DNS_TABLE_INSERT(String data) {

        boolean isInsert = false;
        boolean isData = OPEN_DNS_TABLE_SELECT(data);

        if (isData == false) {

            SQLiteDatabase db = getWritableDatabase();

            String sql = "insert into " + OPEN_DNS_TABLE + " values('" + data + "');";
            try {
                db.execSQL(sql);
                isInsert = true;
                db.close();
            } catch (Exception e) {
                Log.e("openDNS table ", "insert error");
            }
        }

        return isInsert;
    }

    public boolean OPEN_DNS_TABLE_DELETE(String data) {

        boolean isDelete = false;
        boolean isData = OPEN_DNS_TABLE_SELECT(data);

        if (isData == false) {
            SQLiteDatabase db = getWritableDatabase();

            String sql = "delete from " + OPEN_DNS_TABLE + " where " + DNS + " = '" + data + "';";
            try {
                db.execSQL(sql);
                isDelete = true;
                db.close();
            } catch (Exception e) {
                Log.e("openDNS table ", "Delete error");
            }
        }
        return isDelete;
    }

    private ProgressDialog loadingProgress; // IO 입/출력 시스템의 로딩역할 ProgressBar

    public void setLoadingProgress(final Context mContext, final ProgressDialog loadingProgress, final int max, final String message) {
        this.loadingProgress = loadingProgress;
        this.loadingProgress.setMax(max);
        this.loadingProgress.setProgress(0);
        this.loadingProgress.setCancelable(true);
        this.loadingProgress.setCanceledOnTouchOutside(false);
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingProgress.setMessage(message != null ? message : "");


            }
        });
    }
}
