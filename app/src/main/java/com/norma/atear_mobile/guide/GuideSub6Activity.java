package com.norma.atear_mobile.guide;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.project.SharedPref;

/**
 * Created by 효진 on 2016-10-21.
 */

public class GuideSub6Activity extends Activity {

    private TextView tv_text;

    private Context mContext;
    private SharedPref sharedPref;

    int language = 0;

    String guideMsg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_sub6);

        findView();


        language = sharedPref.getValue("language", 0);

        switch (language) {
            case 0: //영어
                guideMsg = "- Supported OS: Android 5.0 or later\n\n" +
                        "- Update\n\n" +
                        "When AtEar Mobile is executed, it checks whether this program is the latest version or not. \n" +
                        "If it is an old version, automatic update will take place.\n";
                break;

            case 1: //한국어
                guideMsg = "- 지원 가능 OS: Android 5.0 이상\n\n" +
                        "- 업데이트 패치: AtEar Mobile 실행 시, 프로그램의 신규 버전 여부를 체크합니다. " +
                        "구 버전일 경우 업데이트 팝업을 통한 자동 업데이트가 이루어집니다.\n";
                break;

            case 2:  //일본어
                guideMsg = "comming soon";
                break;

            case 3:  //중국어
                guideMsg = "comming soon";
                break;

        }


        tv_text.setText(guideMsg);

        LinearLayout btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void findView() {
        mContext = this;
        sharedPref = new SharedPref(mContext);

        tv_text = (TextView) findViewById(R.id.tv_text);
    }
}
