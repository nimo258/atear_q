package com.norma.atear_mobile.guide;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.LocaleChanger;
import com.norma.atear_mobile.project.SharedPref;

/**
 * Created by 효진 on 2016-09-07.
 */
public class GuideMainActivity extends Activity {

    private TextView tv_title1;
    private TextView tv_title2;
    private TextView tv_title3;
    private TextView tv_title4;
    private TextView tv_title5;
    private TextView tv_title6;
    private TextView tv_title;

    private Button btn_help;
    private LinearLayout btn_Language;

    private Context mContext;
    private SharedPref sharedPref;

    int language = 0;



    String supportMsg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_main);

        findView();

        language = sharedPref.getValue("language", 0);
        //language =1;


        tv_title1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(GuideMainActivity.this, GuideSub1Activity.class);
                startActivity(intent);
            }
        });

        tv_title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuideMainActivity.this, GuideSub2Activity.class);
                startActivity(intent);
            }
        });

        tv_title3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuideMainActivity.this, GuideSub3Activity.class);
                startActivity(intent);
            }
        });

        tv_title4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuideMainActivity.this, GuideSub4Activity.class);
                startActivity(intent);
            }
        });

        tv_title5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuideMainActivity.this, GuideSub5Activity.class);
                startActivity(intent);
            }
        });

        tv_title6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuideMainActivity.this, GuideSub6Activity.class);
                startActivity(intent);
            }
        });

        btn_Language.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_language);

                final RadioButton rb_english = (RadioButton) dialog.findViewById(R.id.rb_english);
                final RadioButton rb_korean = (RadioButton) dialog.findViewById(R.id.rb_korean);
                final RadioButton rb_japen = (RadioButton) dialog.findViewById(R.id.rb_japen);
                final RadioButton rb_china = (RadioButton) dialog.findViewById(R.id.rb_china);
                final Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
                final TextView tv_china = (TextView) dialog.findViewById(R.id.tv_china);
                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);

                tv_china.setText("中文");

                tv_title.setText(mContext.getResources().getString(R.string.guide9));
                btn_submit.setText(mContext.getResources().getString(R.string.guide10));

                int language = sharedPref.getValue("language", 0);
                switch (language) {
                    case 0: //영어
                        rb_english.setChecked(true);
                        break;

                    case 1: //한국어
                        rb_korean.setChecked(true);
                        break;

                    case 2:  //일본어
                        rb_japen.setChecked(true);
                        break;

                    case 3: // 중국어
                        rb_china.setChecked(true);
                        break;
                }


                rb_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_korean.setChecked(false);
                            rb_japen.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_korean.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_japen.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_japen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_korean.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_china.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_korean.setChecked(false);
                            rb_japen.setChecked(false);
                        }
                    }
                });


                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rb_english.isChecked() == true) {
                            sharedPref.put("language", 0);

                        }
                        if (rb_korean.isChecked() == true) {
                            Toast.makeText(getApplicationContext(), "This Korean is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 1);

                        }
                        if (rb_japen.isChecked() == true) {
                            Toast.makeText(getApplicationContext(), "This 日本語 is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 2);

                        }
                        if (rb_china.isChecked() == true) {
                            Toast.makeText(getApplicationContext(), "This 中文 is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 3);

                        }
                        LocaleChanger.setLang(mContext, sharedPref.getValue("language",0));
                        dialog.dismiss();
                        Intent intent = new Intent(mContext, GuideMainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();
                return false;
            }
        });


        LinearLayout btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "cs.bae@norma.co.kr"));
                startActivity(intent);
            }
        });
    }


    private void findView() {

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title1 = (TextView) findViewById(R.id.tv_title1);
        tv_title2 = (TextView) findViewById(R.id.tv_title2);
        tv_title3 = (TextView) findViewById(R.id.tv_title3);
        tv_title4 = (TextView) findViewById(R.id.tv_title4);
        tv_title5 = (TextView) findViewById(R.id.tv_title5);
        tv_title6 = (TextView) findViewById(R.id.tv_title6);
        btn_help = (Button) findViewById(R.id.btn_help);
        btn_Language = (LinearLayout) findViewById(R.id.btn_Language);
        mContext = this;
        sharedPref = new SharedPref(mContext);
    }
}
