package com.norma.atear_mobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.norma.atear_mobile.component.SHA256Hash;
import com.norma.atear_mobile.project.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by hyojin on 7/6/16.
 */
public class IntroActivity extends Activity {

    private final static String CONNECT_Url = "https://211.110.140.144/is_license";

    private Context mContext;
    private SharedPref sharedPref;
    private SHA256Hash sha256Hash;

    private final static String RESULT = "result";
    private final static String IS_SUCCESS = "success";

    private String license;
    private String license_date;
    private String versionName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mContext = this;
        sharedPref = new SharedPref(mContext);

        try {//version 확인
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            versionName = info.versionName;
            Log.e("androiVersion", versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        license = sharedPref.getValue("LINCESE", "fail");
        license_date = sharedPref.getValue("LINCESE_DATE", "fail");
        Log.e("licenses", "license :" + license + " date:" + license_date);
        if (!license.equals("fail") && isNetworkConnected() == true) {

            sha256Hash = new SHA256Hash();
            //   Log.e("intro", "http " + license);
            LoginHTTP loginHTTP = new LoginHTTP(mContext, license, sha256Hash.getHashHexString(DeviceSerial()), versionName);
            loginHTTP.execute();
        } else {
            // Log.e("intro", "intro " + license);
            IntroTask mIntroTask = new IntroTask();
            mIntroTask.execute();
        }

    }

    private static String DeviceSerial() {
        try {
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception ignored) {
            return null;
        }

    }

    //network가 연결안되어있거나 코드 성공 못했을 때
    private class IntroTask extends AsyncTask<Void, Void, Void> {

        boolean isLincese = false;

        @Override
        protected Void doInBackground(Void... params) {

            if (license_date.equals("fail")) {
                isLincese = false;
            } else {
                final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
                final Calendar endData = Calendar.getInstance();  //end날짜 -

                //end Date 셋팅
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

                try {
                    endData.setTime(simpleDateFormat.parse(license_date));
                    long row_result = (today.getTimeInMillis() - endData.getTimeInMillis()) / 1000;
                    final long result = row_result / (60 * 60 * 24);  //일계산

                    Log.e("day ::::::", String.valueOf(result));

                    if (result >= 0) {
                        //라이센스 만료
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.licenseMsg), Toast.LENGTH_SHORT).show();

                        isLincese = false;
                    } else {
                        isLincese = true;
                    }
                } catch (Exception e) {
                    isLincese = false;
                }
            }


            try {
                Thread.sleep(1200);
            } catch (Exception e) {

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (isLincese) {
                Intent intent = new Intent(mContext, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                sharedPref.put("LINCESE", "fail");
                sharedPref.getValue("LINCESE_DATE", "fail");
                Intent intent = new Intent(mContext, LoginActivity.class) //release (Company has Account)
//                Intent intent = new Intent(mContext, MainActivity.class) // debug (No Signature Account)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }


        }
    }

    //인증
    private class LoginHTTP extends AsyncTask<Void, Void, Void> {

        private StringBuilder sb;

        private String license_number = "null";
        private String client_hash = "null";
        private String version = "null";
        private int responseCode = 0;

        private Context mContext;

        private LoginHTTP(Context mContext, String license_number, String client_hash, String version) {

            this.mContext = mContext;
            this.license_number = license_number;
            this.client_hash = client_hash;
            this.version = version;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                Thread.sleep(500);

                URL url = new URL(CONNECT_Url);

                //https
                TrustAllHosts();
                HttpsURLConnection httpURLCon = (HttpsURLConnection) url.openConnection();
                httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setConnectTimeout(2500);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");

            /*    //http
                HttpURLConnection httpURLCon = (HttpURLConnection) url.openConnection();
                // httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setConnectTimeout(3000);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");*/

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("license_number", license_number);
                jsonObject.put("client_hash", client_hash);
                jsonObject.put("version", version);

                OutputStream os = httpURLCon.getOutputStream();
                os.write(jsonObject.toString().getBytes());
                os.flush();

                responseCode = httpURLCon.getResponseCode();

                BufferedReader bufferedReader = null;


                sb = new StringBuilder();
                Log.e("httpCode_Tag", String.valueOf(responseCode));


                if (responseCode == 200) {
                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                responseCode = 510;

            } catch (JSONException e) {
                e.printStackTrace();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (responseCode == 200) {
                Log.e("JsonData", sb.toString());
                JSONArray total_array = null;

                try {
                    JSONObject json_object = new JSONObject(sb.toString());
                    String result = json_object.getString(RESULT);
                    String date = "";
                    String newVersion = "";
                    String fileName = "";
                    if (result.equals(IS_SUCCESS)) {
                        date = json_object.getString("expire_date");

                        sharedPref.put("LINCESE_DATE", date);


                        //version check;
                        Intent intent = new Intent(mContext, MainActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        newVersion = json_object.getString("data");

                        if (!version.equals(newVersion)) {
                            fileName = json_object.getString("filename");
                            intent.putExtra("path", fileName);
                        } else {
                            intent.putExtra("path", "null");
                        }

                        //login
                        startActivity(intent);
                        finish();
                    } else {

                        Intent intent = new Intent(mContext, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {

                }

            } else if (responseCode == 510) {
                IntroTask mIntroTask = new IntroTask();
                mIntroTask.execute();
            } else {
                //sharedPref.put("LINCESE", "fail");
                IntroTask mIntroTask = new IntroTask();
                mIntroTask.execute();
            }
        }


    }

    private static void TrustAllHosts() {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    final static HostnameVerifier DO_NOT_VERIFY =
            new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

  /*  private String getLicenseTime(String oldLicense, String newLicense) {

        String day;

        final Calendar today = Calendar.getInstance(); //현재 날짜
        final Calendar oldLicenseTime = Calendar.getInstance(); // 기존날짜

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        try {
            oldLicenseTime.setTime(simpleDateFormat.parse(oldLicense));

            long row_result = (oldLicenseTime.getTimeInMillis() - today.getTimeInMillis()) / 1000;
            final long result = row_result / (60 * 60 * 24);  //일계산

            if (result < 30) {
                final Calendar LicenseTime = Calendar.getInstance();  //server받은 날짜
                LicenseTime.setTime(simpleDateFormat.parse(newLicense));

                final Calendar sesstionTime = Calendar.getInstance();
                int sesstionYear = sesstionTime.get(Calendar.YEAR);
                int sesstionMonth = sesstionTime.get(Calendar.MONTH) + 2;
                int sesstionDay = sesstionTime.get(Calendar.DAY_OF_MONTH);

                if (sesstionMonth > 12) {
                    sesstionYear++;
                    sesstionMonth = sesstionMonth - 12;
                }
                sesstionTime.set(sesstionYear, sesstionMonth, sesstionDay);

                long row_result2 = (LicenseTime.getTimeInMillis() - sesstionTime.getTimeInMillis()) / 1000;
                final long result2 = row_result2 / (60 * 60 * 24);  //일계산

                if (result2 > 0) {
                    day = sesstionYear + "-" + sesstionMonth + "-" + sesstionDay + "-" + 00 + "-" + 00;
                } else {
                    day = newLicense;
                }

            } else {
                day = oldLicense;
            }
        } catch (Exception e) {
            return "fail";
        }
        Log.e("day ::::::", String.valueOf(day));


        return day;
    }*/
}
