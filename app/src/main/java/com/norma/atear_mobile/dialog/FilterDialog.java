package com.norma.atear_mobile.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.norma.atear_mobile.R;

/**
 * Created by hyojin on 7/20/16.
 */
public class FilterDialog extends Dialog {

    private EditText edt_filter;
    private Button btn_submit;

    private Context mContext;
    private OnDismissListener _listener;

    private String data;

    public FilterDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_filter);

        findView();


        edt_filter.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        edt_filter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_filter.getWindowToken(), 0);

                    data = edt_filter.getText().toString();

                    _listener.onDismiss(FilterDialog.this);
                    dismiss();
                    edt_filter.setText("");

                }

                return true;
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data = edt_filter.getText().toString();
                hideSoftInputWindow(edt_filter,false);
                _listener.onDismiss(FilterDialog.this);
                dismiss();
                edt_filter.setText("");
            }
        });


    }

    private void findView(){
        edt_filter = (EditText) findViewById(R.id.edt_filter);
        btn_submit = (Button) findViewById(R.id.btn_submit);

    }


    public void setOnDismissListener(OnDismissListener _listener) {
        this._listener = _listener;

    }

    public String getData() {
        return data;
    }

    public boolean hideSoftInputWindow(View edit_view, boolean bState) {

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService
                (Context.INPUT_METHOD_SERVICE);

        if (bState)
            return imm.showSoftInput(edit_view, 0);
        else
            return imm.hideSoftInputFromWindow
                    (edit_view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

}
