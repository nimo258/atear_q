package com.norma.atear_mobile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.Util;

import java.io.File;

/**
 * Created by hyojin on 7/27/16.
 */
public class LocalDialog extends Dialog {


    private LinearLayout linear_local;
    private LinearLayout.LayoutParams layoutControl;


    private Button btn_submit;

    private OnDismissListener _listener;

    private Context mContexct;
    private String fileName = "";
    public String filePath = "none";



    public LocalDialog(Context context) {
        super(context);
        mContexct = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_local);

        findView();



        final String path = "/sdcard/Norma/Authorized";

        File dirFile = new File(path);

        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        final int total = dirFile.listFiles().length;
        final RadioButton[] rb = new RadioButton[total + 1];


        //layout control
        LinearLayout.LayoutParams rbParams = new LinearLayout.LayoutParams(Util.dp2px(mContexct,20), Util.dp2px(mContexct,20));
        rbParams.leftMargin = 20;

        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvParams.leftMargin = 20;

        LinearLayout ll_none = new LinearLayout(mContexct);
        ll_none.setLayoutParams(layoutControl);
        ll_none.setOrientation(LinearLayout.HORIZONTAL);
        linear_local.addView(ll_none);

        rb[0] = new RadioButton(mContexct);

        rb[0].setLayoutParams(rbParams);
        rb[0].setBackgroundResource(R.drawable.radio_btn_pressed);

        rb[0].setButtonDrawable(new StateListDrawable());

        rb[0].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    filePath = "none";
                    fileName = "none";
                    for (int j = 1; j < total + 1; j++) {
                        if (0 != j) {
                            rb[j].setChecked(false);
                        }
                    }

                }

            }
        });

        ll_none.addView(rb[0]);

        TextView tv_none = new TextView(mContexct);
        tv_none.setLayoutParams(tvParams);
        tv_none.setTextColor(Color.WHITE);
        tv_none.setText(mContexct.getResources().getString(R.string.noneMsg));

        ll_none.addView(tv_none);

        int i = 1;

        for (final File file : dirFile.listFiles()) {

            Log.e("fileName", file.getName());

            final int index = i;
            LinearLayout ll = new LinearLayout(mContexct);
            ll.setLayoutParams(layoutControl);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            linear_local.addView(ll);

            rb[i] = new RadioButton(mContexct);

            rb[i].setLayoutParams(rbParams);
            rb[i].setBackgroundResource(R.drawable.radio_btn_pressed);

            rb[i].setButtonDrawable(new StateListDrawable());
            rb[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        filePath = file.getPath();
                        fileName = file.getName();

                        for (int j = 0; j < total + 1; j++) {
                            if (index != j) {
                                rb[j].setChecked(false);
                            }
                        }

                    }

                }
            });
            ll.addView(rb[i]);

            TextView tv = new TextView(mContexct);
            tv.setLayoutParams(tvParams);
            tv.setTextColor(Color.WHITE);
            tv.setText(file.getName());

            ll.addView(tv);
            i++;


        }

        rb[0].setChecked(true);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _listener.onDismiss(LocalDialog.this);
                dismiss();
            }
        });

    }


    private void findView() {
        btn_submit = (Button) findViewById(R.id.btn_submit);

        linear_local = (LinearLayout) findViewById(R.id.linear_local);
        linear_local.setGravity(Gravity.CENTER);
        linear_local.setGravity(Gravity.LEFT);

        layoutControl = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //layoutControl.leftMargin = 100;
        layoutControl.bottomMargin = Util.dp2px(mContexct,16);
    }




    public void setOnDismissListener(OnDismissListener _listener) {
        this._listener = _listener;

    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

}
