package com.norma.atear_mobile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.Util;

import java.io.File;

/**
 * Created by hyojin on 2018-03-21.
 */

public class FileImportDialog extends Dialog {


    private LinearLayout linear_local;
    private LinearLayout.LayoutParams layoutControl;


    private Button btn_submit;
    private Button btn_cancel;

    private DialogInterface.OnDismissListener _listener;

    private Context mContexct;
    private String fileName = "";
    public String filePath = "none";


    public FileImportDialog(Context context) {
        super(context);
        mContexct = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_file_import);

        findView();


        final String path = "/sdcard/Norma/Authorized";

        File dirFile = new File(path);

        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        final int total = dirFile.listFiles().length;
        final RadioButton[] rb = new RadioButton[total];


        //layout control
        LinearLayout.LayoutParams rbParams = new LinearLayout.LayoutParams(Util.dp2px(mContexct,20), Util.dp2px(mContexct,20));
        rbParams.leftMargin = 20;

        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvParams.leftMargin = 20;

        LinearLayout ll_none = new LinearLayout(mContexct);
        ll_none.setLayoutParams(layoutControl);
        ll_none.setOrientation(LinearLayout.HORIZONTAL);
        linear_local.addView(ll_none);


        int i = 0;

        for (final File file : dirFile.listFiles()) {

            Log.e("fileName", file.getName());

            final int index = i;
            LinearLayout ll = new LinearLayout(mContexct);
            ll.setLayoutParams(layoutControl);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            linear_local.addView(ll);

            rb[i] = new RadioButton(mContexct);

            rb[i].setLayoutParams(rbParams);
            rb[i].setBackgroundResource(R.drawable.radio_btn_pressed);

            rb[i].setButtonDrawable(new StateListDrawable());
            rb[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        filePath = file.getPath();
                        fileName = file.getName();
                        Log.e("fileTest", file.getName());
                        for (int j = 0; j < total; j++) {
                            if (index != j) {
                                rb[j].setChecked(false);
                            }
                        }

                    }

                }
            });
            ll.addView(rb[i]);

            TextView tv = new TextView(mContexct);
            tv.setLayoutParams(tvParams);
            tv.setTextColor(Color.WHITE);
            tv.setText(file.getName());

            ll.addView(tv);
            i++;


        }
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _listener.onDismiss(FileImportDialog.this);
                dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filePath = "none";
                _listener.onDismiss(FileImportDialog.this);
                dismiss();
            }
        });

    }


    private void findView() {
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        linear_local = (LinearLayout) findViewById(R.id.linear_local);
        linear_local.setGravity(Gravity.CENTER);
        linear_local.setGravity(Gravity.LEFT);

        layoutControl = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //layoutControl.leftMargin = 100;
        layoutControl.bottomMargin = Util.dp2px(mContexct,16);
    }


    public void setOnDismissListener(DialogInterface.OnDismissListener _listener) {
        this._listener = _listener;

    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

}
