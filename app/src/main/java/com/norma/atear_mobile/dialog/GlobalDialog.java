package com.norma.atear_mobile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.norma.atear_mobile.MainActivity;
import com.norma.atear_mobile.R;

/**
 * Created by hyojin on 7/20/16.
 */
public class GlobalDialog extends Dialog {

    private int data;
    private int receiver = 5 ;

    private RadioButton rb_isms;
    private RadioButton rb_iso;
    private RadioButton rb_norma;
    private RadioButton rb_none;

    private Button btn_submit;

    private Context mContext;

    private OnDismissListener _listener;


    public GlobalDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_global);

        findView();


        if(receiver == 5) {

            switch (MainActivity.norma_policy.POLICY) {
                case 0:
                    rb_none.setChecked(true);
                    data = 0;
                    break;
                case 1:
                    rb_isms.setChecked(true);
                    data = 1;
                    break;

                case 2:
                    rb_iso.setChecked(true);
                    data = 2;
                    break;

                case 3:
                    rb_norma.setChecked(true);
                    data = 3;
                    break;
            }
        }else{
            switch (receiver) {
                case 0:
                    rb_none.setChecked(true);
                    data = 0;
                    break;
                case 1:
                    rb_isms.setChecked(true);
                    data = 1;
                    break;

                case 2:
                    rb_iso.setChecked(true);
                    data = 2;
                    break;

                case 3:
                    rb_norma.setChecked(true);
                    data = 3;
                    break;
            }


        }


        rb_none.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb_isms.setChecked(false);
                    rb_iso.setChecked(false);
                    rb_norma.setChecked(false);
                }
            }
        });

        rb_isms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb_none.setChecked(false);
                    rb_iso.setChecked(false);
                    rb_norma.setChecked(false);
                }
            }
        });



        rb_iso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb_none.setChecked(false);
                    rb_isms.setChecked(false);
                    rb_norma.setChecked(false);
                }
            }
        });

        rb_norma.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_none.setChecked(false);
                    rb_isms.setChecked(false);
                    rb_iso.setChecked(false);
                }
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (rb_isms.isChecked()) {
                    data = 1;
                } else if (rb_iso.isChecked()) {
                    data = 2;
                } else if (rb_norma.isChecked()) {
                    data = 3;
                } else if (rb_none.isChecked()) {
                    data = 0;
                }
                _listener.onDismiss(GlobalDialog.this);
                dismiss();
            }
        });

    }

    public int setReceiver(int i){
        receiver = i;
        return receiver;
    }

    private void findView() {
        rb_isms = (RadioButton) findViewById(R.id.rb_isms);
        rb_iso = (RadioButton) findViewById(R.id.rb_iso);
        rb_norma = (RadioButton) findViewById(R.id.rb_norma);
        rb_none = (RadioButton) findViewById(R.id.rb_none);
        btn_submit = (Button) findViewById(R.id.btn_submit);
    }



    public void setOnDismissListener(OnDismissListener _listener) {
        this._listener = _listener;
    }

    public int getData() {
        return data;
    }
}
