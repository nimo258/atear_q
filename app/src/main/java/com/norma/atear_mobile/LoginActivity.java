package com.norma.atear_mobile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.component.SHA256Hash;
import com.norma.atear_mobile.guide.GuideMainActivity;
import com.norma.atear_mobile.project.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by hyojin on 7/6/16.
 */
public class LoginActivity extends Activity {

    private final static String INSERT_Url = "https://211.110.140.144/insert_license";
    private final static String CONNECT_Url = "https://211.110.140.144/is_license";

    private boolean LoginLincese = true;

    private String versionName = "0";

    private static String CODE = "aaaa";

    private Context mContext;
    //view
    private EditText edt_code;
    private Button btn_login;
    private TextView tv_guide;

    private int permissionStorage = 1; //저장 공간  //json
    private int permissionLocation = 2;  //wifi 위치정보
    private int permissionCamera = 3;  //camera


    SHA256Hash sha256Hash;

    SharedPref sharedPref;

    private final static String RESULT = "result";
    private final static String IS_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findView();
        edt_code.setHint(mContext.getResources().getString(R.string.edtHintMsg));
        btn_login.setText(mContext.getResources().getString(R.string.loginTextMsg));
        tv_guide.setText(mContext.getResources().getString(R.string.guideMsg));

        //version
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            versionName = info.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //wifi on
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        //wifi mac
        //  device_mac = getMacAddr();


        //마시멜로 체크
        int versionSDK = Build.VERSION.SDK_INT;
        if (versionSDK >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionStorage); ///권한 체크여부

            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            permissionLocation); ///권한 체크여부

                } else {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                                permissionCamera); ///권한 체크여부
                    } else {
                        //	Toast.makeText(getApplicationContext(), "있음", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }

        edt_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_code.getWindowToken(), 0);
                    //  login();

                    int versionSDK = Build.VERSION.SDK_INT;

                    if (versionSDK >= 23) {
                        if ((ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED)) {

                            Toast.makeText(mContext, "no permission", Toast.LENGTH_SHORT).show();
                        } else {
                            HttpLogin();
                        }
                    } else {
                        HttpLogin();
                    }


                }

                return true;
            }
        });

        edt_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // 입력하기 전에
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 입력되는 텍스트에 변화가 있을 때
            }

            @Override
            public void afterTextChanged(Editable s) {
                // 입력이 끝났을 때
                String text = s.toString();
                if (text.length() == 5 || text.length() == 10 || text.length() == 15) {
                    char last = text.charAt(text.length() - 1);
                    if (last != '-') {
                        text = text.substring(0, text.length() - 1);
                        text = text + "-" + last;
                        edt_code.setText(text);
                        edt_code.setSelection(text.length());
                    }
                }
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  login();
                int versionSDK = Build.VERSION.SDK_INT;

                if (versionSDK >= 23) {
                    if ((ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED)) {

                        Toast.makeText(mContext, "Permission Denied", Toast.LENGTH_SHORT).show();
                    } else {
                        HttpLogin();
                    }
                } else {
                    HttpLogin();
                }
            }
        });

        tv_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GuideMainActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        recreate();
    }

    private void findView() {
        mContext = this;

        edt_code = (EditText) findViewById(R.id.edt_code);
        btn_login = (Button) findViewById(R.id.btn_login);
        tv_guide = (TextView) findViewById(R.id.tv_guide);

        edt_code.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        sharedPref = new SharedPref(mContext);

        sha256Hash = new SHA256Hash();
    }

    private void HttpLogin() {
        //code만 맞음 오케이
        if (LoginLincese == false) {
            if (edt_code.getText().toString().equals(CODE)) {
                Log.e("aa", String.valueOf(BataLincese(2017, 5, 5)));
                if (BataLincese(2017, 5, 5) == true) { //라이센스 날짜

                    Intent intent = new Intent(mContext, MainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intent);
                    finish();
                } else {
                    edt_code.setText(null);
                    edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.licenseMsg) + "<font>"));
                }

            } else {
                edt_code.setText(null);
                edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.errorMsg) + "<font>"));
            }
        } else {
            //라이센스
            String lincese = edt_code.getText().toString();

            if (!lincese.equals("")) {
                if (isNetworkConnected() == true) {
                    //   Log.e("aaaaaaa",sha256Hash.getHashHexString(DeviceSerial()));
                    LoginHTTP loginHTTP = new LoginHTTP(mContext, lincese, sha256Hash.getHashHexString(DeviceSerial()), 0, versionName);
                    loginHTTP.execute();
                } else {
                    edt_code.setText("");
                    edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.errorMsg) + "<font>"));

                    Toast.makeText(mContext, mContext.getResources().getString(R.string.networkMsg), Toast.LENGTH_SHORT).show();

                }
            } else {
                edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.errorMsg) + "<font>"));
            }

        }
    }

    //bata버전일경우
    private boolean BataLincese(int year, int month, int day) {

        boolean DayDebug = sharedPref.getValue("bata_license", true);
        //날짜 한번 변경해서 입력하면 영원히 못들어가
        if (DayDebug == false)
            return false;

        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        final Calendar endData = Calendar.getInstance();
        endData.set(year, month - 1, day);// 특정 날짜 셋팅
        long row_result = (today.getTimeInMillis() - endData.getTimeInMillis()) / 1000;
        final long result = row_result / (60 * 60 * 24);  //일계산

        if (result >= 0) {
            sharedPref.put("bata_license", false);
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == permissionStorage) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 권한 허가
                // 해당 권한을 사용해서 작업을 진행할 수 있습니다
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            permissionLocation); ///권한 체크여부

                } else {

                    //	Toast.makeText(getApplicationContext(), "있음", Toast.LENGTH_SHORT).show();

                }

            } else {
                // 권한 거부
                // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if (requestCode == permissionLocation) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 권한 허가
                // 해당 권한을 사용해서 작업을 진행할 수 있습니다
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                            permissionCamera); ///권한 체크여부
                } else {
                    //	Toast.makeText(getApplicationContext(), "있음", Toast.LENGTH_SHORT).show();
                }
            } else {
                // 권한 거부
                // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }


        if (requestCode == permissionCamera) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 권한 허가
                // 해당 권한을 사용해서 작업을 진행할 수 있습니다

            } else {
                // 권한 거부
                // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    private static String DeviceSerial() {
        try {
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception ignored) {
            return null;
        }

    }

    private class LoginHTTP extends AsyncTask<Void, Void, Void> {

        private StringBuilder sb;

        private String license_number = "null";
        private String team_name = "AtEarMobile";
        private String user_name = "mobile user";
        private String user_email = "null";
        private String phone_number = "null";
        private String client_hash = "null";
        private String version = "null";
        private int responseCode = 0;

        private int isNetwork = 0;
        private int isLogin = 0;

        private Context mContext;

        LoginHTTP(Context mContext, String license_number, String client_hash, int isLogin, String version) {

            this.mContext = mContext;
            this.license_number = license_number;
            this.client_hash = client_hash;
            this.version = version;
            this.isLogin = isLogin;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String isUrl = "";

                if (isLogin == 0) {
                    isUrl = INSERT_Url;
                } else {
                    isUrl = CONNECT_Url;
                }

                URL url = new URL(isUrl);


                //https
                TrustAllHosts();
                HttpsURLConnection httpURLCon = (HttpsURLConnection) url.openConnection();
                httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setConnectTimeout(3000);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");
             /*   //http
                HttpURLConnection httpURLCon = (HttpURLConnection) url.openConnection();
                // httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setConnectTimeout(3000);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");
*/
                JSONObject jsonObject = new JSONObject();
                if (isLogin == 0) {
                    jsonObject.put("team_name", team_name);
                    jsonObject.put("user_name", user_name);
                    jsonObject.put("user_email", user_email);
                    jsonObject.put("phone_number", phone_number);
                }
                jsonObject.put("license_number", license_number);
                jsonObject.put("client_hash", client_hash);
                jsonObject.put("version", version);

                OutputStream os = httpURLCon.getOutputStream();
                os.write(jsonObject.toString().getBytes());
                os.flush();

                responseCode = httpURLCon.getResponseCode();

                BufferedReader bufferedReader = null;


                sb = new StringBuilder();
                Log.e("httpCode_Tag", String.valueOf(responseCode));


                if (responseCode == 200) {
                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                isNetwork = 1;

            } catch (JSONException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (isNetwork == 1) {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.networkMsg), Toast.LENGTH_SHORT).show();
            }

            if (responseCode == 200) {
                Log.e("JsonData", sb.toString());
                JSONArray total_array = null;

                try {
                    JSONObject json_object = new JSONObject(sb.toString());
                    String result = json_object.getString(RESULT);
                    String comment = "";
                    String date = "";
                    if (!result.equals(IS_SUCCESS)) {
                        comment = json_object.getString("comment");
                    } else {
                        date = json_object.getString("expire_date");
                    }

                    if (result.equals(IS_SUCCESS)) {

                        sharedPref.put("LINCESE", license_number);
                      //  Log.e("data12341 : ", date + "    liceseTIme : " + getLicenseTime(date));
                        sharedPref.put("LINCESE_DATE", date);
                        //login
                        Intent intent = new Intent(mContext, MainActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(intent);
                        finish();
                    } else {
                        if (comment.equals("not_exist_license")) {
                            edt_code.setText("");
                            //없는 라이센스
                            edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getString(R.string.errorMsg) + "<font>"));
                            sharedPref.put("LINCESE", "fail");
                            sharedPref.put("LINCESE_DATE", "fail");
                        } else {
                            if (isLogin == 0) {
                                LoginHTTP loginHTTP = new LoginHTTP(mContext, license_number, client_hash, 1, version);
                                loginHTTP.execute();
                            } else {
                                edt_code.setText("");
                                if (comment.equals("not_match_hash")) {// 다른 기기 코드
                                    edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.errorMsg) + "<font>"));
                                } else {
                                    //라이센스 만료
                                    edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.licenseMsg) + "<font>"));
                                    sharedPref.put("LINCESE", "fail");
                                    sharedPref.put("LINCESE_DATE", "fail");
                                }
                            }
                        }
                    }

                } catch (Exception e) {

                }

            } else {
                edt_code.setText("");
                edt_code.setHint(Html.fromHtml("<font color =\"red\">" + mContext.getResources().getString(R.string.errorMsg) + "<font>"));
                sharedPref.put("LINCESE", "fail");

            }
        }


    }

    private static void TrustAllHosts() {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

   /* private String getLicenseTime(String licenseDay) {

        String day;

        final Calendar sesstionTime = Calendar.getInstance(); //오늘날짜 셋팅-
        int sesstionYear = sesstionTime.get(Calendar.YEAR);
        int sesstionMonth = sesstionTime.get(Calendar.MONTH) + 2;
        int sesstionDay = sesstionTime.get(Calendar.DAY_OF_MONTH);

        if (sesstionMonth > 12) {
            sesstionYear++;
            sesstionMonth = sesstionMonth - 12;
        }
        sesstionTime.set(sesstionYear, sesstionMonth, sesstionDay);

        final Calendar LicenseTime = Calendar.getInstance();  //end날짜 -

        //end Date 셋팅
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

        try {

            LicenseTime.setTime(simpleDateFormat.parse(licenseDay));
            long row_result = (LicenseTime.getTimeInMillis() - sesstionTime.getTimeInMillis()) / 1000;
            final long result = row_result / (60 * 60 * 24);  //일계산

            if (result > 0) {
                day = sesstionYear + "-" + sesstionMonth + "-" + sesstionDay + "-" + 00 + "-" + 00;
            } else {
                day = licenseDay;
            }
        } catch (Exception e) {
            return "null";
        }
        Log.e("day ::::::", String.valueOf(day));
        return day;
    }*/
}
