package com.norma.atear_mobile.csv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.authorized.data.AuthorizedInfo;
import com.norma.atear_mobile.db.AtEarDB;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

import static com.norma.atear_mobile.csv.CSVImport.OnChangeText.NOT_CSV;
import static com.norma.atear_mobile.csv.CSVImport.OnChangeText.REBUILD;

/**
 * Created by hyojin on 2018-03-21.
 */

//file import
public class CSVImport extends AsyncTask<Void, Object, Integer> implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
    private ProgressDialog loadingBar;
    private OnChangeText mOct;

    String file_path;
    String file_name;
    Context mContext;
    AtEarDB atEarDB;
    private boolean stopSwitcher;


    public CSVImport(Context mContext, AtEarDB atEarDB, String file_path, String file_name) {
        super();
        this.file_name = file_name;
        this.file_path = file_path;
        this.mContext = mContext;
        this.atEarDB = atEarDB;
    }

    public CSVImport(Context mContext, AtEarDB atEarDB, String file_path, String file_name, OnChangeText oct) {
        super();
        this.file_name = file_name;
        this.file_path = file_path;
        this.mContext = mContext;
        this.atEarDB = atEarDB;
        this.mOct = oct;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        loadingBar = new ProgressDialog(mContext);
        loadingBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        loadingBar.setMessage(mContext.getResources().getString(R.string.csv_load));
        loadingBar.setOnCancelListener(this);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(false);
        loadingBar.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getResources().getString(R.string.btnCancelMsg), this);
        try {
            loadingBar.setMax(readCSVLine(file_path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        loadingBar.show();
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        try {
            // csv 데이타 파일
            if (file_path.contains("csv")) {
                File csv = new File(file_path);

                ArrayList<String> bssidList = new ArrayList<>();

                BufferedReader br = new BufferedReader(new FileReader(csv));

                String line = "";
                while ((line = br.readLine()) != null) {
                    // -1 옵션은 마지막 "," 이후 빈 공백도 읽기 위한 옵션
                    String[] token = line.split(",", -1);
                    for (String output : token) {
                        if (output.length() == 17 && Pattern.matches("^^([0-9a-fA-F][0-9a-fA-F][:\\-]){5}[0-9a-fA-F]{2}$", output)) {
//                        if (output.length() == 17) {
                            //mac 만 가능 17글자.
                            bssidList.add(output.toUpperCase());
//                            atEarDB.BSSID_TABLE_INSERT(output.toUpperCase());
                            loadingBar.setProgress(loadingBar.getProgress()+1);

                        }
                    }
                }
                br.close();

                if (bssidList.size() != 0 && !stopSwitcher) {
                    //데이터 넣기 인가 single
                    atEarDB.setLoadingProgress(mContext, loadingBar, bssidList.size(), mContext.getResources().getString(R.string.DB_insert));
                    atEarDB.BSSID_TABLE_INSERT(bssidList);
                    if (mOct != null)/** 기존 setText를 리스너로 변경.(객체의 다형성을 제공하기 위해) */
                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mOct.resultString("- " + file_name);
                            }
                        });
                }else
                    return OnChangeText.NOT_CSV;
            } else {
                return OnChangeText.REBUILD;

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Integer type) {
        super.onPostExecute(type);
        if (type != null && mOct != null) {
            if (type == NOT_CSV)
                mOct.postExecuteString(mContext.getResources().getString(R.string.not_csv));
            else if (type == REBUILD)
                mOct.postExecuteString(mContext.getResources().getString(R.string.rebuild_csv));
        }
        loadingBar.dismiss();
    }

    private int readCSVLine(String CSV_Filename) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(CSV_Filename));
        try {
            byte[] c = new byte[1024];
            int cnt = 0, readChr;
            boolean empty = true;
            while ((readChr = is.read(c)) != -1) {
                empty = false;
                for (int chr = 0; chr < readChr; ++chr) {
                    if (c[chr] == '\n')
                        ++cnt;
                }
            }
            return (cnt == 0 && !empty) ? 1 : cnt;
        } finally {
            is.close();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
//            atEarDB.rollbackInsert();
        stopSwitcher = true;
        cancel(true);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        stopSwitcher = true;
        cancel(true);
    }

    public interface OnChangeText {
        int NOT_CSV = 0x0, REBUILD = 0x1;

        void resultString(String text);

        void postExecuteString(String text);
    }

}

