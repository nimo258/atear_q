package com.norma.atear_mobile.report.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.policy.Policy_Result;
import com.norma.atear_mobile.project.policy.Policy_info;
import com.norma.atear_mobile.report.ReportActivity;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hyojin on 8/2/16.
 */
public class Detail_info_Adapter extends PagerAdapter {


    private Context mContext;

    private LayoutInflater inflater;
    private int count;

    private ArrayList<String> bssidlist;
    private ViewHolder holder;

    private static final String BSSID = "bssid";
    private static final String REPORT_TABLE = "report_table";

    SharedPref sharedPref;

    public Detail_info_Adapter(LayoutInflater inflater, Context mContext, int count, ArrayList<String> bssidlist) {
        this.inflater = inflater;
        this.count = count;
        this.bssidlist = bssidlist;
        this.mContext = mContext;
        sharedPref = new SharedPref(mContext);

//        SetLanguage();
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = null;
        view = inflater.inflate(R.layout.activity_detail_info, null);

        holder = new ViewHolder();

        holder.tv_project = (TextView) view.findViewById(R.id.tv_project);
        holder.tv_location = (TextView) view.findViewById(R.id.tv_location);
        holder.tv_savetime = (TextView) view.findViewById(R.id.tv_savetime);
        holder.tv_gps = (TextView) view.findViewById(R.id.tv_gps);
        holder.tv_bssid = (TextView) view.findViewById(R.id.tv_bssid);
        holder.tv_ssid = (TextView) view.findViewById(R.id.tv_ssid);
        holder.tv_signal = (TextView) view.findViewById(R.id.tv_signal);
        holder.tv_channel = (TextView) view.findViewById(R.id.tv_channel);
        holder.tv_ax_text = (TextView) view.findViewById(R.id.tv_ax_text);
        holder.tv_enc = (TextView) view.findViewById(R.id.tv_enc);
        holder.tv_scantime = (TextView) view.findViewById(R.id.tv_scantime);
        holder.tv_aptype = (TextView) view.findViewById(R.id.tv_aptype);
        holder.tv_oui = (TextView) view.findViewById(R.id.tv_oui);
        holder.tv_comment = (TextView) view.findViewById(R.id.tv_comment);
        holder.tv_alert = (TextView) view.findViewById(R.id.tv_alert);
        holder.tv_policy_type = (TextView) view.findViewById(R.id.tv_policy_type);
        holder.tv_memo = (TextView) view.findViewById(R.id.tv_memo);
        holder.tv_pantest = (TextView) view.findViewById(R.id.tv_pantest);
        holder.btn_picture = (Button) view.findViewById(R.id.btn_picture);


        holder.tv_proTitle = (TextView) view.findViewById(R.id.tv_proTitle);
        holder.tv_loTitle = (TextView) view.findViewById(R.id.tv_loTitle);
        holder.tv_saveTimeTitle = (TextView) view.findViewById(R.id.tv_saveTimeTitle);
        holder.tv_gpsTitle = (TextView) view.findViewById(R.id.tv_gpsTitle);
        holder.tv_signalTitle = (TextView) view.findViewById(R.id.tv_signalTitle);
        holder.tv_chTitle = (TextView) view.findViewById(R.id.tv_chTitle);
        holder.tv_encTitle = (TextView) view.findViewById(R.id.tv_encTitle);
        holder.tv_scanTimeTitle = (TextView) view.findViewById(R.id.tv_scanTimeTitle);
        holder.tv_apTypeTitle = (TextView) view.findViewById(R.id.tv_apTypeTitle);
        holder.tv_ouiTitle = (TextView) view.findViewById(R.id.tv_ouiTitle);
        holder.tv_commentTitle = (TextView) view.findViewById(R.id.tv_commentTitle);
        holder.tv_alertTitle = (TextView) view.findViewById(R.id.tv_alertTitle);
        holder.tv_policyTitle = (TextView) view.findViewById(R.id.tv_policyTitle);
        holder.tv_pentestTitle = (TextView) view.findViewById(R.id.tv_pentestTitle);
        holder.tv_memoTitle = (TextView) view.findViewById(R.id.tv_memoTitle);
        // dbSelect(data.bssid);
        container.addView(view);

        String bssid = bssidlist.get(position);

        final Ap_info data = dbSelect(bssid);

        //title 언어
        holder.tv_proTitle.setText(mContext.getResources().getString(R.string.proTitleMsg));
        holder.tv_loTitle.setText(mContext.getResources().getString(R.string.loTitleMsg));
        holder.tv_saveTimeTitle.setText(mContext.getResources().getString(R.string.saveTimeTitleMsg));
        holder.tv_gpsTitle.setText(mContext.getResources().getString(R.string.gpsTitleMsg));
        holder.tv_signalTitle.setText(mContext.getResources().getString(R.string.signalTitleMsg));
        holder.tv_chTitle.setText(mContext.getResources().getString(R.string.chTitleMsg));
        holder.tv_encTitle.setText(mContext.getResources().getString(R.string.encTitleMsg));
        holder.tv_scanTimeTitle.setText(mContext.getResources().getString(R.string.scanTimeTitleMsg));
        holder.tv_apTypeTitle.setText(mContext.getResources().getString(R.string.apTypeTitleMsg));
        holder.tv_ouiTitle.setText(mContext.getResources().getString(R.string.ouiTitleMsg));
        holder.tv_commentTitle.setText(mContext.getResources().getString(R.string.commentTitleMsg));
        holder.tv_alertTitle.setText(mContext.getResources().getString(R.string.alertTitleMsg));
        holder.tv_policyTitle.setText(mContext.getResources().getString(R.string.polictTitleMsg));
        holder.tv_pentestTitle.setText(mContext.getResources().getString(R.string.pentestTitleMsg));
        holder.tv_memoTitle.setText(mContext.getResources().getString(R.string.memoTitleMsg));


        if (bssid.equals("none")) {
            holder.tv_project.setText("none");
            holder.tv_location.setText("none");
            holder.tv_savetime.setText("none");
            holder.tv_gps.setText("none");
            holder.tv_bssid.setText("none");
            holder.tv_ssid.setText("none");
            holder.tv_signal.setText(String.valueOf("none"));
            holder.tv_channel.setText(String.valueOf("none"));
            holder.tv_ax_text.setText("X");
            holder.tv_enc.setText("none");
            holder.tv_scantime.setText("none");
            holder.tv_aptype.setText("none");
            holder.tv_oui.setText("none");
            holder.tv_comment.setText("none");
            holder.tv_alert.setText("none");
            holder.tv_policy_type.setText("none");
            holder.tv_memo.setText("none");
            holder.tv_pantest.setText("none");

            data.picturePath = "none";
        } else {
            //     dbSelect(bssid);

            holder.tv_project.setText(data.projectName);
            holder.tv_location.setText(data.locationName);
            holder.tv_savetime.setText(data.save_time);
            holder.tv_gps.setText(data.gps);
            holder.tv_bssid.setText(data.bssid);
            holder.tv_ssid.setText(data.ssid);
            holder.tv_signal.setText(String.valueOf(data.signal));
            holder.tv_channel.setText(String.valueOf(data.channel));
            holder.tv_ax_text.setText(data.is11axAp?"O":"X");

            if (data.encryption.equals("WPA/WPA2")) {
                data.encryption = "WPA2";
            }else if(data.encryption.contains("WPA_s")) {
                data.encryption = "WPA2";
            }
            holder.tv_enc.setText(data.encryption);
            holder.tv_scantime.setText(data.scan_time);
            holder.tv_aptype.setText(data.ap_type);
            holder.tv_oui.setText(data.oui);
            holder.tv_comment.setText(data.comment);
            holder.tv_alert.setText(data.alert);
            holder.tv_policy_type.setText(policy_Type(data.policyType));
            holder.tv_memo.setText(data.memo);


            String pantest = "";
            if (data.isPentest == 1) {
                pantest = "failed";
            } else if (data.isPentest == 2) {
                pantest = "Success.  PASSWORD : " + data.pentest_password;
            }else{
                pantest = "none";

            }

            holder.tv_pantest.setText(pantest);
        }

        Log.e("pathSA",data.picturePath);
        holder.btn_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String path = data.picturePath;

                if (!path.equals("none")) {

                    File file = new File(path);
                    if (!file.exists()) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.NoPictureMsg), Toast.LENGTH_SHORT).show();
                    } else {
                        //사진 보여주는 작업.
                        final Dialog dialog = new Dialog(mContext);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_picture);
                        dialog.setTitle("Ap : " + data.ssid);

                        Bitmap bitmap = BitmapFactory.decodeFile(path);

                        ImageView iv_ap = (ImageView) dialog.findViewById(R.id.iv_ap);
                        iv_ap.setImageBitmap(bitmap);
                        iv_ap.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }

                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.NoPictureMsg), Toast.LENGTH_SHORT).show();
                }

            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).finish();
            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

// TODO Auto-generated method stub

        container.removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    public class ViewHolder {
        public TextView tv_project;
        public TextView tv_location;
        public TextView tv_savetime;
        public TextView tv_gps;
        public TextView tv_bssid;
        public TextView tv_ssid;
        public TextView tv_signal;
        public TextView tv_channel;
        public TextView tv_ax_text;
        public TextView tv_enc;
        public TextView tv_scantime;
        public TextView tv_aptype;
        public TextView tv_oui;
        public TextView tv_comment;
        public TextView tv_alert;
        public TextView tv_policy_type;
        public TextView tv_memo;
        public TextView tv_pantest;
        public Button btn_picture;


        public TextView tv_proTitle;
        public TextView tv_loTitle;
        public TextView tv_saveTimeTitle;
        public TextView tv_gpsTitle;
        public TextView tv_signalTitle;
        public TextView tv_chTitle;
        public TextView tv_encTitle;
        public TextView tv_scanTimeTitle;
        public TextView tv_apTypeTitle;
        public TextView tv_ouiTitle;
        public TextView tv_commentTitle;
        public TextView tv_alertTitle;
        public TextView tv_policyTitle;
        public TextView tv_pentestTitle;
        public TextView tv_memoTitle;

    }


    private Ap_info dbSelect(String bssid) {

        AtEarDB atEarDB = new AtEarDB(mContext);

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select * from " + REPORT_TABLE + " where " + BSSID + " = '" + bssid + "'";

        Ap_info ap_info = new Ap_info();

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {
                try {

                    ap_info.projectName = c.getString(0);
                    ap_info.locationName = c.getString(1);
                    ap_info.save_time = c.getString(2);
                    ap_info.gps = c.getString(3);
                    ap_info.bssid = c.getString(4);
                    ap_info.ssid = c.getString(5);
                    ap_info.signal = c.getInt(6);
                    ap_info.channel = c.getInt(7);
                    ap_info.encryption = c.getString(8);
                    ap_info.cipher = c.getString(9);
                    ap_info.auth = c.getString(10);
                    ap_info.scan_time = c.getString(11);
                    ap_info.ap_type = c.getString(12);
                    ap_info.oui = c.getString(13);
                    ap_info.comment = c.getString(14);
                    ap_info.alert = c.getString(15);
                    ap_info.risk = c.getInt(16);
                    ap_info.wps = c.getInt(17);
                    ap_info.isAuthorized = c.getInt(18);
                    ap_info.ssid_filter = c.getInt(19);
                    ap_info.bssid_filter = c.getInt(20);
                    ap_info.policyType = c.getInt(21);
                    ap_info.picturePath = c.getString(22);
                    ap_info.memo = c.getString(23);
                    ap_info.isPentest = c.getInt(24);
                    ap_info.pentest_password = c.getString(25);

                    Log.e("riskTAG", ap_info.ssid + "," + ap_info.risk);
                    //alert setting ///////////////////////////////////////////////
                    if (ReportActivity.SettingFilter == true) {
                        Policy_info policy_info = new Policy_info();

                        if (ap_info.encryption.equals("WPA/WPA2")) {
                            policy_info.enc = "wpa2";
                        } else if (ap_info.encryption.contains("WPA2")) {
                            policy_info.enc = "wpa2";
                        } else if (ap_info.encryption.contains("WPA")) {
                            policy_info.enc = "wpa";
                        } else if (ap_info.encryption.contains("WEP")) {
                            policy_info.enc = "wep";
                        } else if(ap_info.encryption.equals("WPA_s")){
                            policy_info.enc = "wpa2";
                        }else {
                            policy_info.enc = "open";
                        }

                        if (ap_info.ap_type.equals("Soft AP")) {
                            policy_info.type = "Soft AP";
                        } else {
                            policy_info.type = "AP";
                        }

                        if (ap_info.ssid.equals("(Hidden)")) {
                            policy_info.ssid = "hidden ssid";
                        }

                        if (atEarDB.SSID_TABLE_SELECT(ap_info.ssid)) {
                            policy_info.ssid = "authorized ssid";
                        } else {  //일단 다 노말 나중에 default  스패셜 추가해야함 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            policy_info.ssid = "normal ssid";
                        }

                        if (ap_info.isAuthorized == 1) {
                            ap_info.isAuthorized = 1;
                            policy_info.authorized = "1";
                        } else {
                            ap_info.isAuthorized = 0;
                            policy_info.authorized = "0";
                        }

                        if (atEarDB.SINGLE_BSSID_TABLE_SELECT(ap_info.bssid)) {
                            ap_info.isAuthorized = 1;
                            policy_info.authorized = "1";
                        }

                        ap_info.policyType = ReportActivity.norma_policy.POLICY;

                        final Policy_Result policy_result;

                        switch (ap_info.policyType) {

                            case 0:   //none
                                policy_result = ReportActivity.norma_policy.nonData();

                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;

                                break;

                            case 1:    //isms
                                policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                                //출력

                                if (policy_result != null) {
                                    ap_info.alert = policy_result.alert;
                                    ap_info.comment = policy_result.comment;
                                } else {
                                    ap_info.alert = "none";
                                    ap_info.comment = "none";
                                }
                                break;

                            case 2:    //iso
                                policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                                //출력
                                if (policy_result != null) {
                                    ap_info.alert = policy_result.alert;
                                    ap_info.comment = policy_result.comment;

                                } else {
                                    ap_info.alert = "none";
                                    ap_info.comment = "none";

                                }
                                break;

                            case 3:    //norma
                                if (ap_info.wps == 0) {
                                    policy_info.wps = "off";
                                } else {
                                    policy_info.wps = "on";
                                }

                                policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                                //출력
                                if (policy_result != null) {
                                    ap_info.alert = policy_result.alert;
                                    ap_info.comment = policy_result.comment;
                                } else {
                                    ap_info.alert = "none";
                                    ap_info.comment = "none";
                                }

                                break;
                        }

                    }

                    /////////////////////////////////////////////

                } catch (Exception e) {
                    Log.e("json error", "file save");
                }
            } //while end
        } else {

        }
        c.close();
        db.close();

        return ap_info;


    }

    private String policy_Type(int type) {

        String data;

        switch (type) {
            case 0:
                data = "none";
                break;
            case 1:
                data = "ISMS";
                break;
            case 2:
                data = "ISO 27001";
                break;
            case 3:
                data = "NORMA";
                break;
            default:
                data = "none";
                break;
        }
        return data;
    }
}

