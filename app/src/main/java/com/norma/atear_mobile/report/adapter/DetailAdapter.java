package com.norma.atear_mobile.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/19/16.
 */
public class DetailAdapter extends BaseExpandableListAdapter {



    private ArrayList<String> titleList;
    private ArrayList<ArrayList<Ap_info>> apList;
    private ArrayList<String> bssidList;

    private ViewGroupHolder viewGroupHolder;
    private ViewChildHolder viewChildHolder;

    private Context mContext;

    public DetailAdapter(Context mContext, ArrayList<String> titleList, ArrayList<ArrayList<Ap_info>> apList) {
        this.mContext = mContext;
        this.titleList = titleList;
        this.apList = apList;
    }


    @Override
    public int getGroupCount() {
        return titleList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return apList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return titleList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return apList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            viewGroupHolder = new ViewGroupHolder();


            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.detaild_title_item, null);

            viewGroupHolder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            viewGroupHolder.iv_down = (ImageView) convertView.findViewById(R.id.iv_down);
            viewGroupHolder.layout_bar = (RelativeLayout) convertView.findViewById(R.id.layout_bar);
            convertView.setTag(viewGroupHolder);
        } else {
            viewGroupHolder = (ViewGroupHolder) convertView.getTag();
        }
        if (isExpanded) {
            //viewGroupHolder.iv_down.setBackgroundResource(R.drawable.btn_triangle);
            viewGroupHolder.layout_bar.setBackgroundResource(R.drawable.btn_bar_hover);
        } else {
            //     viewGroupHolder.iv_down.setBackgroundResource(R.drawable.btn_triangle_hover);

            viewGroupHolder.layout_bar.setBackgroundResource(R.drawable.btn_bar);
        }


        viewGroupHolder.tv_title.setText(titleList.get(groupPosition).toString());

        return convertView;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        if (apList.get(groupPosition).get(childPosition).isPentest != 0 && groupPosition == 5) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getChildTypeCount() {
        return 2;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        int res = 0;


        if (convertView == null) {
            viewChildHolder = new ViewChildHolder();


            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            res = getChildType(groupPosition, childPosition);


            if (res == 0) {
                convertView = inflater.inflate(R.layout.detaild_item, null);

                viewChildHolder.tv_bssid = (TextView) convertView.findViewById(R.id.tv_bssid);
                viewChildHolder.tv_ssid = (TextView) convertView.findViewById(R.id.tv_ssid);
                viewChildHolder.tv_alert = (TextView) convertView.findViewById(R.id.tv_alert);
                convertView.setTag(viewChildHolder);

            } else {
                convertView = inflater.inflate(R.layout.detaild_item_pantest, null);


                viewChildHolder.tv_bssid = (TextView) convertView.findViewById(R.id.tv_bssid);
                viewChildHolder.tv_ssid = (TextView) convertView.findViewById(R.id.tv_ssid);
                viewChildHolder.tv_isPentest = (TextView) convertView.findViewById(R.id.tv_isPentest);
                //  viewChildHolder.tv_PENTEST_PWD = (TextView) convertView.findViewById(R.id.tv_PENTEST_PWD);
                convertView.setTag(viewChildHolder);
            }


        } else {
            viewChildHolder = (ViewChildHolder) convertView.getTag();
        }

        final Ap_info data = apList.get(groupPosition).get(childPosition);

        if (data.isPentest != 0 && groupPosition == 5) {

            viewChildHolder.tv_bssid.setText(data.bssid);
            viewChildHolder.tv_ssid.setText(data.ssid);

            String isPentest = "";
            if (data.isPentest == 1) {
                isPentest = "failed";
            } else {
                isPentest = "Success";
            }
            viewChildHolder.tv_isPentest.setText(isPentest);

            //viewChildHolder.tv_PENTEST_PWD.setText(data.pentest_password);
        } else {
            viewChildHolder.tv_bssid.setText(data.bssid);
            viewChildHolder.tv_ssid.setText(data.ssid);
            viewChildHolder.tv_alert.setText(data.alert);
        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewGroupHolder {

        public TextView tv_title;

        public ImageView iv_down;

        public RelativeLayout layout_bar;
    }

    private class ViewChildHolder {

        public TextView tv_project;

        public TextView tv_location;

        public TextView tv_savetime;

        public TextView tv_bssid;

        public TextView tv_ssid;

        public TextView tv_alert;

        public TextView tv_isPentest;

        //  public TextView tv_PENTEST_PWD;


    }
}
