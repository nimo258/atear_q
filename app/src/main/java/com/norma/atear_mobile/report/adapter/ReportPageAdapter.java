package com.norma.atear_mobile.report.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.norma.atear_mobile.report.fragment.DashBoardFragment;
import com.norma.atear_mobile.report.fragment.DetailFragment;
import com.norma.atear_mobile.report.fragment.ReportFragment;

/**
 * Created by hyojin on 8/1/16.
 */
public class ReportPageAdapter  extends FragmentPagerAdapter {


    public ReportPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new DashBoardFragment();
            case 1:
                return new ReportFragment();
            case 2:
                return new DetailFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
