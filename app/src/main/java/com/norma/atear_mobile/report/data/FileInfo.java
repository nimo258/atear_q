package com.norma.atear_mobile.report.data;

import android.graphics.drawable.BitmapDrawable;

/**
 * Created by hyojin on 7/18/16.
 */
public class FileInfo {

    public String name;

    public String path;

    public boolean check;

    public BitmapDrawable alpha;


}
