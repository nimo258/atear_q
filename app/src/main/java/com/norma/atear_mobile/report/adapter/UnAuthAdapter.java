package com.norma.atear_mobile.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;

import java.util.ArrayList;

/**
 * Created by hyojin on 2017-05-25.
 */

public class UnAuthAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<Ap_info> unAuthList;

    ViewHolder holder;

    public UnAuthAdapter(Context mContext,ArrayList<Ap_info> unAuthList) {
        super();
        this.mContext = mContext;
        this.unAuthList = unAuthList;
    }

    @Override
    public int getCount() {
        return unAuthList.size();
    }

    @Override
    public Object getItem(int position) {
        return unAuthList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();


            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.unauth_item, null);

            holder.iv_picture = (ImageView) convertView.findViewById(R.id.iv_picture);
            holder.tv_info1 = (TextView) convertView.findViewById(R.id.tv_info1);
            holder.tv_info2 = (TextView) convertView.findViewById(R.id.tv_info2);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Ap_info data = unAuthList.get(position);

       if( data.picturePath.equals("none")){

       }




        return convertView;
    }

    class ViewHolder{

        public ImageView iv_picture;
        public TextView tv_info1;
        public TextView tv_info2;

    }
}
