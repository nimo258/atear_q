package com.norma.atear_mobile.report.graph;

/**
 * Created by hyojin on 8/2/16.
 *  *
 */
public class GraphInfo
{
    private int data        = 0;
    private String name     = null;
    private String color    = null;

    public GraphInfo(int data, String color, String name)
    {
        this.data = data;
        this.color = color;
        this.name = name;
    }

    public GraphInfo(String name)
    {
        this.name = name;
    }

    public int getData()
    {
        return data;
    }
    public void setData(int data)
    {
        this.data = data;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getColor()
    {
        return color;
    }
    public void setColor(String color)
    {
        this.color = color;
    }
}
