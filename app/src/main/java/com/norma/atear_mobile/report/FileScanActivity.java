package com.norma.atear_mobile.report;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.comparator.FileComparator;
import com.norma.atear_mobile.report.adapter.FileAdapter;
import com.norma.atear_mobile.report.data.FileInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by hyojin on 7/18/16.
 */
public class FileScanActivity extends Activity {

    //view
    private GridView gridView;

    private TextView tv_title;
    private EditText edt_search;
    private Button btn_search;
    private Button btn_start;
    private LinearLayout btn_back;
    private LinearLayout btn_del;
    private Context mContext;


    private FileAdapter fileAdapter;
    private ArrayList<FileInfo> fileList;

    private AtEarDB atEarDB;

    private ProgressDialog progressDialog;

    private static final String path = "/sdcard/Norma/Project";
    private static final String REPORT_TABLE = "report_table";

    SharedPref sharedPref;
    FileLoadThread fileLoadThread;

    ArrayList<String> fileNmaeList;
    private String deleteFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //화면 꺼짐 방지
        setContentView(R.layout.activity_file_scan);

        findView();

        SetLanguage();


        edt_search.setHint(mContext.getResources().getString(R.string.searchHintMsg));

        atEarDB.SINGLE_BSSID_TABLE_DROP();
        fileList = searchFile("");

        if (fileList != null) {
            fileAdapter = new FileAdapter(mContext, fileList);

            gridView.setAdapter(fileAdapter);
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edt_search.getText().toString();

                fileList = searchFile(name);
                fileAdapter.setData(fileList);
                fileAdapter.notifyDataSetChanged();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                fileLoadThread = new FileLoadThread();
                fileLoadThread.start();

            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);

                    String name = edt_search.getText().toString();
                    fileList = searchFile(name);
                    fileAdapter.setData(fileList);
                    fileAdapter.notifyDataSetChanged();

                }

                return true;
            }
        });

        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileDelete();
            }
        });

    }


    public void FileDelete() {
        final ArrayList<FileInfo> choiceFiles = new ArrayList<>();

        for (FileInfo data : fileList) {
            if (data.check) {
                choiceFiles.add(data);
            }
        }
        if (choiceFiles.size() == 0) {
            //선택된 파일 X
            Toast.makeText(mContext, mContext.getResources().getString(R.string.noFileMsg), Toast.LENGTH_SHORT).show();
        } else if (choiceFiles.size() == 1) {
            //1개 선택
            deleteFileName = choiceFiles.get(0).name;
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_custom);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
            Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

            btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
            btn_no.setText(mContext.getResources().getString(R.string.noMsg));
            tv_title.setText(mContext.getResources().getString(R.string.dialogTitleMsg));
            tv_text.setText(String.format(mContext.getResources().getString(R.string.nother_a_u_remove_this), deleteFileName));

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(choiceFiles.get(0).path);
                    if (file.exists()) {

                        file.delete();
                        Intent intent = new Intent(mContext, FileScanActivity.class);
                        mContext.startActivity(intent);
                        finish();

                        Toast.makeText(mContext, String.format(mContext.getResources().getString(R.string.nother_Is_removedMsg), deleteFileName), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } else {
            //여러 개 선택
            final int filesize = choiceFiles.size();

            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_custom);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
            Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

            btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
            btn_no.setText(mContext.getResources().getString(R.string.noMsg));
            tv_title.setText(mContext.getResources().getString(R.string.dialogTitleMsg));
            tv_text.setText(String.format(mContext.getResources().getString(R.string.a_u_remove_list), filesize));

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < choiceFiles.size(); i++) {
                        File file = new File(choiceFiles.get(i).path);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    dialog.dismiss();
                    Intent intent = new Intent(mContext, FileScanActivity.class);
                    mContext.startActivity(intent);
                    finish();

                    Toast.makeText(mContext, String.format(mContext.getResources().getString(R.string.Is_removeListMsg), filesize), Toast.LENGTH_SHORT).show();

                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        atEarDB.REPORT_TABLE_DROP();
    }

    private void findView() {

        mContext = this;

        tv_title = (TextView) findViewById(R.id.tv_title);
        gridView = (GridView) findViewById(R.id.gridView);
        edt_search = (EditText) findViewById(R.id.edt_search);

        edt_search.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        btn_search = (Button) findViewById(R.id.btn_search);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_del = (LinearLayout) findViewById(R.id.btn_del);
        atEarDB = new AtEarDB(mContext);
        sharedPref = new SharedPref(mContext);
    }

    private void SetLanguage() {
        tv_title.setText(mContext.getResources().getString(R.string.FileScan_titleMsg));
        btn_search.setText(mContext.getResources().getString(R.string.btnSearchMsg));
        btn_start.setText(mContext.getResources().getString(R.string.viewMsg));
    }

    //파일 서치
    public static ArrayList<FileInfo> searchFile(String name) {

        File searchfile = new File(path);  //폴더위치
        File[] allFiles = searchfile.listFiles();   //위에 안에 파일들

        ArrayList<FileInfo> dataList = new ArrayList<>();
        dataList.clear();

        if (allFiles == null) {
            return dataList;
        }

        for (File file : allFiles) {
            if (file.getName().contains(".txt")) {

                if (file.getName().contains(name)) {

                    FileInfo addInfo = new FileInfo();
                    addInfo.name = file.getName();

                    addInfo.path = file.getPath();
                    addInfo.check = false;
                    dataList.add(addInfo);
                }
            }
        }

        Collections.sort(dataList, new FileComparator());
        Collections.reverse(dataList);

        return dataList;
    }

    class FileLoadThread extends Thread {

        @Override
        public void run() {
            super.run();
            if (fileList.size() == 0) {
                mHandler.sendEmptyMessageDelayed(0, 100);
            } else {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        progressDialog = new ProgressDialog(mContext);
                        progressDialog.setTitle("File Reading...");
                        progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        mHandler.sendEmptyMessageDelayed(1, 1500);
                    }
                });

                fileNmaeList = new ArrayList<>();

                for (FileInfo data : fileList) {
                    if (data.check) {
                        fileNmaeList.add(data.name);
                        file_insert(data.path);
                    }
                }

                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();

                    }
                } catch (Exception e) {

                }


                if (atEarDB.REPORT_TABLE_SELECT() == true) {
                    Intent intent = new Intent(mContext, ReportActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("filelist", fileNmaeList);
                    startActivity(intent);
                    finish();
                } else {
                    mHandler.sendEmptyMessageDelayed(0, 100);
                    //  Toast.makeText(mContext,"파일을 선택해주세요.",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //파일 db
    private void file_insert(String filePath) {


        File json_file = new File(filePath);

        if (!json_file.exists()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.CheckFileMsg), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(json_file), "UTF-8"));
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    //	Log.e("line", line);
                }
            } catch (Exception e) {

            }

            String str_json = sb.toString();

            JSONArray total_array = null;


            try {
                total_array = new JSONArray(str_json);

                for (int i = 0; i < total_array.length(); i++) {

                    JSONObject json_object = total_array.getJSONObject(i); //json

                    Ap_info ap_info = new Ap_info(); //data 담을 곳

                    ap_info.projectName = json_object.getString("projectName");
                    ap_info.locationName = json_object.getString("locationName");
                    ap_info.save_time = json_object.getString("save_time");
                    ap_info.gps = json_object.getString("gps");
                    ap_info.bssid = json_object.getString("bssid");
                    //중복mac일경우 mac_scantime
                    ap_info.bssid = ap_info.bssid.substring(0, 17);

                    ap_info.ssid = json_object.getString("ssid");
                    ap_info.signal = json_object.getInt("signal");
                    ap_info.channel = json_object.getInt("channel");
                    ap_info.encryption = json_object.getString("encryption");
                    ap_info.cipher = json_object.getString("cipher");
                    ap_info.auth = json_object.getString("auth");
                    ap_info.scan_time = json_object.getString("scan_time");
                    ap_info.ap_type = json_object.getString("ap_type");
                    ap_info.oui = json_object.getString("oui");
                    ap_info.comment = json_object.getString("comment");
                    ap_info.alert = json_object.getString("alert");
                    ap_info.risk = json_object.getInt("risk");
                    ap_info.wps = json_object.getInt("wps");
                    ap_info.isAuthorized = json_object.getInt("isAuthorized");
                    ap_info.ssid_filter = json_object.getInt("ssid_filter");
                    ap_info.bssid_filter = json_object.getInt("bssid_filter");
                    ap_info.policyType = json_object.getInt("policy_type");
                    ap_info.picturePath = json_object.getString("picture_path");
                    ap_info.memo = json_object.getString("memo");

                    try {
                        ap_info.isPentest = json_object.getInt("isPentest");
                    } catch (JSONException e) {
                        ap_info.isPentest = json_object.getInt("isPantest");
                    }

                    try {
                        ap_info.pentest_password = json_object.getString("pentest_password");
                    } catch (JSONException e) {
                        ap_info.pentest_password = json_object.getString("pantest_PWD");
                    }

                    atEarDB.REPORT_TABLE_INSERT(ap_info);

                }
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }


        }
    }


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.noFileMsg), Toast.LENGTH_SHORT).show();
                    break;
                case 1:

                    if (fileLoadThread.getState() != Thread.State.RUNNABLE) {
                        try {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();

                            }

                        } catch (Exception e) {

                        }
                    } else {
                        mHandler.sendEmptyMessageDelayed(1, 1000);
                    }
                    break;

                default:
                    break;
            }
        }
    };


}
