package com.norma.atear_mobile.report.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.report.Detail_info_Activity;
import com.norma.atear_mobile.report.ReportActivity;
import com.norma.atear_mobile.report.adapter.DetailAdapter;

import java.util.ArrayList;

/**
 * Created by hyojin on 8/1/16.
 */
public class DetailFragment extends Fragment {

    private View v;
    private ExpandableListView lv_detail;


    private ArrayList<String> titleList;  //title
    private ArrayList<ArrayList<Ap_info>> groupList;  // child

    private ArrayList<Ap_info> detailList;  //디테일 리스트
    private ArrayList<Ap_info> whiteList;  //인가 리스트
    private ArrayList<Ap_info> warningAp_List; // open, wep 리스트
    private ArrayList<Ap_info> ssid_filter_List;  //ssid 필터 리스트
    private ArrayList<Ap_info> picture_filter_List;  //ssid 필터 리스트
    // private ArrayList<Ap_info> bssid_filter_List;  //bssid 필터 리스트
    private ArrayList<Ap_info> overlap_List;  //중복 AP
    private ArrayList<Ap_info> notRealAp_List;
    private ArrayList<Ap_info> grayAp_List;
    private ArrayList<Ap_info> pantest_List; // Pantest실행한 리스트

    private DetailAdapter detailAdapter;

    public static Context mContext;
    private SharedPref sharedPref;



    public DetailFragment() {
        super();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        v = inflater.inflate(R.layout.fragment_details, container, false);


        findView();

        titleList = new ArrayList<>();
        groupList = new ArrayList<>();

        titleList.add(mContext.getResources().getString(R.string.detaileTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.authTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.warningTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.filterSSidTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.pictureTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.pentestTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.GrayApTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.notRealApTitleMsg));
        titleList.add(mContext.getResources().getString(R.string.duplicateTitleMsg));


        UploadThread uploadThread = new UploadThread();
        uploadThread.execute();

        lv_detail.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                ArrayList<String> bssidList = new ArrayList<String>();
                for (Ap_info s : groupList.get(groupPosition)) {
                    bssidList.add(s.bssid);
                }

                Intent intent = new Intent(mContext, Detail_info_Activity.class);
                //  intent.putExtra("bssid", groupList.get(groupPosition).get(childPosition).bssid);
                intent.putExtra("position", childPosition);
                intent.putStringArrayListExtra("bssidList", bssidList);
                startActivity(intent);

                return false;
            }
        });


        return v;
    }


    private void findView() {
        mContext = getContext();

        lv_detail = (ExpandableListView) v.findViewById(R.id.lv_detail);
        sharedPref = new SharedPref(mContext);
    }


    private void getData() {
        detailList = ((ReportActivity) ReportActivity.mContext).detailList();
        whiteList = ((ReportActivity) ReportActivity.mContext).whiteList();
        warningAp_List = ((ReportActivity) ReportActivity.mContext).warningApList();
        ssid_filter_List = ((ReportActivity) ReportActivity.mContext).ssid_filter_List();
        picture_filter_List = ((ReportActivity) ReportActivity.mContext).picture_filter_List();
        overlap_List = ((ReportActivity) ReportActivity.mContext).overlap_List();
        grayAp_List = ((ReportActivity) ReportActivity.mContext).grayAp_List();
        notRealAp_List = ((ReportActivity) ReportActivity.mContext).notRealAp_List();
        pantest_List = ((ReportActivity) ReportActivity.mContext).pantest_List();
    }


    class UploadThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            groupList.add(detailList);
            groupList.add(whiteList);
            groupList.add(warningAp_List);
            groupList.add(ssid_filter_List);
            groupList.add(picture_filter_List);
            groupList.add(pantest_List);
            groupList.add(grayAp_List);
            groupList.add(notRealAp_List);
            groupList.add(overlap_List);


            detailAdapter = new DetailAdapter(mContext, titleList, groupList);
            lv_detail.setAdapter(detailAdapter);

            //dashboard warning click event
            if (ReportActivity.isWarning == true) {
                lv_detail.expandGroup(2);
                ReportActivity.isWarning = false;
            }
        }
    }


}
