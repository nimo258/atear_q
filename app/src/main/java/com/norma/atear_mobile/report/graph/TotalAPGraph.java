package com.norma.atear_mobile.report.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.project.SharedPref;

/**
 * Created by hyojin on 8/2/16.
 */
public class TotalAPGraph extends View {
    Canvas cv;

    public boolean isPDF = false;

    float default_width = 1296;
    float default_height = 1300;


    int total, data1, data2, data3;
    float width, height;
    float width_scale, height_scale;
    float density;

    float top_margin, left_margin;
    float diameter, radius;
    float number_size, text_size, title_size;
    float x1, x2, y1, y2;

    String title = "";

    boolean state_title = false;

    Paint pnt, pnt_sCircle, pnt_lCircle, pnt_text, pnt_title;

    private Context mContext;
    private SharedPref sharedPref;

    public TotalAPGraph(Context context, int total, int data1, int data2, int data3, LinearLayout linearLayout) {
        super(context);
        mContext = context;
        sharedPref = new SharedPref(mContext);

        this.total = total;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;

        ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        this.width = lp.width;
        this.height = lp.height;

    }

    public TotalAPGraph(Context context, int total, int data1, int data2, int data3, int width) {
        super(context);
        mContext = context;
        sharedPref = new SharedPref(mContext);

        this.total = total;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;


        //ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        ////this.width = lp.width;
        //this.height = lp.height;
        this.width = width;
        this.height = (float) (width);
        this.cv = new Canvas();


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        // width 진짜 크기 구하기
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = 0;
        switch (widthMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                widthSize = 100;
                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        // height 진짜 크기 구하기
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = 0;


        switch (heightMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                heightSize = widthSize;

                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                heightSize = widthSize;

                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                heightSize = widthSize; //MeasureSpec.getSize(heightMeasureSpec);

                break;
        }
        widthSize = (int) width;
        heightSize = (int) height;

        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.cv = canvas;

        // if(isPDF == false) {
        cv.drawColor(Color.parseColor("#152a3c"));  //배경색
        //  }else{
        //      cv.drawColor(0xFFFFFFFF);
        //  }

        setParameter();
        setPaint();

        if (state_title) {
            showTitle();     //제목 적기
        }

        filter(mContext.getResources().getString(R.string.lowCase_totalApMsg), "#00aaf6", (int) x1, (int) y1, total, total);    //글자, 색깔, x좌표, y좌표, 전체 데이터, 부분 데이터
        filter(mContext.getResources().getString(R.string.lowCase_WarningApMsg), "#ff505f", (int) x2, (int) y1, total, data1);
        filter(mContext.getResources().getString(R.string.GrayApMsg), "#f9ca4c", (int) x1, (int) y2, total, data2);
        filter(mContext.getResources().getString(R.string.lowCase_notRealApMsg), "#e32d71", (int) x2, (int) y2, total, data3);


    }

    public void filter(String text, String stringColor, float x, float y, int data1, int data2) {
        pnt = new Paint();
        pnt.setAntiAlias(true);
        pnt.setColor(Color.parseColor(stringColor));
        pnt.setTextSize(number_size);

        float width_text = pnt_text.measureText(text);  //글자 폭
        float width_number = pnt.measureText(String.valueOf(data2));    //데이터 폭

        RectF rect = new RectF();
        rect.set(x - radius, y - radius, x + radius, y + radius);   //사각형 365*365 안에 내접함

        cv.drawCircle(x, y, radius, pnt_lCircle);   // 큰 원 그리기
        cv.drawArc(rect, -90, (float) data2 * 360 / (float) data1, true, pnt);
        cv.drawCircle(x, y, 130 * density, pnt_sCircle);  //작은 원 그리기

        //글자 적기
        cv.drawText(text, x - width_text / 2.0f, y + text_size + radius + 36 * density, pnt_text);

        //데이터 값적기
        cv.drawText(String.valueOf(data2), x - width_number / 2.0f, y + number_size * 1.75f / 5.0f, pnt);
    }

    private void setPaint() {
        pnt_sCircle = new Paint();    //작은 원
        pnt_sCircle.setAntiAlias(true);
        pnt_sCircle.setColor(Color.parseColor("#152a3c"));

        pnt_lCircle = new Paint();    //큰 원
        pnt_lCircle.setAntiAlias(true);
        pnt_lCircle.setColor(Color.parseColor("#375e7d"));

        pnt_text = new Paint();
        pnt_text.setAntiAlias(true);
        //   if(isPDF == false) {
        pnt_text.setColor(Color.WHITE);
        //  }else{
        //       pnt_text.setColor(Color.BLACK);
        //   }
        pnt_text.setTextSize(text_size);

        pnt_title = new Paint();
        pnt_title.setAntiAlias(true);
        pnt_title.setColor(Color.parseColor("#FFFFFF"));
        pnt_title.setTextSize(title_size);                      //제목의 paint
    }

    public void setTitle(String title) {
        this.title = title;
        state_title = true;
        default_height += 200;
    }


    private void setParameter() {
        width_scale = width / default_width;
        height_scale = height / default_height;

        density = (float) Math.sqrt(height_scale * width_scale);

        if (state_title)
            top_margin = 500 * density;      //위쪽 여백
        else
            top_margin = 300 * density;

        left_margin = 300 * density;    //왼쪽 여백

        diameter = 365 * density;       //지름
        radius = diameter / 2.0f;    //반지름

        number_size = 90 * density;
        text_size = 55 * density;
        title_size = 80 * density;

        x1 = left_margin;
        x2 = left_margin + 300 * density + diameter;
        y1 = top_margin;
        y2 = top_margin + 106 * density + text_size * 1.75f + diameter;
    }

    private void showTitle() {
        cv.drawText(title, width / 2.0f - pnt_title.measureText(title) / 2, top_margin * 0.3f, pnt_title);
    }


    public Bitmap getBitmap() {
        this.measure(View.MeasureSpec.makeMeasureSpec((int) this.width, View.MeasureSpec.EXACTLY), //가로폭
                View.MeasureSpec.makeMeasureSpec((int) this.height, View.MeasureSpec.EXACTLY)); //세로폭

        this.layout(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight());
        this.buildDrawingCache();

        Bitmap bit = this.getDrawingCache();


        return bit;
    }

}