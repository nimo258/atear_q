package com.norma.atear_mobile.report.excel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.project.comparator.AlertComparator;
import com.norma.atear_mobile.project.comparator.AuthComparator;
import com.norma.atear_mobile.report.graph.AlertGraph;
import com.norma.atear_mobile.report.graph.ChannelGraph;
import com.norma.atear_mobile.report.graph.GraphInfo;
import com.norma.atear_mobile.report.graph.TotalAPGraph;
import com.norma.atear_mobile.report.graph.VerticalBarGraph;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by hyojin on 2017-08-03.
 */

public class ExcelWriter {

    public boolean isStatus = false;

    public String outpath = null;

    private ArrayList<Ap_info> apList;

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private XSSFCell cell;
    XSSFRow row;

    XSSFCellStyle style;

    private String filename = null;
    public UploadAsync uploadAsync = null;
    private WriterAsync writerAsync = null;
    private ArrayList<String> fileNameList = null;

    private ProgressDialog progressDialog;

    int m_row = 11; //행

    int m_row2 = 0;

    boolean isPolicy_iso = false;
    boolean isPolicy_isms = false;
    boolean isPolicy_norma = false;


    private int total_AP = 0;
    private int warning_AP = 0;
    private int gray_AP = 0;
    private int not_real_AP = 0;
    private int alert_high = 0;
    private int alert_middle = 0;
    private int alert_safety = 0;

    //authorized
    private int authAp = 0;   //인가 AP;
    private int unAuthAp = 0; //비인가 AP;
    //enc
    private int toOPEN = 0;
    private int toWEP = 0;
    private int toWPA = 0;
    private int toWPA2 = 0;
    private int toWPA_s = 0;

    private int authOPEN = 0;
    private int authWEP = 0;
    private int authWPA = 0;
    private int authWPA2 = 0;
    private int authWPA_s = 0;

    private int unAuthOPEN = 0;
    private int unAuthWEP = 0;
    private int unAuthWPA = 0;
    private int unAuthWPA2 = 0;
    private int unAuthWPA_s = 0;
    //ssid
    private int authHiddenSsid = 0;
    private int authSpecificSsid = 0;
    private int authdefaultSsid = 0;
    private int authSoftAp = 0;


    private int unAuthHiddenSsid = 0;
    private int unAuthGrayAp = 0;

    //위험도
    private int high = 0;
    private int middle = 0;
    private int safe = 0;

    private ArrayList<Integer> chList;
    private ArrayList<Integer> total_chList;
    private List<GraphInfo> auth_isSSidList;

    private ArrayList<Bitmap> DashBoardList;
    private ArrayList<Bitmap> ReportList_total;
    private ArrayList<Bitmap> ReportList_auth;
    private ArrayList<Bitmap> ReportList_unAuth;

    private ArrayList<Ap_info> auth_apList;
    private ArrayList<Ap_info> unAuth_apList;
    private ArrayList<Ap_info> pantest_apList;
    private ArrayList<Ap_info> picture_apList;

    Context mContext;
    String string_ssid = "SSID";
    String string_bssid = "BSSID";

    public ExcelWriter(Context mContext, String filename, ArrayList<Ap_info> apList) {
        this.mContext = mContext;
        this.filename = filename;
        this.apList = apList;

        File excelDir = new File("/sdcard/Norma/Excel");
        if (!excelDir.exists()) {
            excelDir.mkdirs();
        }
    }

    public void start() {
        isStatus = true;
        uploadAsync = new UploadAsync();
        uploadAsync.execute();
    }


   public class UploadAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         /*   //test용 다시만들기 위해 일단 삭제
            File file = new File("/sdcard/Norma/Excel/" + filename + ".xlsx");
            if (file.exists()) {
                file.delete();
            }*/
            total_AP = 0;
            warning_AP = 0;
            gray_AP = 0;
            not_real_AP = 0;
            alert_high = 0;
            alert_middle = 0;
            alert_safety = 0;

            toOPEN = 0;
            toWEP = 0;
            toWPA = 0;
            toWPA2 = 0;
            toWPA_s = 0;

            authOPEN = 0;
            authWEP = 0;
            authWPA = 0;
            authWPA2 = 0;
            authWPA_s = 0;

            unAuthOPEN = 0;
            unAuthWEP = 0;
            unAuthWPA = 0;
            unAuthWPA2 = 0;
            unAuthWPA_s = 0;

            authHiddenSsid = 0;
            authSpecificSsid = 0;
            authdefaultSsid = 0;
            authSoftAp = 0;

            unAuthHiddenSsid = 0;
            unAuthGrayAp = 0;

            high = 0;
            middle = 0;
            safe = 0;

            chList = new ArrayList<>();
            total_chList = new ArrayList<>();
            auth_isSSidList = new ArrayList<>();

            DashBoardList = new ArrayList<>();
            ReportList_total = new ArrayList<>();
            ReportList_auth = new ArrayList<>();
            ReportList_unAuth = new ArrayList<>();

            auth_apList = new ArrayList<>();
            unAuth_apList = new ArrayList<>();
            pantest_apList = new ArrayList<>();
            picture_apList = new ArrayList<>();

            try {
                progressDialog = new ProgressDialog(mContext);
                progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                progressDialog.setCancelable(false);
                progressDialog.show();
                //      .sendEmptyMessageDelayed(0, 1000);  // 데이터 초기화

            } catch (Exception e) {

            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            for (Ap_info ap_info : apList) {

                switch (ap_info.policyType) {
                    case 1: //isms
                        isPolicy_isms = true;
                        break;

                    case 2: //iso
                        isPolicy_iso = true;
                        break;

                    case 3:
                        isPolicy_norma = true;
                        break;
                }

                //warning
                if (ap_info.encryption.equals("OPEN") || ap_info.encryption.equals("WEP")) {
                    warning_AP++;
                }
                //not real ap
                if (ap_info.ap_type.toUpperCase().equals("SOFT AP")) {
                    not_real_AP++;
                }

                //alert
                switch (ap_info.alert) {
                    case "Safe":
                        alert_safety++;
                        break;
                    case "Middle":
                        alert_middle++;
                        break;
                    default:
                        alert_high++;
                        break;

                }
                //channel
                chList.add(ap_info.channel);

                //total ap
                total_AP++;

                // total_AP = apList.size();

                //   ArrayList<String> grayAp = GrayAP();
                // gray_AP = grayAp.size();


                total_chList.add(ap_info.channel);

                int enc = 0;

                switch (ap_info.encryption) {
                    case "OPEN":
                        toOPEN++;
                        if (ap_info.isAuthorized == 1) {
                            authOPEN++;
                        } else {
                            unAuthOPEN++;
                        }
                        break;
                    case "WEP":
                        toWEP++;
                        if (ap_info.isAuthorized == 1) {
                            authWEP++;
                        } else {
                            unAuthWEP++;
                        }
                        break;
                    case "WPA":
                        toWPA++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA++;
                        } else {
                            unAuthWPA++;
                        }
                        break;
                    case "WPA2":
                        toWPA2++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA2++;
                        } else {
                            unAuthWPA2++;
                        }
                        break;
                    default:
                        toWPA_s++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA_s++;
                        } else {
                            unAuthWPA_s++;
                        }
                        break;

                }

                switch (ap_info.alert) {
                    case "Middle":
                        middle++;
                        break;
                    case "Safe":
                        safe++;
                        break;
                    default:
                        high++;
                        break;
                }

                //gray ap
                if (isGrayAp(ap_info.ssid)) {
                    gray_AP++;
                    ap_info.isGrayAp = true;
                } else
                    ap_info.isGrayAp = false;


                //인가
                if (ap_info.isAuthorized == 1) {
                    auth_apList.add(ap_info); //인가 list 추가
                    authAp++;
                    auth_isSSidList.add(new GraphInfo(ap_info.ssid));

                    if (ap_info.ssid.equals(("(Hidden)")))
                        authHiddenSsid++;


                    if (ap_info.ap_type.toUpperCase().equals("SOFT AP"))
                        authSoftAp++;

                } else {
                    unAuth_apList.add(ap_info);//비인가 list 추가
                    unAuthAp++;
                    if (ap_info.ssid.equals(("(Hidden)")))
                        unAuthHiddenSsid++;
                    if (ap_info.isGrayAp)
                        unAuthGrayAp++;

                }

                //딕셔너리 리스트 추가
                if (ap_info.isPentest != 0)
                    pantest_apList.add(ap_info);

                // 사진 저장되어있으면  사진list추가
                if (!ap_info.picturePath.equals("none")) {
                    picture_apList.add(ap_info);

                }


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            ////////dash board
            TotalAPGraph totalAPGraph = new TotalAPGraph(mContext, total_AP, warning_AP, gray_AP, not_real_AP, 600);
            totalAPGraph.isPDF = true;
            totalAPGraph.setTitle(mContext.getResources().getString(R.string.apStaticsTitleMsg));
            DashBoardList.add(totalAPGraph.getBitmap());

            //alert Graph
            java.util.List<GraphInfo> arrCircleGraph = new ArrayList<>();

            if (alert_high != 0) {
                arrCircleGraph.add(new GraphInfo(alert_high, "#f9c93c", mContext.getResources().getString(R.string.highMsg)));
            }
            if (alert_middle != 0) {
                arrCircleGraph.add(new GraphInfo(alert_middle, "#36b7f0", mContext.getResources().getString(R.string.middleMsg)));
            }
            if (alert_safety != 0) {
                arrCircleGraph.add(new GraphInfo(alert_safety, "#55de6a", mContext.getResources().getString(R.string.safeMsg)));
            }
            AlertGraph alertGraph = new AlertGraph(mContext, arrCircleGraph, 400);
            alertGraph.isPDF = true;
            alertGraph.setTitle(mContext.getResources().getString(R.string.riskTitleMsg));
            DashBoardList.add(alertGraph.getBitmap());

            //channel Graph
            ChannelGraph channelGraph = new ChannelGraph(mContext, 1400);
            channelGraph.isPDF = true;
            channelGraph.sort();
            channelGraph.setTitle(mContext.getResources().getString(R.string.chTitleMsg));
            for (int i : chList) {
                channelGraph.getData(i);
            }
            DashBoardList.add(channelGraph.getBitmap());


            //total
            //total ap
            java.util.List<GraphInfo> totalAuthList = new ArrayList<>();
            if (authAp != 0) {
                totalAuthList.add(new GraphInfo(authAp, "#FFFFFF", mContext.getResources().getString(R.string.authApMsg)));
            }
            if (unAuthAp != 0) {
                totalAuthList.add(new GraphInfo(unAuthAp, "#000000", mContext.getResources().getString(R.string.unAuthApMsg)));
            }
            AlertGraph totalAuthGraph = new AlertGraph(mContext, totalAuthList, 400);
            totalAuthGraph.setTitle(mContext.getResources().getString(R.string.authApMsg));
            ReportList_total.add(totalAuthGraph.getBitmap());

            //total enc
            java.util.List<GraphInfo> totalEncList = new ArrayList<>();
            if (toOPEN != 0) {
                totalEncList.add(new GraphInfo(toOPEN, "#FFFFFF", "OPEN"));
            }
            if (toWEP != 0) {
                totalEncList.add(new GraphInfo(toWEP, "#FF0000", "WEP"));
            }
            if (toWPA != 0) {
                totalEncList.add(new GraphInfo(toWPA, "#00FF00", "WPA"));
            }
            if (toWPA2 != 0) {
                totalEncList.add(new GraphInfo(toWPA2, "#AAAA00", "WPA2"));
            }
            if (toWPA_s != 0) {
                totalEncList.add(new GraphInfo(toWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            AlertGraph totalEncGraph = new AlertGraph(mContext, totalEncList, 400);
            totalEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
            ReportList_total.add(totalEncGraph.getBitmap());

            //total alert  AP
            java.util.List<GraphInfo> totalAlertList = new ArrayList<>();
            totalAlertList.add(new GraphInfo(high, "#FFFFFF", mContext.getResources().getString(R.string.highMsg)));
            totalAlertList.add(new GraphInfo(middle, "#FF0000", mContext.getResources().getString(R.string.middleMsg)));
            totalAlertList.add(new GraphInfo(safe, "#00FF00", mContext.getResources().getString(R.string.safeMsg)));

            VerticalBarGraph totalAlertGraph = new VerticalBarGraph(mContext, totalAlertList, 400);
            totalAlertGraph.setTitle(mContext.getResources().getString(R.string.riskTitleMsg));
            ReportList_total.add(totalAlertGraph.getBitmap());

          /*  //total channel
            ChannelGraph totalChannelList = new ChannelGraph(mContext, 1400);
            for (int i : total_chList) {
                totalChannelList.getData(i);
            }
            totalChannelList.sort();
            totalChannelList.setTitle(chMsg);
            ReportList_total.add(totalChannelList.getBitmap());*/

            //authorized AP
            //인가 ssid
            if (auth_isSSidList.size() == 0) {
                auth_isSSidList.add(new GraphInfo(unAuthAp, "#000000", mContext.getResources().getString(R.string.unAuthApMsg)));
            }
            AlertGraph authSsidGraph = new AlertGraph(mContext, auth_isSSidList, 400);
            authSsidGraph.setStateDuplicate(true);  //중복제거 데이터 모아줌
            authSsidGraph.setTitle(mContext.getResources().getString(R.string.authSsidMsg));
            authSsidGraph.AUTH_SSID = true;
            ReportList_auth.add(authSsidGraph.getBitmap());


            //인가 enc
            java.util.List<GraphInfo> authEncList = new ArrayList<>();
            if (authOPEN != 0) {
                authEncList.add(new GraphInfo(authOPEN, "#FFFFFF", "OPEN"));
            }
            if (authWEP != 0) {
                authEncList.add(new GraphInfo(authWEP, "#FF0000", "WEP"));
            }
            if (authWPA != 0) {
                authEncList.add(new GraphInfo(authWPA, "#00FF00", "WPA"));
            }
            if (authWPA2 != 0) {
                authEncList.add(new GraphInfo(authWPA2, "#AAAA00", "WPA2"));
            }
            if (authWPA_s != 0) {
                authEncList.add(new GraphInfo(authWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            if (authEncList.size() == 0) {
                authEncList.add(new GraphInfo(1, "#FFFFFF", "noData"));
            }
            AlertGraph authEncGraph = new AlertGraph(mContext, authEncList, 400);
            authEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
            ReportList_auth.add(authEncGraph.getBitmap());

            //안전도 중 하에 따른 세부분류
            java.util.List<GraphInfo> authMiddleList = new ArrayList<>();
            //   authMiddleList.add(new GraphInfo(0, "#FFFFFF", defaultSsidMsg));
            authMiddleList.add(new GraphInfo(authSoftAp, "#FF0000", "Soft AP"));
            authMiddleList.add(new GraphInfo(authWEP, "#00FF00", "WEP"));
            //  authMiddleList.add(new GraphInfo(0, "#FF00FF", specificSsidMsg));
            authMiddleList.add(new GraphInfo(authHiddenSsid, "#AA0000", mContext.getResources().getString(R.string.hiddlenSsidMsg)));
            authMiddleList.add(new GraphInfo(authOPEN, "#FFAA00", "OPEN"));
            authMiddleList.add(new GraphInfo(authWPA, "#00FFFF", "WPA"));

            VerticalBarGraph authMiddleGraph = new VerticalBarGraph(mContext, authMiddleList, 800);
            authMiddleGraph.setTitle(mContext.getResources().getString(R.string.notSafeMsg));
            ReportList_auth.add(authMiddleGraph.getBitmap());


            //unAuthorized AP
            //unAuth enc
            java.util.List<GraphInfo> unAuthEncList = new ArrayList<>();
            if (unAuthOPEN != 0) {
                unAuthEncList.add(new GraphInfo(unAuthOPEN, "#FFFFFF", "OPEN"));
            }
            if (unAuthWEP != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWEP, "#FF0000", "WEP"));
            }
            if (unAuthWPA != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA, "#00FF00", "WPA"));
            }
            if (unAuthWPA2 != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA2, "#AAAA00", "WPA2"));
            }
            if (unAuthWPA_s != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            if (authEncList.size() == 0) {
                unAuthEncList.add(new GraphInfo(1, "#FFFFFF", "noData"));
            }
            AlertGraph unAuthEncGraph = new AlertGraph(mContext, unAuthEncList, 400);
            unAuthEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
            ReportList_unAuth.add(unAuthEncGraph.getBitmap());

            // unAuth hiiden , gray
            java.util.List<GraphInfo> unAuthHiddenList = new ArrayList<>();
            unAuthHiddenList.add(new GraphInfo(unAuthHiddenSsid, "#FFFFFF", mContext.getResources().getString(R.string.hiddlenSsidMsg)));
            unAuthHiddenList.add(new GraphInfo(unAuthGrayAp, "#FF0000", "Mobile Provider"));

            VerticalBarGraph unAuthHiddenGraph = new VerticalBarGraph(mContext, unAuthHiddenList, 800);
            unAuthHiddenGraph.setTitle(mContext.getResources().getString(R.string.hiddenProvidearApMsg));
            ReportList_unAuth.add(unAuthHiddenGraph.getBitmap());

            //excel 작업
            writerAsync = new WriterAsync();
            writerAsync.execute();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            Toast.makeText(mContext, mContext.getResources().getString(R.string.string_file_FailMsg), Toast.LENGTH_SHORT).show();
        }
    }


    class WriterAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... params) {


            xlsxWiter();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            isStatus = false;
            Toast.makeText(mContext,mContext.getResources().getString(R.string.string_create_fileMsg) , Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            isStatus = false;
            Toast.makeText(mContext, mContext.getResources().getString(R.string.string_file_FailMsg), Toast.LENGTH_SHORT).show();
        }
    }

    //엑셀 생성
    private int xlsxWiter() {

        if (fileNameList == null)
            return -1;
        //proguard Error Fixed.
        workbook = new XSSFWorkbook();

        //sheet1
        sheet = workbook.createSheet(mContext.getResources().getString(R.string.string_policy_title_main));
        //width 설정
        sheet.setColumnWidth(0, 3129);//컬럼 , 크기   1000 = 약 31px
        sheet.setColumnWidth(1, 6129);
        sheet.setColumnWidth(2, 5032);
        sheet.setColumnWidth(3, 3354);
        sheet.setColumnWidth(4, 9225);
        sheet.setColumnWidth(5, 5709);
        sheet.setColumnWidth(6, 5061);
        sheet.setColumnWidth(7, 2290);
        sheet.setColumnWidth(8, 4064);
        sheet.setColumnWidth(9, 5387);
        sheet.setColumnWidth(10, 3903);
        sheet.setColumnWidth(11, 4387);
        //     sheet.setColumnWidth(12, 15612);
        sheet.setColumnWidth(12, 4387);
        sheet.setColumnWidth(13, 13838);
        sheet.setColumnWidth(14, 11580);
        sheet.setColumnWidth(15, 9225);
        //제목
        //  sheet.addMergedRegion(new CellRangeAddress(3, 5, 2, 15)); //시작행, 끝행, 시작열,끝열
        row = sheet.createRow(0); //행
        cell = row.createCell(0);  //열
        row.setHeight((short) 2200);//행높이 조절  1000 = 50
        style = workbook.createCellStyle();  //style 초기화
        org.apache.poi.ss.usermodel.Font font = workbook.createFont();
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short) 36); //글자크기
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        style.setFont(font);
        style.setAlignment((short) 1); //왼쪽정렬     가운데 = 2
        style.setVerticalAlignment((short) 1); //세로 정렬
        cell.setCellValue(mContext.getResources().getString(R.string.string_excel_title));
        cell.setCellStyle(style);


        //보고서 리스트 제목
        row = sheet.createRow(3); //행
        cell = row.createCell(0);  //열
        row.setHeight((short) 820);//행높이 조절  1000 = 50

        style = workbook.createCellStyle();  //style 생성자
        font = workbook.createFont();
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short) 24); //글자크기
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        style.setAlignment((short) 1); //왼쪽정렬
        style.setVerticalAlignment((short) 1); //세로 정렬
        cell.setCellStyle(style);
        cell.setCellValue(mContext.getResources().getString(R.string.string_report_title));

        m_row = 3; //정적 행
        //보고서 리스트
        for (int i = 0; i < fileNameList.size(); i++) {
            m_row = m_row + 1;
            row = sheet.createRow(m_row); //행
            cell = row.createCell(0);  //열
            row.setHeight((short) 700);//행높이 조절  1000 = 50

            String fileNmae = fileNameList.get(i).replaceAll(".txt", "");
            cell.setCellValue(fileNmae);
            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontName("맑은 고딕");
            font.setFontHeightInPoints((short) 14); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            style.setFont(font);
            style.setAlignment((short) 1); //왼쪽정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            cell.setCellStyle(style);


        }

        //반영된 정책 제목
        if ((isPolicy_iso) || (isPolicy_isms) || (isPolicy_norma)) {
            m_row = m_row + 3;
            row = sheet.createRow(m_row); //행
            cell = row.createCell(0);  //열
            row.setHeight((short) 760);//행높이 조절  1000 = 50

            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontName("맑은 고딕");
            font.setFontHeightInPoints((short) 16); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            style.setFont(font);
            style.setAlignment((short) 1); //왼쪽정렬
            style.setVerticalAlignment((short) 1); //세로 정렬

            cell.setCellStyle(style);
            cell.setCellValue(mContext.getResources().getString(R.string.string_policy_manual_title));

            //반영된 정책
            if (isPolicy_iso) {
                m_row++;
                row = sheet.createRow(m_row); /// /행
                cell = row.createCell(0);  //열
                row.setHeight((short) 1200);//행높이 조절  1000 = 50

                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_iso));
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 14); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                //  style.setWrapText(true);  //자동 줄바꿈
                style.setAlignment((short) 1); //왼쪽정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellStyle(style);
            }
            if (isPolicy_isms) {
                m_row++;
                row = sheet.createRow(m_row); //행
                cell = row.createCell(0);  //열
                row.setHeight((short) 1200);//행높이 조절  1000 = 50

                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_isms));
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 14); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                //style.setWrapText(true);  //자동 줄바꿈
                style.setAlignment((short) 1); //왼쪽정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellStyle(style);
            }
            if (isPolicy_norma) {
                m_row++;
                row = sheet.createRow(m_row); //행
                cell = row.createCell(0);  //열
                row.setHeight((short) 1200);//행높이 조절  1000 = 50

                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_norma));
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 14); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                // style.setWrapText(true);  //자동 줄바꿈
                style.setAlignment((short) 1); //왼쪽정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellStyle(style);
            }
        }

        //DashBoard 제목
        m_row = m_row + 3;
        row = sheet.createRow(m_row); //행
        row.setHeight((short) 1100);//행높이 조절  1000 = 50
        sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

        style = workbook.createCellStyle();  //style 생성자
        //    style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
        //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
        font = workbook.createFont();
        font.setFontHeightInPoints((short) 28); //글자크기
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontName("맑은 고딕");
        style.setFont(font);
        style.setAlignment((short) 2); //글씨 가운데 정렬
        style.setVerticalAlignment((short) 1); //세로 정렬
        style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);

        for (int i = 0; i <= 12; i++) {
            if (i == 0) {
                cell = row.createCell(i);
                cell.setCellStyle(style);
                cell.setCellValue(mContext.getResources().getString(R.string.dashBoardTitleMsg));
            } else {
                cell = row.createCell(i);  //열
                cell.setCellStyle(style);
            }
        }

        //DashBoard
        for (int i = 0; i < DashBoardList.size(); i++) {
            Bitmap bitmap = DashBoardList.get(i);
            switch (i) {
                case 0:
                    m_row = m_row + 2;
                    ImageValue(bitmap, m_row, 0, m_row + 22, 3);
                    break;
                case 1:
                    ImageValue(bitmap, m_row, 4, m_row + 17, 6);
                    break;

                case 2:
                    ImageValue(bitmap, m_row, 7, m_row + 31, 11);
                    m_row = m_row + 31;
                    break;
            }
        }

        //Total 제목
        m_row = m_row + 2;
        row = sheet.createRow(m_row); //행
        row.setHeight((short) 1100);//행높이 조절  1000 = 50
        sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

        style = workbook.createCellStyle();  //style 생성자
        //  style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
        //      style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
        font = workbook.createFont();
        font.setFontHeightInPoints((short) 28); //글자크기
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontName("맑은 고딕");
        style.setFont(font);
        style.setAlignment((short) 2); //글씨 가운데 정렬
        style.setVerticalAlignment((short) 1); //세로 정렬
        style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);

        for (int i = 0; i <= 12; i++) {
            if (i == 0) {
                cell = row.createCell(i);
                cell.setCellStyle(style);
                cell.setCellValue(mContext.getResources().getString(R.string.totalMsg));
            } else {
                cell = row.createCell(i);  //열
                cell.setCellStyle(style);
            }
        }

        //Total
        for (int i = 0; i < ReportList_total.size(); i++) {
            Bitmap bitmap = ReportList_total.get(i);
            switch (i) {
                case 0:
                    m_row = m_row + 2;
                    ImageValue(bitmap, m_row, 0, m_row + 16, 3);
                    break;
                case 1:
                    ImageValue(bitmap, m_row, 4, m_row + 16, 6);
                    break;

                case 2:
                    ImageValue(bitmap, m_row, 7, m_row + 22, 11);
                    m_row = m_row + 22;
                    break;
                case 3:
                    //    ImageValue(bitmap, m_row, 17, m_row + 16, 21);
                    //    m_row = m_row + 16;
                    break;
            }
        }

        if (auth_apList.size() != 0) {
            //Authorized Ap 제목
            m_row = m_row + 2;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //     style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 12; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.authApMsg));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //Authorized 그래프
            for (int i = 0; i < ReportList_auth.size(); i++) {
                Bitmap bitmap = ReportList_auth.get(i);
                switch (i) {
                    case 0:
                        m_row = m_row + 2;
                        ImageValue(bitmap, m_row, 0, m_row + 16, 3);
                        break;
                    case 1:
                        ImageValue(bitmap, m_row, 4, m_row + 16, 6);
                        break;

                    case 2:
                        ImageValue(bitmap, m_row, 7, m_row + 22, 11);
                        m_row = m_row + 22;
                        break;
                }
            }


            //Authorized Ap Detail 제목
            m_row = m_row + 2;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //   style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //   style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 12; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.string_authorizedap_detail_title));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //Authorized Ap Detail 컬럼별 제목
            m_row = m_row + 1;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 820);//행높이 조절  1000 = 50
            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 12); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            cell = row.createCell(0); //날짜

            cell.setCellValue(mContext.getResources().getString(R.string.saveTimeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(1); //project
            cell.setCellValue(mContext.getResources().getString(R.string.proTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(2); //위치
            cell.setCellValue(mContext.getResources().getString(R.string.loTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(3); //ap 유형
            cell.setCellValue(mContext.getResources().getString(R.string.apTypeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(4); //oui
            cell.setCellValue(mContext.getResources().getString(R.string.ouiTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(5); //ssid
            cell.setCellValue(string_ssid);
            cell.setCellStyle(style);

            cell = row.createCell(6); //bssid
            cell.setCellValue(string_bssid);
            cell.setCellStyle(style);

            cell = row.createCell(7); //channel
            cell.setCellValue(mContext.getResources().getString(R.string.detailchTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(8); //power
            cell.setCellValue(mContext.getResources().getString(R.string.signalTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(9); //enc
            cell.setCellValue(mContext.getResources().getString(R.string.encTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(10); //policy type
            cell.setCellValue(mContext.getResources().getString(R.string.polictTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(11); //alert
            cell.setCellValue(mContext.getResources().getString(R.string.alertTitleMsg));
            cell.setCellStyle(style);

            // cell = row.createCell(12); //command
            //   cell.setCellValue(commentTitleMsg);
            //    cell.setCellStyle(style);

            cell = row.createCell(12); //real ap
            cell.setCellValue(mContext.getResources().getString(R.string.string_real_ap_title));
            cell.setCellStyle(style);

            //Authorized Ap Detail data
            AlertComparator alertComparator_unAuth = new AlertComparator();
            alertComparator_unAuth.sortType(1); // 안전한 것부터
            Collections.sort(auth_apList, alertComparator_unAuth);
            Collections.reverse(auth_apList);

            for (int i = 0; i < auth_apList.size(); i++) {
                m_row = m_row + 1;
                row = sheet.createRow(m_row); //행
                row.setHeight((short) 500);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                //   font.setFontHeightInPoints((short) 18); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                Ap_info ap_info = auth_apList.get(i);
                String savetime = ap_info.save_time.substring(0, 10);

                cell = row.createCell(0); //날짜
                cell.setCellValue(savetime);
                cell.setCellStyle(style);

                cell = row.createCell(1); //project
                cell.setCellValue(ap_info.projectName);
                cell.setCellStyle(style);

                cell = row.createCell(2); //위치
                cell.setCellValue(ap_info.locationName);
                cell.setCellStyle(style);

                cell = row.createCell(3); //ap 유형
                cell.setCellValue(ap_info.ap_type);
                cell.setCellStyle(style);

                cell = row.createCell(4); //oui
                cell.setCellValue(ap_info.oui);
                cell.setCellStyle(style);

                cell = row.createCell(5); //ssid
                cell.setCellValue(ap_info.ssid);
                cell.setCellStyle(style);

                cell = row.createCell(6); //bssid
                cell.setCellValue(ap_info.bssid);
                cell.setCellStyle(style);

                cell = row.createCell(7); //channel
                cell.setCellValue(String.valueOf(ap_info.channel));
                cell.setCellStyle(style);

                cell = row.createCell(8); //power
                cell.setCellValue(String.valueOf(ap_info.signal));
                cell.setCellStyle(style);

                String enc = "";
                if (ap_info.encryption.equals("WPA/WPA2")) {
                    enc = "WPA2";
                } else if(ap_info.encryption.contains("WPA_s")) {
                    enc= "WPA2";
                }else {
                    enc = ap_info.encryption;
                }

                cell = row.createCell(9); //enc
                cell.setCellValue(enc);
                cell.setCellStyle(style);

                cell = row.createCell(10); //policy type
                cell.setCellValue(policy_Type(ap_info.policyType));
                cell.setCellStyle(style);

                cell = row.createCell(11); //alert
                cell.setCellValue(ap_info.alert);
                cell.setCellStyle(style);

                String[] temp = ap_info.comment.split(" → ");

                //  cell = row.createCell(12); //command
                //   cell.setCellValue(temp[0]);
                //   cell.setCellStyle(style);

                String isGray = "";
                if ((ap_info.isGrayAp))
                    isGray = "O";
                else
                    isGray = "X";
                cell = row.createCell(12); //real ap
                cell.setCellValue(isGray);
                cell.setCellStyle(style);
            }
        }

        if (unAuth_apList.size() != 0) {
            //unAuthorzied 제목 (image);
            m_row = m_row + 3;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //   style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 12; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.unAuthApMsg));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //UnAuthorized 그래프
            for (int i = 0; i < ReportList_unAuth.size(); i++) {
                Bitmap bitmap = ReportList_unAuth.get(i);
                switch (i) {
                    case 0:
                        m_row = m_row + 2;
                        ImageValue(bitmap, m_row, 0, m_row + 17, 3);
                        break;
                    case 1:
                        ImageValue(bitmap, m_row, 4, m_row + 22, 6);
                        m_row = m_row + 22;
                        break;

                }
            }


            //unAuthorized Ap Detail 제목
            m_row = m_row + 2;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 12)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //     style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 13; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.string_unAuthorizedap_detail_title));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //unAuthorized Ap Detail 컬럼별 제목
            m_row = m_row + 1;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 820);//행높이 조절  1000 = 50
            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 12); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            cell = row.createCell(0); //날짜
            cell.setCellValue(mContext.getResources().getString(R.string.saveTimeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(1); //project
            cell.setCellValue(mContext.getResources().getString(R.string.proTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(2); //위치
            cell.setCellValue(mContext.getResources().getString(R.string.loTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(3); //ap 유형
            cell.setCellValue(mContext.getResources().getString(R.string.apTypeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(4); //oui
            cell.setCellValue(mContext.getResources().getString(R.string.ouiTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(5); //ssid
            cell.setCellValue(string_ssid);
            cell.setCellStyle(style);

            cell = row.createCell(6); //bssid
            cell.setCellValue(string_bssid);
            cell.setCellStyle(style);

            cell = row.createCell(7); //channel
            cell.setCellValue(mContext.getResources().getString(R.string.detailchTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(8); //power
            cell.setCellValue(mContext.getResources().getString(R.string.signalTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(9); //enc
            cell.setCellValue(mContext.getResources().getString(R.string.encTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(10); //policy type
            cell.setCellValue(mContext.getResources().getString(R.string.polictTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(11); //alert
            cell.setCellValue(mContext.getResources().getString(R.string.alertTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(12); //802.11ax
            cell.setCellValue(mContext.getResources().getString(R.string.info_axMsg));
            cell.setCellStyle(style);

            //   cell = row.createCell(12); //command
            //  cell.setCellValue(commentTitleMsg);
            //   cell.setCellStyle(style);

            cell = row.createCell(13); //real ap
            cell.setCellValue(mContext.getResources().getString(R.string.string_real_ap_title));
            cell.setCellStyle(style);

            //unAuthorized Ap Detail data
            AlertComparator alertComparator_unAuth = new AlertComparator();
            alertComparator_unAuth.sortType(0); // 위험한 것부터
            Collections.sort(unAuth_apList, alertComparator_unAuth);
            Collections.reverse(unAuth_apList);

            for (int i = 0; i < unAuth_apList.size(); i++) {
                m_row = m_row + 1;
                row = sheet.createRow(m_row); //행
                row.setHeight((short) 500);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                //   font.setFontHeightInPoints((short) 18); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                Ap_info ap_info = unAuth_apList.get(i);
                String savetime = ap_info.save_time.substring(0, 10);

                cell = row.createCell(0); //날짜
                cell.setCellValue(savetime);
                cell.setCellStyle(style);

                cell = row.createCell(1); //project
                cell.setCellValue(ap_info.projectName);
                cell.setCellStyle(style);

                cell = row.createCell(2); //위치
                cell.setCellValue(ap_info.locationName);
                cell.setCellStyle(style);

                cell = row.createCell(3); //ap 유형
                cell.setCellValue(ap_info.ap_type);
                cell.setCellStyle(style);

                cell = row.createCell(4); //oui
                cell.setCellValue(ap_info.oui);
                cell.setCellStyle(style);

                cell = row.createCell(5); //ssid
                cell.setCellValue(ap_info.ssid);
                cell.setCellStyle(style);

                cell = row.createCell(6); //bssid
                cell.setCellValue(ap_info.bssid);
                cell.setCellStyle(style);

                cell = row.createCell(7); //channel
                cell.setCellValue(String.valueOf(ap_info.channel));
                cell.setCellStyle(style);

                cell = row.createCell(8); //power
                cell.setCellValue(String.valueOf(ap_info.signal));
                cell.setCellStyle(style);

                String enc = "";
                if (ap_info.encryption.equals("WPA_s")) {
                    enc = "WPA/WPA2";
                } else {
                    enc = ap_info.encryption;
                }

                cell = row.createCell(9); //enc
                cell.setCellValue(enc);
                cell.setCellStyle(style);

                cell = row.createCell(10); //policy type
                cell.setCellValue(policy_Type(ap_info.policyType));
                cell.setCellStyle(style);

                cell = row.createCell(11); //alert
                cell.setCellValue(ap_info.alert);
                cell.setCellStyle(style);

                cell = row.createCell(12); //802.11ax
                cell.setCellValue(ap_info.is11axAp?"O":"X");
                cell.setCellStyle(style);

                String[] temp = ap_info.comment.split(" → ");

                //   cell = row.createCell(12); //command
                //    cell.setCellValue(temp[0]);
                //    cell.setCellStyle(style);

                String isGray = "";
                if ((ap_info.isGrayAp))
                    isGray = "O";
                else
                    isGray = "X";
                cell = row.createCell(13); //real ap
                cell.setCellValue(isGray);
                cell.setCellStyle(style);
            }
        }


        if (pantest_apList.size() != 0) {
            //Pentest  Ap Detail 제목
            m_row = m_row + 3;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 14)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //    style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 14; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.detailpentestTitleMsg));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //Pentest Ap Detail 컬럼별 제목
            m_row = m_row + 1;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 820);//행높이 조절  1000 = 50
            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 12); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            cell = row.createCell(0); //날짜
            cell.setCellValue(mContext.getResources().getString(R.string.saveTimeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(1); //project
            cell.setCellValue(mContext.getResources().getString(R.string.proTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(2); //위치
            cell.setCellValue(mContext.getResources().getString(R.string.loTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(3); //ap 유형
            cell.setCellValue(mContext.getResources().getString(R.string.apTypeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(4); //oui
            cell.setCellValue(mContext.getResources().getString(R.string.ouiTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(5); //ssid
            cell.setCellValue(string_ssid);
            cell.setCellStyle(style);

            cell = row.createCell(6); //bssid
            cell.setCellValue(string_bssid);
            cell.setCellStyle(style);

            cell = row.createCell(7); //channel
            cell.setCellValue(mContext.getResources().getString(R.string.detailchTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(8); //power
            cell.setCellValue(mContext.getResources().getString(R.string.signalTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(9); //enc
            cell.setCellValue(mContext.getResources().getString(R.string.encTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(10); //policy type
            cell.setCellValue(mContext.getResources().getString(R.string.polictTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(11); //alert
            cell.setCellValue(mContext.getResources().getString(R.string.alertTitleMsg));
            cell.setCellStyle(style);

            //   cell = row.createCell(12); //command
            ////    cell.setCellValue(commentTitleMsg);
            //   cell.setCellStyle(style);

            cell = row.createCell(12); //password
            cell.setCellValue(mContext.getResources().getString(R.string.string_password));
            cell.setCellStyle(style);

            cell = row.createCell(13); //Authorized / UnAuthorized
            cell.setCellValue(mContext.getResources().getString(R.string.string_is_authorized));
            cell.setCellStyle(style);

            cell = row.createCell(14); //real ap
            cell.setCellValue(mContext.getResources().getString(R.string.string_real_ap_title));
            cell.setCellStyle(style);


            //Pentest Ap Detail data
            Collections.sort(pantest_apList, new AuthComparator());
            Collections.reverse(pantest_apList);
            for (int i = 0; i < pantest_apList.size(); i++) {
                m_row = m_row + 1;
                row = sheet.createRow(m_row); //행
                row.setHeight((short) 500);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                //   font.setFontHeightInPoints((short) 18); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                Ap_info ap_info = pantest_apList.get(i);
                String savetime = ap_info.save_time.substring(0, 10);

                cell = row.createCell(0); //날짜
                cell.setCellValue(savetime);
                cell.setCellStyle(style);

                cell = row.createCell(1); //project
                cell.setCellValue(ap_info.projectName);
                cell.setCellStyle(style);

                cell = row.createCell(2); //위치
                cell.setCellValue(ap_info.locationName);
                cell.setCellStyle(style);

                cell = row.createCell(3); //ap 유형
                cell.setCellValue(ap_info.ap_type);
                cell.setCellStyle(style);

                cell = row.createCell(4); //oui
                cell.setCellValue(ap_info.oui);
                cell.setCellStyle(style);

                cell = row.createCell(5); //ssid
                cell.setCellValue(ap_info.ssid);
                cell.setCellStyle(style);

                cell = row.createCell(6); //bssid
                cell.setCellValue(ap_info.bssid);
                cell.setCellStyle(style);

                cell = row.createCell(7); //channel
                cell.setCellValue(String.valueOf(ap_info.channel));
                cell.setCellStyle(style);

                cell = row.createCell(8); //power
                cell.setCellValue(String.valueOf(ap_info.signal));
                cell.setCellStyle(style);

                String enc = "";
                if (ap_info.encryption.equals("WPA_s")) {
                    enc = "WPA/WPA2";
                } else {
                    enc = ap_info.encryption;
                }

                cell = row.createCell(9); //enc
                cell.setCellValue(enc);
                cell.setCellStyle(style);

                cell = row.createCell(10); //policy type
                cell.setCellValue(policy_Type(ap_info.policyType));
                cell.setCellStyle(style);

                cell = row.createCell(11); //alert
                cell.setCellValue(ap_info.alert);
                cell.setCellStyle(style);

                String[] temp = ap_info.comment.split(" → ");

                //    cell = row.createCell(12); //command
                //    cell.setCellValue(temp[0]);
                //   cell.setCellStyle(style);
//
                cell = row.createCell(12); //password
                cell.setCellValue(ap_info.pentest_password);
                cell.setCellStyle(style);

                String isAuth;
                if (ap_info.isAuthorized == 1)
                    isAuth = mContext.getResources().getString(R.string.authApMsg);
                else
                    isAuth = mContext.getResources().getString(R.string.unAuthApMsg);

                cell = row.createCell(13); //Authorized / UnAuthorized
                cell.setCellValue(isAuth);
                cell.setCellStyle(style);

                String isGray = "";
                if ((ap_info.isGrayAp))
                    isGray = "O";
                else
                    isGray = "X";
                cell = row.createCell(14); //real ap
                cell.setCellValue(isGray);
                cell.setCellStyle(style);
            }
        }

        if (picture_apList.size() != 0) {
            //발견하여 추적된 비인가 AP 제목 (picture)
            m_row = m_row + 3;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 1100);//행높이 조절  1000 = 50
            sheet.addMergedRegion(new CellRangeAddress(m_row, m_row, 0, 15)); //병합 시작행, 끝행, 시작열,끝열

            style = workbook.createCellStyle();  //style 생성자
            //    style.setFillForegroundColor(HSSFColor.AQUA.index); //셀 색상
            //    style.setFillPattern(CellStyle.SOLID_FOREGROUND);  //색상 패턴 적용
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 28); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            for (int i = 0; i <= 15; i++) {
                if (i == 0) {
                    cell = row.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(mContext.getResources().getString(R.string.string_pictureAp_title));
                } else {
                    cell = row.createCell(i);  //열
                    cell.setCellStyle(style);
                }
            }

            //발견하여 추적된 비인가 AP 컬럼
            m_row = m_row + 1;
            row = sheet.createRow(m_row); //행
            row.setHeight((short) 820);//행높이 조절  1000 = 50
            style = workbook.createCellStyle();  //style 생성자
            font = workbook.createFont();
            font.setFontHeightInPoints((short) 12); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("맑은 고딕");
            style.setFont(font);
            style.setAlignment((short) 2); //글씨 가운데 정렬
            style.setVerticalAlignment((short) 1); //세로 정렬
            style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);

            cell = row.createCell(0); //날짜
            cell.setCellValue(mContext.getResources().getString(R.string.saveTimeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(1); //project
            cell.setCellValue(mContext.getResources().getString(R.string.proTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(2); //위치
            cell.setCellValue(mContext.getResources().getString(R.string.loTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(3); //ap 유형
            cell.setCellValue(mContext.getResources().getString(R.string.apTypeTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(4); //oui
            cell.setCellValue(mContext.getResources().getString(R.string.ouiTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(5); //ssid
            cell.setCellValue(string_ssid);
            cell.setCellStyle(style);

            cell = row.createCell(6); //bssid
            cell.setCellValue(string_bssid);
            cell.setCellStyle(style);

            cell = row.createCell(7); //channel
            cell.setCellValue(mContext.getResources().getString(R.string.detailchTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(8); //power
            cell.setCellValue(mContext.getResources().getString(R.string.signalTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(9); //enc
            cell.setCellValue(mContext.getResources().getString(R.string.encTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(10); //policy type
            cell.setCellValue(mContext.getResources().getString(R.string.polictTitleMsg));
            cell.setCellStyle(style);

            cell = row.createCell(11); //alert
            cell.setCellValue(mContext.getResources().getString(R.string.alertTitleMsg));
            cell.setCellStyle(style);

            // cell = row.createCell(12); //command
            //    cell.setCellValue(commentTitleMsg);
            //   cell.setCellStyle(style);

            cell = row.createCell(12); //real ap
            cell.setCellValue(mContext.getResources().getString(R.string.string_real_ap_title));
            cell.setCellStyle(style);

            cell = row.createCell(13); //ap 발견 사진
            cell.setCellValue(mContext.getResources().getString(R.string.string_picture));
            cell.setCellStyle(style);

            cell = row.createCell(14); //ap 발견 정보
            cell.setCellValue(mContext.getResources().getString(R.string.string_memo));
            cell.setCellStyle(style);

            cell = row.createCell(15); //Authorized / UnAuthorized
            cell.setCellValue(mContext.getResources().getString(R.string.string_is_authorized));
            cell.setCellStyle(style);

            //picture ap detail data
            Collections.sort(picture_apList, new AuthComparator());
            Collections.reverse(picture_apList);
            for (int i = 0; i < picture_apList.size(); i++) {
                m_row = m_row + 1;
                row = sheet.createRow(m_row); //행
                row.setHeight((short) 5340);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                //   font.setFontHeightInPoints((short) 18); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                Ap_info ap_info = picture_apList.get(i);
                String savetime = ap_info.save_time.substring(0, 10);

                cell = row.createCell(0); //날짜
                cell.setCellValue(savetime);
                cell.setCellStyle(style);

                cell = row.createCell(1); //project
                cell.setCellValue(ap_info.projectName);
                cell.setCellStyle(style);

                cell = row.createCell(2); //위치
                cell.setCellValue(ap_info.locationName);
                cell.setCellStyle(style);

                cell = row.createCell(3); //ap 유형
                cell.setCellValue(ap_info.ap_type);
                cell.setCellStyle(style);

                cell = row.createCell(4); //oui
                cell.setCellValue(ap_info.oui);
                cell.setCellStyle(style);

                cell = row.createCell(5); //ssid
                cell.setCellValue(ap_info.ssid);
                cell.setCellStyle(style);

                cell = row.createCell(6); //bssid
                cell.setCellValue(ap_info.bssid);
                cell.setCellStyle(style);

                cell = row.createCell(7); //channel
                cell.setCellValue(String.valueOf(ap_info.channel));
                cell.setCellStyle(style);

                cell = row.createCell(8); //power
                cell.setCellValue(String.valueOf(ap_info.signal));
                cell.setCellStyle(style);

                String enc = "";
                if (ap_info.encryption.equals("WPA_s")) {
                    enc = "WPA/WPA2";
                } else {
                    enc = ap_info.encryption;
                }

                cell = row.createCell(9); //enc
                cell.setCellValue(enc);
                cell.setCellStyle(style);

                cell = row.createCell(10); //policy type
                cell.setCellValue(policy_Type(ap_info.policyType));
                cell.setCellStyle(style);

                cell = row.createCell(11); //alert
                cell.setCellValue(ap_info.alert);
                cell.setCellStyle(style);

                String[] temp = ap_info.comment.split(" → ");

                //   cell = row.createCell(12); //command
                //   cell.setCellValue(temp[0]);
                //   cell.setCellStyle(style);

                String isGray = "";
                if ((ap_info.isGrayAp))
                    isGray = "O";
                else
                    isGray = "X";
                cell = row.createCell(12); //real ap
                cell.setCellValue(isGray);
                cell.setCellStyle(style);

                cell = row.createCell(13); //picture
                ImageValue(ap_info.picturePath, m_row, 13, m_row + 1, 14);
                cell.setCellStyle(style);

                cell = row.createCell(14); //ap info
                cell.setCellValue(ap_info.memo);
                cell.setCellStyle(style);

                String isAuth;
                if (ap_info.isAuthorized == 1)
                    isAuth = mContext.getResources().getString(R.string.authApMsg);
                else
                    isAuth = mContext.getResources().getString(R.string.unAuthApMsg);

                cell = row.createCell(15); //Authorized / UnAuthorized
                cell.setCellValue(isAuth);
                cell.setCellStyle(style);

            }
        }


        //sheet2 정책 설명
        if (isPolicy_iso || isPolicy_isms || isPolicy_norma) {
            sheet = workbook.createSheet(mContext.getResources().getString(R.string.string_sheet_policy_manual));
            //width 설정
            sheet.setColumnWidth(0, 6032);//컬럼 , 크기   1000 = 약 31px
            sheet.setColumnWidth(1, 9322);
            sheet.setColumnWidth(2, 9322);
            sheet.setColumnWidth(3, 18580);

            //제목
            //  sheet.addMergedRegion(new CellRangeAddress(3, 5, 2, 15)); //시작행, 끝행, 시작열,끝열
            row = sheet.createRow(0); //행
            cell = row.createCell(0);  //열
            row.setHeight((short) 720);//행높이 조절  1000 = 50
            style = workbook.createCellStyle();  //style 초기화
            font = workbook.createFont();
            font.setFontName("맑은 고딕");
            font.setFontHeightInPoints((short) 18); //글자크기
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);

            style.setFont(font);
            style.setAlignment((short) 1); //왼쪽정렬     가운데 = 2
            style.setVerticalAlignment((short) 1); //세로 정렬
            cell.setCellValue(mContext.getResources().getString(R.string.string_policy_title));
            cell.setCellStyle(style);

            //iso
            if (isPolicy_iso == true) {

                //iso 제목
                m_row2 = m_row2 + 3;
                row = sheet.createRow(m_row2); //행
                cell = row.createCell(0);  //열
                row.setHeight((short) 720);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 초기화
                font = workbook.createFont();
                font.setFontName("맑은 고딕");
                font.setFontHeightInPoints((short) 16); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);

                style.setFont(font);
                style.setAlignment((short) 1); //왼쪽정렬     가운데 = 2
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_iso_title));
                cell.setCellStyle(style);

                //iso 컬럼 제목
                m_row2 = m_row2 + 2;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 620);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 12); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_menual));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_info));
                cell.setCellStyle(style);

                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_iso_title));
                cell.setCellStyle(style);

                //iso data1
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 1800);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_1));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data2_1));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_1));
                cell.setCellStyle(style);

                //iso data2
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_2));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data2_2));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_defualt));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_2));
                cell.setCellStyle(style);

                //iso data3
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_info));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_3));
                cell.setCellStyle(style);

                //iso data4
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_hidden));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_4));
                cell.setCellStyle(style);

                //iso data5
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_softmac));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_5));
                cell.setCellStyle(style);

                //iso data6
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_open));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_6));
                cell.setCellStyle(style);

                //iso data7
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wep));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_7));
                cell.setCellStyle(style);

                //iso data8
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wpa));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_8));
                cell.setCellStyle(style);

                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 6, m_row2, 0, 0)); //병합 시작행, 끝행, 시작열,끝열
                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 6, m_row2, 1, 1)); //병합 시작행, 끝행, 시작열,끝열


                //iso data9
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_3));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data2_3));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_9));
                cell.setCellStyle(style);

                //iso data10
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_4));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data2_4));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_10));
                cell.setCellStyle(style);

            }

            //isms
            if (isPolicy_isms == true) {
                //isms 제목
                m_row2 = m_row2 + 3;
                row = sheet.createRow(m_row2); //행
                cell = row.createCell(0);  //열
                row.setHeight((short) 720);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 초기화
                font = workbook.createFont();
                font.setFontName("맑은 고딕");
                font.setFontHeightInPoints((short) 16); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);

                style.setFont(font);
                style.setAlignment((short) 1); //왼쪽정렬     가운데 = 2
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_isms_title));
                cell.setCellStyle(style);

                //isms 컬럼 제목
                m_row2 = m_row2 + 2;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 620);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 12); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_menual));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_info));
                cell.setCellStyle(style);

                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_Measures));
                cell.setCellStyle(style);

                //isms data1
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 1800);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data1_1));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_1));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_1));
                cell.setCellStyle(style);

                //isms data2
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data1_2));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_2));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_defualt));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_2));
                cell.setCellStyle(style);

                //isms data3
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_info));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_3));
                cell.setCellStyle(style);

                //isms data4
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_hidden));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_4));
                cell.setCellStyle(style);

                //isms data5
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_softmac));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_5));
                cell.setCellStyle(style);

                //isms data6
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_open));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_6));
                cell.setCellStyle(style);

                //isms data7
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wep));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_7));
                cell.setCellStyle(style);

                //isms data8
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wpa));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_8));
                cell.setCellStyle(style);

                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 6, m_row2, 0, 0)); //병합 시작행, 끝행, 시작열,끝열
                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 6, m_row2, 1, 1)); //병합 시작행, 끝행, 시작열,끝열


                //isms data9
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data1_3));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_3));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_9));
                cell.setCellStyle(style);

                //isms data10
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data1_4));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_4));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_10));
                cell.setCellStyle(style);

            }

            //norma
            if (isPolicy_norma == true) {
                //norma 제목
                m_row2 = m_row2 + 3;
                row = sheet.createRow(m_row2); //행
                cell = row.createCell(0);  //열
                row.setHeight((short) 720);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 초기화
                font = workbook.createFont();
                font.setFontName("맑은 고딕");
                font.setFontHeightInPoints((short) 16); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);

                style.setFont(font);
                style.setAlignment((short) 1); //왼쪽정렬     가운데 = 2
                style.setVerticalAlignment((short) 1); //세로 정렬
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_norma_title));
                cell.setCellStyle(style);

                //norma 컬럼 제목
                m_row2 = m_row2 + 2;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 620);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 12); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_menual));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_info));
                cell.setCellStyle(style);

                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.string_policy_Measures));
                cell.setCellStyle(style);

                //norma data1
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 1800);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_1));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data2_1));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_1));
                cell.setCellStyle(style);

                //norma data2
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_2));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_2));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_defualt));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_2));
                cell.setCellStyle(style);

                //norma data3
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_info));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_3));
                cell.setCellStyle(style);

                //norma data4
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_hidden));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_4));
                cell.setCellStyle(style);

                //norma data5
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_softmac));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_5));
                cell.setCellStyle(style);

                //norma data6
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_open));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_6));
                cell.setCellStyle(style);

                //norma data7
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wep));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_7));
                cell.setCellStyle(style);

                //norma data8
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wpa));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data3_8));
                cell.setCellStyle(style);

                //norma data9
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                //  cell.setCellValue("A13.1.2 네트워크 서비스 보안");
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                //cell.setCellValue("무선 통신망에서 구성되는 모든 서비스의 보안 매커니즘, 서비스 수준, 관리 요구 사항을 식별하고 네트워크 서비스 협약에 포함여 관리 가능");
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue(mContext.getResources().getString(R.string.policy_ssid_wps));
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_norma_wps));
                cell.setCellStyle(style);

                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 7, m_row2, 0, 0)); //병합 시작행, 끝행, 시작열,끝열
                sheet.addMergedRegion(new CellRangeAddress(m_row2 - 7, m_row2, 1, 1)); //병합 시작행, 끝행, 시작열,끝열


                //norma data10
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_3));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_3));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_9));
                cell.setCellStyle(style);

                //norma data11
                m_row2 = m_row2 + 1;
                row = sheet.createRow(m_row2); //행
                row.setHeight((short) 2320);//행높이 조절  1000 = 50
                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 2); //글씨 가운데 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);

                cell = row.createCell(0); //정책
                cell.setCellValue(mContext.getResources().getString(R.string.policy_iso_data1_4));
                cell.setCellStyle(style);

                cell = row.createCell(1); //정책 설명
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data2_4));
                cell.setCellStyle(style);

                cell = row.createCell(2); //세부 사항
                cell.setCellValue("");
                cell.setCellStyle(style);

                style = workbook.createCellStyle();  //style 생성자
                font = workbook.createFont();
                font.setFontHeightInPoints((short) 11); //글자크기
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
                font.setFontName("맑은 고딕");
                style.setWrapText(true);  //줄바꿈
                style.setFont(font);
                style.setAlignment((short) 1); //글씨 왼쪽 정렬
                style.setVerticalAlignment((short) 1); //세로 정렬
                style.setBorderBottom(CellStyle.BORDER_THIN);  //테두리 두껍게
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                cell = row.createCell(3); //조치 방안
                cell.setCellValue(mContext.getResources().getString(R.string.policy_isms_data3_10));
                cell.setCellStyle(style);

            }
        }

        /**
         *
         * 아래는 저장
         */


        //파일 저장

        if (fileNameList.size() == 1) {
            filename = fileNameList.get(0).replaceAll(".txt", "");
        } else {
            filename = today() + "_" + mContext.getResources().getString(R.string.string_Integrated_report);
        }

        File output = new File("/sdcard/Norma/Excel/" + filename + ".xlsx");

        if (output.exists()) {
            //  file.delete();
            //중복
            int count = 1;
            boolean isFile = true;
            while (isFile) {
                String num = String.format("%1$02d", count);
                File tempfile = new File("/sdcard/Norma/Excel/" + filename + num + ".xlsx");
                if (tempfile.exists()) {
                    count++;
                } else {
                    isFile = false;
                    filename = filename + num;
                    Log.e("filename", filename);
                }
            }
        }
        outpath = "/sdcard/Norma/Excel/" + filename + ".xlsx";
        File file = new File(outpath);
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(file);
            workbook.write(fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null)
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

        }

        return 1;
    }


    //이미지 적용 - 그래프
    private int ImageValue(Bitmap bitmap, int row1, int col1, int row2, int col2) {
        try {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            // byte[] bytes = IOUtils.toByteArray(is);
            int pictureIdx = workbook.addPicture(stream.toByteArray(), workbook.PICTURE_TYPE_PNG);
            //  Log.e("stream", String.valueOf(stream.size()));
            stream.close();
            // is.close();

            CreationHelper helper = workbook.getCreationHelper();
            //Creates the top-level drawing patriarch.
            try {
                Drawing drawing = sheet.createDrawingPatriarch();
                ClientAnchor anchor = helper.createClientAnchor();

                //create an anchor with upper left cell _and_ bottom right cell
                anchor.setRow1(row1); //Row 1
                anchor.setCol1(col1); //Column A
                anchor.setRow2(row2); //Row 2
                anchor.setCol2(col2); //Column B

                //Creates a picture
                Picture pict = drawing.createPicture(anchor, pictureIdx);

            } catch (Exception e) {
                Log.e("error", String.valueOf(e));
                return -1;
            }
            //Create an anchor that is attached to the worksheet


        } catch (Exception e) {
            Log.e("error", String.valueOf(e));
        }

        return 0;
    }

    // 이미지 적용 -  사진
    private int ImageValue(String path, int row1, int col1, int row2, int col2) {
        FileInputStream is;


        try {
            File file = new File(path);
            if (!file.exists()) {
                AssetFileDescriptor fileDescriptor = mContext.getAssets().openFd("img/no_image_found.png");
                is = fileDescriptor.createInputStream();
            } else
                is = new FileInputStream(path);

            int pictureIdx = workbook.addPicture(IOUtils.toByteArray(is), workbook.PICTURE_TYPE_PNG);
            is.close();


            CreationHelper helper = workbook.getCreationHelper();
            //Creates the top-level drawing patriarch.
            try {
                Drawing drawing = sheet.createDrawingPatriarch();
                ClientAnchor anchor = helper.createClientAnchor();

                //create an anchor with upper left cell _and_ bottom right cell
                anchor.setRow1(row1); //Row 1
                anchor.setCol1(col1); //Column A
                anchor.setRow2(row2); //Row 2
                anchor.setCol2(col2); //Column B

                //Creates a picture
                Picture pict = drawing.createPicture(anchor, pictureIdx);

            } catch (Exception e) {
                Log.e("error", String.valueOf(e));
                return -1;
            }
            //Create an anchor that is attached to the worksheet


        } catch (Exception e) {
            Log.e("error", String.valueOf(e));
        }

        return 0;
    }

    //gray ap 판단 로직
    private boolean isGrayAp(String ssid) {
        if (ssid.toUpperCase().contains("LG U+ ROUTER") || ssid.toUpperCase().contains("MYLGNET") || ssid.toUpperCase().contains("U+ZONE")
                || ssid.toUpperCase().contains("U+NET") || ssid.toUpperCase().contains("KT_WLAN") || ssid.toUpperCase().contains("POCKETFI2740")
                || ssid.toUpperCase().contains("NESPOT") || ssid.toUpperCase().contains("CNMDATA6028") || ssid.toUpperCase().contains("SO070VOIP")
                || ssid.toUpperCase().contains("cjwifi") || ssid.toUpperCase().contains("HELLOWIRELESS") || ssid.toUpperCase().contains("SK_WIFI")
                || ssid.toUpperCase().contains("T_WIFI"))
            return true;


        return false;
    }

    public void getFileNameList(ArrayList<String> fileNameList) {
        this.fileNameList = fileNameList;
    }


    private String policy_Type(int type) {

        String data;

        switch (type) {
            case 0:
                data = "none";
                break;
            case 1:
                data = "K-ISMS";
                break;
            case 2:
                data = "ISO 27001";
                break;
            case 3:
                data = "NORMA";
                break;
            default:
                data = "none";
                break;
        }
        return data;
    }


    public String getPath() {
        return outpath;
    }

    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String result = year + "_" + month + "_" + day;

        return result;
    }
}
