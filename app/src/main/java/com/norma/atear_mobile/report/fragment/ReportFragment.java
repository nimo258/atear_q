package com.norma.atear_mobile.report.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.report.ReportActivity;
import com.norma.atear_mobile.report.graph.AlertGraph;
import com.norma.atear_mobile.report.graph.ChannelGraph;
import com.norma.atear_mobile.report.graph.GraphInfo;
import com.norma.atear_mobile.report.graph.VerticalBarGraph;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyojin on 8/1/16.
 */
public class ReportFragment extends Fragment {

    private View v;
    private Context mContext;

    private Button btn_totalAp;
    private Button btn_authorized;
    private Button btn_unAuthorized;

    private LinearLayout layout_totalAuth;
    private LinearLayout layout_to_channel;
    private LinearLayout layout_to_enc;
    private LinearLayout layout_alert;

    private LinearLayout layout_auth_ssid;
    private LinearLayout layout_auth_enc;
    private LinearLayout layout_authMiddle;


    private LinearLayout layout_unAuthEnc;
    private LinearLayout layout_unAuthHidden;

    private ArrayList<Ap_info> apList;
    //authorized
    private int authAp = 0;   //인가 AP;
    private int unAuthAp = 0; //비인가 AP;
    //enc
    private int toOPEN = 0;
    private int toWEP = 0;
    private int toWPA = 0;
    private int toWPA2 = 0;
    private int toWPA_s = 0;

    private int authOPEN = 0;
    private int authWEP = 0;
    private int authWPA = 0;
    private int authWPA2 = 0;
    private int authWPA_s = 0;

    private int unAuthOPEN = 0;
    private int unAuthWEP = 0;
    private int unAuthWPA = 0;
    private int unAuthWPA2 = 0;
    private int unAuthWPA_s = 0;
    //ssid

    private int authSpecificSsid = 0;
    private int authdefaultSsid = 0;
    private int authSoftAp = 0;
    private int authHiddenSsid = 0;

    private int unAuthHiddenSsid = 0;
    private int unAuthGrayAp = 0;

    //위험도
    private int high = 0;
    private int middle = 0;
    private int safe = 0;
    private int alert_default;

    private static final int HIGH = 4;
    private static final int MIDDLE = 8;
    private static final int SAFE = 10;


    private ArrayList<Integer> total_chList;   //채널 리스트
    private ArrayList<Integer> total_encList;   //  open = 0  ,  wep  = 1   ,  wpa  = 2    , wpa2 = 3   ,  wpa/wpa2  = 4;
    private ArrayList<Integer> total_alert;

    List<GraphInfo> auth_isSSidList;
    private ArrayList<Integer> auth_encList;
    private ArrayList<String> auth_ssid;

    private ArrayList<Integer> unAuth_encList;
    private int unAuthHidden = 0;

    private AtEarDB atEarDB;
    private SharedPref sharedPref;

    // risk
    // 중복 mac    10 0x02 2

    // 정책 위험   100 0x04 4
    // 정책 중간  1000 0x08 8
    // 정책 안전 10000 0x10 16

    // softMac  100000 0x20 32
    // 접속 기록 1000000 0x40 64

    ListView lv_unAuth;


    public ReportFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        v = inflater.inflate(R.layout.fragment_report, container, false);

        findView();

        SetLanguage();

        UploadThread uploadThread = new UploadThread();
        uploadThread.execute();


        btn_totalAp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (layout_totalAuth.getVisibility() == View.VISIBLE) {
                    layout_totalAuth.setVisibility(View.GONE);
                    layout_to_channel.setVisibility(View.GONE);
                    layout_to_enc.setVisibility(View.GONE);
                    layout_alert.setVisibility(View.GONE);

                    btn_totalAp.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_totalAuth.setVisibility(View.VISIBLE);
                    layout_to_channel.setVisibility(View.VISIBLE);
                    layout_to_enc.setVisibility(View.VISIBLE);
                    layout_alert.setVisibility(View.VISIBLE);

                    btn_totalAp.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        btn_authorized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout_auth_ssid.getVisibility() == View.VISIBLE) {
                    layout_auth_ssid.setVisibility(View.GONE);
                    layout_auth_enc.setVisibility(View.GONE);
                    layout_authMiddle.setVisibility(View.GONE);

                    btn_authorized.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_auth_ssid.setVisibility(View.VISIBLE);
                    layout_auth_enc.setVisibility(View.VISIBLE);
                    layout_authMiddle.setVisibility(View.VISIBLE);

                    btn_authorized.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        btn_unAuthorized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout_unAuthEnc.getVisibility() == View.VISIBLE) {
                    layout_unAuthEnc.setVisibility(View.GONE);
                    layout_unAuthHidden.setVisibility(View.GONE);

                    btn_unAuthorized.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_unAuthEnc.setVisibility(View.VISIBLE);
                    layout_unAuthHidden.setVisibility(View.VISIBLE);

                    btn_unAuthorized.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        return v;
    }


    private void findView() {

        mContext = getContext();

        btn_totalAp = (Button) v.findViewById(R.id.btn_totalAp);
        btn_authorized = (Button) v.findViewById(R.id.btn_authorized);
        btn_unAuthorized = (Button) v.findViewById(R.id.btn_unAuthorized);

        layout_totalAuth = (LinearLayout) v.findViewById(R.id.layout_totalAuth);
        layout_to_channel = (LinearLayout) v.findViewById(R.id.layout_to_channel);
        layout_to_enc = (LinearLayout) v.findViewById(R.id.layout_to_enc);
        layout_alert = (LinearLayout) v.findViewById(R.id.layout_alert);
        layout_auth_ssid = (LinearLayout) v.findViewById(R.id.layout_auth_ssid);
        layout_auth_enc = (LinearLayout) v.findViewById(R.id.layout_auth_enc);
        layout_authMiddle = (LinearLayout) v.findViewById(R.id.layout_authMiddle);
        layout_unAuthEnc = (LinearLayout) v.findViewById(R.id.layout_unAuthEnc);
        layout_unAuthHidden = (LinearLayout) v.findViewById(R.id.layout_unAuthHidden);

        lv_unAuth = (ListView) v.findViewById(R.id.lv_unAuth);

        atEarDB = new AtEarDB(mContext);

        sharedPref = new SharedPref(mContext);
    }

    private void SetLanguage() {
        btn_totalAp.setText(mContext.getResources().getString(R.string.btnTotalApMsg));
        btn_authorized.setText(mContext.getResources().getString(R.string.btnAuthApMsg));
        btn_unAuthorized.setText(mContext.getResources().getString(R.string.btnUnAuthApMsg));
    }


    class UploadThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setView();
        }
    }


    private void getData() {
        toOPEN = ((ReportActivity) ReportActivity.mContext).toOPEN();
        toWEP = ((ReportActivity) ReportActivity.mContext).toWEP();
        toWPA = ((ReportActivity) ReportActivity.mContext).toWPA();
        toWPA2 = ((ReportActivity) ReportActivity.mContext).toWPA2();
        toWPA_s = ((ReportActivity) ReportActivity.mContext).toWPA_s();
        authOPEN = ((ReportActivity) ReportActivity.mContext).authOPEN();
        authWEP = ((ReportActivity) ReportActivity.mContext).authWEP();
        authWPA = ((ReportActivity) ReportActivity.mContext).authWPA();
        authWPA2 = ((ReportActivity) ReportActivity.mContext).authWPA2();
        authWPA_s = ((ReportActivity) ReportActivity.mContext).authWPA_s();
        authSoftAp = ((ReportActivity) ReportActivity.mContext).authSoftAp();
        authHiddenSsid = ((ReportActivity) ReportActivity.mContext).authHiddenSsid();
        unAuthWEP = ((ReportActivity) ReportActivity.mContext).unAuthWEP();
        unAuthWPA = ((ReportActivity) ReportActivity.mContext).unAuthWPA();
        unAuthWPA2 = ((ReportActivity) ReportActivity.mContext).unAuthWPA2();
        unAuthWPA_s = ((ReportActivity) ReportActivity.mContext).unAuthWPA_s();
        unAuthHiddenSsid = ((ReportActivity) ReportActivity.mContext).unAuthHiddenSsid();
        unAuthGrayAp = ((ReportActivity) ReportActivity.mContext).unAuthGrayAp();
        unAuthHiddenSsid = ((ReportActivity) ReportActivity.mContext).unAuthHiddenSsid();
        authAp = ((ReportActivity) ReportActivity.mContext).authAp();
        unAuthAp = ((ReportActivity) ReportActivity.mContext).unAuthAp();
        total_chList = ((ReportActivity) ReportActivity.mContext).chList();
        auth_isSSidList = ((ReportActivity) ReportActivity.mContext).auth_isSSidList();
        high = ((ReportActivity) ReportActivity.mContext).high();
        middle = ((ReportActivity) ReportActivity.mContext).middle();
        safe = ((ReportActivity) ReportActivity.mContext).safe();
        unAuthOPEN = ((ReportActivity) ReportActivity.mContext).unAuthOPEN();
    }


    private void setView() {
        //total ap
        List<GraphInfo> totalAuthList = new ArrayList<>();
        if (authAp != 0) {
            totalAuthList.add(new GraphInfo(authAp, "#FFFFFF", mContext.getResources().getString(R.string.authApMsg)));
        }


        if (unAuthAp != 0) {
            totalAuthList.add(new GraphInfo(unAuthAp, "#000000", mContext.getResources().getString(R.string.unAuthApMsg)));
        }


        AlertGraph totalAuthGraph = new AlertGraph(mContext, totalAuthList, btn_totalAp.getWidth());
        totalAuthGraph.setTitle(mContext.getResources().getString(R.string.authApMsg));
        layout_totalAuth.addView(totalAuthGraph);

        //total channel
        ChannelGraph totalChannelList = new ChannelGraph(mContext, btn_totalAp.getWidth());
        for (int i : total_chList) {
            totalChannelList.getData(i);
        }
        totalChannelList.sort();
        totalChannelList.setTitle(mContext.getResources().getString(R.string.chMsg));
        totalChannelList.initData();
        layout_to_channel.addView(totalChannelList);


        //total enc
        List<GraphInfo> totalEncList = new ArrayList<>();
        if (toOPEN != 0) {
            totalEncList.add(new GraphInfo(toOPEN, "#FFFFFF", "OPEN"));
        }
        if (toWEP != 0) {
            totalEncList.add(new GraphInfo(toWEP, "#FF0000", "WEP"));
        }
        if (toWPA != 0) {
            totalEncList.add(new GraphInfo(toWPA, "#00FF00", "WPA"));
        }
        if (toWPA2 != 0) {
            totalEncList.add(new GraphInfo(toWPA2, "#AAAA00", "WPA2"));
        }
        if (toWPA_s != 0) {
            totalEncList.add(new GraphInfo(toWPA_s, "#AA00AA", "WPA/WPA2"));
        }
        AlertGraph totalEncGraph = new AlertGraph(mContext, totalEncList, btn_totalAp.getWidth());
        totalEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
        layout_to_enc.addView(totalEncGraph);


        //total alert  AP
        List<GraphInfo> totalAlertList = new ArrayList<>();
        totalAlertList.add(new GraphInfo(high, "#FFFFFF", mContext.getResources().getString(R.string.highMsg)));
        totalAlertList.add(new GraphInfo(middle, "#FF0000", mContext.getResources().getString(R.string.middleMsg)));
        totalAlertList.add(new GraphInfo(safe, "#00FF00", mContext.getResources().getString(R.string.safeMsg)));

        VerticalBarGraph totalAlertGraph = new VerticalBarGraph(mContext, totalAlertList, btn_totalAp.getWidth());
        totalAlertGraph.setTitle(mContext.getResources().getString(R.string.riskTitleMsg));
        layout_alert.addView(totalAlertGraph);

        //authorized AP
        //인가 ssid
        if (auth_isSSidList.size() == 0) {
            auth_isSSidList.add(new GraphInfo(unAuthAp, "#000000", "noData"));
        }
        AlertGraph authSsidGraph = new AlertGraph(mContext, auth_isSSidList, btn_authorized.getWidth());
        authSsidGraph.setStateDuplicate(true);  //중복제거 데이터 모아줌
        authSsidGraph.setTitle(mContext.getResources().getString(R.string.authSsidMsg));
        authSsidGraph.AUTH_SSID = true;
        layout_auth_ssid.addView(authSsidGraph);


        //인가 enc
        List<GraphInfo> authEncList = new ArrayList<>();
        if (authOPEN != 0) {
            authEncList.add(new GraphInfo(authOPEN, "#FFFFFF", "OPEN"));
        }
        if (authWEP != 0) {
            authEncList.add(new GraphInfo(authWEP, "#FF0000", "WEP"));
        }
        if (authWPA != 0) {
            authEncList.add(new GraphInfo(authWPA, "#00FF00", "WPA"));
        }
        if (authWPA2 != 0) {
            authEncList.add(new GraphInfo(authWPA2, "#AAAA00", "WPA2"));
        }
        if (authWPA_s != 0) {
            authEncList.add(new GraphInfo(authWPA_s, "#AA00AA", "WPA/WPA2"));
        }
        if (authEncList.size() == 0) {
            authEncList.add(new GraphInfo(unAuthAp, "#FFFFFF", "noData"));
        }
        AlertGraph authEncGraph = new AlertGraph(mContext, authEncList, btn_authorized.getWidth());
        authEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
        layout_auth_enc.addView(authEncGraph);


        //안전도 중 하에 따른 세부분류
        List<GraphInfo> authMiddleList = new ArrayList<>();
        //  authMiddleList.add(new GraphInfo(0, "#FFFFFF", defaultSsidMsg));
        authMiddleList.add(new GraphInfo(authSoftAp, "#FF0000", "Soft AP"));
        authMiddleList.add(new GraphInfo(authWEP, "#00FF00", "WEP"));
        //    authMiddleList.add(new GraphInfo(0, "#FF00FF", specificSsidMsg));
        authMiddleList.add(new GraphInfo(authHiddenSsid, "#AA0000", mContext.getResources().getString(R.string.hiddlenSsidMsg)));
        authMiddleList.add(new GraphInfo(authOPEN, "#FFAA00", "OPEN"));
        authMiddleList.add(new GraphInfo(authWPA, "#00FFFF", "WPA"));

        VerticalBarGraph authMiddleGraph = new VerticalBarGraph(mContext, authMiddleList, btn_authorized.getWidth());
        authMiddleGraph.setTitle(mContext.getResources().getString(R.string.notSafeMsg));
        layout_authMiddle.addView(authMiddleGraph);

        //unAuthorized AP
        //unAuth enc
        List<GraphInfo> unAuthEncList = new ArrayList<>();
        if (unAuthOPEN != 0) {
            unAuthEncList.add(new GraphInfo(unAuthOPEN, "#FFFFFF", "OPEN"));
        }
        if (unAuthWEP != 0) {
            unAuthEncList.add(new GraphInfo(unAuthWEP, "#FF0000", "WEP"));
        }
        if (unAuthWPA != 0) {
            unAuthEncList.add(new GraphInfo(unAuthWPA, "#00FF00", "WPA"));
        }
        if (unAuthWPA2 != 0) {
            unAuthEncList.add(new GraphInfo(unAuthWPA2, "#AAAA00", "WPA2"));
        }
        if (unAuthWPA_s != 0) {
            unAuthEncList.add(new GraphInfo(unAuthWPA_s, "#AA00AA", "WPA/WPA2"));
        }

        AlertGraph unAuthEncGraph = new AlertGraph(mContext, unAuthEncList, btn_unAuthorized.getWidth());
        unAuthEncGraph.setTitle(mContext.getResources().getString(R.string.TypeApMsg));
        layout_unAuthEnc.addView(unAuthEncGraph);

        // unAuth hiiden , gray
        List<GraphInfo> unAuthHiddenList = new ArrayList<>();
        unAuthHiddenList.add(new GraphInfo(unAuthHiddenSsid, "#FFFFFF", mContext.getResources().getString(R.string.hiddlenSsidMsg)));
        unAuthHiddenList.add(new GraphInfo(unAuthGrayAp, "#FF0000", mContext.getResources().getString(R.string.mobileProviderMsg)));

        VerticalBarGraph unAuthHiddenGraph = new VerticalBarGraph(mContext, unAuthHiddenList, btn_unAuthorized.getWidth());
        unAuthHiddenGraph.setTitle(mContext.getResources().getString(R.string.hiddenProvidearApMsg));
        layout_unAuthHidden.addView(unAuthHiddenGraph);
    }


}
