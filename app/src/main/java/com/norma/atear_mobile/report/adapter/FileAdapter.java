package com.norma.atear_mobile.report.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.report.data.FileInfo;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/18/16.
 */
public class FileAdapter extends BaseAdapter {


    private ViewHolder holder;
    private Context mContext;


    private ArrayList<FileInfo> fileList;
    SharedPref sharedPref;
    int language = 0;

//    String dialogTitleMsg = "";
//    String yesMsg = "";
//    String noMsg = "";


    public FileAdapter(Context mContext, ArrayList<FileInfo> list) {
        this.mContext = mContext;
        this.fileList = list;
        sharedPref = new SharedPref(mContext);
        language = sharedPref.getValue("language", 0);

    //TODO setLanguage 안쓰이고있음..
//        SetLanguage();
    }

    @Override
    public int getCount() {
        return fileList.size();
    }

    @Override
    public Object getItem(int position) {
        return fileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.file_item, parent, false);
            holder.iv_file = (ImageView) convertView.findViewById(R.id.iv_file);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final FileInfo data = fileList.get(position);


        holder.tv_name.setText(data.name);

        //TODO 현재 안쓰는 영역
//        String dialogMsg = "";
//        String ToastMsg = "";
//
//        switch (language) {
//            case 0: //영어
//                dialogMsg = " Delete this " + data.name + "?";
//                ToastMsg = data.name + " was removed.";
//                break;
//
//            case 1: //한국어
//                dialogMsg = data.name + "을 삭제 하시겠습니까?";
//                ToastMsg = data.name + "은 삭제되었습니다.";
//                mContext.getResources().getStringArray(R.array.adt_title_array)
//                break;
//
//            case 2:  //일본어
//                dialogMsg = data.name + "\nを削除しますか?";
//                ToastMsg = data.name + " は削除されました。";
//                break;
//
//            case 3:  //중국어
//                dialogMsg = "是否删除（" + data.name + "）";
//                ToastMsg = "已删除（" + data.name + "）";
//                break;
//
//        }

        data.alpha = (BitmapDrawable) holder.iv_file.getBackground();

        if (data.check == true) {
            data.alpha.mutate().setAlpha(50);//반투명
        } else {
            data.alpha.setAlpha(0xff);
        }

        holder.iv_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!data.check) {// not choice
                    data.check = true;
                    data.alpha.mutate().setAlpha(50);//반투명
                    notifyDataSetChanged();
                    //  Toast.makeText(mContext, "true:" + data.name, Toast.LENGTH_SHORT).show();

                } else { //choice

                    data.check = false;
                    data.alpha.setAlpha(0xff);
                    notifyDataSetChanged();
                    //  Toast.makeText(mContext, "false:" + data.name, Toast.LENGTH_SHORT).show();
                }


            }
        });
        return convertView;
    }


    private class ViewHolder {

        public ImageView iv_file;

        public TextView tv_name;


    }

//    private void SetLanguage() {
//
//        int language = sharedPref.getValue("language", 0);
//
//        switch (language) {
//            case 0: //영어
//                dialogTitleMsg = "File Delete";
//                yesMsg = "Yes";
//                noMsg = "No";
//                break;
//
//            case 1: //한국어
//                dialogTitleMsg = "File Delete";
//                yesMsg = "Yes";
//                noMsg = "No";
//                break;
//
//            case 2:  //일본어
//                dialogTitleMsg = "レポートファイル削除";
//                yesMsg = "削除";
//                noMsg = "キャンセル";
//                break;
//            case 3: //중국어
//                dialogTitleMsg = "删除文件";
//                yesMsg = "是";
//                noMsg = "否";
//                break;
//
//        }
//
//    }

    public void setData(ArrayList<FileInfo> fileList) {
        this.fileList = fileList;
    }




}