package com.norma.atear_mobile.report.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.report.ReportActivity;
import com.norma.atear_mobile.report.graph.AlertGraph;
import com.norma.atear_mobile.report.graph.ChannelGraph;
import com.norma.atear_mobile.report.graph.GraphInfo;
import com.norma.atear_mobile.report.graph.TotalAPGraph;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hyojin on 8/3/16.
 *
 *  12월 5일 warning ap 클릭 이벤트 추가
 */
public class DashBoardFragment extends Fragment {

    private View v;

    private Button btn_Detail;
    private Button btn_statics;
    private Button btn_channel;
    private Button btn_alert;
    private Button btn_back;

    private LinearLayout layout_statics;
    private LinearLayout layout_channel;
    private LinearLayout layout_alert;

    private AtEarDB atEarDB;
    private Context mContext;
    private SharedPref sharedPref;

    private int total_AP = 0;
    private int warning_AP = 0;
    private int gray_AP = 0;
    private int not_real_AP = 0;
    private int alert_high = 0;
    private int alert_middle = 0;
    private int alert_safety = 0;

    private ArrayList<Integer> chList;

    public DashBoardFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        v = inflater.inflate(R.layout.fragment_dash_board, container, false);

        findView();
        SetLanguage();

        UploadThread uploadThread = new UploadThread();
        uploadThread.execute();

        btn_statics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout_statics.getVisibility() == View.VISIBLE) {
                    layout_statics.setVisibility(View.GONE);
                    btn_statics.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_statics.setVisibility(View.VISIBLE);
                    //   totalBitmap.recycle();
                    btn_statics.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        btn_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout_channel.getVisibility() == View.VISIBLE) {
                    layout_channel.setVisibility(View.GONE);
                    btn_channel.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_channel.setVisibility(View.VISIBLE);
                    //   channelBitmap.recycle();
                    btn_channel.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        btn_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout_alert.getVisibility() == View.VISIBLE) {
                    layout_alert.setVisibility(View.GONE);
                    btn_alert.setBackgroundResource(R.drawable.btn_bar);
                } else {
                    layout_alert.setVisibility(View.VISIBLE);
                    //  alertBitmap.recycle();
                    btn_alert.setBackgroundResource(R.drawable.btn_bar_hover);
                }
            }
        });

        //warning ap 보기
        layout_statics.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ((ReportActivity) ReportActivity.mContext).mViewPager.setCurrentItem(2); //detail탭으로 이동
                ReportActivity.isWarning = true; //Tag
                return false;
            }
        });

        return v;

    }

   /* @Override
    public void onPause() {

        alertBitmap.recycle();
        channelBitmap.recycle();
        totalBitmap.recycle();

        alertBitmap = null;
        channelBitmap = null;
        totalBitmap = null;
    }*/

    private void findView() {
        mContext = getContext();

        btn_back = (Button) v.findViewById(R.id.btn_back);
        //  btn_Detail = (Button) v.findViewById(R.id.btn_Detail);
        btn_statics = (Button) v.findViewById(R.id.btn_statics);
        btn_channel = (Button) v.findViewById(R.id.btn_channel);
        btn_alert = (Button) v.findViewById(R.id.btn_alert);

        layout_statics = (LinearLayout) v.findViewById(R.id.layout_statics);
        layout_channel = (LinearLayout) v.findViewById(R.id.layout_channel);
        layout_alert = (LinearLayout) v.findViewById(R.id.layout_alert);

        atEarDB = new AtEarDB(mContext);

        sharedPref = new SharedPref(mContext);
    }

    private void SetLanguage() {
        btn_statics.setText(mContext.getResources().getString(R.string.apStaticsTitleMsg));
        btn_channel.setText(mContext.getResources().getString(R.string.chTitleMsg));
        btn_alert.setText(mContext.getResources().getString(R.string.riskTitleMsg));
    }


    private void getData() {


        total_AP = ((ReportActivity) ReportActivity.mContext).total_AP();
        Log.e("totalAP_SIZE", String.valueOf(total_AP));

        warning_AP = ((ReportActivity) ReportActivity.mContext).warning_AP();
        gray_AP = ((ReportActivity) ReportActivity.mContext).gray_AP();
        not_real_AP = ((ReportActivity) ReportActivity.mContext).not_real_AP();
        chList = ((ReportActivity) ReportActivity.mContext).chList();
        alert_high = ((ReportActivity) ReportActivity.mContext).alert_high();
        alert_middle = ((ReportActivity) ReportActivity.mContext).alert_middle();
        alert_safety = ((ReportActivity) ReportActivity.mContext).alert_safety();

    }

    class UploadThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setView();
        }
    }


    private void setView() {


        TotalAPGraph totalAPGraph = new TotalAPGraph(mContext, total_AP, warning_AP, gray_AP, not_real_AP, btn_statics.getWidth());
        layout_statics.addView(totalAPGraph);

        //channel Graph
        ChannelGraph channelGraph = new ChannelGraph(mContext, btn_channel.getWidth());
        Log.e("channelSize:::", String.valueOf(chList.size()));
        for (int i : chList) {
            channelGraph.getData(i);
        }
        channelGraph.sort();
        channelGraph.initData();
        layout_channel.addView(channelGraph);

        //alert Graph
        List<GraphInfo> arrCircleGraph = new ArrayList<>();

        if (alert_high != 0) {
            arrCircleGraph.add(new GraphInfo(alert_high, "#f9c93c", mContext.getResources().getString(R.string.highMsg)));
        }
        if (alert_middle != 0) {
            arrCircleGraph.add(new GraphInfo(alert_middle, "#36b7f0", mContext.getResources().getString(R.string.middleMsg)));
        }
        if (alert_safety != 0) {
            arrCircleGraph.add(new GraphInfo(alert_safety, "#55de6a", mContext.getResources().getString(R.string.safeMsg)));
        }
        AlertGraph alertGraph = new AlertGraph(v.getContext(), arrCircleGraph, btn_alert.getWidth());
        layout_alert.addView(alertGraph);
    }

    private static void recycleBitmap(View view) {
        Drawable d = view.getBackground();
        if (d instanceof BitmapDrawable) {
            Bitmap b = ((BitmapDrawable) d).getBitmap();
            b.recycle();
        } // 현재로서는 BitmapDrawable 이외의 drawable 들에 대한 직접적인 메모리 해제는 불가능하다.

        d.setCallback(null);
    }


}