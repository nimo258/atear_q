package com.norma.atear_mobile.report.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by hyojin on 8/2/16.
 */
public class ChannelGraph extends View {
    Canvas cv;

    public boolean isPDF = false;

    float default_width = 1296;
    float default_height = 2000;

    float width, height;
    float width_scale, height_scale;
    float density;

    float top_margin, left_margin;
    float title_size, data_size, text_size, bar_height;

    String title = "";

    int[] data = new int[15];
    int[] data2 = new int[15];

    int size = 0;   //데이터 총 갯수

    boolean state_sort = false;
    boolean state_title = false;

    Paint pnt_graph, pnt_data, pnt_ch, pnt_line, pnt_title;

    int largest_data = 0;

    public ChannelGraph(Context context, LinearLayout linearLayout) {
        super(context);

        ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        this.width = lp.width;
        this.height = lp.height;
    }

    public ChannelGraph(Context context, int width) {
        super(context);

        //ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        //this.width = lp.width;
        //this.height = lp.height;
        this.width = width;
        this.height = (float) (width * 1.54);


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // width 진짜 크기 구하기
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = 0;
        switch (widthMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                widthSize = 100;
                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        // height 진짜 크기 구하기
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = 0;


        switch (heightMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                heightSize = (int) (widthSize * 1.54);

                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                heightSize = (int) (widthSize * 1.54);

                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                heightSize = (int) (widthSize * 1.54); //MeasureSpec.getSize(heightMeasureSpec);

                break;
        }
        //Log.e("heightSize", String.valueOf(heightSize));

        widthSize = (int) width;
        heightSize = (int) height;

        if (!title.equals("")) {
            heightSize = heightSize + 200;
        }

        setMeasuredDimension(widthSize, heightSize);
    }


    public void initData(){
        for (int i = 0; i < 14; i++)
            data2[i] = i;

        if (state_sort)
            for (int i = 0; i < 14; i++)
                for (int j = 0; j < 13; j++)
                    if (data[j] < data[j + 1]) {
                        int temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;

                        temp = data2[j];
                        data2[j] = data2[j + 1];
                        data2[j + 1] = temp;
                    }

        for (int i = 0; i < 14; i++)
            if (data[i] > largest_data)
                largest_data = data[i];


    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.cv = canvas;

     //   int largest_data = 0;

        // if(isPDF == false) {
        cv.drawColor(Color.parseColor("#152a3c"));  //배경색
        //  }else{
        //      cv.drawColor(0xFFFFFFFF);
        //  }

        setParameter();
        setPaint();
//largest_data <= 48 && largest_data >= 18
        //bar_height = 1;
        if (largest_data <= 14)
            bar_height = 2.5f; //20
        else if (largest_data > 14 && largest_data <= 38)
            bar_height = 1; //50
        else if (largest_data > 38 && largest_data <= 88)
            bar_height = 0.5f; //100
        else if (largest_data > 88 && largest_data <= 238)
            bar_height = 0.2f; //250
        else if (largest_data > 238 && largest_data <= 488)
            bar_height = 0.1f;// 500
        else if (largest_data > 488 && largest_data <= 978)
            bar_height = 0.05f;  //1000
        else if(largest_data > 978 && largest_data <= 3000){
            bar_height = 0.01f;//5000
        }else if(largest_data > 3000 && largest_data <= 10000){
            bar_height = 0.005f;
        }else { //25000개
            bar_height = 0.002f;
            Log.e("argestdata", String.valueOf(largest_data));
        }


        //else 49~98
        //가장 큰 데이터값 기준으로 세로선 맞춤

        for (int i = 0; i < 14; i++)
            showGraph(i, data2[i], data[i]); //14ch이상은 0으로 지정

        showLine();

        if (state_title)
            showTitle();
    }

    public void showGraph(int ch, int index, int data) {
        String text_ch;

        if (index == 0)
            text_ch = "802.11ac";
        else
            text_ch = "ch" + index;

        float width_text_ch = pnt_ch.measureText(text_ch);  //글자 폭

        cv.drawRect(left_margin, top_margin + ch * 120 * density, left_margin + data * bar_height * 17, top_margin + ch * 120 * density + 60 * density, pnt_graph);
        cv.drawText(text_ch, 180 * density - width_text_ch / 2.0f, top_margin + ch * 120 * density + 30 * density + text_size * 1.75f / 5.0f, pnt_ch);
        cv.drawText(String.valueOf(data), left_margin + data * bar_height * 17 + 20 * density, top_margin + ch * 120 * density + 30 * density + data_size * 1.75f / 5.0f, pnt_data);
    }

    public void showLine() {
        for (int i = 0; i <= 5; i++) {
            cv.drawRect(left_margin + i * 10 * 17 * density, top_margin - 30 * density,
                    left_margin + i * 10 * 17 * density + 2 * density, top_margin + 13 * 120 * density + 100 * density, pnt_line);

            cv.drawText(String.valueOf((int) (i * 10 / bar_height)),
                    left_margin + i * 10 * 17 * density - pnt_line.measureText(String.valueOf((int) (i * 10 / bar_height))) / 2.0f,
                    top_margin + 13 * 120 * density + 160 * density, pnt_line);
        }

        cv.drawText("Total Count : " + size, left_margin + 17 * 17 * density - pnt_line.measureText("Total Count : " + size) / 2.0f, top_margin + 13 * 120 * density  + 250 * density, pnt_ch);
    }

    public void setTitle(String title) {
        this.title = title;
        state_title = true;
        default_height += 200;
    }

    public void getData(int ch) {
        if (ch >= 14)
            data[0]++;  //14ch이상은 0으로 지정
        else
            data[ch]++;

        size++;
    }

    public void sort() {
        this.state_sort = true;
    }

    private void setParameter() {
        width_scale = width / default_width;
        height_scale = height / default_height;

        density = (float) Math.sqrt(width_scale * height_scale);

        if (state_title)
            top_margin = 320 * density;
        else
            top_margin = 120 * density;

        left_margin = 320 * density;

        title_size = 80 * density;
        data_size = 45 * density;
        text_size = 50 * density;
        bar_height = 0.5f * density;  //막대 그래프 길이 비율
    }

    private void setPaint() {
        pnt_graph = new Paint();
        pnt_graph.setAntiAlias(true);
        pnt_graph.setColor(Color.parseColor("#2eabc9"));

        pnt_data = new Paint();
        pnt_data.setColor(Color.WHITE);
        pnt_data.setTextSize(data_size);

        pnt_ch = new Paint();
        pnt_ch.setColor(Color.parseColor("#7cb2dc"));
        pnt_ch.setTextSize(text_size);

        pnt_line = new Paint();
        pnt_line.setAntiAlias(true);
        pnt_line.setColor(Color.parseColor("#385c7a"));
        pnt_line.setTextSize(data_size);

        pnt_title = new Paint();
        pnt_title.setAntiAlias(true);
        pnt_title.setColor(Color.parseColor("#FFFFFF"));
        pnt_title.setTextSize(title_size);
    }

    private void showTitle() {
        cv.drawText(title, width / 2 - pnt_title.measureText(title) / 2, top_margin / 2, pnt_title);
    }

    public Bitmap getBitmap(){
        this.measure(View.MeasureSpec.makeMeasureSpec((int) this.width, View.MeasureSpec.EXACTLY), //가로폭
                View.MeasureSpec.makeMeasureSpec((int) this.height, View.MeasureSpec.EXACTLY)); //세로폭

        this.layout(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight());
        this.buildDrawingCache();

        Bitmap bit = this.getDrawingCache();


        return bit;
    }
}