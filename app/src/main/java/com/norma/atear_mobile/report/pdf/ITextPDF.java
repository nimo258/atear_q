package com.norma.atear_mobile.report.pdf;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.policy.Policy_Result;
import com.norma.atear_mobile.project.policy.Policy_info;
import com.norma.atear_mobile.report.ReportActivity;
import com.norma.atear_mobile.report.graph.AlertGraph;
import com.norma.atear_mobile.report.graph.ChannelGraph;
import com.norma.atear_mobile.report.graph.GraphInfo;
import com.norma.atear_mobile.report.graph.TotalAPGraph;
import com.norma.atear_mobile.report.graph.VerticalBarGraph;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by hyojin on 8/16/16.
 */
public class ITextPDF {

    public UploadPDF uploadPDF;

    private ArrayList<Ap_info> apList;

    public CreatePDFThread createPDFthread;

    private Context mContext;
    private SharedPref sharedPref;
    private AtEarDB atEarDB;

    private int total_AP = 0;
    private int warning_AP = 0;
    private int gray_AP = 0;
    private int not_real_AP = 0;
    private int alert_high = 0;
    private int alert_middle = 0;
    private int alert_safety = 0;

    //authorized
    private int authAp = 0;   //인가 AP;
    private int unAuthAp = 0; //비인가 AP;
    //enc
    private int toOPEN = 0;
    private int toWEP = 0;
    private int toWPA = 0;
    private int toWPA2 = 0;
    private int toWPA_s = 0;

    private int authOPEN = 0;
    private int authWEP = 0;
    private int authWPA = 0;
    private int authWPA2 = 0;
    private int authWPA_s = 0;

    private int unAuthOPEN = 0;
    private int unAuthWEP = 0;
    private int unAuthWPA = 0;
    private int unAuthWPA2 = 0;
    private int unAuthWPA_s = 0;
    //ssid
    private int authHiddenSsid = 0;
    private int authSpecificSsid = 0;
    private int authdefaultSsid = 0;
    private int authSoftAp = 0;


    private int unAuthHiddenSsid = 0;
    private int unAuthGrayAp = 0;

    private ArrayList<Integer> total_chList;   //채널 리스트
    java.util.List<GraphInfo> auth_isSSidList;
    //위험도
    private int high = 0;
    private int middle = 0;
    private int safe = 0;

    private ArrayList<Integer> chList;

    private String proName = "";
    private String loName = "";
    private String outpath = "/sdcard/PDFTest.pdf";

    private ArrayList<Bitmap> DashBoardList;
    private ArrayList<Bitmap> ReportList_total;
    private ArrayList<Bitmap> ReportList_auth;
    private ArrayList<Bitmap> ReportList_unAuth;

    private static final String REPORT_TABLE = "report_table";
    private static final String SSID = "ssid";
    private static final String AUTHORIZED = "isAuthorized";

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font whiteSubFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.WHITE);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static BaseFont NanumBarunGothicFont;
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    private static Font smallNomal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    String dashBoardTitleMsg = "";
    String reportTitleMsg = "";
    String detailsTitleMsg = "";
    String totalMsg = "";

    String apStaticsTitleMsg = "";
    String chTitleMsg = "";
    String riskTitleMsg = "";

    String highMsg = "";
    String middleMsg = "";
    String safeMsg = "";


    String authApMsg = "";
    String unAuthApMsg = "";
    String chMsg = "";
    String authSsidMsg = "";
    String TypeApMsg = "";
    String hiddenProvidearApMsg = "";
    String notSafeMsg = "";
    String defaultSsidMsg = "";
    String specificSsidMsg = "";
    String hiddlenSsidMsg = "";

    String detaileTitleMsg = "";
    String authTitleMsg = "";
    String filterSSidTitleMsg = "";
    String pictureTitleMsg = "";
    String pentestTitleMsg = "";
    String duplicateTitleMsg = "";

    String proTitleMsg = "";
    String loTitleMsg = "";
    String saveTimeTitleMsg = "";
    String gpsTitleMsg = "";
    String signalTitleMsg = "";
    String detailchTitleMsg = "";
    String encTitleMsg = "";
    String scanTimeTitleMsg = "";
    String apTypeTitleMsg = "";
    String ouiTitleMsg = "";
    String commentTitleMsg = "";
    String alertTitleMsg = "";
    String polictTitleMsg = "";
    String detailpentestTitleMsg = "";
    String memoTitleMsg = "";
    String authMsg = "";

    int language = 0;

    public ITextPDF(Context mContext, String fileName) {
        super();

        this.mContext = mContext;
        sharedPref = new SharedPref(mContext);

        SetLanguage();

        outpath = "/sdcard/Norma/PDF/" + fileName;
        atEarDB = new AtEarDB(mContext);


        try {
            if (language == 2 || language == 3)
                NanumBarunGothicFont = BaseFont.createFont("assets/fonts/simsun.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            else
                NanumBarunGothicFont = BaseFont.createFont("assets/fonts/NanumBarunGothic.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            smallBold = new Font(NanumBarunGothicFont, 12, Font.BOLD);
            subFont = new Font(NanumBarunGothicFont, 16);
            catFont = new Font(NanumBarunGothicFont, 18, Font.BOLD);
            whiteSubFont = new Font(NanumBarunGothicFont, 16, Font.BOLD, BaseColor.WHITE);
            smallNomal = new Font(NanumBarunGothicFont, 12, Font.NORMAL);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        apList = UploadApList();

        uploadPDF = new UploadPDF();
        // this.DashBoardList = DashBoardList;
    }

    public void CreatePDF() {
        Document document = new Document(PageSize.A4.rotate(), 10f, 10f, 100f, 0f);

        File dir = new File("/sdcard/Norma/PDF");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //output file path
        File file = new File(outpath + ".pdf");

        if (file.exists()) {
            //  file.delete();
            //중복
            int count = 1;
            boolean isFile = true;
            while (isFile) {

                String num = String.format("%1$02d", count);
                file = new File(outpath + num + ".pdf");
                if (file.exists()) {
                    count++;
                } else {
                    isFile = false;
                    outpath = outpath + num;
                    Log.e("filename", outpath);
                }

            }
        }

        try {
            PdfWriter.getInstance(document, new FileOutputStream(outpath + ".pdf"));
            document.open();
            addMetaData(document);

            try {
                addData(document);
                //   addContent(document);
            } catch (Exception e) {
                Log.e("PdfError", String.valueOf(e));
            }
            document.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private static void addMetaData(Document document) {
        document.addTitle("Norma PDF");

    }

    private void addData(Document document)
            throws DocumentException, IOException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        //Main
        preface.add(new Paragraph("   Atear Report", catFont));
        addEmptyLine(preface, 5);
        document.add(preface);

        document.setMargins(10, 10, 10, 10); //next pages margins


        Rectangle pageSize = new Rectangle(216, 720); //배경색
        pageSize.setBackgroundColor(new BaseColor(0xFF, 0xFF, 0XDE)); //now page color
        document.add(pageSize);
        //dash board
        document.newPage();

        pageSize = new Rectangle(1000, 1000);
        pageSize.setBackgroundColor(new BaseColor(21, 42, 60));
        document.add(pageSize);


        document.add(new Paragraph(dashBoardTitleMsg, whiteSubFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);

        document.add(createDashBoardTable(DashBoardList)); //dash board table


        //report
        ////total
        document.newPage();

        pageSize = new Rectangle(1000, 1000);
        pageSize.setBackgroundColor(new BaseColor(21, 42, 60));
        document.add(pageSize);

        document.add(new Paragraph(totalMsg, whiteSubFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);

        document.add(createReportTable_total(ReportList_total)); //dash board table

        ////auth
        document.newPage();

        pageSize = new Rectangle(1000, 1000);
        pageSize.setBackgroundColor(new BaseColor(21, 42, 60));
        document.add(pageSize);

        document.add(new Paragraph(authApMsg, whiteSubFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);

        document.add(createReportTable_auth(ReportList_auth)); //dash board table

        ////auth
        document.newPage();

        pageSize = new Rectangle(1000, 1000);
        pageSize.setBackgroundColor(new BaseColor(21, 42, 60));
        document.add(pageSize);

        document.add(new Paragraph(unAuthApMsg, whiteSubFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);

        document.add(createReportTable_unAuth(ReportList_unAuth)); //dash board table


        //details
        //auth
        document.newPage();
        //addEmptyLine(preface, 5);
        document.add(new Paragraph(authApMsg, subFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);
        document.add(createDetilsTable(1));  //auth table

        //unAUth
        document.newPage();
        //addEmptyLine(preface, 5);
        document.add(new Paragraph(unAuthApMsg, subFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);
        document.add(createDetilsTable(0));  //imAItj table

        //Pantest
        document.newPage();
        //addEmptyLine(preface, 5);
        document.add(new Paragraph(pentestTitleMsg, subFont));
        preface = new Paragraph();
        addEmptyLine(preface, 1);
        document.add(preface);
        document.add(createPantestTable());  //Pantesting table*/
    }

    private PdfPTable createReportTable_unAuth(ArrayList<Bitmap> list)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);

        table.setWidthPercentage(100f);
        //  table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        for (Bitmap bitmap : list) {

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);
                image.scaleAbsolute(bitmap.getWidth() / 3, bitmap.getHeight() / 3);  //image size

                PdfPCell c1 = new PdfPCell();
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.NO_BORDER);
                c1.setImage(image);
                table.addCell(c1);
            } catch (Exception e) {

            }
        }


        return table;

    }

    private PdfPTable createReportTable_auth(ArrayList<Bitmap> list)
            throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        table.setWidthPercentage(100f);
        //  table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        for (Bitmap bitmap : list) {

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);
                image.scaleAbsolute(bitmap.getWidth() / 4, bitmap.getHeight() / 4);  //image size

                PdfPCell c1 = new PdfPCell();
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.NO_BORDER);
                c1.setImage(image);
                table.addCell(c1);


            } catch (Exception e) {

            }
        }


        return table;

    }

    private PdfPTable createReportTable_total(ArrayList<Bitmap> list)
            throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        table.setWidthPercentage(100f);
        //  table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        int count = 0;
        for (Bitmap bitmap : list) {

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);
                image.scaleAbsolute(bitmap.getWidth() / 3, bitmap.getHeight() / 3);  //image size

                PdfPCell c1 = new PdfPCell();
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.NO_BORDER);
                c1.setImage(image);
                if (count == 1 || count == 2) {
                    c1.setRowspan(2);
                }
                table.addCell(c1);
                count++;
            } catch (Exception e) {

            }
        }


        return table;

    }

    private PdfPTable createDashBoardTable(ArrayList<Bitmap> list)
            throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        table.setWidthPercentage(100f);
        //  table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        for (Bitmap bitmap : list) {

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);
                image.scaleAbsolute(bitmap.getWidth() / 3, bitmap.getHeight() / 3);  //image size

                PdfPCell c1 = new PdfPCell();
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(Rectangle.NO_BORDER);
                c1.setImage(image);
                table.addCell(c1);
            } catch (Exception e) {

            }
        }


        return table;

    }

    private PdfPTable createPantestTable()
            throws DocumentException {


        PdfPTable table = new PdfPTable(10);

        table.setWidthPercentage(100f);
        // table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

        BaseColor baseColor = new BaseColor(255, 255, 0); //색갈

        PdfPCell c1 = new PdfPCell(new Phrase(proTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(loTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(apTypeTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(scanTimeTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("bssid", smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("ssid", smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(encTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(authMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(detailpentestTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Password", smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        for (Ap_info ap_info : apList) {
            if (ap_info.isPentest != 0) {
                c1 = new PdfPCell(new Phrase(ap_info.projectName, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.locationName, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.ap_type, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.scan_time, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.bssid, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.ssid, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                if (ap_info.encryption.equals("WPA/WPA2")) {
                    ap_info.encryption = "WPA2";
                } else if (ap_info.encryption.contains("WPA_s")) {
                    ap_info.encryption = "WPA2";
                }
                c1 = new PdfPCell(new Phrase(ap_info.encryption, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);


                String authorzied = "";
                if (ap_info.isAuthorized == 0) {
                    authorzied = unAuthApMsg;
                } else {
                    authorzied = authApMsg;
                }

                c1 = new PdfPCell(new Phrase(authorzied, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                String isPentest = "";
                if (ap_info.isPentest == 1) {
                    isPentest = "Failed";
                } else {
                    isPentest = "Success";
                }

                c1 = new PdfPCell(new Phrase(isPentest, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);

                c1 = new PdfPCell(new Phrase(ap_info.pentest_password, smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);
            }

        }


        return table;

    }


    private PdfPTable createDetilsTable(int type)  // -1 = all   , 0 = unAuth   , 1 = Auth
            throws DocumentException {


        PdfPTable table = new PdfPTable(11);

        table.setWidthPercentage(100f);
        // table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

        BaseColor baseColor = new BaseColor(255, 255, 0); //색갈

        PdfPCell c1 = new PdfPCell(new Phrase(proTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(loTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(apTypeTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(scanTimeTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("bssid", smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("ssid", smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(detailchTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(signalTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(encTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(polictTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase(alertTitleMsg, smallBold));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBackgroundColor(baseColor);
        table.addCell(c1);

        int count = 0;
        for (Ap_info ap_info : apList) {
            if (type == 1 && ap_info.isAuthorized != 1) {
                continue;
            } else if (type == 0 && ap_info.isAuthorized != 0) {
                continue;
            }

            if (ap_info.ssid_filter == 1) {
                continue;
            }

            c1 = new PdfPCell(new Phrase(ap_info.projectName, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.locationName, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.ap_type, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.scan_time, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.bssid, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.ssid, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(String.valueOf(ap_info.channel), smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(String.valueOf(ap_info.signal), smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);


            if (ap_info.encryption.equals("WPA/WPA2")) {
                ap_info.encryption = "WPA2";
            } else if (ap_info.encryption.contains("WPA_s")) {
                ap_info.encryption = "WPA2";
            }
            c1 = new PdfPCell(new Phrase(ap_info.encryption, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(policy_Type(ap_info.policyType), smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(ap_info.alert, smallNomal));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            count = 1;
        }
        if (count == 0) {
            for (int j = 0; j < 11; j++) {

                c1 = new PdfPCell(new Phrase("none", smallNomal));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);
            }


        }

        return table;

    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }


    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String result = year + "_" + month + "_" + day;

        return result;
    }

    //통신사 AP
    private ArrayList<String> GrayAP() {

        ArrayList<String> list = new ArrayList<>();

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select " + SSID + " from " + REPORT_TABLE + " where " + SSID + " LIKE " +
                "'KT_WLAN%' or " + SSID + " LIKE 'LG U+ Router%' or " + SSID + " LIKE '%mylgnet%' or "
                + SSID + " LIKE '%U+zone%' or " + SSID + " LIKE '%U+net%' or " + SSID + " LIKE '%PocketFi2740%' or " + SSID + " LIKE '%nespot%' or "
                + SSID + " LIKE '%CNMDATA6028%' or " + SSID + " LIKE '%SO070VOIP%' or " + SSID + " LIKE 'cjwifi%' or " + SSID + " LIKE 'HelloWireless%' or "
                + SSID + " LIKE '%SK_wifi%' or " + SSID + " LIKE '%T_wifi%'";


        Cursor c = db.rawQuery(sql, null);


        if (c.getCount() != 0) {
            while (c.moveToNext()) {

                String ssid = c.getString(0);

                boolean filter = false; // 필터링 여부
                if (ReportActivity.SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }
                    }
                }
                // ssid 필터
                if (filter == true) {
                    continue;
                }


                list.add(ssid);
            }
        } else {

        }
        c.close();
        db.close();
        return list;
    }

    //통신사 AP
    private int unAuthGrayAP() {

        ArrayList<String> list = new ArrayList<>();

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select " + SSID + " , " + AUTHORIZED + " from " + REPORT_TABLE + " where " + SSID + " LIKE " +
                "'KT_WLAN%' or " + SSID + " LIKE 'LG U+ Router%' or " + SSID + " LIKE '%mylgnet%' or "
                + SSID + " LIKE '%U+zone%' or " + SSID + " LIKE '%U+net%' or " + SSID + " LIKE '%PocketFi2740%' or " + SSID + " LIKE '%nespot%' or "
                + SSID + " LIKE '%CNMDATA6028%' or " + SSID + " LIKE '%SO070VOIP%' or " + SSID + " LIKE 'cjwifi%' or " + SSID + " LIKE 'HelloWireless%' or "
                + SSID + " LIKE '%SK_wifi%' or " + SSID + " LIKE '%T_wifi%'";


        Cursor c = db.rawQuery(sql, null);


        if (c.getCount() != 0) {
            while (c.moveToNext()) {

                String ssid = c.getString(0);
                int authorized = c.getInt(1);
                boolean filter = false; // 필터링 여부
                if (ReportActivity.SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }
                    }
                }
                // ssid 필터
                if (filter == true) {
                    continue;
                }

                if (authorized == 0) {
                    list.add(ssid);
                }
            }
        } else {

        }
        c.close();
        db.close();
        return list.size();
    }

    public class UploadPDF extends AsyncTask<Void, Void, Void> {

        java.util.List<GraphInfo> totalEncList;

        // int[] count = new int[50];
        // List<String> APname = new ArrayList<>();

        ArrayList<GraphInfo> graphInfoList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total_AP = 0;
            warning_AP = 0;
            gray_AP = 0;
            not_real_AP = 0;
            alert_high = 0;
            alert_middle = 0;
            alert_safety = 0;

            total_chList = new ArrayList<>();

            totalEncList = new ArrayList<>();

            graphInfoList = new ArrayList<>();

            auth_isSSidList = new ArrayList<>();

            toOPEN = 0;
            toWEP = 0;
            toWPA = 0;
            toWPA2 = 0;
            toWPA_s = 0;

            authOPEN = 0;
            authWEP = 0;
            authWPA = 0;
            authWPA2 = 0;
            authWPA_s = 0;

            unAuthOPEN = 0;
            unAuthWEP = 0;
            unAuthWPA = 0;
            unAuthWPA2 = 0;
            unAuthWPA_s = 0;

            authHiddenSsid = 0;
            authSpecificSsid = 0;
            authdefaultSsid = 0;
            authSoftAp = 0;

            unAuthHiddenSsid = 0;
            unAuthGrayAp = 0;

            high = 0;
            middle = 0;
            safe = 0;

            chList = new ArrayList<>();
            DashBoardList = new ArrayList<>();
            ReportList_total = new ArrayList<>();
            ReportList_auth = new ArrayList<>();
            ReportList_unAuth = new ArrayList<>();

        }

        @Override
        protected Void doInBackground(Void... params) {

            for (Ap_info ap_info : apList) {

                boolean filter = false; // 필터링 여부
                if (ReportActivity.SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ap_info.ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }

                    }
                }
                // ssid 필터
                if (filter == true) {
                    ap_info.ssid_filter = 1;
                    continue;
                }


                //warning
                if (ap_info.encryption.equals("OPEN") || ap_info.encryption.equals("WEP")) {
                    warning_AP++;
                }
                //not real ap
                if (ap_info.ap_type.equals("Soft AP")) {
                    not_real_AP++;
                }
                //alert setting

        /*        Policy_info policy_info = new Policy_info();

                if (ap_info.encryption.equals("WPA_s")) {
                    ap_info.encryption = "wpa2";
                }


                if (ap_info.wps == 0) {
                    policy_info.wps = "off";
                } else {
                    policy_info.wps = "on";
                }
                */


                if (ReportActivity.SettingFilter == true) {
                    Policy_info policy_info = new Policy_info();

                    if (ap_info.encryption.equals("WPA/WPA2")) {
                        policy_info.enc = "wpa2";
                        ap_info.encryption = "wpa2";
                    } else if (ap_info.encryption.contains("WPA2")) {
                        policy_info.enc = "wpa2";
                    } else if (ap_info.encryption.contains("WPA")) {
                        policy_info.enc = "wpa";
                    } else if (ap_info.encryption.contains("WEP")) {
                        policy_info.enc = "wep";
                    } else if (ap_info.encryption.contains("WPA_s")) {
                        policy_info.enc = "wpa2";
                        ap_info.encryption = "wpa2";
                    } else {
                        policy_info.enc = "open";
                    }

                    if (ap_info.ap_type.equals("Soft AP")) {
                        policy_info.type = "Soft AP";
                    } else {
                        policy_info.type = "AP";
                    }

                    if (ap_info.ssid.equals("(Hidden)")) {
                        policy_info.ssid = "hidden ssid";
                    }

                    if (atEarDB.SSID_TABLE_SELECT(ap_info.ssid)) {
                        policy_info.ssid = "authorized ssid";
                    } else {  //일단 다 노말 나중에 default  스패셜 추가해야함 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        policy_info.ssid = "default ssid";
                    }

                    if (ap_info.isAuthorized == 1) {
                        ap_info.isAuthorized = 1;
                        policy_info.authorized = "1";
                        policy_info.ssid = "authorized ssid";
                    } else {
                        ap_info.isAuthorized = 0;
                        policy_info.authorized = "0";
                    }

                    if (atEarDB.SINGLE_BSSID_TABLE_SELECT(ap_info.bssid)) {
                        ap_info.isAuthorized = 1;
                        policy_info.authorized = "1";
                        policy_info.ssid = "authorized ssid";
                    }

                    ap_info.policyType = ReportActivity.norma_policy.POLICY;

                    final Policy_Result policy_result;

                    switch (ap_info.policyType) {

                        case 0:   //none
                            policy_result = ReportActivity.norma_policy.nonData();

                            ap_info.alert = policy_result.alert;
                            ap_info.comment = policy_result.comment;

                            break;

                        case 1:    //isms
                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력

                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;
                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";
                            }
                            break;

                        case 2:    //iso
                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력
                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;

                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";

                            }
                            break;

                        case 3:    //norma
                            if (ap_info.wps == 0) {
                                policy_info.wps = "off";
                            } else {
                                policy_info.wps = "on";
                            }

                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력
                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;
                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";
                            }

                            break;
                    }

                }
                //total
                if (ap_info.isAuthorized == 1) {
                    authAp++;
                } else {
                    unAuthAp++;
                }

                //alert
                switch (ap_info.alert) {
                    case "Safe":
                        alert_safety++;
                        break;
                    case "Middle":
                        alert_middle++;
                        break;
                    default:
                        alert_high++;
                        break;

                }
                //channel
                chList.add(ap_info.channel);

                //total ap
                total_AP++;


                // total_AP = apList.size();

                ArrayList<String> grayAp = GrayAP();
                gray_AP = grayAp.size();


                total_chList.add(ap_info.channel);

                int enc = 0;

                switch (ap_info.encryption) {
                    case "OPEN":
                        toOPEN++;
                        if (ap_info.isAuthorized == 1) {
                            authOPEN++;
                        } else {
                            unAuthOPEN++;
                        }
                        break;
                    case "WEP":
                        toWEP++;
                        if (ap_info.isAuthorized == 1) {
                            authWEP++;
                        } else {
                            unAuthWEP++;
                        }
                        break;
                    case "WPA":
                        toWPA++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA++;
                        } else {
                            unAuthWPA++;
                        }
                        break;
                    case "WPA2":
                        toWPA2++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA2++;
                        } else {
                            unAuthWPA2++;
                        }
                        break;
                    default:
                        toWPA_s++;
                        if (ap_info.isAuthorized == 1) {
                            authWPA_s++;
                        } else {
                            unAuthWPA_s++;
                        }
                        break;

                }

                switch (ap_info.alert) {
                    case "Middle":
                        middle++;
                        break;
                    case "Safe":
                        safe++;
                        break;
                    default:
                        high++;
                        break;
                }

                //인가
                if (ap_info.isAuthorized == 1) {
                    auth_isSSidList.add(new GraphInfo(ap_info.ssid));

                    if (ap_info.ssid.equals(("(Hidden)"))) {
                        authHiddenSsid++;
                    }
                } else {
                    if (ap_info.ssid.equals(("(Hidden)"))) {
                        unAuthHiddenSsid++;
                    }
                }
            }
            unAuthGrayAp = unAuthGrayAP();

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ////////dash board

            TotalAPGraph totalAPGraph = new TotalAPGraph(mContext, total_AP, warning_AP, gray_AP, not_real_AP, 600);
            totalAPGraph.isPDF = true;
            totalAPGraph.setTitle(apStaticsTitleMsg);
            DashBoardList.add(totalAPGraph.getBitmap());

            //alert Graph
            java.util.List<GraphInfo> arrCircleGraph = new ArrayList<>();

            if (alert_high != 0) {
                arrCircleGraph.add(new GraphInfo(alert_high, "#f9c93c", highMsg));
            }
            if (alert_middle != 0) {
                arrCircleGraph.add(new GraphInfo(alert_middle, "#36b7f0", middleMsg));
            }
            if (alert_safety != 0) {
                arrCircleGraph.add(new GraphInfo(alert_safety, "#55de6a", safeMsg));
            }
            AlertGraph alertGraph = new AlertGraph(mContext, arrCircleGraph, 400);
            alertGraph.isPDF = true;
            alertGraph.setTitle(riskTitleMsg);
            DashBoardList.add(alertGraph.getBitmap());

            //channel Graph
            ChannelGraph channelGraph = new ChannelGraph(mContext, 1400);
            channelGraph.isPDF = true;
            channelGraph.sort();
            channelGraph.setTitle(chTitleMsg);
            for (int i : chList) {
                channelGraph.getData(i);
            }
            DashBoardList.add(channelGraph.getBitmap());


            ///////report

            //total ap
            java.util.List<GraphInfo> totalAuthList = new ArrayList<>();
            if (authAp != 0) {
                totalAuthList.add(new GraphInfo(authAp, "#FFFFFF", authApMsg));
            }
            if (unAuthAp != 0) {
                totalAuthList.add(new GraphInfo(unAuthAp, "#000000", unAuthApMsg));
            }
            AlertGraph totalAuthGraph = new AlertGraph(mContext, totalAuthList, 400);
            totalAuthGraph.setTitle(authApMsg);
            ReportList_total.add(totalAuthGraph.getBitmap());

            //total alert  AP
            java.util.List<GraphInfo> totalAlertList = new ArrayList<>();
            totalAlertList.add(new GraphInfo(high, "#FFFFFF", highMsg));
            totalAlertList.add(new GraphInfo(middle, "#FF0000", middleMsg));
            totalAlertList.add(new GraphInfo(safe, "#00FF00", safeMsg));

            VerticalBarGraph totalAlertGraph = new VerticalBarGraph(mContext, totalAlertList, 400);
            totalAlertGraph.setTitle(riskTitleMsg);
            ReportList_total.add(totalAlertGraph.getBitmap());


            //total channel
            ChannelGraph totalChannelList = new ChannelGraph(mContext, 1400);
            for (int i : total_chList) {
                totalChannelList.getData(i);
            }
            totalChannelList.sort();
            totalChannelList.setTitle(chMsg);
            ReportList_total.add(totalChannelList.getBitmap());


            //total enc
            java.util.List<GraphInfo> totalEncList = new ArrayList<>();
            if (toOPEN != 0) {
                totalEncList.add(new GraphInfo(toOPEN, "#FFFFFF", "OPEN"));
            }
            if (toWEP != 0) {
                totalEncList.add(new GraphInfo(toWEP, "#FF0000", "WEP"));
            }
            if (toWPA != 0) {
                totalEncList.add(new GraphInfo(toWPA, "#00FF00", "WPA"));
            }
            if (toWPA2 != 0) {
                totalEncList.add(new GraphInfo(toWPA2, "#AAAA00", "WPA2"));
            }
            if (toWPA_s != 0) {
                totalEncList.add(new GraphInfo(toWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            AlertGraph totalEncGraph = new AlertGraph(mContext, totalEncList, 400);
            totalEncGraph.setTitle(TypeApMsg);
            ReportList_total.add(totalEncGraph.getBitmap());


            //authorized AP
            //인가 ssid
            if (auth_isSSidList.size() == 0) {
                auth_isSSidList.add(new GraphInfo(unAuthAp, "#000000", unAuthApMsg));
            }
            AlertGraph authSsidGraph = new AlertGraph(mContext, auth_isSSidList, 400);
            authSsidGraph.setStateDuplicate(true);  //중복제거 데이터 모아줌
            authSsidGraph.setTitle(authSsidMsg);
            authSsidGraph.AUTH_SSID = true;
            ReportList_auth.add(authSsidGraph.getBitmap());


            //인가 enc
            java.util.List<GraphInfo> authEncList = new ArrayList<>();
            if (authOPEN != 0) {
                authEncList.add(new GraphInfo(authOPEN, "#FFFFFF", "OPEN"));
            }
            if (authWEP != 0) {
                authEncList.add(new GraphInfo(authWEP, "#FF0000", "WEP"));
            }
            if (authWPA != 0) {
                authEncList.add(new GraphInfo(authWPA, "#00FF00", "WPA"));
            }
            if (authWPA2 != 0) {
                authEncList.add(new GraphInfo(authWPA2, "#AAAA00", "WPA2"));
            }
            if (authWPA_s != 0) {
                authEncList.add(new GraphInfo(authWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            if (authEncList.size() == 0) {
                authEncList.add(new GraphInfo(1, "#FFFFFF", "noData"));
            }
            AlertGraph authEncGraph = new AlertGraph(mContext, authEncList, 400);
            authEncGraph.setTitle(TypeApMsg);
            ReportList_auth.add(authEncGraph.getBitmap());

            //안전도 중 하에 따른 세부분류
            java.util.List<GraphInfo> authMiddleList = new ArrayList<>();
            authMiddleList.add(new GraphInfo(0, "#FFFFFF", defaultSsidMsg));
            authMiddleList.add(new GraphInfo(authSoftAp, "#FF0000", "Soft AP"));
            authMiddleList.add(new GraphInfo(authWEP, "#00FF00", "WEP"));
            authMiddleList.add(new GraphInfo(0, "#FF00FF", specificSsidMsg));
            authMiddleList.add(new GraphInfo(authHiddenSsid, "#AA0000", hiddlenSsidMsg));
            authMiddleList.add(new GraphInfo(authOPEN, "#FFAA00", "OPEN"));
            authMiddleList.add(new GraphInfo(authWPA, "#00FFFF", "WPA"));

            VerticalBarGraph authMiddleGraph = new VerticalBarGraph(mContext, authMiddleList, 400);
            authMiddleGraph.setTitle(notSafeMsg);
            ReportList_auth.add(authMiddleGraph.getBitmap());


            //unAuthorized AP
            //unAuth enc
            java.util.List<GraphInfo> unAuthEncList = new ArrayList<>();
            if (unAuthOPEN != 0) {
                unAuthEncList.add(new GraphInfo(unAuthOPEN, "#FFFFFF", "OPEN"));
            }
            if (unAuthWEP != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWEP, "#FF0000", "WEP"));
            }
            if (unAuthWPA != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA, "#00FF00", "WPA"));
            }
            if (unAuthWPA2 != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA2, "#AAAA00", "WPA2"));
            }
            if (unAuthWPA_s != 0) {
                unAuthEncList.add(new GraphInfo(unAuthWPA_s, "#AA00AA", "WPA/WPA2"));
            }
            if (authEncList.size() == 0) {
                unAuthEncList.add(new GraphInfo(1, "#FFFFFF", "noData"));
            }
            AlertGraph unAuthEncGraph = new AlertGraph(mContext, unAuthEncList, 400);
            unAuthEncGraph.setTitle(TypeApMsg);
            ReportList_unAuth.add(unAuthEncGraph.getBitmap());

            // unAuth hiiden , gray

            java.util.List<GraphInfo> unAuthHiddenList = new ArrayList<>();
            unAuthHiddenList.add(new GraphInfo(unAuthHiddenSsid, "#FFFFFF", hiddlenSsidMsg));
            unAuthHiddenList.add(new GraphInfo(unAuthGrayAp, "#FF0000", "Mobile Provider"));

            VerticalBarGraph unAuthHiddenGraph = new VerticalBarGraph(mContext, unAuthHiddenList, 800);
            unAuthHiddenGraph.setTitle(hiddenProvidearApMsg);
            ReportList_unAuth.add(unAuthHiddenGraph.getBitmap());


            //  CreatePDF();
            createPDFthread = new CreatePDFThread();
            createPDFthread.execute();

        }
    }

    public class CreatePDFThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            CreatePDF();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    private ArrayList<Ap_info> UploadApList() {

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select * from " + REPORT_TABLE;

        ArrayList<Ap_info> list = new ArrayList<>();

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {
                try {
                    Ap_info ap_info = new Ap_info();
                    ap_info.projectName = c.getString(0);
                    ap_info.locationName = c.getString(1);
                    ap_info.save_time = c.getString(2);
                    ap_info.gps = c.getString(3);
                    ap_info.bssid = c.getString(4);
                    ap_info.ssid = c.getString(5);
                    ap_info.signal = c.getInt(6);
                    ap_info.channel = c.getInt(7);
                    ap_info.encryption = c.getString(8);
                    ap_info.cipher = c.getString(9);
                    ap_info.auth = c.getString(10);
                    ap_info.scan_time = c.getString(11);
                    ap_info.ap_type = c.getString(12);
                    ap_info.oui = c.getString(13);
                    ap_info.comment = c.getString(14);
                    ap_info.alert = c.getString(15);
                    ap_info.risk = c.getInt(16);
                    ap_info.wps = c.getInt(17);
                    ap_info.isAuthorized = c.getInt(18);
                    ap_info.ssid_filter = c.getInt(19);
                    ap_info.bssid_filter = c.getInt(20);
                    ap_info.policyType = c.getInt(21);
                    ap_info.picturePath = c.getString(22);
                    ap_info.memo = c.getString(23);
                    ap_info.isPentest = c.getInt(24);
                    ap_info.pentest_password = c.getString(25);
                    //모든데이터
                    list.add(ap_info);


                } catch (Exception e) {
                    Log.e("json error", "file save");
                }


            } //while end
        }
        c.close();
        db.close();

        return list;
    }

    public String getPath() {
        return outpath;
    }

    private String policy_Type(int type) {

        String data;

        switch (type) {
            case 0:
                data = "none";
                break;
            case 1:
                data = "ISMS";
                break;
            case 2:
                data = "ISO 27001";
                break;
            case 3:
                data = "NORMA";
                break;
            default:
                data = "none";
                break;

        }
        return data;
    }

    private void SetLanguage() {

        language = sharedPref.getValue("language", 0);

        switch (language) {
            case 0: //영어
                dashBoardTitleMsg = "Dash Board";
                reportTitleMsg = "Report";
                detailsTitleMsg = "Details";
                totalMsg = "Total";

                apStaticsTitleMsg = "AP STATICS";
                chTitleMsg = "CHANNEL OF AP";
                riskTitleMsg = "AP Distrbution by Risk Degree";

                highMsg = "High";
                middleMsg = "Middle";
                safeMsg = "Safe";

                authApMsg = "Authorized AP";
                unAuthApMsg = "unAuthorized AP";
                chMsg = "AP Channel";
                authSsidMsg = "Authorized SSID";
                TypeApMsg = "Type of AP";
                hiddenProvidearApMsg = "Hidden AP & Mobile Provider's AP";
                notSafeMsg = "Details of AP which is Not Safe";
                defaultSsidMsg = "Default SSID";
                specificSsidMsg = "Specific SSID";
                hiddlenSsidMsg = "Hidden SSID";

                detaileTitleMsg = "Detailed AP Table";
                authTitleMsg = "Authorized AP";
                filterSSidTitleMsg = "Filtered SSID";
                pictureTitleMsg = "Inspection Spot Picture";
                pentestTitleMsg = "Pen-Testing History";
                duplicateTitleMsg = "Duplicate MAC";

                proTitleMsg = "Project";
                loTitleMsg = "Location";
                saveTimeTitleMsg = "Save Time";
                gpsTitleMsg = "GPS";
                signalTitleMsg = "Signal";
                detailchTitleMsg = "Channel";
                encTitleMsg = "Encyption";
                scanTimeTitleMsg = "Scan Time";
                apTypeTitleMsg = "AP Type";
                ouiTitleMsg = "OUI";
                commentTitleMsg = "Comment";
                alertTitleMsg = "Alert";
                polictTitleMsg = "Policy Type";
                detailpentestTitleMsg = "Pentest";
                memoTitleMsg = "Memo";
                authMsg = "Authorized";

                break;

            case 1: //한국어
                dashBoardTitleMsg = "Dash Board";
                reportTitleMsg = "Report";
                detailsTitleMsg = "Details";
                totalMsg = "Total";

                apStaticsTitleMsg = "AP STATICS";
                chTitleMsg = "CHANNEL OF AP";
                riskTitleMsg = "AP Distrbution by Risk Degree";

                highMsg = "High";
                middleMsg = "Middle";
                safeMsg = "Safe";

                authApMsg = "Authorized AP";
                unAuthApMsg = "unAuthorized AP";
                chMsg = "AP Channel";
                authSsidMsg = "Authorized SSID";
                TypeApMsg = "Type of AP";
                hiddenProvidearApMsg = "Hidden AP & Mobile Provider's AP";
                notSafeMsg = "Details of AP which is Not Safe";
                defaultSsidMsg = "Default SSID";
                specificSsidMsg = "Specific SSID";
                hiddlenSsidMsg = "Hidden SSID";

                proTitleMsg = "Project";
                loTitleMsg = "Location";
                saveTimeTitleMsg = "Save Time";
                gpsTitleMsg = "GPS";
                signalTitleMsg = "Signal";
                detailchTitleMsg = "Channel";
                encTitleMsg = "Encyption";
                scanTimeTitleMsg = "Scan Time";
                apTypeTitleMsg = "AP Type";
                ouiTitleMsg = "OUI";
                commentTitleMsg = "Comment";
                alertTitleMsg = "Alert";
                polictTitleMsg = "Policy Type";
                detailpentestTitleMsg = "Pentest";
                memoTitleMsg = "Memo";
                authMsg = "Authorized";
                break;

            case 2:  //일본어
                dashBoardTitleMsg = "ダッシュボード";
                reportTitleMsg = "レポート";
                detailsTitleMsg = "詳細";
                totalMsg = "合計";

                apStaticsTitleMsg = "AP統計";
                chTitleMsg = "チャンネル状況";
                riskTitleMsg = "危険度分布";

                highMsg = "危険";
                middleMsg = "普通";
                safeMsg = "安全";

                authApMsg = "認可AP";
                unAuthApMsg = "非認可AP";
                chMsg = "脆弱AP詳細";
                authSsidMsg = "認可SSID";
                TypeApMsg = "暗号化タイプ";
                hiddenProvidearApMsg = "隠蔽SSIDとFREE Wi-Fi";
                notSafeMsg = "脆弱AP詳細";
                defaultSsidMsg = "デフォルトSSID";
                specificSsidMsg = "疑わしいAP";
                hiddlenSsidMsg = "隠蔽SSID";

                detaileTitleMsg = "APテーブル詳細";
                authTitleMsg = "認可AP";
                filterSSidTitleMsg = "フィルターされたSSID";
                pictureTitleMsg = "検知スポット画像";
                pentestTitleMsg = "侵入テスト履歴";
                duplicateTitleMsg = "MACアドレス重複";

                proTitleMsg = "プロジェクト";
                loTitleMsg = "ロケーション";
                saveTimeTitleMsg = "保存日時";
                gpsTitleMsg = "位置情報";
                signalTitleMsg = "信号強度";
                detailchTitleMsg = "チャンネル";
                encTitleMsg = "暗号化";
                scanTimeTitleMsg = "デバイスタイプ";
                apTypeTitleMsg = "メーカー";
                ouiTitleMsg = "デバイスタイプ";
                commentTitleMsg = "コメント";
                alertTitleMsg = "危険度";
                polictTitleMsg = "ポリシー";
                detailpentestTitleMsg = "侵入テスト";
                memoTitleMsg = "メモ";
                authMsg = "認定";
                break;

            case 3: //중국어
                dashBoardTitleMsg = "仪表板";
                reportTitleMsg = "报告";
                detailsTitleMsg = "细节";
                totalMsg = "所有";

                apStaticsTitleMsg = "AP静力学";
                chTitleMsg = "AP频道";
                riskTitleMsg = "按风险程度分配AP";

                highMsg = "高安全";
                middleMsg = "中等安全";
                safeMsg = "安全";

                authApMsg = "已授权AP";
                unAuthApMsg = "未经授权AP";
                chMsg = "AP频道";
                authSsidMsg = "已授权SSID";
                TypeApMsg = "AP类型";
                hiddenProvidearApMsg = "隐藏的AP&移动供应商AP";
                notSafeMsg = "不安全的AP细节";
                defaultSsidMsg = "默认的SSID";
                specificSsidMsg = "表示特定词汇的SSID";
                hiddlenSsidMsg = "隐藏的SSID";

                proTitleMsg = "项目";
                loTitleMsg = "位置";
                saveTimeTitleMsg = "节省时间";
                gpsTitleMsg = "GPS";
                signalTitleMsg = "Signal";
                detailchTitleMsg = "Channel";
                encTitleMsg = "加密";
                scanTimeTitleMsg = "查找时间";
                apTypeTitleMsg = "AP类型";
                ouiTitleMsg = "OUI";
                commentTitleMsg = "评价";
                alertTitleMsg = "警惕";
                polictTitleMsg = "政策类型";
                detailpentestTitleMsg = "Pen测试";
                memoTitleMsg = "备忘录";
                authMsg = "已授权";
                break;
        }


    }

}
