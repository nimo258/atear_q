package com.norma.atear_mobile.report.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.norma.atear_mobile.project.SharedPref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyojin on 8/2/16.
 */
public class VerticalBarGraph extends View
{
	Canvas cv;
	Context mContext;

	public boolean isPDF = false;

	float default_width = 1296;
	float default_height = 1400;

	float width, height;
	float width_scale, height_scale;
	float density;

	float bottom_margin, left_margin, right_margin;
	float data_size, text_size, title_size;

	float x, y;
	float bar_height;  //막대 그래프 길이 비율

	String title = "";

	int largest_data = 0;   //가장 큰 데이터 값

	boolean state_small = false; //글자 수가 너무 많을 떄 true
	boolean state_duplicate = false;
	boolean state_title = false;    // 제목이 있을 때 true

	List<String> color = new ArrayList<>();
	List<GraphInfo> arrBarGraph = null;

	Paint pnt_name, pnt_line, pnt_graph, pnt_data, pnt_title;

	public VerticalBarGraph(Context context, List<GraphInfo> arrBarGraph, LinearLayout linearLayout)
	{
		super(context);
		mContext = context;
		this.arrBarGraph = arrBarGraph;
		ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

		this.width = lp.width;
		this.height = lp.height;

	}

	public VerticalBarGraph(Context context, List<GraphInfo> arrBarGraph, int width) {
		super(context);
		mContext = context;
		this.arrBarGraph = arrBarGraph;
		//ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

		//this.width = lp.width;
		//this.height = lp.height;
		this.width = width;
		this.height = (float) (width * 1.1);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);



		// width 진짜 크기 구하기
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch(widthMode) {
			case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
				widthSize = widthMeasureSpec;
				break;
			case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
				widthSize = 100;
				break;
			case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
				widthSize = MeasureSpec.getSize(widthMeasureSpec);
				break;
		}

		// height 진짜 크기 구하기
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;


		switch(heightMode) {
			case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
				heightSize = (int)(widthSize*1.1);

				break;
			case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
				heightSize =  (int)(widthSize*1.1);

				break;
			case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
				heightSize = (int)(widthSize*1.1); //MeasureSpec.getSize(heightMeasureSpec);

				break;


		}
		widthSize = (int) width;
		heightSize = (int) height;

		setMeasuredDimension(widthSize, heightSize);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

	}


	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		this.cv = canvas;

		setParameter();
		setPaint();

		//	if(isPDF == false) {
		cv.drawColor(Color.parseColor("#152a3c"));  //배경색
		//	}else{
		//		cv.drawColor(0xFFFFFFFF);
		//	}

		float total_name = 0;

		if (arrBarGraph.size() > 5)
		{
			total_name = 1000;
		}

		for (int i = 0; i < arrBarGraph.size(); i++)
		{
			total_name += pnt_name.measureText(arrBarGraph.get(i).getName());

			if (largest_data < arrBarGraph.get(i).getData())
				largest_data = arrBarGraph.get(i).getData();
		}

		if (largest_data > 45 && largest_data <= 95)
			bar_height = 1;
		else if (largest_data >= 8 && largest_data <= 45)
			bar_height = 2;
		else if (largest_data >= 0 && largest_data < 8)
			bar_height = 10;
		else if (largest_data > 95 && largest_data <= 495)
			bar_height = 0.2f;
		else if (largest_data > 495 && largest_data <= 1000)
			bar_height = 0.1f;
		else if(largest_data > 1000 && largest_data <= 4500){
			bar_height = 0.02f;
		}else if(largest_data > 4500 && largest_data <= 10000){
			bar_height = 0.01f;
		}else{	//20000
			bar_height = 0.005f;
		}

		//가장 큰 데이터값 기준으로 가로선 맞춤

		if (total_name > width - left_margin * 3 - right_margin * 3)
			state_small = true;

		if (state_title)
			showTitle();

		for (int i = 0; i < arrBarGraph.size(); i++)
			showGraph(i, arrBarGraph);

		showLine();
	}

	public void showGraph(int index, List<GraphInfo> arrBarGraph)
	{
		float width_name = pnt_name.measureText(arrBarGraph.get(index).getName());  //글자 폭
		float width_data = pnt_data.measureText(String.valueOf(arrBarGraph.get(index).getData()));  //데이터 폭

		float temp = 4 * index + 2;
		float temp1 = temp - 1;
		float temp2 = temp + 1;
		float temp3 = arrBarGraph.size() * 4;

		cv.drawRect(left_margin + x * temp1 / temp3,
				y - 10 * arrBarGraph.get(index).getData() * bar_height * density, left_margin + x * temp2 / temp3, y, pnt_graph); //그래프 그리기

		cv.drawText(String.valueOf(arrBarGraph.get(index).getData()), left_margin + x * temp / temp3 - width_data / 2.0f,
				y - 10 * arrBarGraph.get(index).getData() * bar_height * density - 25 * density, pnt_data);  //그래프 위에 데이터 값 쓰기

		if (state_small)    //글자 수가 너무 많을 때
		{
			SharedPref sharedPref = new SharedPref(mContext );
			int language = sharedPref.getValue("language",0);
			if(language == 3)//중국어
				pnt_name.setTextSize(text_size / 2.0f);
			else if(language == 1)//영어
				pnt_name.setTextSize(text_size / 1.5f);
			else
				pnt_name.setTextSize(text_size / 1.3f);
			Path mLine = new Path();

			mLine.moveTo(left_margin + x * temp / temp3 - pnt_name.measureText(arrBarGraph.get(index).getName()) * 0.5f,
					y + pnt_name.measureText(arrBarGraph.get(index).getName()) * 0.8f + 20 * density);

			mLine.lineTo(left_margin + x * temp / temp3 + 100 * density, y - 20 * density);

			cv.drawTextOnPath(String.valueOf(arrBarGraph.get(index).getName()), mLine, 0, 0, pnt_name);
		}
		else
			cv.drawText(arrBarGraph.get(index).getName(), left_margin + x * temp / temp3 - width_name / 2.0f, y + 75 * density, pnt_name);  //x축 밑에 글자 쓰기
	}

	public void showLine()
	{
		String value;

		for (int i = 0; i < 5; i++)
		{
			value = String.format("%.1f", i * 25 / bar_height);       //소수점 1자리 남기기

			cv.drawText(value, left_margin - pnt_line.measureText(value) - 20 * density,
					y - i * 25 * 10 * density + data_size * 1.75f / 5.0f, pnt_line);
			//세로축 범례 쓰기

			cv.drawRect(left_margin, y - i * 25 * 10 * density,
					width - right_margin - 50 * density, y - i * 25 * 10 * density + 2 * density, pnt_line);
			//가로 선 그리기
		}
	}

	public boolean setStateDuplicate(boolean state_duplicate)
	{
		this.state_duplicate = state_duplicate;
		return state_duplicate;
	}

	private void showTitle()
	{
		cv.drawText(title, width / 2 - pnt_title.measureText(title) / 2, bottom_margin * 0.8f, pnt_title);
	}

	public void setTitle(String title)
	{
		this.title = title;
		state_title = true;
		default_height += 200;
	}

	private void setPaint()
	{
		pnt_name = new Paint();
		pnt_name.setColor(Color.parseColor("#7cb2dc"));
		pnt_name.setTextSize(text_size);

		pnt_line = new Paint();
		pnt_line.setAntiAlias(true);
		pnt_line.setColor(Color.parseColor("#385c7a"));
		pnt_line.setTextSize(data_size);

		pnt_graph = new Paint();
		pnt_graph.setAntiAlias(true);
		pnt_graph.setColor(Color.parseColor("#2eabc9"));

		pnt_data = new Paint();
		pnt_data.setColor(Color.WHITE);
		pnt_data.setTextSize(data_size);

		pnt_title = new Paint();
		pnt_title.setAntiAlias(true);
		pnt_title.setColor(Color.parseColor("#FFFFFF"));
		pnt_title.setTextSize(title_size);
	}

	private void setParameter()
	{
		width_scale = width / default_width;
		height_scale = height / default_height;

		density = (float) Math.sqrt(width_scale * height_scale);

		bottom_margin = 200 * density;
		left_margin = 180 * density;
		right_margin = 50 * density;

		x = width - left_margin - right_margin;
		y = height - bottom_margin;

		data_size = 45 * density;
		text_size = 50 * density;
		title_size = 80 * density;

		bar_height = 1;  //막대 그래프 길이 비율
	}

	//중복 제거 함수
	public void removeDuplicate()
	{
		List<String> temp = new ArrayList<>();
		int[] count = new int[100];

		for (int i = 0; i < arrBarGraph.size(); i++)
		{
			if (temp.contains(arrBarGraph.get(i).getName()))
			{
				count[temp.indexOf(arrBarGraph.get(i).getName())]++;
				arrBarGraph.remove(i);
				i--;
			} else
				temp.add(arrBarGraph.get(i).getName());
		}

		for (int i = 0; i < temp.size(); i++)
		{
			arrBarGraph.get(i).setData(count[i] + 1);
			arrBarGraph.get(i).setName(temp.get(i));
		}
		state_duplicate = false; //1번만 실행되게 하기
	}

	public Bitmap getBitmap(){
		this.measure(View.MeasureSpec.makeMeasureSpec((int)this.width, View.MeasureSpec.EXACTLY), //가로폭
				View.MeasureSpec.makeMeasureSpec((int)this.height, View.MeasureSpec.EXACTLY)); //세로폭

		this.layout(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight());
		this.buildDrawingCache();

		Bitmap bit = this.getDrawingCache();


		return bit;
	}
}