package com.norma.atear_mobile.report;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.report.adapter.Detail_info_Adapter;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/21/16.
 */
public class Detail_info_Activity extends FragmentActivity {

    private ViewPager mPager;
    private Detail_info_Adapter adapter;
    private ArrayList<String> bssidList;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_info);
        mContext = this;

        Intent intent = getIntent();

        int position = intent.getIntExtra("position", 0);
        bssidList = intent.getStringArrayListExtra("bssidList");

        mPager = (ViewPager) findViewById(R.id.mPager);

        adapter = new Detail_info_Adapter(getLayoutInflater(), mContext, bssidList.size(), bssidList );

        mPager.setAdapter(adapter);

        mPager.setCurrentItem(position);




    }
}
