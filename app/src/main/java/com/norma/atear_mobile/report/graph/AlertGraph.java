package com.norma.atear_mobile.report.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyojin on 8/2/16.
 */
public class AlertGraph extends View {
    Canvas cv;

    private static final String TAG = "AlertGraphClass";
    public boolean isPDF = false;

    public boolean AUTH_SSID = false;

    float default_width = 1440;
    float default_height = 1000;

    float width, height;
    float width_scale, height_scale;
    float density;

    float top_margin, left_margin;    //왼쪽 여백
    float diameter, radius;    //반지름

    float x, y;

    float line1, line2, line3, line4;

    float text_size, title_size;

    String title = "";

    boolean state_duplicate = false;
    boolean state_title = false;


    List<String> color = new ArrayList<>();
    List<GraphInfo> arrCircleGraph = null;

    public AlertGraph(Context context, List<GraphInfo> arrCircleGraph, LinearLayout linearLayout)// LinearLayout linearLayout)
    {
        super(context);

        this.arrCircleGraph = arrCircleGraph;

        ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        this.width = lp.width;
        this.height = lp.height;

        //this.width = width;
        //this.height =  (float)(width*0.9);
    }


    public AlertGraph(Context context, List<GraphInfo> arrCircleGraph, int width)// LinearLayout linearLayout)
    {
        super(context);

        this.arrCircleGraph = arrCircleGraph;

        //ViewGroup.LayoutParams lp = linearLayout.getLayoutParams();

        //this.width = lp.width;
        //this.height = lp.height;

        this.width = width;
        this.height = (float) (width * 0.8);
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.cv = canvas;


        // if(isPDF == false) {
        cv.drawColor(Color.parseColor("#152a3c"));  //배경색
        // }else{
        //     cv.drawColor(0xFFFFFFFF);
        // }
        if (state_duplicate)
            removeDuplicate();

        setParameter();

        if (state_title)
            showTitle();     //제목 적기

        drawCircleGraph(arrCircleGraph);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        // width 진짜 크기 구하기
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = 0;
        switch (widthMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                widthSize = 100;
                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        // height 진짜 크기 구하기
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = 0;


        switch (heightMode) {
            case MeasureSpec.UNSPECIFIED://mode 가 셋팅되지 않은 크기가 넘어올때
                heightSize = (int) (widthSize * 0.9);

                break;
            case MeasureSpec.AT_MOST://wrap_content (뷰 내부의 크기에 따라 크기가 달라짐)
                heightSize = (int) (widthSize * 0.9);

                break;
            case MeasureSpec.EXACTLY://fill_parent, match_parent (외부에서 이미 크기가 지정되었음)
                heightSize = (int) (widthSize * 0.9); //MeasureSpec.getSize(heightMeasureSpec);

                break;


        }

        setMeasuredDimension((int) width, (int) height);
        // setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

    }


    public void drawCircleGraph(List<GraphInfo> arrCircleGraph) {
        int i;
        int total = 0;  //데이터 총합
        int size = arrCircleGraph.size();
        float[] data = new float[size]; //0부터 각 항목의 중심 지점 까지의 크기

        List<Float> data_set = new ArrayList<>();
        List<Float> angle_set = new ArrayList<>();
        List<Float> x_set = new ArrayList<>();
        List<Float> y_set = new ArrayList<>();

        for (i = 0; i < size; i++)
            total += arrCircleGraph.get(i).getData();

        Paint pnt_stroke = new Paint();
        pnt_stroke.setAntiAlias(true);
        pnt_stroke.setColor(Color.parseColor("#152a3c"));
        pnt_stroke.setStyle(Paint.Style.STROKE);
        pnt_stroke.setStrokeWidth(5 * density);       //그래프 외곽선의 paint

        RectF rectF = new RectF(x - radius, y - radius, x + radius, y + radius);

        for (i = 0; i < size; i++) {
            data_set.add(arrCircleGraph.get(i).getData() * 1.0f / total);
            angle_set.add(data_set.get(i) * 360);

            for (int j = 0; j < i + 1; j++) {
                if (j != i)
                    data[i] += angle_set.get(j);
                else
                    data[i] += angle_set.get(j) / 2;
            }

            x_set.add(x + setXCoordinate(data[i]));
            y_set.add(y - setYCoordinate(data[i]));
        }   // 데이터 입력

        Paint pnt = new Paint();
        pnt.setAntiAlias(true);
        pnt.setTextSize(text_size);     //그래프의 paint

        for (i = 0; i < size; i++) {
            float temp = 0;

            pnt.setColor(Color.parseColor(arrCircleGraph.get(size - i - 1).getColor()));    //그래프 색깔지정

            for (int j = size - i - 1; j >= 0; j--)
                temp += angle_set.get(j);

            int count = arrCircleGraph.get(i).getData(); //갯수

            cv.drawArc(rectF, -90, temp, true, pnt);   //그래프 그리기
            cv.drawArc(rectF, -90, temp, true, pnt_stroke);    //그래프 외곽선 그리기
            drawData(arrCircleGraph.get(i).getName(), setQuadrant(data[i]), data_set.get(i), x_set.get(i), y_set.get(i), count);      //각 항목 텍스트, 선 그리기
        }
    }

    //중복 제거 함수
    public void removeDuplicate() {
        List<String> temp = new ArrayList<>();
        int[] count = new int[100];

        setColor(color);        //색깔 지정

        for (int i = 0; i < arrCircleGraph.size(); i++) {
            if (temp.contains(arrCircleGraph.get(i).getName())) {
                count[temp.indexOf(arrCircleGraph.get(i).getName())]++;
                arrCircleGraph.remove(i);
                i--;
            } else
                temp.add(arrCircleGraph.get(i).getName());
        }

        for (int i = 0; i < temp.size(); i++) {
            arrCircleGraph.get(i).setData(count[i] + 1);
            arrCircleGraph.get(i).setColor(color.get(i % 5));
            arrCircleGraph.get(i).setName(temp.get(i));
        }
        state_duplicate = false; //1번만 실행되게 하기
    }

    public void drawData(String name, int quadrant, float data, float x, float y, int count) {
        Paint pnt_text = new Paint();
        pnt_text.setAntiAlias(true);
        pnt_text.setColor(Color.parseColor("#7cb2dc"));
        pnt_text.setTextSize(text_size);
        pnt_text.setStrokeWidth(5 * density);

        String value = String.format("%.1f", (data * 100));
        if (!name.equals("noData")) {
            if (quadrant == 1) {
                cv.drawLine(x, y, x, y - line3, pnt_text);
                cv.drawLine(x, y - line3, x + line1, y - line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x + line2, y - line3 + text_size * 1.75f / 5.0f, pnt_text);
            } else if (quadrant == 2) {
                cv.drawLine(x, y, x + line3, y, pnt_text);
                cv.drawLine(x + line3, y, x + line3, y - line1, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x + line3 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")") * 0.25f, y - line1 - text_size * 1.75f / 5.0f, pnt_text);
            } else if (quadrant == 3) {
                cv.drawLine(x, y, x + line3, y, pnt_text);
                cv.drawLine(x + line3, y, x + line3, y + line1, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x + line3 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")") * 0.2f, y + line2 + text_size * 3.5f / 5.0f, pnt_text);
            } else if (quadrant == 4) {
                cv.drawLine(x, y, x, y + line3, pnt_text);
                cv.drawLine(x, y + line3, x + line1, y + line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x + line2, y + line3 + text_size * 1.75f / 5.0f, pnt_text);
            } else if (quadrant == 5) {
                cv.drawLine(x, y, x, y + line3, pnt_text);
                cv.drawLine(x, y + line3, x - line1, y + line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x - line2 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")"), y + line3 + text_size * 1.75f / 5.0f, pnt_text);
            } else if (quadrant == 6) {
                cv.drawLine(x, y, x - line4, y, pnt_text);
                cv.drawLine(x - line4, y, x - line4, y + line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x - line4 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")") * 0.7f, y + line2 + text_size * 3.5f / 5.0f, pnt_text);
            } else if (quadrant == 7) {
                cv.drawLine(x, y, x - line4, y, pnt_text);
                cv.drawLine(x - line4, y, x - line4, y - line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x - line4 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")") * 0.7f, y - line3 - text_size * 1.75f / 5.0f, pnt_text);
            } else if (quadrant == 8) {
                cv.drawLine(x, y, x, y - line3, pnt_text);
                cv.drawLine(x, y - line3, x - line1, y - line3, pnt_text);
                cv.drawText(name + " : " +value + "%"+"("+count+")", x - line2 - pnt_text.measureText(name + " : " +value + "%"+"("+count+")"), y - line3 + text_size * 1.75f / 5.0f, pnt_text);
            }
        }else{
            cv.drawLine(x, y, x, y + line3, pnt_text);
            cv.drawLine(x, y + line3, x + line1, y + line3, pnt_text);
            cv.drawText(name , x + line2, y + line3 + text_size * 1.75f / 5.0f, pnt_text);
        }
    }

    private void showTitle() {
        Paint pnt_title = new Paint();
        pnt_title.setAntiAlias(true);
        pnt_title.setColor(Color.parseColor("#FFFFFF"));
        pnt_title.setTextSize(title_size);

        cv.drawText(title, width / 2 - pnt_title.measureText(title) / 2, top_margin / 2 - radius / 2, pnt_title);
    }

    public void setTitle(String title) {
        this.title = title;
        state_title = true;
        default_height += 200;
    }

    private void setParameter() {
        width_scale = width / default_width;
        height_scale = height / default_height;


        density = (float) Math.sqrt(height_scale * width_scale);

        if (state_title)
            top_margin = 700 * density;      //위쪽 여백
        else
            top_margin = 500 * density;

        left_margin = width / 2;    //왼쪽 여백
        if (AUTH_SSID == false) {
            diameter = 550 * density;//지름
        } else {
            diameter = 480 * density;//지름
        }
        radius = diameter / 2;    //반지름

        x = left_margin;
        y = top_margin;

        line1 = 40 * density;
        line2 = 50 * density;
        line3 = 60 * density;
        line4 = 90 * density;

        title_size = 80 * density;
        text_size = 40 * density;
    }

    public boolean setStateDuplicate(boolean state_duplicate) {
        this.state_duplicate = state_duplicate;
        return state_duplicate;
    }

    private void setColor(List<String> color) {
        color.add("#FF0000");
        color.add("#7700FF");
        color.add("#FF00FF");
        color.add("#00FF00");
        color.add("#0000FF");
    }

    private int setQuadrant(float angle) {
        if (0 < angle && angle <= 60)
            return 1;
        else if (60 < angle && angle <= 90)
            return 2;
        else if (90 < angle && angle <= 120)
            return 3;
        else if (120 < angle && angle <= 180)
            return 4;
        else if (180 < angle && angle <= 240)
            return 5;
        else if (240 < angle && angle <= 270)
            return 6;
        else if (270 < angle && angle <= 300)
            return 7;
        else if (300 < angle && angle <= 360)
            return 8;
        else
            return 0;
    }

    private float setXCoordinate(float angle) {
        return radius * (float) Math.sin(radianResult(angle));
    }

    private float setYCoordinate(float angle) {
        return radius * (float) Math.cos(radianResult(angle));
    }

    private double radianResult(float angle) {
        return angle * (Math.PI / 180);
    }

    public Bitmap getBitmap() {
        this.measure(View.MeasureSpec.makeMeasureSpec((int) this.width, View.MeasureSpec.EXACTLY), //가로폭
                View.MeasureSpec.makeMeasureSpec((int) this.height, View.MeasureSpec.EXACTLY)); //세로폭

        this.layout(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight());
        this.buildDrawingCache();

        Bitmap bit = this.getDrawingCache();


        return bit;
    }
}