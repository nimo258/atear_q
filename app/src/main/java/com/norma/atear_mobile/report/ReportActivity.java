package com.norma.atear_mobile.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.MediaScanner;
import com.norma.atear_mobile.csv.CSVRoder;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.dialog.FilterDialog;
import com.norma.atear_mobile.dialog.GlobalDialog;
import com.norma.atear_mobile.dialog.LocalDialog;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.policy.Norma_policy;
import com.norma.atear_mobile.project.policy.Policy_Result;
import com.norma.atear_mobile.project.policy.Policy_info;
import com.norma.atear_mobile.report.adapter.ReportPageAdapter;
import com.norma.atear_mobile.report.excel.ExcelWriter;
import com.norma.atear_mobile.report.graph.GraphInfo;
import com.norma.atear_mobile.report.pdf.ITextPDF;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by hyojin on 7/8/16.
 */
public class ReportActivity extends FragmentActivity implements View.OnClickListener {

    public static boolean isWarning = false;  //warning 클릭시

    public static Context mContext;

    private ReportPageAdapter reportPageAdapter;

    private DrawerLayout drawerLayout;  //슬라이드 레이아웃
    private View drawerView;

    private TextView tv_title;
    private LinearLayout btn_back2;

    private LinearLayout btn_back;
    private Button btn_dash_board;
    private Button btn_report;
    private Button btn_detail;
    private Button btn_global_setting;
    private Button btn_filter_setting;
    private Button btn_local_setting;
    private Button btn_submit;
    private Button btn_filter;
    private LinearLayout btn_menu;
    private Button btn_pdf;

    private ProgressDialog progressDialog;

    private TextView tv_global;
    private TextView tv_filter;
    private TextView tv_local;
    private TextView tv_globalTitle;
    private TextView tv_localTitle;
    private TextView tv_filterTitle;
    private TextView tv_title2;
    private EditText edt_filter;

    public ViewPager mViewPager;
    //layout
    private LinearLayout linear_filter;
    private LinearLayout.LayoutParams layoutControl;

    //data
    int global = 0;

    public static Norma_policy norma_policy;
    public static boolean SettingFilter = false;

    public static ArrayList<String> filter_list;

    private AtEarDB atEarDB;

    ITextPDF iTextPDF;
    ExcelWriter excelWriter;

    private static final String REPORT_TABLE = "report_table";
    private static final String SSID = "ssid";
    private static final String AUTHORIZED = "isAuthorized";

    private static final int IS_TRUE = 1;

    private int total_AP = 0;
    private int warning_AP = 0;
    private int gray_AP = 0;
    private int not_real_AP = 0;
    private int alert_high = 0;
    private int alert_middle = 0;
    private int alert_safety = 0;


    private int authAp = 0;   //인가 AP;
    private int unAuthAp = 0; //비인가 AP;
    //enc
    private int toOPEN = 0;
    private int toWEP = 0;
    private int toWPA = 0;
    private int toWPA2 = 0;
    private int toWPA_s = 0;

    private int authOPEN = 0;
    private int authWEP = 0;
    private int authWPA = 0;
    private int authWPA2 = 0;
    private int authWPA_s = 0;

    private int unAuthOPEN = 0;
    private int unAuthWEP = 0;
    private int unAuthWPA = 0;
    private int unAuthWPA2 = 0;
    private int unAuthWPA_s = 0;
    //ssid

    private int authSpecificSsid = 0;
    private int authdefaultSsid = 0;
    private int authSoftAp = 0;
    private int authHiddenSsid = 0;

    private int unAuthHiddenSsid = 0;
    private int unAuthGrayAp = 0;

    //위험도
    private int high = 0;
    private int middle = 0;
    private int safe = 0;

    private ArrayList<Integer> chList;

    private ArrayList<Integer> total_chList;   //채널 리스트
    java.util.List<GraphInfo> auth_isSSidList;

    private ArrayList<Ap_info> apList;

    private ArrayList<Ap_info> detailList;  //디테일 리스트
    private ArrayList<Ap_info> whiteList;  //인가 리스트
    private ArrayList<Ap_info> warningAp_List; // open, wep 리스트
    private ArrayList<Ap_info> ssid_filter_List;  //ssid 필터 리스트
    private ArrayList<Ap_info> picture_filter_List;  //ssid 필터 리스트
    // private ArrayList<Ap_info> bssid_filter_List;  //bssid 필터 리스트
    private ArrayList<Ap_info> overlap_List;  //중복 AP
    private ArrayList<Ap_info> pantest_List; // Pantest실행한 리스트
    private ArrayList<Ap_info> notRealAp_List; //soft AP 리스트
    private ArrayList<Ap_info> grayAp_List;  //모바일 ap 리스트

    private ArrayList<Ap_info> unAuthList;

    SharedPref sharedPref;
    int language = 0;
    UploadThread uploadThread;

    ArrayList<String> fileNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //화면 꺼짐 방지
        setContentView(R.layout.activity_report);

        findView();

        SetLanguage();

        tv_title.setText(mContext.getResources().getString(R.string.dashBoardTitleMsg));

        fileNameList = getIntent().getStringArrayListExtra("filelist");

        btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {

                }
/*
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = new ProgressDialog(ReportActivity.this);
                            progressDialog.setMessage(progressMsg);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            mHandler.sendEmptyMessageDelayed(0, 1000);  // 데이터 초기화
                        }
                    });
                } catch (Exception e) {

                }
                iTextPDF = new ITextPDF(mContext, today() + "_Report");
                 iTextPDF.CreatePDF();
                  iTextPDF.uploadPDF.execute();*/
                excelWriter = new ExcelWriter(mContext, "report_test", apList);
                excelWriter.getFileNameList(fileNameList);  //보고서 list
                excelWriter.start();
                mHandler.sendEmptyMessageDelayed(0, 1000);
                //    Toast.makeText(mContext, createPDFMsg, Toast.LENGTH_SHORT).show();
            }
        });


        ReportActivity.norma_policy = new Norma_policy();

        uploadThread = new UploadThread();
        uploadThread.execute();


        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tv_title.setText(mContext.getResources().getString(R.string.dashBoardTitleMsg));
                        btn_dash_board.setBackgroundResource(R.drawable.btn_over);
                        btn_report.setBackgroundResource(R.drawable.btn);
                        btn_detail.setBackgroundResource(R.drawable.btn);
                        break;
                    case 1:
                        tv_title.setText(mContext.getResources().getString(R.string.reportTitleMsg));
                        btn_dash_board.setBackgroundResource(R.drawable.btn);
                        btn_report.setBackgroundResource(R.drawable.btn_over);
                        btn_detail.setBackgroundResource(R.drawable.btn);
                        break;
                    case 2:
                        tv_title.setText(mContext.getResources().getString(R.string.detailsTitleMsg));
                        btn_dash_board.setBackgroundResource(R.drawable.btn);
                        btn_report.setBackgroundResource(R.drawable.btn);
                        btn_detail.setBackgroundResource(R.drawable.btn_over);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = edt_filter.getText().toString();

                if (!data.equals("")) {
                    final TextView tv = new TextView(mContext);

                    tv.setLayoutParams(layoutControl);

                    Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                    img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                    tv.setCompoundDrawablePadding(0);
                    tv.setCompoundDrawables(null, null, img, null);


                    tv.setText(data);
                    tv.setTextColor(0xFF768b9c);

                    linear_filter.addView(tv);
                    filter_list.add(data);

                    tv_filter.setText("- ");

                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            linear_filter.removeView(tv);

                            filter_list.remove(tv.getText().toString());

                            if (filter_list.size() != 0) {

                                tv_filter.setText("- ");
                            } else {
                                tv_filter.setText("- None");
                            }
                        }
                    });
                    edt_filter.setText("");
                }
            }
        });


        final GlobalDialog globalDialog = new GlobalDialog(mContext);
        globalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        globalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        globalDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface $dialog) {
                GlobalDialog dialog = (GlobalDialog) $dialog;
                int data = dialog.getData();
                switch (data) {
                    case 0:
                        ReportActivity.norma_policy.non_policy();
                        tv_global.setText("- None");
                        break;

                    case 1:
                        ReportActivity.norma_policy.policy_isms_auth();
                        tv_global.setText("- ISMS");

                        break;

                    case 2:
                        ReportActivity.norma_policy.policy_iso_auth();
                        tv_global.setText("- ISO 27001");
                        break;

                    case 3:
                        ReportActivity.norma_policy.policy_norma_auth();
                        tv_global.setText("- NOMRA");
                        break;

                }
            }
        });

        btn_global_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalDialog.setReceiver(0);
                globalDialog.show();
            }
        });

        final FilterDialog filterDialog = new FilterDialog(mContext);
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        filterDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface $dialog) {
                FilterDialog dialog = (FilterDialog) $dialog;
                String data = dialog.getData();

                if (!data.equals("")) {
                    final TextView tv = new TextView(mContext);

                    tv.setLayoutParams(layoutControl);

                    Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                    img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                    tv.setCompoundDrawablePadding(0);
                    tv.setCompoundDrawables(null, null, img, null);


                    tv.setText(data);
                    tv.setTextColor(0xFF768b9c);

                    linear_filter.addView(tv);

                    filter_list.add(data);
                    tv_filter.setText("- ");

                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            linear_filter.removeView(tv);

                            filter_list.remove(tv.getText().toString());

                            if (filter_list.size() != 0) {

                                tv_filter.setText("- ");
                            } else {
                                tv_filter.setText("- None");
                            }
                        }
                    });

                }
            }
        });

        btn_filter_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        final LocalDialog localDialog = new LocalDialog(mContext);
        localDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        localDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        localDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface $dialog) {
                LocalDialog dialog = (LocalDialog) $dialog;

                String fileName = dialog.getFileName();
                String filePath = dialog.getFilePath();

                if (!filePath.equals("none")) {
                    new CSVRoder(ReportActivity.this, atEarDB, filePath, fileName, new CSVRoder.OnChangeText() {
                        @Override
                        public void resultString(String text) {
                            tv_local.setText(text);
                        }

                        @Override
                        public void postExecuteString(String text) {
                            Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
                        }
                    }).execute();
                } else {
                    atEarDB.SINGLE_BSSID_TABLE_DROP();
                    tv_local.setText("- None");
                }
            }
        });


        btn_local_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Intent intent = new Intent(mContext, FileListActivity.class);
                // startActivityForResult(intent, FILE_RESULT);

                localDialog.show();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (filter_list != null && filter_list.size() != 0)
                    SettingFilter = true;

                uploadThread = new UploadThread();
                uploadThread.execute();

                drawerLayout.closeDrawers();
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(drawerView);
            }
        });


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        edt_filter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_filter.getWindowToken(), 0);

                    String data = edt_filter.getText().toString();

                    if (!data.equals("")) {
                        final TextView tv = new TextView(mContext);

                        tv.setLayoutParams(layoutControl);

                        Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                        img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                        tv.setCompoundDrawablePadding(0);
                        tv.setCompoundDrawables(null, null, img, null);


                        tv.setText(data);
                        tv.setTextColor(0xFF768b9c);

                        linear_filter.addView(tv);
                        filter_list.add(data);

                        tv_filter.setText("- ");

                        tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                linear_filter.removeView(tv);

                                filter_list.remove(tv.getText().toString());

                                if (filter_list.size() != 0) {

                                    tv_filter.setText("- ");
                                } else {
                                    tv_filter.setText("- None");
                                }
                            }
                        });

                    }

                }

                return true;
            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mViewPager.setAdapter(reportPageAdapter);
    }

    private void findView() {

        drawerLayout = (DrawerLayout) findViewById(R.id.dl_activity_main_drawer);
        drawerView = findViewById(R.id.drawer);

        mContext = this;

        tv_globalTitle = (TextView) findViewById(R.id.tv_globalTitle);
        tv_localTitle = (TextView) findViewById(R.id.tv_localTitle);
        tv_filterTitle = (TextView) findViewById(R.id.tv_filterTitle);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title2 = (TextView) findViewById(R.id.tv_title2);

        btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_back2 = (LinearLayout) findViewById(R.id.btn_back2);

        btn_dash_board = (Button) findViewById(R.id.btn_dashboard);
        btn_report = (Button) findViewById(R.id.btn_report);
        btn_detail = (Button) findViewById(R.id.btn_detail);
        btn_filter_setting = (Button) findViewById(R.id.btn_filter_setting);
        btn_global_setting = (Button) findViewById(R.id.btn_global_setting);
        btn_local_setting = (Button) findViewById(R.id.btn_local_setting);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_filter = (Button) findViewById(R.id.btn_filter);
        btn_menu = (LinearLayout) findViewById(R.id.btn_menu);
        btn_pdf = (Button) findViewById(R.id.btn_pdf);

        tv_global = (TextView) findViewById(R.id.tv_global);
        tv_filter = (TextView) findViewById(R.id.tv_filter);
        tv_local = (TextView) findViewById(R.id.tv_local);
        edt_filter = (EditText) findViewById(R.id.edt_filter);

        mViewPager = (ViewPager) findViewById(R.id.Pager);

        linear_filter = (LinearLayout) findViewById(R.id.linear_filter);
        linear_filter.setGravity(Gravity.CENTER);
        linear_filter.setGravity(Gravity.LEFT);

        layoutControl = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        btn_dash_board.setOnClickListener(this);
        btn_report.setOnClickListener(this);
        btn_detail.setOnClickListener(this);

        filter_list = new ArrayList<>();

        atEarDB = new AtEarDB(mContext);

        sharedPref = new SharedPref(mContext);

        SettingFilter = false;
    }

    private void SetLanguage() {
        btn_dash_board.setText(mContext.getResources().getString(R.string.dashBoardTitleMsg));
        btn_report.setText(mContext.getResources().getString(R.string.reportTitleMsg));
        btn_detail.setText(mContext.getResources().getString(R.string.detailsTitleMsg));
        tv_title2.setText(mContext.getResources().getString(R.string.reportTitleMsg));

        tv_globalTitle.setText(mContext.getResources().getString(R.string.g_titleMsg));
        tv_localTitle.setText(mContext.getResources().getString(R.string.l_titleMsg));
        tv_filterTitle.setText(mContext.getResources().getString(R.string.filterTitleMsg));

        edt_filter.setHint(mContext.getResources().getString(R.string.filterHintMsg));
        btn_submit.setText(mContext.getResources().getString(R.string.submitMsg));
        btn_local_setting.setText(mContext.getResources().getString(R.string.settingsMsg));
        btn_global_setting.setText(mContext.getResources().getString(R.string.settingsMsg));
        btn_filter_setting.setText(mContext.getResources().getString(R.string.addMsg));
        btn_filter.setText(mContext.getResources().getString(R.string.wifi_btnFilterMsg));
        btn_pdf.setText(mContext.getResources().getString(R.string.sendPdfMsg));
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:

                    if (excelWriter.isStatus == false) {
                        File file = new File(excelWriter.getPath());
                        if (!file.exists()) {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.pdfFailMsg), Toast.LENGTH_SHORT).show();
                        } else {
                            //android N url 변경 이슈   strictMode로 빌드 타입 변경.

                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            final Uri fileUri = Uri.fromFile(file);
                            //email
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("plain/text");

                            intent.putExtra(Intent.EXTRA_STREAM, fileUri);
                            mContext.startActivity(intent);
                            try {
                                MediaScanner scanner = MediaScanner.newInstance(ReportActivity.this);
                                scanner.mediaScanning(excelWriter.getPath());
                            } catch (Exception e) {
                                Log.e("MediaScanner", "error");
                            }
                        }
                    } else {
                        mHandler.sendEmptyMessageDelayed(0, 500);
                    }

              /*      if (iTextPDF.uploadPDF.getStatus() == AsyncTask.Status.FINISHED) {
                        if (iTextPDF.createPDFthread == null) {
                            mHandler.sendEmptyMessageDelayed(0, 500);
                        } else {
                            if (iTextPDF.createPDFthread.getStatus() == AsyncTask.Status.FINISHED) {
                                try {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                } catch (Exception e) {

                                }

                                File file = new File(iTextPDF.getPath() + ".pdf");
                                if (!file.exists()) {
                                    Toast.makeText(mContext, pdfFailMsg, Toast.LENGTH_SHORT).show();
                                } else {
                                    final Uri fileUri = Uri.fromFile(file);
                                    //email
                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.setType("plain/text");

                                    intent.putExtra(Intent.EXTRA_STREAM, fileUri);
                                    startActivity(intent);
                                    try {
                                        MediaScanner scanner = MediaScanner.newInstance(ReportActivity.this);
                                        scanner.mediaScanning(iTextPDF.getPath() + ".pdf");
                                    } catch (Exception e) {
                                        Log.e("MediaScanner", "error");
                                    }
                                }
                            } else {
                                mHandler.sendEmptyMessageDelayed(0, 500);
                            }
                        }
                    } else {
                        mHandler.sendEmptyMessageDelayed(0, 500);
                    }*/


                    break;
                case 1:
                    if (uploadThread.getStatus() == AsyncTask.Status.FINISHED) {
                        try {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {

                        }
                    } else {
                        mHandler.sendEmptyMessageDelayed(1, 300);
                    }
                    break;

                default:
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {

                    }
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dashboard:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.btn_report:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.btn_detail:
                mViewPager.setCurrentItem(2);
                break;
        }
    }

    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String result = year + "_" + month + "_" + day;

        return result;
    }

    public class UploadThread extends AsyncTask<Void, Void, Void> {

        java.util.List<GraphInfo> totalEncList;
        ArrayList<GraphInfo> graphInfoList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total_AP = 0;
            warning_AP = 0;
            gray_AP = 0;
            not_real_AP = 0;
            alert_high = 0;
            alert_middle = 0;
            alert_safety = 0;
            authAp = 0;
            unAuthAp = 0;

            total_chList = new ArrayList<>();

            totalEncList = new ArrayList<>();

            graphInfoList = new ArrayList<>();

            auth_isSSidList = new ArrayList<>();

            toOPEN = 0;
            toWEP = 0;
            toWPA = 0;
            toWPA2 = 0;
            toWPA_s = 0;

            authOPEN = 0;
            authWEP = 0;
            authWPA = 0;
            authWPA2 = 0;
            authWPA_s = 0;

            unAuthOPEN = 0;
            unAuthWEP = 0;
            unAuthWPA = 0;
            unAuthWPA2 = 0;
            unAuthWPA_s = 0;

            authHiddenSsid = 0;
            authSpecificSsid = 0;
            authdefaultSsid = 0;
            authSoftAp = 0;

            unAuthHiddenSsid = 0;
            unAuthGrayAp = 0;

            //위험도
            high = 0;
            middle = 0;
            safe = 0;

            chList = new ArrayList<>();

            detailList = new ArrayList<>();
            whiteList = new ArrayList<>();
            warningAp_List = new ArrayList<>();
            ssid_filter_List = new ArrayList<>();
            picture_filter_List = new ArrayList<>();
            overlap_List = new ArrayList<>();
            pantest_List = new ArrayList<>();
            notRealAp_List = new ArrayList<>();
            grayAp_List = new ArrayList<>();
            unAuthList = new ArrayList<>();

            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = new ProgressDialog(ReportActivity.this);
                        progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        mHandler.sendEmptyMessageDelayed(1, 1000);  // 데이터 초기화
                    }
                });
            } catch (Exception e) {

            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            //DB select
            if (apList == null) {
                apList = UploadApList();
            }
            for (Ap_info ap_info : apList) {

                boolean filter = false; // 필터링 여부

                if (SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ap_info.ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }
                    }
                }
                //  Log.e("AAAAKDSFJDSLK",ap_info.ssid+"   : " +String.valueOf(ap_info.ssid_filter));

                // ssid 필터
                if (SettingFilter == true && filter == true) {
                    //  Log.e("ANhyojin",ap_info.ssid+"   1");
                    ap_info.ssid_filter = 1;
                    //  continue;
                } else if (SettingFilter == true && filter == false) {
                    ap_info.ssid_filter = 0;
                    //  Log.e("ANhyojin",ap_info.ssid+"    0");
                }

                if (SettingFilter == true) {
                    Policy_info policy_info = new Policy_info();

                    if (ap_info.encryption.equals("WPA/WPA2")) {
                        policy_info.enc = "wpa2";
                        ap_info.encryption = "wpa2";
                    } else if (ap_info.encryption.contains("RSN-SAE") || ap_info.encryption.contains("WPA3")) {
                        policy_info.enc = "wpa3";
                        ap_info.encryption = "wpa3";
                    } else if (ap_info.encryption.contains("RSN-EAP") || ap_info.encryption.contains("EAP_WPA2/3")) {
                        policy_info.enc = "eap_wpa2/3";
                        ap_info.encryption = "eap_wpa2_wpa3";
                    } else if (ap_info.encryption.contains("WPA2")) {
                        policy_info.enc = "wpa2";
                    } else if (ap_info.encryption.contains("WPA")) {
                        policy_info.enc = "wpa";
                    } else if (ap_info.encryption.contains("WEP")) {
                        policy_info.enc = "wep";
                    } else if (ap_info.encryption.contains("WPA_s")) {
                        policy_info.enc = "wpa2";
                        ap_info.encryption = "wpa2";
                    } else {
                        policy_info.enc = "open";
                    }

                    if (ap_info.ap_type.equals("Soft AP")) {
                        policy_info.type = "Soft AP";
                    } else {
                        policy_info.type = "AP";
                    }

                    if (ap_info.ssid.equals("(Hidden)")) {
                        policy_info.ssid = "hidden ssid";
                    }

                    if (atEarDB.SSID_TABLE_SELECT(ap_info.ssid)) {
                        policy_info.ssid = "authorized ssid";
                    } else {  //일단 다 노말 나중에 default  스패셜 추가해야함 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        policy_info.ssid = "default ssid";
                    }

                    if (ap_info.isAuthorized == 1) {
                        ap_info.isAuthorized = 1;
                        policy_info.authorized = "1";
                        policy_info.ssid = "authorized ssid";
                    } else {
                        ap_info.isAuthorized = 0;
                        policy_info.authorized = "0";
                    }

                    if (atEarDB.SINGLE_BSSID_TABLE_SELECT(ap_info.bssid)) {
                        ap_info.isAuthorized = 1;
                        policy_info.authorized = "1";
                        policy_info.ssid = "authorized ssid";
                    }

                    ap_info.policyType = ReportActivity.norma_policy.POLICY;
                //    Log.e("ap_info.policyType", "::" + ap_info.policyType);
                    final Policy_Result policy_result;

                    switch (ap_info.policyType) {

                        case 0:   //none
                            policy_result = ReportActivity.norma_policy.nonData();

                            ap_info.alert = policy_result.alert;
                            ap_info.comment = policy_result.comment;
                            break;

                        case 1:    //isms
                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력

                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;
                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";
                            }
                            break;

                        case 2:    //iso
                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력
                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;
                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";
                            }
                            break;

                        case 3:    //norma
                            if (ap_info.wps == 0) {
                                policy_info.wps = "off";
                            } else {
                                policy_info.wps = "on";
                            }

                            policy_result = ReportActivity.norma_policy.resultData(policy_info); //data 가공
                            //출력
                            if (policy_result != null) {
                                ap_info.alert = policy_result.alert;
                                ap_info.comment = policy_result.comment;
                            } else {
                                ap_info.alert = "none";
                                ap_info.comment = "none";
                            }
                            break;
                    }
                }

                //Detail Setting
                if (ap_info.ssid_filter == 0) {
                    detailList.add(ap_info);
                }
                if (ap_info.isPentest != 0) {
                    pantest_List.add(ap_info);
                }

                //인가 데이터
                if (ap_info.isAuthorized == IS_TRUE) {
                    if (ap_info.ssid_filter == 0) {
                        whiteList.add(ap_info);
                    }
                } else {
                    //비인가.
                    if (ap_info.ssid_filter == 0) {
                        unAuthList.add(ap_info);
                    }
                }

                //ssid 필터된 데이터
                if (ap_info.ssid_filter == 1) {
                    ssid_filter_List.add(ap_info);
                }

                //사진이 찍힌 데이터
                if (!ap_info.picturePath.equals("none")) {
                    if (ap_info.ssid_filter != IS_TRUE) {
                        picture_filter_List.add(ap_info);
                    }
                }

                //중복mac 데이터
                if ((ap_info.risk & 0x02) == 0x02) {
                    overlap_List.add(ap_info);
                }


                ///////////graph Setting
                if (ap_info.ssid_filter != IS_TRUE) {

                    //warning
                    if (ap_info.encryption.equals("OPEN") || ap_info.encryption.equals("WEP")) {
                        warning_AP++;
                        warningAp_List.add(ap_info);
                    }
                    //not real ap
                    if (ap_info.ap_type.equals("Soft AP")) {
                        not_real_AP++;
                        notRealAp_List.add(ap_info);
                        if (ap_info.isAuthorized == 1) {
                            authSoftAp++;
                        }
                    }

                    //alert
                    switch (ap_info.alert) {
                        case "Safe":
                            alert_safety++;
                            break;
                        case "Middle":
                            alert_middle++;
                            break;
                        default:
                            alert_high++;
                            break;
                    }
                    //channel
                    chList.add(ap_info.channel);

                    //total ap
                    total_AP++;
                    // total_AP = apList.size();

                    //total
                    if (ap_info.isAuthorized == 1) {
                        authAp++;
                    } else {
                        unAuthAp++;
                    }

                    total_chList.add(ap_info.channel);

                    int enc = 0;

                    switch (ap_info.encryption) {
                        case "OPEN":
                            toOPEN++;
                            if (ap_info.isAuthorized == 1) {
                                authOPEN++;
                            } else {
                                unAuthOPEN++;
                            }
                            break;
                        case "WEP":
                            toWEP++;
                            if (ap_info.isAuthorized == 1) {
                                authWEP++;
                            } else {
                                unAuthWEP++;
                            }
                            break;
                        case "WPA":
                            toWPA++;
                            if (ap_info.isAuthorized == 1) {
                                authWPA++;
                            } else {
                                unAuthWPA++;
                            }
                            break;
                        case "WPA2":
                            toWPA2++;
                            if (ap_info.isAuthorized == 1) {
                                authWPA2++;
                            } else {
                                unAuthWPA2++;
                            }
                            break;
                        default:
                            toWPA_s++;

                            if (ap_info.isAuthorized == 1) {
                                authWPA_s++;
                            } else {
                                unAuthWPA_s++;
                            }
                            break;

                    }

                    switch (ap_info.alert) {
                        case "High":
                            high++;
                            break;
                        case "Middle":
                            middle++;
                            break;
                        case "Safe":
                            safe++;
                            break;
                        default:
                            high++;
                            break;
                    }

                    //인가
                    if (ap_info.isAuthorized == 1) {
                        auth_isSSidList.add(new GraphInfo(ap_info.ssid));

                        if (ap_info.ssid.equals(("(Hidden)"))) {
                            authHiddenSsid++;
                        }
                    } else {
                        if (ap_info.ssid.equals(("(Hidden)"))) {
                            unAuthHiddenSsid++;
                        }
                    }
                }

            }


            grayAp_List = GrayAPList();
            gray_AP = grayAp_List.size();
            unAuthGrayAp = unAuthGrayAP();

            if (detailList.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";

                detailList.add(ap_info);
            }

            if (whiteList.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";


                whiteList.add(ap_info);
            }

            if (ssid_filter_List.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";


                ssid_filter_List.add(ap_info);
            }

            if (picture_filter_List.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";

                picture_filter_List.add(ap_info);
            }

            if (overlap_List.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";

                overlap_List.add(ap_info);
            }

            if (pantest_List.size() == 0) {
                Ap_info ap_info = new Ap_info();
                ap_info.isPentest = 0;
                ap_info.pentest_password = "none";
                ap_info.alert = "none";
                // ap_info.save_time = "non";
                ap_info.bssid = "none";
                ap_info.ssid = "none";


                pantest_List.add(ap_info);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (reportPageAdapter == null) {
                reportPageAdapter = new ReportPageAdapter(getSupportFragmentManager());
            } else {
                reportPageAdapter.notifyDataSetChanged();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.dataChangeMsg), Toast.LENGTH_SHORT).show();
            }
            mViewPager.setAdapter(reportPageAdapter);
        }
    }


    private ArrayList<Ap_info> UploadApList() {

        SQLiteDatabase db = atEarDB.getWritableDatabase();
        //group by bssid order by power
        String sql = "select * from " + REPORT_TABLE;

        ArrayList<Ap_info> list = new ArrayList<>();

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {
                try {
                    Ap_info ap_info = new Ap_info();
                    ap_info.projectName = c.getString(0);
                    ap_info.locationName = c.getString(1);
                    ap_info.scan_time = c.getString(11);
                    ap_info.save_time = c.getString(2);
                    ap_info.gps = c.getString(3);
                    ap_info.bssid = c.getString(4);
                    ap_info.ssid = c.getString(5);
                    ap_info.signal = c.getInt(6);
                    ap_info.channel = c.getInt(7);
                    ap_info.encryption = c.getString(8);
                    ap_info.cipher = c.getString(9);
                    ap_info.auth = c.getString(10);
                    //ap_info.scan_time = c.getString(11);
                    ap_info.ap_type = c.getString(12);
                    ap_info.oui = c.getString(13);
                    ap_info.comment = c.getString(14);
                    ap_info.alert = c.getString(15);
                    ap_info.risk = c.getInt(16);
                    ap_info.wps = c.getInt(17);
                    ap_info.isAuthorized = c.getInt(18);
                    ap_info.ssid_filter = c.getInt(19);
                    ap_info.bssid_filter = c.getInt(20);
                    ap_info.policyType = c.getInt(21);
                    ap_info.picturePath = c.getString(22);
                    ap_info.memo = c.getString(23);
                    ap_info.isPentest = c.getInt(24);
                    ap_info.pentest_password = c.getString(25);
                    //모든데이터
                    list.add(ap_info);

                } catch (Exception e) {
                    Log.e("json error", "file save");
                }

            } //while end
        }
        c.close();
        db.close();

        return list;
    }


    private String policy_Type(int type) {

        String data;

        switch (type) {
            case 0:
                data = "none";
                break;
            case 1:
                data = "ISMS";
                break;
            case 2:
                data = "ISO 27001";
                break;
            case 3:
                data = "NORMA";
                break;
            default:
                data = "none";
                break;
        }
        return data;
    }

    //통신사 AP
    private ArrayList<Ap_info> GrayAPList() {

        ArrayList<Ap_info> list = new ArrayList<>();

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select * from " + REPORT_TABLE + " where " + SSID + " LIKE " +
                "'KT_WLAN%' or " + SSID + " LIKE 'LG U+ Router%' or " + SSID + " LIKE '%mylgnet%' or "
                + SSID + " LIKE '%U+zone%' or " + SSID + " LIKE '%U+net%' or " + SSID + " LIKE '%PocketFi2740%' or " + SSID + " LIKE '%nespot%' or "
                + SSID + " LIKE '%CNMDATA6028%' or " + SSID + " LIKE '%SO070VOIP%' or " + SSID + " LIKE 'cjwifi%' or " + SSID + " LIKE 'HelloWireless%' or "
                + SSID + " LIKE '%SK_wifi%' or " + SSID + " LIKE '%T_wifi%'";

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {
                Ap_info ap_info = new Ap_info();
                ap_info.projectName = c.getString(0);
                ap_info.locationName = c.getString(1);
                ap_info.save_time = c.getString(2);
                ap_info.gps = c.getString(3);
                ap_info.bssid = c.getString(4);
                ap_info.ssid = c.getString(5);
                ap_info.signal = c.getInt(6);
                ap_info.channel = c.getInt(7);
                ap_info.encryption = c.getString(8);
                ap_info.cipher = c.getString(9);
                ap_info.auth = c.getString(10);
                ap_info.scan_time = c.getString(11);
                ap_info.ap_type = c.getString(12);
                ap_info.oui = c.getString(13);
                ap_info.comment = c.getString(14);
                ap_info.alert = c.getString(15);
                ap_info.risk = c.getInt(16);
                ap_info.wps = c.getInt(17);
                ap_info.isAuthorized = c.getInt(18);
                ap_info.ssid_filter = c.getInt(19);
                ap_info.bssid_filter = c.getInt(20);
                ap_info.policyType = c.getInt(21);
                ap_info.picturePath = c.getString(22);
                ap_info.memo = c.getString(23);
                ap_info.isPentest = c.getInt(24);
                ap_info.pentest_password = c.getString(25);

                String ssid = ap_info.ssid;

                boolean filter = false; // 필터링 여부
                if (SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }
                    }
                }

                // ssid 필터
                if (filter == true) {
                    continue;
                }

                list.add(ap_info);
            }
        }

        c.close();
        db.close();
        return list;
    }

    //통신사 AP
    private int unAuthGrayAP() {

        ArrayList<String> list = new ArrayList<>();

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select " + SSID + " , " + AUTHORIZED + " from " + REPORT_TABLE + " where " + SSID + " LIKE " +
                "'KT_WLAN%' or " + SSID + " LIKE 'LG U+ Router%' or " + SSID + " LIKE '%mylgnet%' or "
                + SSID + " LIKE '%U+zone%' or " + SSID + " LIKE '%U+net%' or " + SSID + " LIKE '%PocketFi2740%' or " + SSID + " LIKE '%nespot%' or "
                + SSID + " LIKE '%CNMDATA6028%' or " + SSID + " LIKE '%SO070VOIP%' or " + SSID + " LIKE 'cjwifi%' or " + SSID + " LIKE 'HelloWireless%' or "
                + SSID + " LIKE '%SK_wifi%' or " + SSID + " LIKE '%T_wifi%'";

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {
            while (c.moveToNext()) {

                String ssid = c.getString(0);
                int authorized = c.getInt(1);
                boolean filter = false; // 필터링 여부
                if (SettingFilter == true) {
                    ArrayList<String> filterList = ReportActivity.filter_list;

                    for (String s : filterList) {
                        if (ssid.toLowerCase().contains(s.toLowerCase())) {
                            filter = true;
                            break;
                        }
                    }
                }
                // ssid 필터
                if (filter == true) {
                    continue;
                }

                if (authorized == 0) {
                    list.add(ssid);
                }
            }
        } else {

        }
        c.close();
        db.close();
        return list.size();
    }


    public int total_AP() {
        return total_AP;
    }

    public int warning_AP() {
        return warning_AP;
    }

    public int gray_AP() {
        return gray_AP;
    }

    public int alert_high() {
        return alert_high;
    }

    public int alert_middle() {
        return alert_middle;
    }

    public int alert_safety() {
        return alert_safety;
    }

    public int not_real_AP() {
        return not_real_AP;
    }

    public ArrayList<Integer> chList() {
        return chList;
    }

    public int authAp() {
        return authAp;
    }

    public int unAuthAp() {
        return unAuthAp;
    }

    public int high() {
        return high;
    }

    public int middle() {
        return middle;
    }

    public int safe() {
        return safe;
    }

    public int toOPEN() {
        return toOPEN;
    }

    public int toWEP() {
        return toWEP;
    }

    public int toWPA() {
        return toWPA;
    }

    public int toWPA2() {
        return toWPA2;
    }

    public int toWPA_s() {
        return toWPA_s;
    }

    public int authOPEN() {
        return authOPEN;
    }

    public int authWEP() {
        return authWEP;
    }

    public int authWPA() {
        return authWPA;
    }

    public int authWPA2() {
        return authWPA2;
    }

    public int authWPA_s() {
        return authWPA_s;
    }

    public int authSoftAp() {
        return authSoftAp;
    }

    public int authHiddenSsid() {
        return authHiddenSsid;
    }

    public int unAuthOPEN() {
        return unAuthOPEN;
    }

    public int unAuthWEP() {
        return unAuthWEP;
    }

    public int unAuthWPA() {
        return unAuthWPA;
    }

    public int unAuthWPA2() {
        return unAuthWPA2;
    }

    public int unAuthWPA_s() {
        return unAuthWPA_s;
    }

    public int unAuthHiddenSsid() {
        return unAuthHiddenSsid;
    }

    public int unAuthGrayAp() {
        return unAuthGrayAp;
    }

    public List<GraphInfo> auth_isSSidList() {
        return auth_isSSidList;
    }

    public ArrayList<Ap_info> detailList() {
        return detailList;
    }

    public ArrayList<Ap_info> whiteList() {
        return whiteList;
    }

    public ArrayList<Ap_info> warningApList() {
        return warningAp_List;
    }

    public ArrayList<Ap_info> ssid_filter_List() {
        return ssid_filter_List;
    }

    public ArrayList<Ap_info> picture_filter_List() {
        return picture_filter_List;
    }

    public ArrayList<Ap_info> overlap_List() {
        return overlap_List;
    }

    public ArrayList<Ap_info> notRealAp_List() {
        return notRealAp_List;
    }

    public ArrayList<Ap_info> grayAp_List() {
        return grayAp_List;
    }

    public ArrayList<Ap_info> pantest_List() {
        return pantest_List;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
