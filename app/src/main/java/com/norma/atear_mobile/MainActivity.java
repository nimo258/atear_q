package com.norma.atear_mobile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.authorized.AuthorizedActivity;
import com.norma.atear_mobile.authorized.data.AuthorizedInfo;
import com.norma.atear_mobile.component.LocaleChanger;
import com.norma.atear_mobile.component.SHA256Hash;
import com.norma.atear_mobile.component.Util;
import com.norma.atear_mobile.data.OpenDNS;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.ProjectActivity;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.policy.Norma_policy;
import com.norma.atear_mobile.project.policy.Policy_setting;
import com.norma.atear_mobile.report.FileScanActivity;
import com.norma.atear_mobile.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.core.app.ActivityCompat;

public class MainActivity extends Activity implements View.OnClickListener {

    private String apkUrl = "https://211.110.140.144/apk_download/";

    private Context mContext;

    private TextView tv_version;
    private Button btn_project;
    private Button btn_report;
    private Button btn_setting;
    private Button btn_policy;

    private final static String PACKAGE_NAME = "com.norma.atear_mobile";

    private final String DB_NAME = Util.oui_db;


    private static String TAG = "Vandor db Ready";
    public static Activity MainAct;


    private String path = "";
    private SharedPref sharedPref;
    private SHA256Hash sha256Hash;
    private AtEarDB atEarDB;

    public static Norma_policy norma_policy;

    private ProgressDialog progressDialog;
    private APK_DownLoadThread apk_downLoadThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findView();
        LocaleChanger.setLang(this, new SharedPref(this).getValue("language", 0));
        RemovePolicy();

        // RemoveAPK(); //update시 apk 제거

        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            String versionName = info.versionName;
            tv_version.setText("v" + versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //apk update
        // Nugat 이상이면 update.
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
            try {
                Intent intent = getIntent();

                path = intent.getStringExtra("path");

                if (!path.equals("null")) {
                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                    Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                    tv_title.setText(getResources().getString(R.string.updateTItleMsg));
                    tv_text.setText(getResources().getString(R.string.updateDialogMsg));
                    btn_yes.setText(getResources().getString(R.string.yesMsg));
                    btn_no.setText(getResources().getString(R.string.noMsg));

                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (isNetworkConnected() == true) {
                                String license = sharedPref.getValue("LINCESE", "fail");

                                apk_downLoadThread = new APK_DownLoadThread(mContext, license, sha256Hash.getHashHexString(DeviceSerial()), path);
                                apk_downLoadThread.execute();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(mContext, getResources().getString(R.string.netWorkCheckMsg), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            } catch (Exception e) {
                Log.e("pathException", "error");
            }
        }


        try { //강제 와이파이
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(true);
        } catch (Exception e) {

        }

        File normaDir = new File("/sdcard/Norma");
        if (!normaDir.exists()) {
            normaDir.mkdirs();
        }

        File authorizedDir = new File("/sdcard/Norma/Authorized");
        if (!authorizedDir.exists()) {
            authorizedDir.mkdirs();
        }

        File pictureDir = new File("/sdcard/Norma/Pictures");
        if (!pictureDir.exists()) {
            pictureDir.mkdirs();
        }

      /*  File pdfDir = new File("/sdcard/Norma/PDF");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }*/

        File excelDir = new File("/sdcard/Norma/Excel");
        if (!excelDir.exists()) {
            excelDir.mkdirs();
        }


        try {
            if (!isCheckDB()) { // DB가 없으면 복사
                copyDB(mContext);
                Log.e(TAG, "db copy ");
            } else {
                Log.e(TAG, "db ok ");

            }
            // delDBcache();
        } catch (Exception e) {

        }

        norma_policy = new Norma_policy();
        norma_policy.POLICY = sharedPref.getValue("policyType", 0);

        switch (norma_policy.POLICY) {
            case 0:
                norma_policy.non_policy();
                break;

            case 1:
                norma_policy.policy_isms_auth();
                break;

            case 2:
                norma_policy.policy_iso_auth();
                break;

            case 3:
                norma_policy.policy_norma_auth();
                break;
        }

        //open dns 설정
        OpenDNS openDNS = new OpenDNS();

        if (atEarDB.OPEN_DNS_TABLE_SELECT() < openDNS.dnsList.length) {
            for (String dns : openDNS.dnsList) {
                atEarDB.OPEN_DNS_TABLE_INSERT(dns);
            }
        }

        int sampleMacFile = sharedPref.getValue("sampleMAC", 0);
        if (sampleMacFile == 0) {
            setSampleCSV();
        }
    }

    private void findView() {

        mContext = this;
        MainAct = this;

        btn_project = (Button) findViewById(R.id.btn_project);
        btn_report = (Button) findViewById(R.id.btn_report);
        btn_setting = (Button) findViewById(R.id.btn_setting);
        btn_policy = (Button) findViewById(R.id.btn_policy);

        tv_version = (TextView) findViewById(R.id.tv_version);

        btn_project.setOnClickListener(this);
        btn_report.setOnClickListener(this);
        btn_setting.setOnClickListener(this);
        btn_policy.setOnClickListener(this);

        sha256Hash = new SHA256Hash();
        sharedPref = new SharedPref(mContext);

        atEarDB = new AtEarDB(mContext);
    }

    private void SetLanguage() {

        btn_project.setText(getResources().getString(R.string.projectMsg));
        btn_report.setText(getResources().getString(R.string.reportMsg));
        btn_policy.setText(getResources().getString(R.string.authorizedMsg));
        btn_setting.setText(getResources().getString(R.string.settingsMsg));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_project:
                Intent proIntent = new Intent(mContext, ProjectActivity.class);
                startActivity(proIntent);
                break;

            case R.id.btn_report:
                Intent rpIntent = new Intent(mContext, FileScanActivity.class);
                startActivity(rpIntent);
                break;

            case R.id.btn_setting:
                Intent stIntent = new Intent(mContext, SettingsActivity.class);
                startActivity(stIntent);
                break;

            case R.id.btn_policy:
                Intent poIntent = new Intent(mContext, AuthorizedActivity.class);
                startActivity(poIntent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SetLanguage();
    }

    //DB 버전 체크
    public boolean isVersion21() {
        int versionSDK = Build.VERSION.SDK_INT;
        return versionSDK >= 21;
    }

    public void delDBcache() {

        File dir = new File("/data/data/" + PACKAGE_NAME + "/databases/");

        try {
            for (File file : dir.listFiles()) {
                if (file.getName().equals(DB_NAME)) {
                    Log.e("cacheFile", file.getName());
                }
            }
        } catch (Exception e) {

        }
    }


    // DB가 있나 체크하기
    public boolean isCheckDB() {

        String filePath;
        //   if (isVersion21()) {
        //       filePath = "/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        //   } else {
        filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        //    }

        //filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        File file = new File(filePath);

        return file.exists();

    }

    // DB를 복사하기
    // assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
    public void copyDB(Context mContext) {
        AssetManager manager = mContext.getAssets();
        String folderPath;
        String filePath;

        //if (isVersion21()) {
        //	folderPath = "/data/" + PACKAGE_NAME + "/databases";
        //	filePath = "/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        //	} else {
        folderPath = "/data/data/" + PACKAGE_NAME + "/databases";
        filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        //}
        //folderPath = "/data/data/" + PACKAGE_NAME + "/databases";
        //filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;


        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open("db/" + DB_NAME);
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            } else {
                folder.mkdirs();
            }

         /*   if (file.exists()) {
               // file.delete();
               // file.createNewFile();
            }*/

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        } catch (IOException e) {
            Log.e("ErrorMessage : ", e.getMessage());
        }
    }

    private class APK_DownLoadThread extends AsyncTask<Void, Void, Void> {
        private StringBuilder sb;
        private String license_number = "null";
        private String client_hash = "null";
        private String version = "null";
        private String path = "";
        private int responseCode = 0;


        private Context mContext;


        private APK_DownLoadThread(Context mContext, String license_number, String client_hash, String path) {

            this.mContext = mContext;
            this.license_number = license_number;
            this.client_hash = client_hash;
            this.path = path;

            String folderPath = "/sdcard/Norma/Apk";
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = new ProgressDialog(mContext);
                        progressDialog.setMessage(getResources().getString(R.string.fileDownloadMsg));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        mHandler.sendEmptyMessageDelayed(0, 1000);
                    }
                });
            } catch (Exception e) {

            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                Thread.sleep(500);


                String updateUrl = apkUrl + path;
                URL url = new URL(updateUrl);

                //https
                TrustAllHosts();
                HttpsURLConnection httpURLCon = (HttpsURLConnection) url.openConnection();
                httpURLCon.setConnectTimeout(3000);
                httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("license_number", license_number);
                jsonObject.put("client_hash", client_hash);


                OutputStream os = httpURLCon.getOutputStream();
                os.write(jsonObject.toString().getBytes());
                os.flush();

                responseCode = httpURLCon.getResponseCode();

                sb = new StringBuilder();
                Log.e("httpCode_Tag", String.valueOf(responseCode));

                //"sdcard/Norma/Apk"
                //com.norma.atear_mobile

                if (responseCode == 200) {

                    File file = new File("/sdcard/Norma/Apk/AtEarMobile.apk");

                    if (file.exists()) {
                        file.delete();
                    }

                    FileOutputStream fos = new FileOutputStream(file);
                    InputStream is = httpURLCon.getInputStream();
                    int bufferLength = 0;
                    int downloadedSize = 0;
                    byte[] tmpByte = new byte[1024];

                    while ((bufferLength = is.read(tmpByte)) > 0) {
                        Log.e("bufferLength", String.valueOf(bufferLength));
                        fos.write(tmpByte, 0, bufferLength);
                        downloadedSize += bufferLength;
                    }

                    is.close();
                    fos.close();
                    httpURLCon.disconnect();

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                responseCode = 403;
            } catch (JSONException e) {
                e.printStackTrace();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();

                }
            } catch (Exception e) {

            }

            if (responseCode == 403) {
                Toast.makeText(mContext, getResources().getString(R.string.netWorkCheckMsg), Toast.LENGTH_SHORT).show();
            }
            /*
            if (responseCode == 200) {
               // Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();


            } else {
             //   Toast.makeText(mContext, "failed.", Toast.LENGTH_LONG).show();
            }*/
        }


    }

    private static void TrustAllHosts() {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0: //다운로드 thread
                    if (apk_downLoadThread.getStatus() == AsyncTask.Status.FINISHED) {

                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());

                        File apkFile = new File("/sdcard/Norma/Apk/AtEarMobile.apk");
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.content.Intent.ACTION_VIEW);

                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                        startActivity(intent);

                    } else {
                        mHandler.sendEmptyMessageDelayed(0, 500);
                    }
                    break;

                default:
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();

                        }
                    } catch (Exception e) {

                    }
                    break;
            }
        }
    };


    private static String DeviceSerial() {
        try {
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception ignored) {
            return null;
        }

    }

    //check network
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    //sdcard 정책파일 삭제
    private void RemovePolicy() {

        String[] filePath = {"/sdcard/Norma/Policy/policy_isms_auth.txt", "/sdcard/Norma/Policy/policy_isms_not_auth.txt", "/sdcard/Norma/Policy/policy_iso_auth.txt",
                "/sdcard/Norma/Policy/policy_iso_not_auth.txt", "/sdcard/Norma/Policy/policy_norma_auth.txt", "/sdcard/Norma/Policy/policy_norma_not_auth.txt"};

        for (String path : filePath) {
            File file = new File(path);

            if (file.exists() == true) {
                file.delete();
            }
        }

        File dir = new File("/sdcard/Norma/Policy");

        if (dir.exists() == true) {
            dir.delete();
            Log.e("MainActivity", "RemovePolicy");
        }

        File newDir = new File("/data/data/" + PACKAGE_NAME + "/Policy");
        if (!newDir.exists()) {
            newDir.mkdirs();

            Policy_setting policy_setting = new Policy_setting(mContext);
            policy_setting.start();
        }

    }

    boolean setSampleCSV() {
        // ProgressDialog progressDialog;
        String[] sampleBssidList = {"00:00:00:00:00:01", "00:00:00:00:00:02", "00:00:00:00:00:03", "00:00:00:00:00:04", "00:00:00:00:00:05"};
        File  file = new File("/sdcard/Norma/Authorized/SampleMac.csv");
        if (file.exists()) {
            file.delete();
        }
        try {
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(file), "MS949"));

            for (int i = 0; i < sampleBssidList.length; i++) {
                bw.write(sampleBssidList[i] + "\r\n");
            }
            bw.close();
            sharedPref.put("sampleMAC", 1);
            Log.e("sampleFIle", "OK..!");
            return true;
        } catch (Exception e) {
            Log.e("sampleFIle", String.valueOf(e));
        }
        return false;
    }


}
