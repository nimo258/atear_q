package com.norma.atear_mobile.component;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.StringDef;
import java.util.Locale;

/**
 * Created by norma_android on 2017-10-24.
 */

public class LocaleChanger {
    public final static String EN="en",KO="ko",JA="ja",ZH="zh";

    @StringDef(value = {EN,KO,JA,ZH})
    public @interface Lang{}
    public static void setLang(Context ctx, @Lang String lang){
        Configuration config = new Configuration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        ctx.getResources().updateConfiguration(config, ((Activity)ctx).getBaseContext().getResources().getDisplayMetrics());
    }

    public static void setLang(Context ctx,int lang){
        Configuration config = new Configuration();
        Locale locale = new Locale(parseInt(lang));
        Locale.setDefault(locale);
        config.locale = locale;
        ctx.getResources().updateConfiguration(config, ((Activity)ctx).getBaseContext().getResources().getDisplayMetrics());
    }
    private static String parseInt(int lang){
        return 3==lang?ZH:2==lang?JA:1==lang?KO:EN;
    }
}
