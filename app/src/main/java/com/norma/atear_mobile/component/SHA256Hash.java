package com.norma.atear_mobile.component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by 효진 on 2016-09-05.
 */
public class SHA256Hash {

    private static final String ALGORITHM = "SHA-256";

    public byte[] getHash(byte[] input) {
        try {
            MessageDigest md = MessageDigest.getInstance(ALGORITHM);
            return md.digest(input);
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
            return null;
        }
    }

    public byte[] getHash(InputStream input) throws IOException {
        try {
            MessageDigest md = MessageDigest.getInstance(ALGORITHM);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = input.read(buffer)) != -1) {
                md.update(buffer, 0, read);
            }
            return md.digest();
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
            return null;
        }
    }

    public byte[] getHash(File file) throws IOException {
        byte[] hash = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            hash = getHash(bis);
        } finally {
            if (bis != null)
                try {
                    bis.close();
                } catch (IOException ie) {
                }
        }
        return hash;
    }

    public String getHashHexString(byte[] input) {
        byte[] hash = getHash(input);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            sb.append(Integer.toString((hash[i] & 0xf0) >> 4, 16));
            sb.append(Integer.toString(hash[i] & 0x0f, 16));
        }
        return sb.toString();
    }

    public String getHashHexString(String input) {
        return getHashHexString(input.getBytes());
    }

    public String getHashHexString(String input, String charsetName)
            throws UnsupportedEncodingException {
        return getHashHexString(input.getBytes(charsetName));
    }

    public String getHashHexString(InputStream input) throws IOException {
        byte[] hash = getHash(input);
        StringBuffer sb = new StringBuffer(hash.length * 2);
        for (int i = 0; i < hash.length; i++) {
            sb.append(Integer.toString((hash[i] & 0xf0) >> 4, 16));
            sb.append(Integer.toString(hash[i] & 0x0f, 16));
        }
        return sb.toString();
    }

    public String getHashHexString(File file) throws IOException {
        byte[] hash = getHash(file);
        StringBuffer sb = new StringBuffer(hash.length * 2);
        for (int i = 0; i < hash.length; i++) {
            sb.append(Integer.toString((hash[i] & 0xf0) >> 4, 16));
            sb.append(Integer.toString(hash[i] & 0x0f, 16));
        }
        return sb.toString();
    }

}