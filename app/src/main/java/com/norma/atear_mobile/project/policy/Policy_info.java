package com.norma.atear_mobile.project.policy;

/**
 * Created by hyojin on 7/11/16.
 */
public class Policy_info {

	public String enc = "";

	public String ssid = "";
	//인가된
	public String authorized = "";
	//ap
	public String type = "";

	public String wps = "";

	public Policy_info() {
		super();
	}
	@Override
	public int hashCode() {
		final  int prime = 31;
		int result = 1;
		result = prime * result + ((enc.equals("")) ? 0: enc.hashCode())
				+((ssid.equals("")) ? 0: ssid.hashCode())
				+((authorized.equals("")) ? 0: authorized.hashCode())
				+((type.equals("")) ? 0: type.hashCode())
				+((wps.equals("")) ? 0: wps.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
		{
			return true;
		}
		// make sure o can be cast to this class
		if (obj == null || obj.getClass() != getClass())
		{
			// cannot cast
			return false;
		}

		Policy_info e = (Policy_info) obj;
		return this.enc.equals(e.enc)&&this.ssid.equals(e.ssid)&&this.authorized.equals(e.authorized)
				&&this.type.equals(e.type)&&this.wps.equals(e.wps);

	}
}
