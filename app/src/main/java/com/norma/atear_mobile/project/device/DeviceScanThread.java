package com.norma.atear_mobile.project.device;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.text.format.Formatter;
import android.util.Log;

import com.norma.atear_mobile.component.Util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import jcifs.netbios.NbtAddress;

/**
 * Created by hyojin on 7/14/16.
 */
public class DeviceScanThread extends AsyncTask<Void, Void, Void> {

    private String hostName;
    private String myIp;
    private String myMac;
    private String myVendorName;
    private String myDeviceName;
    private String sub_mac;
    private String tempWifiName;
    private String wifiName;
    private String BSSID;

    public static String result_InternetIP;
    public static String result_ISPName;

    private int readIpListSize;
    private int readIpSize;
    private int gateway;
    private int progress = 0;

    private String ipAddress; // 192.168.19.1
    private static String connectedIP; // 192.168.19

    public ArrayList<String> readIpList = new ArrayList<String>();
    public static ArrayList<DeviceInfo> arDeviceInfo;

    private SQLiteDatabase db_oui;

    private DhcpInfo dhcp;
    private WifiManager wifiMgr;

    public ConnectivityManager connectivityManager = null;

    CountDownLatch latch1 = new CountDownLatch(1);
    static CountDownLatch latch3;

    private Context mContext;


    public DeviceScanThread(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<DeviceInfo> getDeviceInfo() {
        return arDeviceInfo;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        arDeviceInfo = new ArrayList<DeviceInfo>();

        wifiName = new String();
        tempWifiName = new String();

        connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        readIpList.clear();


        // WIFI INFO
        wifiMgr = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        dhcp = wifiMgr.getDhcpInfo();
        gateway = dhcp.gateway;
      //  getInternetIP(); // DeviceInfoActivity에서 쓸 내용인데, 시간이 걸려서 앞으로 뺌.
        // IP INFO
        ipAddress = Formatter.formatIpAddress(gateway); // 192.168.19.1
        // subnet 계샨
        connectedIP = ipAddress.substring(0, ipAddress.lastIndexOf(".")); // 192.168.19
        // SETTING WIFI NAME
        tempWifiName = wifiInfo.getSSID(); // "NORMA"
        int wifiNameLength = tempWifiName.length();
        wifiName = tempWifiName.replaceAll("\"", ""); // NORMA

        myIp = Formatter.formatIpAddress(wifiMgr.getConnectionInfo() // 192.168.19.10
                .getIpAddress());
        myMac = wifiInfo.getMacAddress().toUpperCase();
        myVendorName = Build.MANUFACTURER.toUpperCase();
        myDeviceName = "MY DEVICE";
    }

    @Override
    protected Void doInBackground(Void... params) {

        if (!wifiName.equals("<unknown ssid>")) {
            InetAddress inetAddress;
            // SEND PING
            // subnet prifix계산해서 i < 255 이거 수정
            for (int i = 0; i < 255; i++) {
                progress = i;
                String Ip = connectedIP + "." + String.valueOf(i + 1);
                try {
                    inetAddress = InetAddress.getByName(Ip);
                    if (inetAddress.isReachable(10) == true) {
                    }
                    if (i == 253) {
                    }
                } catch (UnknownHostException e) {
                } catch (IOException e) {
                } catch (IllegalArgumentException e) {
                }
            }
            readArpCache();
        }

        latch1.countDown();

        readIpSize = readIpList.size();
        latch3 = new CountDownLatch(readIpSize + 1); // Mydevice 포함헤야함!
        // Insert My Device Info

        DeviceInfo myDevice = new DeviceInfo();
        myDevice.ip = myIp;
        myDevice.mac = myMac;
        myDevice.deviceName = myDeviceName;
        myDevice.vendorName = myVendorName;


        arDeviceInfo.add(myDevice);

        latch3.countDown();

        try {
            BSSID = getMacFromArpCache(ipAddress);

            latch1.await();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        wifiMgr = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        wifiName = wifiInfo.getSSID();
        Log.e("size:::", String.valueOf(readIpSize));

        readIpListSize = readIpList.size();
        if (!(readIpList.size() == 0)) {
            for (int i = 0; i < readIpList.size(); i++) {

                final String Ip = readIpList.get(i);
                String Input_vendor = null;
                try {
                    final String tempMac = getMacFromArpCache(Ip).toUpperCase();
                    if (!tempMac.equals(null)) {
                        sub_mac = tempMac.substring(0, 8).toUpperCase();
                        Input_vendor = getVendor(sub_mac);

                    }
                    //hostName
                    NbtAddress addr = NbtAddress.getByName(Ip);
                    hostName = addr.getHostName();

                    if (hostName.contains("�") || hostName.contains("Â")) {
                        hostName = "Expressless Characters";
                    }
                    //hostName end;

                    DeviceInfo deviceInfo = new DeviceInfo();
                    deviceInfo.ip = Ip;
                    deviceInfo.mac = tempMac;
                    deviceInfo.deviceName = hostName;
                    deviceInfo.vendorName = Input_vendor;
                    arDeviceInfo.add(deviceInfo);

                } catch (NullPointerException e) {
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }


            }
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

    }

    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);
    }

    public String getMacFromArpCache(String ip) {
        if (ip == null)
            return null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        if (mac.matches("00:00:00:00:00:00")) {
                            break;
                        } else {
                            return mac;
                        }
                    } else {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void readArpCache() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && !splitted[3].equals("00:00:00:00:00:00")) {
                    String ip = splitted[0];
                    readIpList.add(ip);
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        readIpList.remove(0);
    }


    public void getInternetIP() {
        String address_getIP = "http://ifcfg.me/ip";
        String address_getISP = "http://ifcfg.me/isp";

        result_InternetIP = HttpConnect.getResponseFromUrl(address_getIP);
        if (result_InternetIP.length() > 30)
            result_InternetIP = "Unknown";
        result_ISPName = HttpConnect.getResponseFromUrl(address_getISP);

    }

    // Get Vendor name (app_oui database)
    public String getVendor(String sub_mac) {
        // sub_mac : 00:00:00

        //sub_mac = tempMac.substring(0, 8).toUpperCase(); //mac 에서 0 8까지 받아와서 함수 호출.

        String tempMac = sub_mac;
        String vendorName = "";
        Cursor cursor;
        // app_oui DB open
        db_oui = mContext.openOrCreateDatabase(Util.oui_db, Context.MODE_PRIVATE, null);
        // Read with SQL command
        cursor = db_oui.rawQuery("SELECT vendor FROM app_oui " + "Where submac='" + tempMac + "'", null);
        if (cursor.moveToFirst()) {
            vendorName = cursor.getString(cursor.getColumnIndex("vendor")); // n행
        } else {
            vendorName = "";
        }
        // DB&Cursor close
        cursor.close();
        db_oui.close();
        return vendorName;
    }

}
