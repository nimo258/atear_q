package com.norma.atear_mobile.project.device;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hyojin on 7/14/16.
 */
public class DeviceInfo implements Parcelable {

    int icon;
    public String ip = "";
    public String mac = "";
    public String deviceName = "";
    public String vendorName = "";
    public String date;

    public DeviceInfo() {

    }

    public String getIP() {
        return ip;
    }


    public DeviceInfo(Parcel in) {
        // TODO Auto-generated constructor stub
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DeviceInfo createFromParcel(Parcel in) {
            return new DeviceInfo(in);
        }

        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub

    }
}
