package com.norma.atear_mobile.project.comparator;

import com.norma.atear_mobile.report.data.FileInfo;

import java.util.Comparator;

/**
 * Created by hyojin on 8/10/16.
 */
public class FileComparator  implements Comparator<FileInfo> {
    @Override
    public int compare(FileInfo a1, FileInfo a2) {
        return a1.name.compareToIgnoreCase(a2.name);
    }
}
