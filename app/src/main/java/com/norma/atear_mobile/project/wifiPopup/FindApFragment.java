package com.norma.atear_mobile.project.wifiPopup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.Util;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.ProjectActivity;
import com.norma.atear_mobile.project.SharedPref;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by hyojin on 7/27/16.
 */
public class FindApFragment extends Fragment implements SurfaceHolder.Callback {

    private View v;
    //camera
    public static SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private static Camera mCamera;

    private String projectName;
    private String locationName;

    private AtEarDB atEarDB;

    private static TextView tv_camera;
    private Button btn_save;
    private RelativeLayout linear_camera;
    //find ap
    List<ScanResult> mScanResult;
    ScanResult scanResult;
    WifiManager wifiManger;
    DbmDialogAsync dbmda;

    String bssid = new String();
    String ssid = new String();

    int rssi;
    boolean toggle = false;
    int ding;
    int soundPlay;
    int connectChecker;

    Context mContext;


    SoundPool soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
    Vibrator vibe;
    TextView tv_rssi;
    TextView tv_not_detected;


    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    ProgressBar progressBar;
    // Spinner spin_mac;

    // EditText edit_add_mac;
    // Button btn_add_mac;

    long[] highPattern = {0, 100, 100};
    long[] normalPattern = {0, 100, 400};
    long[] lowPattern = {0, 100, 900};


    SharedPref sharedPref;
    int language = 0;

    String pictureBase64;


    public FindApFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }

        v = inflater.inflate(R.layout.fragment_findap, container, false);


        findView();


        tv_camera.setText(mContext.getResources().getString(R.string.cameraMsg));

        projectName = sharedPref.getValue("projectName", "return");
        locationName = sharedPref.getValue("locationName", "return");

        //camera
        try {
            mHolder = mSurfaceView.getHolder();
            mHolder.addCallback(this);
        } catch (Exception e) {

        }

        //find ap
        bssid = "";
        ssid = "";
        Intent intent = getActivity().getIntent();
        bssid = intent.getStringExtra("bssid");

        wifiManger = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        wifiManger.startScan();
        mScanResult = wifiManger.getScanResults();

        arrayList = new ArrayList<String>();

        for (int i = 0; i < mScanResult.size(); i++) {
            scanResult = mScanResult.get(i);
            arrayList.add(scanResult.BSSID.toUpperCase());
        }

        arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item,
                arrayList);

        vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        ding = soundPool.load(mContext, R.raw.beepdoub, 1);

        tv_not_detected.setVisibility(View.INVISIBLE);


        progressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggle) {
                    toggle = false;

                } else {
                    toggle = true;
                    dbmda = new DbmDialogAsync();
                    dbmda.execute();
                }
            }
        });


        linear_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSurfaceView.getVisibility() == View.VISIBLE) {
                    mSurfaceView.setVisibility(View.INVISIBLE);
                    tv_camera.setVisibility(View.VISIBLE);
                    mCamera.release();
                    mCamera = null;
                } else {
                    mSurfaceView.setVisibility(View.VISIBLE);
                    tv_camera.setVisibility(View.INVISIBLE);
                }
            }
        });

        //save
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSurfaceView.getVisibility() == View.VISIBLE) {
                    if (mCamera != null) {
                        mCamera.takePicture(null, null, takePicture);
                    }
                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.camera_saveMsg), Toast.LENGTH_SHORT).show();
                }

            }
        });


        return v;
    }

    private void findView() {
        mContext = getContext();
        tv_rssi = (TextView) v.findViewById(R.id.tv_rssi);
        tv_not_detected = (TextView) v.findViewById(R.id.tv_not_detected);
        btn_save = (Button) v.findViewById(R.id.btn_save);

        tv_camera = (TextView) v.findViewById(R.id.tv_camera);
        linear_camera = (RelativeLayout) v.findViewById(R.id.linear_camera);
        mSurfaceView = (SurfaceView) v.findViewById(R.id.surfaceView);

        atEarDB = new AtEarDB(mContext);
        sharedPref = new SharedPref(mContext);
    }


    public static void CameraUI() {
        if (mSurfaceView.getVisibility() == View.VISIBLE) {
            mSurfaceView.setVisibility(View.INVISIBLE);
            tv_camera.setVisibility(View.VISIBLE);
            if (mCamera != null) {
                mCamera.release();
                mCamera = null;
            }

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (mCamera == null)
                mCamera = Camera.open();

            if (mCamera != null) {
                mCamera.setDisplayOrientation(displayCamera());  //카메라 가로모드


                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setRotation(90);
                // parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

                mCamera.setParameters(parameters);

                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();

            }
        } catch (Exception e) {
            mCamera.release();
            mCamera = null;
        }
    }

    private int displayCamera() {
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int result = (90 - degrees + 360) % 360;
        return result;
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera == null) {
            try {
                if (mCamera == null)
                    mCamera = Camera.open();

                if (mCamera != null) {
                    mCamera.setDisplayOrientation(displayCamera());  //카메라 가로모드


                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setRotation(90);
                    // parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

                    mCamera.setParameters(parameters);

                    mCamera.setPreviewDisplay(holder);
                    mCamera.startPreview();

                }
            } catch (Exception e) {
                mCamera.release();
                mCamera = null;
            }
        }

        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(width, height);

        // List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        // if (sizes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
        //     parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        //}

        // Camera.Size cs = sizes.get(0);
        //parameters.setPreviewSize(width, height);
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // mCamera.stopPreview();
        // mCamera.release();
        // mCamera = null;
    }

    Camera.PictureCallback takePicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            if (data != null) {

                mCamera.startPreview();

                //앱이 실행되는 동안만 메모리에 잠시 저장 ,앱이끝나면 자동삭제
                FileOutputStream outStream = null;
                String captureImage = String.format("/sdcard/Norma/Pictures/%d.jpg",
                        System.currentTimeMillis());

                try {
                    outStream = new FileOutputStream(captureImage);
                    outStream.write(data);
                    outStream.close();

                    BitmapFactory.Options options = new BitmapFactory.Options(); //bitmap -> imageView
                    options.inSampleSize = 4; //이미지 품질 디폴트 , ex)2 = 1/2, 4 =1/4

                    Bitmap bitmap = BitmapFactory.decodeFile(captureImage, options);
                    if (bitmap.getWidth() > bitmap.getHeight()) {
                        bitmap = rotate(bitmap, 90); //image 90 회전
                    }
                    captureImageDelete(captureImage); //기존 파일 삭제

                    bitmap = Bitmap.createScaledBitmap(bitmap, 1200, 1300, true);

                    //파일 저장.
                    OutputStream out = null;
                    final String pictureName = "/sdcard/Norma/Pictures/" + projectName + "_" + locationName + "_" + ssid + "_" + today() + ".png";

                    final File pictureFile = new File(pictureName);

                    if (pictureFile.exists()) {
                        pictureFile.delete();
                    }

                    pictureFile.createNewFile();
                    out = new FileOutputStream(pictureFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

                    //memo Popup

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_memo);
                    //  dialog.setTitle("Memo save");

                    ImageView iv_img = (ImageView) dialog.findViewById(R.id.iv_img);
                    final EditText edt_memo = (EditText) dialog.findViewById(R.id.edt_memo);
                    Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
                    Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_editTitle = (TextView) dialog.findViewById(R.id.tv_editTitle);

                    tv_title.setText(mContext.getResources().getString(R.string.memoTitleMsg));
                    tv_editTitle.setText(mContext.getResources().getString(R.string.memoEditMsg));
                    btn_save.setText(mContext.getResources().getString(R.string.btnSaveMsg));
                    btn_cancel.setText(mContext.getResources().getString(R.string.btnCancelMsg));

                    iv_img.setImageBitmap(bitmap);

                    pictureBase64 = Util.encodeToBase64(bitmap);

                    edt_memo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(edt_memo.getWindowToken(), 0);

                            }

                            return true;
                        }
                    });

                    btn_save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String value = edt_memo.getText().toString();
                            if (value.equals("")) {
                                value = "none";
                            }
                            ProjectActivity.picture_hashMap.put(bssid, pictureBase64);

                            //test code
                            try {
                                File file = new File("/sdcard/aa.txt");
                                if (file.exists()) {
                                    file.delete();
                                }
                                FileOutputStream fos = new FileOutputStream(file);
                                fos.write(pictureBase64.toString().getBytes());

                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            //test code
                            atEarDB.PACKET_TABLE_UPDATE_MEMO(bssid, pictureName, value);
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.pictureSaveMsg), Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                            getActivity().finish();
                        }
                    });

                    btn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pictureFile.exists()) {
                                pictureFile.delete();
                            }
                            dialog.cancel();
                        }
                    });

                    dialog.show();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    };
    
    //find ap
    public class DbmDialogAsync extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            // TODO Auto-generated method stub
            rssi = -100;
            connectChecker = 0;
            wifiManger.startScan();
            mScanResult = wifiManger.getScanResults();

//            Log.e("testttt11","rssi : "+rssi);

            for (int i = 0; i < mScanResult.size(); i++) {
                scanResult = mScanResult.get(i);
                if (scanResult.BSSID.toUpperCase().equals(bssid.toUpperCase())) {
                    rssi = scanResult.level;
                    ssid = scanResult.SSID;
                    connectChecker = 1;
                    Log.w("testttt",ssid+" :: "+rssi);
                    break;
                } else
                    continue;
            }

            Log.e("testttt2","rssi : "+rssi);
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(100 + rssi);
                    }
                });


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (connectChecker > 0) {
                            tv_not_detected.setVisibility(View.INVISIBLE);
                            tv_rssi.setText(rssi + " DBM");
                            Log.e("testtt2","rssi : "+rssi);

                        } else {
                            tv_not_detected.setVisibility(View.VISIBLE);
                            tv_rssi.setText("Not detected");

                            tv_not_detected.setText(mContext.getResources().getString(R.string.detectionMsg));
                        }
                    }
                });

                AudioManager clsAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                switch (clsAudioManager.getRingerMode()) {
                    case AudioManager.RINGER_MODE_NORMAL:
                        // 소리 모드
                        if (rssi > -50) {
                            soundPlay = soundPool.play(ding, 1, 1, 0, -1, 2);
                            vibe.vibrate(highPattern, 1);
                        } else if (rssi > -75) {
                            soundPlay = soundPool.play(ding, 1, 1, 0, -1, 1);
                            vibe.vibrate(normalPattern, 1);
                        } else if (rssi > -100) {
                            soundPlay = soundPool.play(ding, 1, 1, 0, -1, (float) 0.5);
                            vibe.vibrate(lowPattern, 1);
                        } else { // Not detected
                            soundPool.stop(soundPlay);
                            vibe.cancel();
                        }
                        break;
                    case AudioManager.RINGER_MODE_VIBRATE:
                        // 진동 모드
                        if (rssi > -50) {
                            vibe.vibrate(highPattern, 1);
                        } else if (rssi > -75) {
                            vibe.vibrate(normalPattern, 1);
                        } else if (rssi > -100) {
                            vibe.vibrate(lowPattern, 1);
                        } else {
                            vibe.cancel();
                        }
                        break;

                    case AudioManager.RINGER_MODE_SILENT:
                        // 무음 모드
                        break;
                }

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (soundPool != null) {
                soundPool.stop(soundPlay);
            }
            vibe.cancel();
            toggle = false;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            Handler handler = new Handler();

            if (toggle == true) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        soundPool.stop(soundPlay);
                        vibe.cancel();
                        DbmDialogAsync dbmDialogAsync = new DbmDialogAsync();
                        dbmDialogAsync.execute();
                    }
                }, 100);//0.2sec
            } else {
                tv_not_detected.setText("");
                soundPool.stop(soundPlay);
                vibe.cancel();
                Log.e("end", "End");
            }
        }

    }


    private void captureImageDelete(String path) {

        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }


    // image 방향 회전
    public Bitmap rotate(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees);
            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != converted) {
                    bitmap = null;
                    bitmap = converted;
                    converted = null;
                }
            } catch (OutOfMemoryError ex) {
                //Toast.makeText(getApplicationContext(), "메모리부족", 0).show();
            }
        }
        return bitmap;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().finish();
        // mCamera.stopPreview();
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
        //mCamera = null;
        soundPool.stop(soundPlay);
        vibe.cancel();
        toggle = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        soundPool.stop(soundPlay);
        vibe.cancel();
        toggle = false;

        //


    }

    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH) + 1;
        int day = today.get(Calendar.DAY_OF_MONTH);
        int hour = today.get(Calendar.HOUR_OF_DAY);
        int minute = today.get(Calendar.MINUTE);
        int second = today.get(Calendar.SECOND);
        String result = String.valueOf(year) + "-" + String.valueOf(month) + "-"
                + String.valueOf(day) + "_" + String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second);
        //2016-12-08-11h8m30s.jng
        return result;
    }
}
