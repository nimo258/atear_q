package com.norma.atear_mobile.project.wifiPopup;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.adapter.WifiPopupAdapter;


/**
 * Created by hyojin on 7/13/16.
 */
public class WifiPopupActivity extends FragmentActivity {

    private  WifiPopupAdapter wifiPopupAdapter;

    private  ViewPager mViewPager;

    private Button btn_apinfo;
    private Button btn_findap;
    private Button btn_stationinfo;
    private LinearLayout btn_back;

    private Context mContext;
    private SharedPref sharedPref;

    private TextView tv_wifi_popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_popup);

        findView();


        NotificationManager nm =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(1245);
        tv_wifi_popup.setText(mContext.getResources().getString(R.string.apInfoMsg));

        wifiPopupAdapter = new WifiPopupAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(wifiPopupAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tv_wifi_popup.setText(mContext.getResources().getString(R.string.apInfoMsg));
                        FindApFragment.CameraUI();
                        break;
                    case 1:
                        tv_wifi_popup.setText(mContext.getResources().getString(R.string.foundAPMsg));
                        break;
                    case 2:
                        tv_wifi_popup.setText(mContext.getResources().getString(R.string.stationInfoMsg));
                        FindApFragment.CameraUI();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btn_apinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
                // tv_wifi_popup.setText("AP Info");
            }
        });


        btn_findap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
                // tv_wifi_popup.setText("Find AP");
            }
        });

        btn_stationinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);
                // tv_wifi_popup.setText("Station Info");
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }

    private void findView() {

        mContext = this;

        mViewPager = (ViewPager)findViewById(R.id.Pager);

        btn_back = (LinearLayout)findViewById(R.id.btn_back);
        btn_apinfo = (Button)findViewById(R.id.btn_apinfo);
        btn_findap = (Button)findViewById(R.id.btn_findap);
        btn_stationinfo = (Button)findViewById(R.id.btn_stationinfo);
        tv_wifi_popup = (TextView)findViewById(R.id.tv_wifi_popup);

        sharedPref = new SharedPref(mContext);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }
}

