package com.norma.atear_mobile.project.wifiPopup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.BuildConfig;
import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Device_HashMap;
import com.norma.atear_mobile.project.ProjectActivity;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.device.DeviceAdapter;
import com.norma.atear_mobile.project.device.DeviceInfo;
import com.norma.atear_mobile.project.device.DeviceScanThread;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/27/16.
 */
public class StationFragment extends Fragment {

    private View v;
    private Context mContext;

    private TextView tv_wifi;
    private TextView tv_total;
    private Button btn_deviceScan;

    private ListView lv_device;

    private LinearLayout linear_button;
    private RelativeLayout linear_total;

    private ProgressDialog progressDialog;
    private WifiManager wifiManager;

    private ArrayList<DeviceInfo> deviceList;
    private DeviceAdapter deviceAdapter;

    private DeviceScanThread deviceScanThread;

    private String bssid ="";
    private boolean isDevice = false;

    SharedPref sharedPref;
    int language = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        v = inflater.inflate(R.layout.fragment_device, container, false);

        findView();



        tv_wifi.setText(mContext.getResources().getString(R.string.station_connectMsg));

        Intent intent = getActivity().getIntent();

        bssid = intent.getStringExtra("bssid");

        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        String wifibssid = wifiInfo.getBSSID();
        String gatewayIp = Formatter.formatIpAddress(wifiInfo.getIpAddress());

        if ( wifibssid != null&&!gatewayIp.equals("0.0.0.0")) { // current ap
            if ( wifibssid.toUpperCase().contains(bssid)) {
                tv_wifi.setVisibility(View.GONE);
            }else{
                btn_deviceScan.setVisibility(View.GONE);
            }
        } else {
            btn_deviceScan.setVisibility(View.GONE);
        }



    /*    //if(ApInfoFragment.wifiConnect){
        //    tv_wifi.setVisibility(View.GONE);

        //}else{
        btn_deviceScan.setVisibility(View.GONE);
        // }
*/
        btn_deviceScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>28){
                    Toast.makeText(getContext(),getContext().getResources().getString(R.string.deprecatedFunction),Toast.LENGTH_SHORT).show();
                    return;
                }
                linear_button.setVisibility(View.GONE);
                linear_total.setVisibility(View.VISIBLE);
                lv_device.setVisibility(View.VISIBLE);

                if (!isDevice) {
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {

                    }
                    try {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog = new ProgressDialog(mContext);
                                progressDialog.setMessage(mContext.getResources().getString(R.string.station_progressMsg));
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                mHandler.sendEmptyMessageDelayed(1, 1000);
                            }
                        });
                    } catch (Exception e) {

                    }
                    deviceScanThread = new DeviceScanThread(mContext);
                    deviceScanThread.execute();
                    isDevice = true;

                } else {
                   // Toast.makeText(mContext, "실행중", Toast.LENGTH_SHORT).show();
                }

            }
        });


        return v;
    }

    private void findView() {

        mContext = getContext();

        tv_wifi = (TextView) v.findViewById(R.id.tv_wifi);
        tv_total = (TextView) v.findViewById(R.id.tv_total);

        btn_deviceScan = (Button) v.findViewById(R.id.btn_deviceScan);

        lv_device = (ListView) v.findViewById(R.id.lv_device);
        linear_button = (LinearLayout) v.findViewById(R.id.linear_button);
        linear_total = (RelativeLayout) v.findViewById(R.id.linear_total);

        sharedPref = new SharedPref(mContext);
    }



    @Override
    public void onStop() {
        super.onStop();
        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:

                    break;
                case 1:
                    try {
                        if (deviceScanThread.getStatus() == AsyncTask.Status.FINISHED) {

                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();

                            }

                            deviceList = deviceScanThread.getDeviceInfo();

                            //형식
                            // {
                            //      stations :  [
                            //          {
                            //              ip : 0.0.0.0 ,
                            //              mac : aa ,
                            //              hostname : name
                            //          } ,
                            //          {
                            //              ip : 1.1.1.1,
                            //              mac : bb,
                            //              hostname : name2
                            //          }
                            //      ]
                            // }
                            Device_HashMap device_hashMap = new Device_HashMap();
                            for (DeviceInfo device : deviceList) {
                                device_hashMap.put(device.ip, device);
                            }
                            ProjectActivity.ap_hashMap.put(bssid, device_hashMap);

                            Log.e("device size ", String.valueOf(ProjectActivity.ap_hashMap.size()));

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    deviceAdapter = new DeviceAdapter(mContext, deviceList);
                                    lv_device.setAdapter(deviceAdapter);
                                }
                            });
                            tv_total.setText("Total : " + deviceList.size());


                            isDevice = false;
                        } else {
                            mHandler.sendEmptyMessageDelayed(1, 1500);
                        }
                    } catch (Exception e) {

                    }

                default:


                    break;
            }
        }
    };
}
