package com.norma.atear_mobile.project.policy;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.norma.atear_mobile.project.SharedPref;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by hyojin on 7/21/16.
 */
public class Policy_setting extends Thread {

    private int count = 0;

    private final static String PACKAGE_NAME = "com.norma.atear_mobile";

    private String[] filePath = {"/data/data/" + PACKAGE_NAME + "/Policy/policy_isms_auth.txt", "/data/data/" + PACKAGE_NAME + "/Policy/policy_isms_not_auth.txt", "/data/data/" + PACKAGE_NAME + "/Policy/policy_iso_auth.txt",
            "/data/data/" + PACKAGE_NAME + "/Policy/policy_iso_not_auth.txt", "/data/data/" + PACKAGE_NAME + "/Policy/policy_norma_auth.txt", "/data/data/" + PACKAGE_NAME + "/Policy/policy_norma_not_auth.txt"};


//    private String[] filePath = {"/sdcard/Norma/Policy/policy_isms_auth.txt", "/sdcard/Norma/Policy/policy_isms_not_auth.txt", "/sdcard/Norma/Policy/policy_iso_auth.txt",
    //           "/sdcard/Norma/Policy/policy_iso_not_auth.txt", "/sdcard/Norma/Policy/policy_norma_auth.txt", "/sdcard/Norma/Policy/policy_norma_not_auth.txt"};

    private String[] assetsPath;

    private String[] assetsKoreaPath = {"json/policy_isms_auth.txt", "json/policy_isms_not_auth.txt", "json/policy_iso_auth.txt",
            "json/policy_iso_not_auth.txt", "json/policy_norma_auth.txt", "json/policy_norma_not_auth.txt"};

    private String[] assetsEnglishPath = {"json/policy_isms_auth_English.txt", "json/policy_isms_not_auth_English.txt", "json/policy_iso_auth_English.txt",
            "json/policy_iso_not_auth_English.txt", "json/policy_norma_auth_English.txt", "json/policy_norma_not_auth_English.txt"};

    private String[] assetsJapanesePath = {"json/policy_isms_auth_Japanese.txt", "json/policy_isms_not_auth_Japanese.txt", "json/policy_iso_auth_Japanese.txt",
            "json/policy_iso_not_auth_Japanese.txt", "json/policy_norma_auth_Japanese.txt", "json/policy_norma_not_auth_Japanese.txt"};

    private String[] assetsChinesePath = {"json/policy_isms_auth_Chinese.txt", "json/policy_isms_not_auth_Chinese.txt", "json/policy_iso_auth_Chinese.txt",
            "json/policy_iso_not_auth_Chinese.txt", "json/policy_norma_auth_Chinese.txt", "json/policy_norma_not_auth_Chinese.txt"};

    private Context mContext;
    private SharedPref sharedPref;

    String successMsg = "";
    String failMsg = "";

    public Policy_setting(Context mContext) {
        super();
        this.mContext = mContext;
        sharedPref = new SharedPref(mContext);
    }

    @Override
    public void run() {
        super.run();

 /*       File normaDir = new File("/sdcard/Norma");
        if (!normaDir.exists()) {
            normaDir.mkdirs();
        }*/

        File dir = new File("/data/fdata/" + PACKAGE_NAME + "/Policy");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        for (int i = 0; i < filePath.length; i++) {
            File json_file = new File(filePath[i]);

            if (json_file.exists()) {
                json_file.delete();
            }

            int language = sharedPref.getValue("language", 0);

            switch (language) {
                case 0: //영어
                    assetsPath = assetsEnglishPath;

                    successMsg = "Policy setting of English";
                    failMsg = "failed";
                    break;
                case 1: // 한국어
                    assetsPath = assetsKoreaPath;

                    successMsg = "한국어 정책 설정";
                    failMsg = "실패";
                    break;

                case 2: //일어
                    assetsPath = assetsJapanesePath;

                    successMsg = "日本語のポリシー設定";
                    failMsg = "失敗";
                    break;

                case 3: // 중국어
                    assetsPath = assetsChinesePath;

                    successMsg = "设置中文政策";
                    failMsg = "失败";
                    break;
                default:
                    assetsPath = assetsEnglishPath;

                    successMsg = "Policy setting of English";
                    failMsg = "failed";
                    break;
            }

            try {
                InputStream is = mContext.getAssets().open(assetsPath[i]);

                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                String text = new String(buffer);

                FileOutputStream fos = new FileOutputStream(json_file);
                fos.write(text.toString().getBytes());
                fos.close();
                count++;
            } catch (Exception e) {

            }


        }
        mHandler.sendEmptyMessageDelayed(0, 1000);

    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    if (count == 0) {
                        Toast.makeText(mContext, failMsg, Toast.LENGTH_SHORT).show();
                        Log.e("jsonFile_Create ", "not create");
                    } else {
                        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
                        Log.e("jsonFile_Create ", "create !!");
                    }
                    break;

                default:
                    break;
            }
        }
    };


}
