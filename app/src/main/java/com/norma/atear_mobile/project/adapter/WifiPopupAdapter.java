package com.norma.atear_mobile.project.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.norma.atear_mobile.project.wifiPopup.ApInfoFragment;
import com.norma.atear_mobile.project.wifiPopup.FindApFragment;
import com.norma.atear_mobile.project.wifiPopup.StationFragment;

/**
 * Created by hyojin on 7/27/16.
 */
public class WifiPopupAdapter  extends FragmentPagerAdapter {


    public WifiPopupAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ApInfoFragment();
            case 1:
               return new FindApFragment();
            case 2:
               return new StationFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
