package com.norma.atear_mobile.project.device;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by hyojin on 7/14/16.
 */
public class HttpConnect {
    public static String getResponseFromUrl(String url) {
        String result = "";

        try {
            URL url_ = new URL(url);
            HttpURLConnection con = (HttpURLConnection) url_.openConnection();
            con.setConnectTimeout(5000);
            con.setUseCaches(false);
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                for (;;) {
                    String line = br.readLine();
                    if (line == null)
                        break;
                    result += line;
                }
                br.close();
            }
            con.disconnect();
        } catch (Exception e) {
        }

        return result;
    }

    public static String postResponseFromUrl(String url, String post) {
        String result = "";

        try {
            URL url_ = new URL(url);
            HttpURLConnection con = (HttpURLConnection) url_.openConnection();
            con.setRequestMethod("POST");
            con.setConnectTimeout(5000);
            con.setUseCaches(false);
            con.setDoOutput(true);

            PrintWriter pw = new PrintWriter(new OutputStreamWriter(
                    con.getOutputStream()));
            pw.write(post);
            pw.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            for (;;) {
                String line = br.readLine();
                if (line == null)
                    break;
                result += line;
            }
            br.close();
            pw.close();

            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void postToUrl(String url, String post) {
        String result = "";

        try {
            URL url_ = new URL(url);
            HttpURLConnection con = (HttpURLConnection) url_.openConnection();
            con.setRequestMethod("POST");
            con.setConnectTimeout(5000);
            con.setUseCaches(false);
            con.setDoOutput(true);

            PrintWriter pw = new PrintWriter(new OutputStreamWriter(
                    con.getOutputStream()));
            pw.write(post);
            pw.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            for (;;) {
                String line = br.readLine();
                if (line == null)
                    break;
                result += line;
            }

            br.close();
            pw.close();

            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
