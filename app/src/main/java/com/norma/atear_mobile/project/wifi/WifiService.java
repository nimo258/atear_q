package com.norma.atear_mobile.project.wifi;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.norma.atear_mobile.MainActivity;
import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.Util;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.comparator.AlertComparator;
import com.norma.atear_mobile.project.comparator.SignalComparator;
import com.norma.atear_mobile.project.policy.Policy_Result;
import com.norma.atear_mobile.project.policy.Policy_info;
import com.norma.atear_mobile.utils.Dot11axInfo;
import com.norma.atear_mobile.utils.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;


/**
 * Created by hyojin on 2017-03-28.
 */

public class WifiService extends Service {

    public static boolean AtEar_WIFI = false; //scan main Thread
    public static boolean wifi_running = false;  //wifi 일시정지 여부 ture = scan중 , false = 일시정지

    //wifi
    public Ap_info ap_info;
    private WifiManager wifiManager;
    private WifiInfo wifiInfo;

    //db
    private AtEarDB atEarDB;
    private SQLiteDatabase db_oui;
    private static final String PACKET_TABLE = "packet_table";

    public static String currentBssid = ""; //연결된 bssid

    //running
    private static WifiThread wifiThread;
    private Context mContext;


    public static boolean degree = false; // false = 신호세기 ,  ture = 위험도  sort
    public static boolean auth = true;  // ture = 인가 뿌리기  , false 안뿌리기
    public static boolean unAuth = true;  //ture =비인가  뿌리기  , false 안뿌리기

    private String string_kill_message = "";
    private int language = 0;

    SharedPref sharedPref;

    @Override
    public void onCreate() {
        super.onCreate();
        findView();
        atEarDB.PACKET_TABLE_DELETE();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AtEar_WIFI = true;
        if (wifiThread == null) {
            wifiThread = new WifiThread();
            wifiThread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void findView() {
        mContext = getApplicationContext();
        atEarDB = new AtEarDB(mContext);

        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        sharedPref = new SharedPref(mContext);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        AtEar_WIFI = false;
        try {
            wifiThread = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Atear WIFI TAG", "onDestroy");

        NotificationManager nm =
                (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        nm.cancelAll();


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        //  NotificationManager nm =
        //         (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        //  nm.cancelAll();
        Log.e("Atear WIFI TAG", "onTaskRemoved");
        Toast.makeText(mContext, mContext.getResources().getString(R.string.shutdown_msg), Toast.LENGTH_LONG).show();
        stopSelf();

    }

    class WifiThread extends Thread {


        public WifiThread() {
            super();
        }

        @Override
        public void run() {
            super.run();

            while (AtEar_WIFI) {

                ArrayList<Ap_info> ap_list = new ArrayList<>();

                /*ap_list = null;
                ap_list = new ArrayList<>();*/
                if (!wifi_running) {
                    Log.e("AtearWIFI_SCAN_TAG", "no run");
                    //wifiManager.startScan();

                    try {
                        sleep(2500);
                    } catch (InterruptedException e) {

                    }
                } else {
                    Log.e("Atear WIFI_SCAN_TAG", "running......");
                    wifiInfo = wifiManager.getConnectionInfo();
                    try {
                        currentBssid = wifiInfo.getBSSID();
                        if (Formatter.formatIpAddress(wifiInfo.getIpAddress()).equals("0.0.0.0")) {
                            currentBssid = "";
                        }
                    } catch (Exception e) {
                        currentBssid = "";
                    }

                    wifiManager.startScan();

                    //netID가 존재 할때 hidden 에 중복mac 처리용..
                    StringBuffer hiddenBssid = new StringBuffer();

                    if (wifiManager.getScanResults() != null) {

                        //main scan
                        for (ScanResult sr_list : wifiManager.getScanResults()) {

                            Ap_info ap_info = new Ap_info();

                            String ssid = sr_list.SSID;
                            String bssid = sr_list.BSSID;
                            int signal = sr_list.level;
                            int channel = sr_list.frequency;
                            int risk = 0;

                            ap_info.risk = risk;
                            String encryption;
                            String apType;
                            Policy_info policy_info = new Policy_info();
                            // soft mac check
                            // 02, 03, 06, 07, 0A, 0B, 0E, 0F
                            int tempInteger = 0;
                            String tempString = bssid.toUpperCase().substring(0, 2);
                            tempString = tempString.substring(1).toUpperCase();
                            if (tempString.equals("2") || tempString.equals("3") || tempString.equals("6")
                                    || tempString.equals("7") || tempString.equals("A") || tempString.equals("B")
                                    || tempString.equals("E") || tempString.equals("F")) {
                                // ap_info.risk |= 0x20;
                                // Log.e("ssid", ssid+"     "+bssid);
                                apType = "Soft AP";
                                policy_info.type = "Soft AP";
                            } else {
                                apType = "ap";
                                policy_info.type = "AP";
                            }

                            String capabilities = sr_list.capabilities;
//                            Log.e("WF","["+ssid+"] signal: "+ signal); //RSSI
//                            Log.e("WF","["+ssid+"] capa: "+ capabilities); //암호화타입
                            ArrayList<String> capaDataArrayList = new ArrayList<String>();
                            String[] capabiltitiesArray = capabilities.split("]");
                            String[] capaTempDataArray;

                            Log.e("NORMA","SSID: "+ssid+", capabilities: "+capabilities);
                            int wps;

                            //암호화 필터 재가공
                            for (int j = 0; j < capabiltitiesArray.length; j++) {
                                capabiltitiesArray[j] = capabiltitiesArray[j].replace("[", "");
                                capaTempDataArray = capabiltitiesArray[j].split("-");
                                for (int k = 0; k < capaTempDataArray.length; k++) {
                                    capaDataArrayList.add(capaTempDataArray[k]);
//                                    Log.e("NORMA",capaTempDataArray[k]);
                                }
                            }
//                            Logger.error(capaDataArrayLis);
                            if (capaDataArrayList.contains("PSK") && capaDataArrayList.contains("WPA")) {
                                encryption = "WPA/WPA2";
                                policy_info.enc = "wpa2";
                            } else if (capaDataArrayList.contains("PSK") && capaDataArrayList.contains("PSK+SAE")) {
                                encryption = "WPA2 & 3";
                                policy_info.enc = "wpa2&3";
                            } else if (capaDataArrayList.contains("SAE")) {
                                encryption = "WPA3";
                                policy_info.enc = "wpa3";
                            } else if (capaDataArrayList.contains("EAP")) {
                                encryption = "EAP_WPA2";
                                policy_info.enc = "eap_wpa2";
                            } else if (capaDataArrayList.contains("WPA2")) {
                                encryption = "WPA2";
                                policy_info.enc = "wpa2";
                            } else if (capaDataArrayList.contains("WPA")) {
                                encryption = "WPA";
                                policy_info.enc = "wpa";
                            } else if (capaDataArrayList.contains("WEP")) {
                                encryption = "WEP";
                                policy_info.enc = "wep";
                            } else {
                                encryption = "OPEN";
                                policy_info.enc = "open";
                            }
                            //eap
                            if (capaDataArrayList.contains("EAP")) {
                                ap_info.risk |= 0x64;
                                //   Log.e("eapInfo",  ssid);
                            }
                            try {
                                ap_info.is11axAp = Dot11axInfo.is11ax(sr_list);
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                               /* if (capaDataArrayList.contains("IBSS"))
                                    apType = "ad_hoc";
                                else if (capaDataArrayList.contains("BSS"))
                                    apType = "station";
                                else if (capaDataArrayList.contains("ESS"))
                                    apType = "ap";
                                else
                                    apType = "unknown";*/

                            /*    if ((ap_info.risk & 0x20) == 0x20) {
                                    apType = "Soft AP";
                                    policy_info.type = "Soft AP";
                                } else {
                                    apType = "ap";
                                    policy_info.type = "AP";
                                }*/

                            //wps check;
                            if (capaDataArrayList.contains("WPS")) {
                                wps = 1;
                            } else {
                                wps = 0;
                            }

                            if (!ssid.equals("")) {
                                ap_info.ssid = ssid;
                            } else {
                                ap_info.ssid = "(Hidden)";
                                policy_info.ssid = "hidden ssid";
                                hiddenBssid.append(bssid + ",");
                            }

                            ap_info.bssid = bssid.toUpperCase();
                            ap_info.signal = signal;
                            ap_info.encryption = encryption;
                            ap_info.ap_type = apType;
                            ap_info.wps = wps;
                            ap_info.scan_time = today();
                            ap_info.channel = convertFrequencyToChannel(channel);
                            ap_info.bssid_filter = 0;
                            ap_info.ssid_filter = 0;
                            ap_info.picturePath = "none";
                            ap_info.memo = "none";
                            ap_info.isPentest = 0;
                            ap_info.pentest_password = "none";
                            ap_info.auth = " ";
                            ap_info.cipher = " ";

                            String sub_mac = ap_info.bssid.substring(0, 8).toUpperCase();
                            ap_info.oui = getVendor(sub_mac);
                            //////////////////////////// 정책 /////////////////////////////////////////////////////////////////////////////


                            if (atEarDB.SSID_TABLE_SELECT(ap_info.ssid)) {
                                policy_info.ssid = "authorized ssid";
                            } else {  //일단 다 노말 나중에 default  스패셜 추가해야함 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                policy_info.ssid = "default ssid";
                            }

                            //사용자가 지정한 인가.
                            if (atEarDB.BSSID_TABLE_SELECT(ap_info.bssid)) {
                                ap_info.isAuthorized = 1;
                                policy_info.authorized = "1";
                                policy_info.ssid = "authorized ssid";
                            } else {
                                ap_info.isAuthorized = 0;
                                policy_info.authorized = "0";
                            }
                            //파일로 읽어 들인 인가.
                            if (atEarDB.SINGLE_BSSID_TABLE_SELECT(ap_info.bssid)) {
                                ap_info.isAuthorized = 1;
                                policy_info.authorized = "1";
                                policy_info.ssid = "authorized ssid";
                            }
                            // Log.e("policy_test", policy_info.ssid + " , " + policy_info.type + " , " + policy_info.authorized + " , " + policy_info.enc+ ","+policy_info.hashCode());

                            final Policy_Result policy_result;

                            ap_info.policyType = MainActivity.norma_policy.POLICY;
//                            Log.e("policy","type:"+ap_info.policyType);
                            switch (ap_info.policyType) {

                                case 0:   //none
                                    policy_result = MainActivity.norma_policy.nonData();

                                    ap_info.alert = policy_result.alert;
                                    ap_info.comment = policy_result.comment;
                                    break;

                                case 1:    //isms
                                    policy_result = MainActivity.norma_policy.resultData(policy_info); //data 가공
                                    //출력

                                    if (policy_result != null) {
                                        ap_info.alert = policy_result.alert;
                                        ap_info.comment = policy_result.comment;
                                    } else {
                                        ap_info.alert = "none";
                                        ap_info.comment = "none";
                                    }
                                    break;

                                case 2:    //iso
                                    policy_result = MainActivity.norma_policy.resultData(policy_info); //data 가공
                                    //출력
                                    if (policy_result != null) {
                                        ap_info.alert = policy_result.alert;
                                        ap_info.comment = policy_result.comment;
                                    } else {
                                        ap_info.alert = "none";
                                        ap_info.comment = "none";
                                    }
                                    break;

                                case 3:    //norma
                                    if (ap_info.wps == 0) {
                                        policy_info.wps = "off";
                                    } else {
                                        policy_info.wps = "on";
                                    }

                                    policy_result = MainActivity.norma_policy.resultData(policy_info); //data 가공
                                    //출력
                                    if (policy_result != null) {
                                        ap_info.alert = policy_result.alert;
                                        ap_info.comment = policy_result.comment;
                                    } else {
                                        ap_info.alert = "none";
                                        ap_info.comment = "none";
                                    }
                                    break;
                            }

                            switch (ap_info.alert) {
                                case "High":
                                    ap_info.risk |= 0x04;
                                    break;
                                case "Middle":
                                    ap_info.risk |= 0x08;
                                    break;
                                case "Safe":
                                    ap_info.risk |= 0x10;
                                    break;
                                default:
                                    ap_info.risk |= 0x04;
                                    break;
                            }

                            ////////////////////////////////////////////////////////////////////////////
                            // risk check
                            // 중복 mac    10 0x02 2

                            // 정책 위험   100 0x04 4
                            // 정책 중간  1000 0x08 8
                            // 정책 안전 10000 0x10 16

                            // softMac  100000 0x20 32
                            // 접속 기록 1000000 0x40 64

                            //EAP인증 방식 10000000 0x80 128
                            //0x64  eap

                            boolean dupHiddenSSID = true;

                            //중복mac
                            int arrayCount = 0;
                            for (Ap_info wifiListResult : ap_list) {
                                if (ap_info.bssid.equalsIgnoreCase(wifiListResult.bssid)) { // duplicated
                                    // MAC
                                    if (hiddenBssid.toString().contains(bssid) && (
                                            ap_info.ssid.toUpperCase().contains("HIDDEN") || wifiListResult.ssid.toUpperCase().contains("HIDDEN"))) {
                                        //hidden 이면서 중복mac 일 경우
                                        if (ap_info.ssid.toUpperCase().contains("HIDDEN"))
                                            dupHiddenSSID = false;
                                    } else {
                                        ap_info.risk |= 0x02;
                                        ap_list.get(arrayCount).risk |= 0x02;
                                        atEarDB.PACKET_TABLE_UPDATE_RISK(ap_list.get(arrayCount));
                                    }
                                }
                                arrayCount++;

                            }


                            if (hiddenBssid.toString().contains(bssid) && !dupHiddenSSID) {
                                //연결된 hidden 이면서 중복mac 일 경우
                            } else {
                                atEarDB.PACKET_TABLE_UPDATE(ap_info);
                            }
                            //filter
                            boolean isfilter = false;

                            if (WifiActivity.filter_list != null) {
                                if (WifiActivity.filter_list.size() > 0) {
                                    for (String s : WifiActivity.filter_list) {

                                        if (ssid.toLowerCase().contains(s.toLowerCase())) {
                                            isfilter = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (isfilter) {
                                atEarDB.PACKET_TABLE_UPDATE_SSIDFITTLER(ap_info.bssid);
                                Log.e("filter", ssid);
                                //	continue;
                            }

                            if (!auth && ap_info.isAuthorized == 1 || isfilter) {
                                //인가 된거 제외
                            } else if (!unAuth && ap_info.isAuthorized == 0 || isfilter) {
                                //비인가 된거 제외
                            } else {
                                if (hiddenBssid.toString().contains(bssid) && !dupHiddenSSID) {// hidden이고 중복mac 일경우 list 제외
                                    Log.e("hidden21", bssid + "  " + "  " + ap_info.bssid);
                                } else {
                                    ap_list.add(ap_info);
                                }

                            }
                        }// scan end


                        //sort
                        if (!degree) {
                            Collections.sort(ap_list, new SignalComparator());
                            Collections.reverse(ap_list);
                        } else {
                            Collections.sort(ap_list, new AlertComparator());
                            Collections.reverse(ap_list);
                        }


                        try {
                            if (!currentBssid.equals("")) {

                                //연결된 건 젤 위로.
                                for (Ap_info result : ap_list) {
                                    if (result.bssid.equals(currentBssid.toUpperCase())) {
                                        ap_list.remove(result);
                                        ap_list.add(0, result);

                                        break;
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }

                        Ap_info.viewApList = ap_list;
                        try {
                            sleep(2500);
                        } catch (InterruptedException e) {

                        }
                    }
                }
            }
        }
    }


    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-

        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String hour = String.valueOf(today.get(Calendar.HOUR_OF_DAY));
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        String minute = String.valueOf(today.get(Calendar.MINUTE));
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        String second = String.valueOf(today.get(Calendar.SECOND));
        if (second.length() == 1) {
            second = "0" + second;
        }
        String result = year + "-" + month + "-"
                + day + " " + hour + ":" + minute + ":" + second;


        return result;
    }

    //vendeor
    public String getVendor(String sub_mac) {
        // sub_mac : 00:00:00

        //private SQLiteDatabase db_oui;
        //sub_mac = tempMac.substring(0, 8).toUpperCase(); //mac 에서 0 8까지 받아와서 함수 호출.

        String tempMac = sub_mac;
        String vendorName = "";
        Cursor cursor;
        // app_oui DB open
        db_oui = openOrCreateDatabase(Util.oui_db, Context.MODE_PRIVATE, null);

        // Read with SQL command

        try {
            cursor = db_oui.rawQuery("SELECT vendor FROM app_oui " + "Where submac='" + tempMac + "'", null);

            if (cursor.moveToFirst()) {
                vendorName = cursor.getString(cursor.getColumnIndex("vendor")); // n행
            } else {
                vendorName = "";
            }
            // DB&Cursor close
            cursor.close();
            db_oui.close();
        } catch (Exception e) {
            vendorName = "";
        }
        return vendorName;
    }

    //channel
    public static int convertFrequencyToChannel(int freq) {
        if (freq >= 2412 && freq <= 2484) {
            if (freq == 2484)
                return (freq - 2412) / 5;
            return (freq - 2412) / 5 + 1;
        } else if (freq >= 5170 && freq <= 5825) {
            return (freq - 5170) / 5 + 34;
        } else {
            return -1;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
