package com.norma.atear_mobile.project.service;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import com.norma.atear_mobile.db.AtEarDB;

import org.apache.http.conn.util.InetAddressUtils;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by hyojin on 7/14/16.
 */
public class DNSThread extends Thread {

    private ArrayList<String> defaultDNS, googleDNS;
    private Context mContext;
    private WifiManager wifiManager;
    public  boolean dnsToggle = false;

    private String gateIp;
    private String gateSsid;
    private String gateMac;
    private WifiInfo info = null;

    private DhcpInfo dhcp;
    private String tempGateIp;
    private String tempGateMac;
    private String tempGateSsid;
    private String bssid;

    private AtEarDB atEarDB;

    public DNSThread(Context context, String bssid) {
        this.mContext = context;
        this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        this.info = this.wifiManager.getConnectionInfo();
        this.dnsToggle = true;
        this.gateSsid = null;
        this.gateIp = null;
        this.gateMac = null;
        this.bssid = bssid;
        atEarDB = new AtEarDB(context);
    }


    @Override
    public void run() {
        //	Log.e("wifi dns service on", "yes!");
        // Log.e("DNS is on", "as");
        dhcp = wifiManager.getDhcpInfo();
        tempGateIp = Formatter.formatIpAddress(dhcp.gateway);
        tempGateMac = getMacFromArpCache(tempGateIp);
        tempGateSsid = info.getSSID();


        if (gateIp == null || gateMac == null || gateSsid == null) { // connecting
            gateIp = tempGateIp;
            gateMac = tempGateMac;
            gateSsid = tempGateSsid;
            checkDNSSpoofing(gateIp, gateMac, gateSsid);

        } else {
            if (gateIp.equals(tempGateIp) && gateMac.equals(tempGateMac)) { // ip
                // same,
                // mac
                // same
                gateIp = tempGateIp;
                gateMac = tempGateMac;
                gateSsid = tempGateSsid;
                dnsToggle = false;
            } else if (gateIp.equals(tempGateIp) && !gateMac.equals(tempGateMac)) { // ip
                // same,
                // mac
                // dif
                gateIp = tempGateIp;
                gateMac = tempGateMac;
                gateSsid = tempGateSsid;
                checkDNSSpoofing(gateIp, gateMac, gateSsid);
            } else if (!gateIp.equals(tempGateIp) && !gateMac.equals(tempGateMac)) { // ip
                // dif,
                // mac
                // dif

                if (tempGateIp == null || tempGateMac == null || tempGateSsid == null) { // temporal
                    // disconnect
                    gateIp = tempGateIp;
                    gateMac = tempGateMac;
                    gateSsid = tempGateSsid;
                    dnsToggle = false;
                } else { // wifi handoff
                    gateIp = tempGateIp;
                    gateMac = tempGateMac;
                    gateSsid = tempGateSsid;
                    checkDNSSpoofing(gateIp, gateMac, gateSsid);
                }
            } else { // ip dif, mac same
                dnsToggle = false;
            }
        }


    }


    private void checkDNSSpoofing(String ip, String mac, String ssid) {

        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        String dns1 = intToIp(dhcpInfo.dns1);
        String dns2 = intToIp(dhcpInfo.dns2);

        defaultDNS = new ArrayList<String>();
        googleDNS = new ArrayList<String>();

        if (dns1.equals("") || dns1 == null || dns1.equals("0.0.0.0")) {
            //Log.e("DNSThread_TAG", "dns1 null");
        } else {
            Log.e("dns1", dns1);
            // Ap's DNS server
            try {
                InetAddress inetAddress[] = InetAddress.getAllByName("naver.com");
                for (InetAddress tempInet : inetAddress) {
                    if (!tempInet.isLoopbackAddress() && InetAddressUtils.isIPv4Address(tempInet.getHostAddress())) {
                        defaultDNS.add(tempInet.getHostAddress());
                    }
                }
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                Log.e("inetAddress excep", "");
            }

            // google's DNS
            resolveDomain("naver.com", "8.8.8.8");

            ArrayList<String> tempDnsList = new ArrayList<String>();
            tempDnsList = (ArrayList<String>) defaultDNS.clone();

            if (tempDnsList.size() > 0 && googleDNS.size() > 0) {
                for (String tempIP : defaultDNS) {
                    Log.e("DNSThread_TAG", "google dns: " + googleDNS + "   ,   tempDNS: " + tempIP);

                    if (googleDNS.contains(tempIP)) {

                        tempDnsList.remove(tempIP);
                        googleDNS.remove(tempIP);

                    } else {
                        // dns spoofing
                        dnsPopup(ip, mac, ssid);
                        break;
                    }
                }

                // if (googleDNS.size() != 0 && tempDnsList.size() == 0) {
                // // dns spoofing
                // dnsPopup(ip, mac, ssid);
                // }
            }else{

            }
            dnsToggle = false;
        }
    }

    private void dnsPopup(String ip, String mac, String ssid) {

        long now = System.currentTimeMillis();

        Date date = new Date(now);

        SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일");
        SimpleDateFormat CurTimeFormat = new SimpleDateFormat("HH시 mm분 ss초");

        String strCurDate = CurDateFormat.format(date);
        String strCurTime = CurTimeFormat.format(date);
        String strNow = strCurDate + " " + strCurTime;

        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        String dns1 = intToIp(dhcpInfo.dns1);


        atEarDB.CONNECT_AP_TABLE_UPDATE_SPOOFING(bssid, 2, dns1, mac, strNow);
        dnsToggle =false;

        //}
    }

    private void resolveDomain(String domain, String serverName) {
        // String hostName = "";
        Lookup lookup;

        try {
            lookup = new Lookup(domain);
            Resolver resolver = new SimpleResolver(serverName);
            lookup.setResolver(resolver);
            Record recs[] = lookup.run();
            if (recs == null) {
                // error
                //	Log.e("DNSThread_TAG", "googleDNS null");
            }
            for (Record rec : recs) {
                googleDNS.add(rec.rdataToString());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // Log.e("resolve DNS exception", "");

        }

        // if (hostName.endsWith(".")) {
        // hostName = hostName.substring(0, hostName.length() - 1);
        // }
    }

    private String getMacFromArpCache(String ip) {
        if (ip == null)
            return null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        if (mac.matches("00:00:00:00:00:00")) {
                            break;
                        } else {
                            return mac;
                        }
                    } else {
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    private String intToIp(int i) {
        return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
    }


}
