package com.norma.atear_mobile.project.comparator;

import com.norma.atear_mobile.data.Ap_info;

import java.util.Comparator;



/**
 * Created by hyojin on 7/8/16.
 */
public class SsidComparator implements Comparator<Ap_info> {

	@Override
	public int compare(Ap_info a1, Ap_info a2) {
		return a1.ssid.compareToIgnoreCase(a2.ssid);
	}
}
