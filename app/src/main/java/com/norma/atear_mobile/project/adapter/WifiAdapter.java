package com.norma.atear_mobile.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.project.wifiPopup.WifiPopupActivity;
import com.norma.atear_mobile.utils.Logger;

import java.util.ArrayList;


/**
 * Created by hyojin on 7/8/16.
 */
public class WifiAdapter extends BaseAdapter {


    private ViewHolder holder;

    private ArrayList<Ap_info> ap_list;
    private Context mContext;

    public WifiAdapter(Context mContext, ArrayList<Ap_info> ap_list) {
        this.mContext = mContext;
        this.ap_list = ap_list;

    }

    @Override
    public int getCount() {
        return ap_list.size();
    }

    @Override
    public Object getItem(int position) {
        return ap_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();


            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.wifi_list_item, null);

            holder.tv_ssid = (TextView) convertView.findViewById(R.id.tv_ssid);
            holder.tv_bssid = (TextView) convertView.findViewById(R.id.tv_bssid);
            holder.tv_ch = (TextView) convertView.findViewById(R.id.tv_ch);
            holder.tv_wps = (TextView) convertView.findViewById(R.id.tv_wps);

            holder.iv_signal = (ImageView) convertView.findViewById(R.id.iv_signal);
            holder.tv_enc = (TextView) convertView.findViewById(R.id.tv_enc);
            holder.iv_alert = (ImageView) convertView.findViewById(R.id.iv_alert);
            holder.iv_lock = (ImageView) convertView.findViewById(R.id.iv_lock);
            holder.tv_signal = (TextView) convertView.findViewById(R.id.tv_signal);
            holder.tv_duplicate = (TextView) convertView.findViewById(R.id.tv_duplicate);
            holder.tv_axAp = (TextView) convertView.findViewById(R.id.tv_axAp);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Ap_info data = ap_list.get(position);

        holder.tv_ssid.setText(data.ssid);
        holder.tv_bssid.setText("[" + data.bssid + "]");
        holder.tv_ch.setText("[ch:" + String.valueOf(data.channel) + "]");
//        holder.tv_axAp.setVisibility(data.is11axAp?View.VISIBLE:View.GONE);
        if (data.wps == 1) {
            holder.tv_wps.setText("[WPS]");
        } else {
            holder.tv_wps.setText("");
        }


        //  holder.tv_signal.setText(String.valueOf(data.signal));
//		holder.tv_enc.setText(data.encyption);

        String enc = data.encryption;
        if (data.encryption.equals("WPA/WPA2")) {
            enc = "WPA2";
        }
        holder.tv_enc.setText("[" + enc + "]");

        if (enc.equals("OPEN")) {
            //   holder.iv_lock.setImageDrawable(null);
            holder.iv_lock.setVisibility(View.INVISIBLE);
        } else {
            //   holder.iv_lock.setImageDrawable(null);
            //  holder.iv_lock.setImageResource(R.drawable.scan_lock);
            holder.iv_lock.setVisibility(View.VISIBLE);
        }

        //신호세기
        holder.tv_signal.setText(String.valueOf(data.signal));
        if (data.signal < -75) {
            //  holder.iv_signal.setImageDrawable(null);
            holder.iv_signal.setImageResource(R.drawable.img_least_signal);
        } else if (data.signal < -50) {
            // holder.iv_signal.setImageDrawable(null);
            holder.iv_signal.setImageResource(R.drawable.img_low_signal);
        } else if (data.signal < -25) {
            //   holder.iv_signal.setImageDrawable(null);
            holder.iv_signal.setImageResource(R.drawable.img_high_signal);
        } else {
            // holder.iv_signal.setImageDrawable(null);
            holder.iv_signal.setImageResource(R.drawable.img_highest_signal);
        }


        // risk check
        // 중복 mac    10 0x02 2

        // 정책 위험   100 0x04 4
        // 정책 중간  1000 0x08 8
        // 정책 안전 10000 0x10 16

        // softMac  100000 0x20 32
        // 접속 기록 1000000 0x40 64

        if ((data.risk & 0x10) == 0x10) {
            //       holder.iv_alert.setImageDrawable(null);
            holder.iv_alert.setImageResource(R.drawable.icon_green);
        } else if ((data.risk & 0x08) == 0x08) {
            //    holder.iv_alert.setImageDrawable(null);
            holder.iv_alert.setImageResource(R.drawable.icon_yellow);
        } else if ((data.risk & 0x04) == 0x04) {
            //    holder.iv_alert.setImageDrawable(null);
            holder.iv_alert.setImageResource(R.drawable.icon_red);
        } else {
            //   holder.iv_alert.setImageDrawable(null);
            holder.iv_alert.setImageResource(R.drawable.icon_yellow);
        }

        if (data.alert.equals("none")) {
            holder.iv_alert.setVisibility(View.INVISIBLE);
        } else {
            holder.iv_alert.setVisibility(View.VISIBLE);
        }

        /*if ((data.risk & 0x02) == 0x02) {
            holder.tv_bssid.setTextColor(Color.RED);
        } else {
            holder.tv_bssid.setTextColor(0xFF798997);
        }*/


        //인가 AP 표시
        if (data.isAuthorized == 1) {
            holder.tv_ssid.setTextColor(0xFF69E085);
            holder.tv_bssid.setTextColor(0xFF69E085);
            holder.tv_ch.setTextColor(0xFF69E085);
            holder.tv_wps.setTextColor(0xFF69E085);
        } else {
            holder.tv_ssid.setTextColor(0xFFFFFFFF);
            holder.tv_bssid.setTextColor(0xFF798997);
            holder.tv_ch.setTextColor(0xFF798997);
            holder.tv_wps.setTextColor(0xFF798997);
        }


        //중복 mac
        if ((data.risk & 0x02) == 0x02) {
            holder.tv_duplicate.setVisibility(View.VISIBLE);
        } else {
            holder.tv_duplicate.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.bssid != null && !data.bssid.equals("")) {

                    Intent intent = new Intent(mContext, WifiPopupActivity.class);
                    intent.putExtra("bssid", data.bssid);
                    intent.putExtra("ssid", data.ssid);
                    mContext.startActivity(intent);
                } else {
                    //  Toast.makeText(mContext,"Not Read Data",Toast.LENGTH_SHORT).show();
                }
            }
        });


        return convertView;
    }


    class ViewHolder {

        public TextView tv_ssid;

        public TextView tv_bssid;

        public TextView tv_ch;

        public TextView tv_wps;

        public ImageView iv_signal;

        public ImageView iv_alert;

        public TextView tv_enc;

        public ImageView iv_lock;

        public TextView tv_signal;

        public TextView tv_duplicate;

        public TextView tv_axAp;
    }

    public void setData(ArrayList<Ap_info> apList) {
        this.ap_list.clear();
        this.ap_list.addAll(apList);

    }
}
