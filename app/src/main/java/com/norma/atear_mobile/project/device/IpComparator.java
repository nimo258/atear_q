package com.norma.atear_mobile.project.device;

import java.util.Comparator;

/**
 * Created by hyojin on 7/14/16.
 */
public class IpComparator implements Comparator<DeviceInfo> {


    @Override
    public int compare(DeviceInfo lhs, DeviceInfo rhs) {
        // TODO Auto-generated method stub
        String firstIp;
        String secondIp;
        int firstIpLast;
        int secondIpLast;
        firstIp = lhs.getIP();
        secondIp = rhs.getIP();
        firstIpLast = Integer.valueOf(firstIp.substring(
                firstIp.lastIndexOf(".") + 1, firstIp.length()));
        secondIpLast = Integer.valueOf(secondIp.substring(
                secondIp.lastIndexOf(".") + 1, secondIp.length()));
        return firstIpLast < secondIpLast ? -1 : firstIpLast > secondIpLast ? 1
                : 0;
    }
}
