package com.norma.atear_mobile.project.wifi;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.MainActivity;
import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.ProjectActivity;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.adapter.WifiAdapter;
import com.norma.atear_mobile.project.device.DeviceInfo;
import com.norma.atear_mobile.report.FileScanActivity;
import com.norma.atear_mobile.utils.Throttle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by hyojin on 7/8/16.
 */
public class WifiActivity extends Activity implements View.OnClickListener {

    private static boolean ViewHandler = false;

    //view
    private com.norma.atear_mobile.project.progressbar.AVLoadingIndicatorView pb_scan;
    private ListView lv_wifi;
    private ImageView btn_Scan;
    private ImageView btn_Stop;
    private ImageView btn_Reset;
    private CheckBox cb_degree;
    private CheckBox cb_auth;
    private CheckBox cb_unAuth;
    private Button btn_filter;
    private Button btn_submit;
    private EditText edt_filter;
    private TextView tv_apOption;
    private TextView tv_filter;

    private TextView tv_filter_side;
    private ImageView btn_local_save;
    private ImageView btn_cloude_save;
    private LinearLayout btn_back;
    private LinearLayout btn_back2;
    private LinearLayout btn_menu;

    private View drawerView;
    ImageView iv_filter;
    //layout
    private DrawerLayout drawerLayout;  //슬라이드 레이아웃
    private LinearLayout linear_filter;   //filter 생성할 곳
    private LinearLayout.LayoutParams layoutControl; //filter contorl


    private WifiAdapter wifiAdapter;
    //progress
    private ProgressDialog progressDialog;
    //wifi
    private WifiManager wifiManager;

    //db
    private AtEarDB atEarDB;
    private SQLiteDatabase db_oui;
    private static final String PACKET_TABLE = "packet_table";

    private Context mContext;


    private int allScanSize = 0; //총 스캔 갯수
    private int scanSize = 0;  // 현재 화면에 스캔된 갯수
    //message

    SharedPref sharedPref;
    int language = 0;

    public static ArrayList<String> filter_list;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //화면 꺼짐 방지
        setContentView(R.layout.activity_wifi);
        //  AtEar_WIFI = true;

        findView();
        //언어

        Intent intent = getIntent();
        //project생성 시 정의된 filter list 불러오기
        ArrayList<String> intentfilterList = intent.getStringArrayListExtra("filterList");

        if (intentfilterList != null) {
            for (String filter : intentfilterList) {

                final TextView tv = new TextView(mContext);

                tv.setLayoutParams(layoutControl);
                Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                tv.setCompoundDrawablePadding(0);
                tv.setCompoundDrawables(null, null, img, null);
                tv.setText(filter);
                tv.setTextColor(0xFFFFFFFF);
                linear_filter.addView(tv);
                filter_list.add(filter);

                tv_filter_side.setText("-");
                tv_apOption.setText("");
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        linear_filter.removeView(tv);
                        filter_list.remove(tv.getText().toString());
                        if (filter_list.size() != 0) {
                            tv_apOption.setText("");
                            tv_filter_side.setText("- ");
                        } else {
                            tv_apOption.setText(mContext.getResources().getString(R.string.wifi_mainFilterMsg));
                            tv_filter_side.setText("- None");
                        }
                    }
                });
                edt_filter.setText("");
            }

            if (filter_list.size() != 0) {
                StringBuffer sb = new StringBuffer();
                for (String s : filter_list) {
                    sb.append(s + " ");
                }
                tv_filter.setText(sb.toString());
            } else {
                tv_filter.setText("none");
            }


        }

        edt_filter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == event.KEYCODE_SETTINGS) {
                }
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_filter.getWindowToken(), 0);

                    String filter = edt_filter.getText().toString();

                    if (!filter.equals("")) {
                        final TextView tv = new TextView(mContext);

                        tv.setLayoutParams(layoutControl);
                        Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                        img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                        tv.setCompoundDrawablePadding(0);
                        tv.setCompoundDrawables(null, null, img, null);
                        tv.setText(filter);
                        tv.setTextColor(0xFFFFFFFF);
                        linear_filter.addView(tv);
                        filter_list.add(filter);

                        tv_filter_side.setText("-");

                        tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                linear_filter.removeView(tv);
                                filter_list.remove(tv.getText().toString());

                                if (filter_list.size() != 0) {

                                    tv_filter_side.setText("- ");
                                } else {
                                    tv_filter_side.setText("- None");
                                }
                            }
                        });

                        edt_filter.setText("");
                    } else {
                        switch (language) {
                            case 0: //영어
                                Toast.makeText(mContext, "Please enter Text.", Toast.LENGTH_SHORT).show();
                                break;
                            case 1: //한국어
                                Toast.makeText(mContext, "텍스트를 입력하세요.", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:  //일본어
                                Toast.makeText(mContext, "テキストを入力してください。", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:  //중국어
                                Toast.makeText(mContext, "请录入文本", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
                return true;
            }
        });


        cb_auth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!cb_unAuth.isChecked() && !isChecked) {
                    cb_auth.setChecked(true);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.not_checked_option), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cb_unAuth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!cb_auth.isChecked() && !isChecked) {
                    cb_unAuth.setChecked(true);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.not_checked_option), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            switch (Throttle.INSTANCE.isEnabled(this)){
                case Throttle.ENABLED:
                    Toast.makeText(this,"Wi-Fi 쓰로틀링이 실행상태입니다.",Toast.LENGTH_SHORT).show();
                    break;
//                case Throttle.DISABLED: // 꺼짐
//                default: // 오류
            }
        }
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void setNotifcation(String message) {
        Intent intent = new Intent(mContext, WifiActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);

        //Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //android version 0
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channel_id = createNotificationChannel(mContext);
            notificationBuilder = new NotificationCompat.Builder(mContext,channel_id);
        }else{
            notificationBuilder = new NotificationCompat.Builder(mContext);
        }*/

        //NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.atear_icon_mini)  //icon
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.atear_icon))
                .setContentTitle("AtEar Mobile")
                .setContentText(message)
                .setAutoCancel(true)
                // .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1245 /* ID of notification */, notificationBuilder.build());
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (MainActivity.norma_policy == null) {
            Toast.makeText(mContext, "service Threading end!", Toast.LENGTH_SHORT);
            finish();
        }

        NotificationManager nm =
                (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        nm.cancelAll();

        ViewHandler = true;
        startService(new Intent(mContext, WifiService.class));

        scanUI(WifiService.wifi_running);
        if (WifiService.wifi_running) {
            mHandler.sendEmptyMessageDelayed(0, 2500);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ViewHandler = false;
        if (MainActivity.norma_policy != null) {
            String message = "";
            if (WifiService.wifi_running)
                message = mContext.getResources().getString(R.string.noti_scanning);
            else
                message = mContext.getResources().getString(R.string.noti_no_scan);

            setNotifcation(message);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        WifiService.wifi_running = false;
        WifiService.AtEar_WIFI = false;

        stopService(new Intent(mContext, WifiService.class));
    }

    private void findView() {

        mContext = this;

        drawerLayout = (DrawerLayout) findViewById(R.id.dl_activity_main_drawer);
        linear_filter = (LinearLayout) findViewById(R.id.linear_filter);
        linear_filter.setGravity(Gravity.CENTER);
        linear_filter.setGravity(Gravity.LEFT);

        layoutControl = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //   layoutControl.leftMargin = 30;
        // layoutControl.topMargin = 25;

        drawerView = findViewById(R.id.drawer);

        lv_wifi = (ListView) findViewById(R.id.lv_wifi);
        btn_Scan = (ImageView) findViewById(R.id.btn_Scan);
        btn_Stop = (ImageView) findViewById(R.id.btn_Stop);
        btn_Reset = (ImageView) findViewById(R.id.btn_Reset);
        btn_filter = (Button) findViewById(R.id.btn_filter);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_local_save = (ImageView) findViewById(R.id.btn_local_save);
        btn_cloude_save = (ImageView) findViewById(R.id.btn_cloude_save);
        btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_back2 = (LinearLayout) findViewById(R.id.btn_back2);
        btn_menu = (LinearLayout) findViewById(R.id.btn_menu);
        iv_filter = (ImageView) findViewById(R.id.iv_filter);
        tv_apOption = (TextView) findViewById(R.id.tv_apOption);
        tv_filter = (TextView) findViewById(R.id.tv_filter);
        tv_filter_side = (TextView) findViewById(R.id.tv_filter_side);
        edt_filter = (EditText) findViewById(R.id.edt_filter);
        cb_auth = (CheckBox) findViewById(R.id.cb_auth);
        cb_unAuth = (CheckBox) findViewById(R.id.cb_unAuth);
        cb_degree = (CheckBox) findViewById(R.id.cb_degree);

        btn_Scan.setOnClickListener(this);
        btn_Stop.setOnClickListener(this);
        btn_Reset.setOnClickListener(this);
        btn_filter.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_local_save.setOnClickListener(this);
        btn_cloude_save.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_menu.setOnClickListener(this);
        btn_back2.setOnClickListener(this);

        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        atEarDB = new AtEarDB(mContext);


        pb_scan = (com.norma.atear_mobile.project.progressbar.AVLoadingIndicatorView) findViewById(R.id.pb_scan);

        sharedPref = new SharedPref(mContext);

        filter_list = new ArrayList<>();

    }


    private void scanUI(boolean check) {
        if (check) {
            btn_Scan.setBackgroundResource(R.drawable.btn_scan_loading);
            //    btn_Scan.setVisibility(View.GONE);
            pb_scan.setVisibility(View.VISIBLE);
        } else {
            btn_Scan.setBackgroundResource(R.drawable.btn_scan_pressed);
            //   btn_Scan.setVisibility(View.VISIBLE);
            pb_scan.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //scan Button

            case R.id.btn_Scan:
                WifiService.wifi_running = true;

                mHandler.sendEmptyMessageDelayed(0, 300);
                scanUI(WifiService.wifi_running);
                break;

            case R.id.btn_Stop:
                //ap 개수
                //  Toast.makeText(mContext,wifiManager.getScanResults().size()+"개",Toast.LENGTH_LONG).show();
                allScanSize = atEarDB.PACKET_TABLE_SELECT_SIZE(); //총 갯수

                if (WifiService.wifi_running == true) {
                    scanSize = wifiManager.getScanResults().size(); // 화면에 현재 스캔된 갯수
                }
                if (allScanSize > 0) {
                    final Dialog stop_dialog = new Dialog(mContext);
                    stop_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    stop_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    stop_dialog.setCancelable(true);
                    stop_dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title_stop = (TextView) stop_dialog.findViewById(R.id.tv_title);
                    TextView tv_text_stop = (TextView) stop_dialog.findViewById(R.id.tv_text);
                    Button btn_yes_stop = (Button) stop_dialog.findViewById(R.id.btn_yes);
                    Button btn_no_stop = (Button) stop_dialog.findViewById(R.id.btn_no);

                    btn_no_stop.setVisibility(View.GONE);

                    String msg = mContext.getResources().getString(R.string.wifi_allScanSizeMsg) + allScanSize + "\n\n"
                            + mContext.getResources().getString(R.string.wifi_scanSizeMsg) + scanSize;

                    tv_title_stop.setText(mContext.getResources().getString(R.string.wifi_titleMsg));
                    tv_text_stop.setText(msg);
                    btn_yes_stop.setText(mContext.getResources().getString(R.string.yesMsg));
                    //   btn_no.setText(noMsg);

                    btn_yes_stop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            stop_dialog.dismiss();
                        }
                    });
                    stop_dialog.show();
                }

                WifiService.wifi_running = false;
                scanUI(WifiService.wifi_running);
                break;

            case R.id.btn_Reset:
                WifiService.wifi_running = false;
                scanUI(WifiService.wifi_running);
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                }

                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = new ProgressDialog(WifiActivity.this);
                            progressDialog.setMessage(mContext.getResources().getString(R.string.resetProgressMsg));
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            mHandler.sendEmptyMessageDelayed(2, 3000);  // 데이터 초기화
                        }
                    });
                } catch (Exception e) {

                }

                WifiService.wifi_running = false;
                scanUI(WifiService.wifi_running);

                atEarDB.PACKET_TABLE_DELETE();

                break;


            case R.id.btn_filter: //text filter
                String filter = edt_filter.getText().toString();

                if (!filter.equals("")) {
                    final TextView tv = new TextView(mContext);

                    tv.setLayoutParams(layoutControl);
                    Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                    img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                    tv.setCompoundDrawablePadding(0);
                    tv.setCompoundDrawables(null, null, img, null);
                    tv.setText(filter);
                    tv.setTextColor(0xFFFFFFFF);
                    linear_filter.addView(tv);
                    filter_list.add(filter);

                    tv_filter_side.setText("-");
                    tv_apOption.setText("");
                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            linear_filter.removeView(tv);
                            filter_list.remove(tv.getText().toString());

                            if (filter_list.size() != 0) {
                                tv_apOption.setText("");
                                tv_filter_side.setText("- ");
                            } else {
                                tv_apOption.setText(mContext.getResources().getString(R.string.wifi_mainFilterMsg));
                                tv_filter_side.setText("- None");
                            }
                        }
                    });

                    edt_filter.setText("");
                } else {
                    switch (language) {
                        case 0: //영어
                            Toast.makeText(mContext, "Please enter Text.", Toast.LENGTH_SHORT).show();
                            break;

                        case 1: //한국어
                            Toast.makeText(mContext, "텍스트를 입력하세요.", Toast.LENGTH_SHORT).show();
                            break;

                        case 2:  //일본어
                            Toast.makeText(mContext, "テキストを入力してください。", Toast.LENGTH_SHORT).show();
                            break;
                        case 3:  //중국어
                            Toast.makeText(mContext, "请录入文本", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                break;

            case R.id.btn_submit:
                //filter
                if (filter_list.size() != 0) {
                    StringBuffer sb = new StringBuffer();
                    for (String s : filter_list) {
                        sb.append(s + " ");
                    }
                    tv_filter.setText(sb.toString());
                } else {
                    tv_filter.setText("none");
                }

                //checked
                WifiService.degree = cb_degree.isChecked();
                WifiService.auth = cb_auth.isChecked();
                WifiService.unAuth = cb_unAuth.isChecked();

                drawerLayout.closeDrawers();
                break;

            case R.id.btn_local_save:

                final Dialog dialogSave = new Dialog(mContext);
                dialogSave.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogSave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogSave.setCancelable(false);
                dialogSave.setContentView(R.layout.dialog_custom);

                TextView tv_title2 = (TextView) dialogSave.findViewById(R.id.tv_title);
                TextView tv_text2 = (TextView) dialogSave.findViewById(R.id.tv_text);
                Button btn_yes2 = (Button) dialogSave.findViewById(R.id.btn_yes);
                Button btn_no2 = (Button) dialogSave.findViewById(R.id.btn_no);

                tv_title2.setText(mContext.getResources().getString(R.string.wifi_saveDialogTitleMsg));
                tv_text2.setText(mContext.getResources().getString(R.string.wifi_saveMsg));
                btn_yes2.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no2.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {
                        }
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog = new ProgressDialog(WifiActivity.this);
                                    progressDialog.setMessage(mContext.getResources().getString(R.string.wifi_stopProgressMsg));
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    mHandler.sendEmptyMessageDelayed(4, 3000);
                                }
                            });
                        } catch (Exception e) {

                        }
                        dialogSave.dismiss();
                    }
                });

                btn_no2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogSave.dismiss();
                    }
                });
                dialogSave.show();
                break;

            case R.id.btn_cloude_save:
                Toast.makeText(mContext, "coming soon.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn_menu:
                drawerLayout.openDrawer(drawerView);
                break;

            case R.id.btn_back:

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.wifi_backDialogTitleMsg));
                tv_text.setText(mContext.getResources().getString(R.string.wifi_backMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;

            case R.id.btn_back2:
                drawerLayout.closeDrawers();
                break;
        }
    }


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0: //listview 셋팅
                    if (Ap_info.viewApList != null) {
                        if (wifiAdapter == null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ArrayList<Ap_info> data = Ap_info.viewApList;

                                    wifiAdapter = new WifiAdapter(mContext, data);
                                    lv_wifi.setAdapter(wifiAdapter);
                                }
                            });
                        } else {
                            if (WifiService.wifi_running) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ArrayList<Ap_info> data = Ap_info.viewApList;
                                        // if (data.size() != 0) {
                                        wifiAdapter.setData(data);
                                        wifiAdapter.notifyDataSetChanged();
                                        //   }
                                    }
                                });
                            }
                        }
                    }

                    if (ViewHandler == true && WifiService.wifi_running == true) {
                        mHandler.sendEmptyMessageDelayed(0, 2500);
                    }
                    break;

                case 1:
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }
                    break;

                case 2: //데이터 reset
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }


                    if (wifiAdapter != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<Ap_info> data = Ap_info.viewApList;
                                wifiAdapter.setData(data);
                                wifiAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    break;

                case 4:  //파일 저장
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }
                    file_save();
                default:
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.wifi_backDialogTitleMsg));
                tv_text.setText(mContext.getResources().getString(R.string.wifi_backMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                return false;
            default:
                return false;
        }
    }

    private String intToIp(int i) {
        return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
    }


    private void file_save() {
        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {

        }
        SharedPref sharedPref = new SharedPref(mContext);
        String projectName = sharedPref.getValue("projectName", "return");
        String locationName = sharedPref.getValue("locationName", "return");
        String save_time = sharedPref.getValue("saveTime", "return");
        String gps = sharedPref.getValue("gps", "return");
        String today = sharedPref.getValue("today", "return");
        String normaDir = "/sdcard/Norma";

        File normaFile = new File(normaDir);

        if (!normaFile.exists()) {
            normaFile.mkdirs();
        }

        String dir = "/sdcard/Norma/Project";

        File dirFile = new File(dir);

        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }

        SQLiteDatabase db = atEarDB.getWritableDatabase();

        String sql = "select * from " + PACKET_TABLE;

        Cursor c = db.rawQuery(sql, null);

        if (c.getCount() != 0) {

            JSONArray json_array = new JSONArray();
            int cnt = 0;
            while (c.moveToNext()) {
                try {
                    JSONObject json_object = new JSONObject();

                    json_object.put("projectName", projectName);
                    json_object.put("locationName", locationName);
                    json_object.put("save_time", save_time);
                    json_object.put("gps", gps);

                    String bssid = c.getString(0);


                    json_object.put("ssid", c.getString(1));
                    json_object.put("signal", String.valueOf(c.getInt(2)));
                    json_object.put("channel", String.valueOf(c.getInt(3)));
                    json_object.put("encryption", c.getString(4));
                    json_object.put("cipher", c.getString(5));
                    json_object.put("auth", c.getString(6));
                    json_object.put("scan_time", c.getString(7));
                    json_object.put("ap_type", c.getString(8));
                    json_object.put("oui", c.getString(9));
                    json_object.put("comment", c.getString(10));
                    json_object.put("alert", c.getString(11));
                    int risk = c.getInt(12);
                    json_object.put("risk", String.valueOf(risk));
                    // risk check
                    // 중복 mac    10 0x02 2

                    // 정책 위험   100 0x04 4
                    // 정책 중간  1000 0x08 8
                    // 정책 안전 10000 0x10 16

                    // softMac  100000 0x20 32
                    // 접속 기록 1000000 0x40 64

                    if ((risk & 0x02) == 0x02) {
                        json_object.put("duplicated", String.valueOf(1));
                        json_object.put("bssid", bssid + "_" + c.getString(7));
                    } else {
                        json_object.put("duplicated", String.valueOf(0));
                        json_object.put("bssid", bssid);

                    }


                    json_object.put("wps", String.valueOf(c.getInt(13)));
                    json_object.put("isAuthorized", String.valueOf(c.getInt(14)));
                    json_object.put("ssid_filter", String.valueOf(c.getInt(15)));
                    json_object.put("bssid_filter", String.valueOf(c.getInt(16)));
                    json_object.put("policy_type", String.valueOf(c.getInt(17)));
                    json_object.put("picture_path", c.getString(18));
                    json_object.put("memo", c.getString(19));
                    json_object.put("isPentest", String.valueOf(c.getInt(20)));
                    json_object.put("pentest_password", c.getString(21));

                    int station_cnt = 0;
                    //deviceScan
                    if (ProjectActivity.ap_hashMap.get(bssid) == null) {
                        json_object.put("station_count", String.valueOf(station_cnt));
                    } else {
                        try {
                            JSONArray stationArray = new JSONArray();
                            Iterator<String> iter = ProjectActivity.ap_hashMap.get(bssid).keySet().iterator();
                            while (iter.hasNext()) {
                                String mac = iter.next();
                                station_cnt++;
                                JSONObject stationObject = new JSONObject();

                                DeviceInfo device = ProjectActivity.ap_hashMap.get(bssid).get(mac);
                                stationObject.put("ip", device.getIP());
                                stationObject.put("mac", device.mac);
                                stationObject.put("hostname", device.deviceName);
                                Log.e("device", device.ip);
                                stationArray.put(stationObject);
                            }
                            json_object.put("station_count", String.valueOf(station_cnt));
                            json_object.put("stations", stationArray);
                        } catch (Exception e) {
                            Log.e("jsonerror", String.valueOf(e));
                        }
                    }

                    if (ProjectActivity.picture_hashMap.get(bssid) == null) {
                        json_object.put("pictureBase64", "none");
                    } else {
                        String pictureBase64 = ProjectActivity.picture_hashMap.get(bssid);
                        json_object.put("pictureBase64", pictureBase64);
                    }

                    if (cnt == 0) {
                        String mapBase64 = ProjectActivity.picture_hashMap.get("mapimage");
                        json_object.put("mapimage", mapBase64);
                    } else {
                        json_object.put("mapimage", "none");
                    }
                    cnt++;
                    json_array.put(json_object);
                } catch (Exception e) {
                    Log.e("json error", "file save");
                }
            } //while end

            //old jsonfile
            String jsonFile;
            //new project
            String newJson = json_array.toString();

            StringBuilder sb = new StringBuilder();

            String policy = "";

            switch (MainActivity.norma_policy.POLICY) {
                case 0: //none
                    policy = "NONE";
                    break; //isms
                case 1:
                    policy = "ISMS";
                    break;

                case 2: //iso27001
                    policy = "ISO27001";
                    break;

                case 3: //norma
                    policy = "NORMA";
                    break;

                default:
                    policy = "NONE";
                    break;
            }

            File projectFile = new File(dir + "/" + today + "_" + projectName + "_" + locationName + ".txt");
            try {


                if (projectFile.exists()) {
                    projectFile.delete();

                } else {
                    //   jsonFile = newJson;
                }

                jsonFile = newJson;

                FileOutputStream fos = new FileOutputStream(projectFile);
                fos.write(jsonFile.toString().getBytes());
                fos.close();

                // Toast.makeText(mContext, "File save.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, FileScanActivity.class);
                startActivity(intent);
                finish();

            } catch (Exception e) {

            }
        } else {
            switch (language) {
                case 0: //영어
                    Toast.makeText(mContext, "no data. Please use after scanning.", Toast.LENGTH_SHORT).show();
                    break;

                case 1: //한국어
                    Toast.makeText(mContext, "데이터가 없습니다. 스캔 후 이용해 주세요.", Toast.LENGTH_SHORT).show();
                    break;

                case 2:  //일본어
                    Toast.makeText(mContext, "データなし。スキャン後にご使用ください。", Toast.LENGTH_SHORT).show();
                    break;

                case 3:  //중국어
                    Toast.makeText(mContext, "没有数据。请扫描后使用。", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        c.close();
        db.close();
    }
}
