package com.norma.atear_mobile.project;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.norma.atear_mobile.MainActivity;
import com.norma.atear_mobile.R;
import com.norma.atear_mobile.component.Util;
import com.norma.atear_mobile.csv.CSVRoder;
import com.norma.atear_mobile.data.Ap_HashMap;
import com.norma.atear_mobile.data.Picture_HashMap;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.dialog.FilterDialog;
import com.norma.atear_mobile.dialog.GlobalDialog;
import com.norma.atear_mobile.dialog.LocalDialog;
import com.norma.atear_mobile.project.wifi.WifiActivity;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by hyojin on 7/8/16.
 */
public class ProjectActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener {

    //view
    private TextView tv_gps;
    private EditText edt_proName;
    private EditText edt_loName;
    private Button btn_StartScan;
    private Button btn_global_setting;
    private Button btn_filter_setting;
    private Button btn_local_setting;
    private LinearLayout btn_back;
    private TextView tv_global;
    private TextView tv_filter;
    private TextView tv_local;
    private TextView tv_map;


    //layout
    private LinearLayout linear_filter;
    private LinearLayout.LayoutParams layoutControl;
    private LinearLayout linear_map;

    ArrayList<String> filter_list;

    //gps
    private GoogleMap mGoogleMap;
    private Double latitude;   //위도
    private Double longitude; //경도
    private GpsInfo gps;  //Provider
    private LatLng loc; //좌표
    private Marker marker; //마커
    private MarkerOptions markerOptions; //마커 옵션

    private SharedPref sharedPref;   //캐시 저장
    private Context mContext;

    private String project_name;
    private String location_name;
    private String save_gps;

    private AtEarDB atEarDB;


    private static final int FILE_RESULT = 11;
    private static boolean maker_change = false; //maker 변경 유무

    public static Ap_HashMap ap_hashMap;
    public static Picture_HashMap picture_hashMap;

    private ProgressDialog progressDialog;

    private String mapBase64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        findView();

        //  SetLanguage();

        ap_hashMap = new Ap_HashMap();
        picture_hashMap = new Picture_HashMap();
        //drop 인가 mac
        atEarDB.SINGLE_BSSID_TABLE_DROP();
        //network 여부 체크  (맵을 보여줄지 말지 )
        if (isNetworkConnected() == true) {
            linear_map.setVisibility(View.VISIBLE);
            tv_map.setVisibility(View.GONE);

        } else {
            linear_map.setVisibility(View.GONE);
            tv_map.setVisibility(View.VISIBLE);

        }

        //hot spot 체크
        if (isHotSpot(mContext) == true) {
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_custom);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
            Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

            tv_text.setText(mContext.getResources().getString(R.string.hotSpotMsg));
            btn_no.setVisibility(View.GONE);
            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }

        //gps
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//화면 회전 세로로고정

        SupportMapFragment supportMapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);


        String pro = sharedPref.getValue("projectName", "return");
        if (!pro.equals("return")) {
            edt_proName.setText(pro);
        }

        btn_StartScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapIncode();
                progressDialog = new ProgressDialog(ProjectActivity.this);
                progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                progressDialog.setCancelable(false);
                progressDialog.show();
                mHandler.sendEmptyMessage(0);

            }
        });
        MainActivity.norma_policy.POLICY = sharedPref.getValue("policyType", 0);

        switch (MainActivity.norma_policy.POLICY) {
            case 0:
                tv_global.setText("- None");
                MainActivity.norma_policy.non_policy();
                break;

            case 1:
                tv_global.setText("- ISMS");
                MainActivity.norma_policy.policy_isms_auth();
                break;

            case 2:
                tv_global.setText("- ISO 27001");
                MainActivity.norma_policy.policy_iso_auth();
                break;

            case 3:
                tv_global.setText("- NOMRA");
                MainActivity.norma_policy.policy_norma_auth();
                break;
        }


        btn_global_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final GlobalDialog globalDialog = new GlobalDialog(mContext);
                globalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                globalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                globalDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface $dialog) {
                        GlobalDialog dialog = (GlobalDialog) $dialog;
                        int data = dialog.getData();
                        switch (data) {
                            case 0:
                                MainActivity.norma_policy.non_policy();
                                tv_global.setText("- None");
                                break;

                            case 1:
                                MainActivity.norma_policy.policy_isms_auth();
                                tv_global.setText("- ISMS");
                                break;

                            case 2:
                                MainActivity.norma_policy.policy_iso_auth();
                                tv_global.setText("- ISO 27001");
                                break;

                            case 3:
                                MainActivity.norma_policy.policy_norma_auth();
                                tv_global.setText("- NOMRA");
                                break;
                        }
                    }
                });
                globalDialog.show();
            }
        });


        btn_filter_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FilterDialog filterDialog = new FilterDialog(mContext);
                filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                filterDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface $dialog) {
                        FilterDialog dialog = (FilterDialog) $dialog;
                        String dialogData = dialog.getData();

                        String[] array = dialogData.split(",");
                        for (String data : array) {

                            if (!data.equals("")) {
                                final TextView tv = new TextView(mContext);
                                tv.setLayoutParams(layoutControl);

                                Drawable img = getResources().getDrawable(R.drawable.btn_x_hover);

                                img.setBounds(0, 5, img.getIntrinsicWidth(), img.getIntrinsicHeight());

                                tv.setCompoundDrawablePadding(0);
                                tv.setCompoundDrawables(null, null, img, null);

                                tv.setText(data);
                                tv.setTextColor(0xFF768B9C);
                                //  tv.setTextSize(18);

                                linear_filter.addView(tv);
                                filter_list.add(data);

                                tv_filter.setText("- ");

                                tv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        linear_filter.removeView(tv);
                                        filter_list.remove(tv.getText().toString());

                                        if (filter_list.size() != 0) {

                                            tv_filter.setText("- ");
                                        } else {
                                            tv_filter.setText("- None");
                                        }
                                    }
                                });

                            }
                        }
                    }
                });
                filterDialog.show();
            }
        });

        btn_local_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final LocalDialog localDialog = new LocalDialog(mContext);
                localDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                localDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                localDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface $dialog) {
                        LocalDialog dialog = (LocalDialog) $dialog;

                        final String fileName = dialog.getFileName();
                        final String filePath = dialog.getFilePath();
                        Log.e("csvFile", filePath + "," + fileName);
                        if (!filePath.equals("none")) {
                            new CSVRoder(ProjectActivity.this, atEarDB, filePath, fileName, new CSVRoder.OnChangeText() {
                                @Override
                                public void resultString(String text) {
                                    tv_local.setText(text);
                                }

                                @Override
                                public void postExecuteString(String text) {
                                    Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
                                }
                            }).execute();
                        } else {
                            atEarDB.SINGLE_BSSID_TABLE_DROP();
                            tv_local.setText("- None");
                        }
                    }
                });

                localDialog.show();
            }
        });


        //edt Text 이벤트 발생

        edt_proName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    //InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    //imm.hideSoftInputFromWindow(edt_proName.getWindowToken(), 0);
                    edt_loName.requestFocus();

                }

                return true;
            }
        });


        edt_loName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_loName.getWindowToken(), 0);
                }
                return true;
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void findView() {

        mContext = this;
        tv_gps = (TextView) findViewById(R.id.tv_gps);
        edt_proName = (EditText) findViewById(R.id.edt_proName);
        edt_loName = (EditText) findViewById(R.id.edt_loName);
        btn_StartScan = (Button) findViewById(R.id.btn_StartScan);
        btn_global_setting = (Button) findViewById(R.id.btn_global_setting);
        btn_filter_setting = (Button) findViewById(R.id.btn_filter_setting);
        btn_local_setting = (Button) findViewById(R.id.btn_local_setting);
        btn_back = (LinearLayout) findViewById(R.id.btn_back);
        tv_global = (TextView) findViewById(R.id.tv_global);
        tv_filter = (TextView) findViewById(R.id.tv_filter);
        tv_local = (TextView) findViewById(R.id.tv_local);
        tv_map = (TextView) findViewById(R.id.tv_map);

        linear_map = (LinearLayout) findViewById(R.id.linear_map);
        linear_filter = (LinearLayout) findViewById(R.id.linear_filter);
        linear_filter.setGravity(Gravity.CENTER);
        linear_filter.setGravity(Gravity.LEFT);

        layoutControl = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        atEarDB = new AtEarDB(mContext);

        filter_list = new ArrayList<>();

        mapBase64 = "null?";

        sharedPref = new SharedPref(mContext);
    }


    @Override
    protected void onPause() {
        super.onPause();
        maker_change = false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        maker_change = false;
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                if (!mapBase64.equals("null?")) {
                    mHandler.sendEmptyMessage(1);
                } else {
                    mHandler.sendEmptyMessageDelayed(0, 100);
                }
            } else if (msg.what == 1) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                startScan();
            }
        }
    };

    private void MapIncode() {
        if (linear_map.getVisibility() == View.GONE || latitude == 0.0) {
            mapBase64 = "none";
            return;
        }
        mGoogleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                if (snapshot == null) {
                    mapBase64 = "none";
                    picture_hashMap.put("mapimage", mapBase64);
                }else {
                    mapBase64 = Util.encodeToBase64(snapshot);
                    picture_hashMap.put("mapimage", mapBase64);
                }

            }
        });

    }

    private void startScan() {
        if (checkGPS() == true) {

            project_name = edt_proName.getText().toString();
            location_name = edt_loName.getText().toString();

            String location_old = sharedPref.getValue("locationName", "null");//전에 사용되었던 location
            String day_old = sharedPref.getValue("today", "return");

            if (!project_name.equals("") && !isStringDouble(project_name)) { //projet
                if (isStringTo(project_name)) {
                    if (!location_name.equals("")) { // location
                        if (isStringTo(location_name)) {

                            if (location_name.equals(location_old) && day_old.equals(today())) { //lcation 캐시  중복 확인.
                                //location이 전정보와 동일 할때

                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.dialog_custom);

                                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                                tv_title.setText(mContext.getResources().getString(R.string.samLocationTitleMsg));
                                tv_text.setText(mContext.getResources().getString(R.string.samLocationMsg));
                                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                                btn_yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sharedPref.put("projectName", project_name);
                                        sharedPref.put("locationName", location_name);
                                        sharedPref.put("today", today()); //로케이션 중복 확인
                                        sharedPref.put("saveTime", save_today()); // project saveTime
                                        sharedPref.put("gps", save_gps); // project saveTime
                                        Intent intent = new Intent(mContext, WifiActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putStringArrayListExtra("filterList", filter_list);
                                        startActivity(intent);
                                        finish();
                                        dialog.dismiss();
                                    }
                                });

                                btn_no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            } else {
                                sharedPref.put("projectName", project_name);
                                sharedPref.put("locationName", location_name);
                                sharedPref.put("today", today());
                                sharedPref.put("saveTime", save_today()); // project saveTime
                                sharedPref.put("gps", save_gps); // project saveTime
                                Intent intent = new Intent(mContext, WifiActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putStringArrayListExtra("filterList", filter_list);

                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.edit_stringValue), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.edit_location), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.edit_stringValue), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.edit_projName), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.gpsMsg), Toast.LENGTH_SHORT).show();
        }
    }

    //맵 클릭
    @Override
    public void onMapClick(LatLng latLng) {

        if (maker_change == true) {
            marker.remove();
            loc = latLng;

            latitude = loc.latitude;
            longitude = loc.longitude;

            markerOptions.position(loc);

            marker = mGoogleMap.addMarker(markerOptions);

            String gps = "GPS :" + latitude.floatValue() + "," + longitude.floatValue();
            save_gps = gps;
            tv_gps.setText("GPS :" + gps);


            AlertDialog.Builder alt_bld = new AlertDialog.Builder(mContext);
            final String finalGpsMsg = mContext.getResources().getString(R.string.connect_gpsLocate);
            alt_bld.setMessage(mContext.getResources().getString(R.string.connect_question)).setCancelable(
                    false).setPositiveButton(mContext.getResources().getString(R.string.yesMsg),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            maker_change = false;
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 17));
                            mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);// 화면 이동

                        }
                    }).setNegativeButton(mContext.getResources().getString(R.string.noMsg),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Action for 'NO' Button
                            tv_gps.setText(finalGpsMsg);
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alt_bld.create();
            // Title for AlertDialog
            alert.setTitle("maker change");
            // Icon for AlertDialog

            alert.show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mGoogleMap == null) {
            Log.e("projectAct", "google map null");
        }
        gps = new GpsInfo(mContext);

        latitude = gps.getLatitude();
        longitude = gps.getLongitude();

        gps.stopUsingGPS();

        loc = new LatLng(latitude, longitude);
        //loc = new LatLng(37.4444, 127.222);
        Log.d("mr", "la: " + latitude);
        Log.d("mr", "lo: " + longitude);

        String gps = latitude.floatValue() + "," + longitude.floatValue();
        save_gps = gps;
        tv_gps.setText("GPS :" + gps);
        //permission check;
        int versionSDK = Build.VERSION.SDK_INT;
        if (versionSDK >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 17));
        //마커로 현재 t표시
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);// 화면 이동 불가.
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true); //줌 +- 기능.

        markerOptions = new MarkerOptions()
                .title(mContext.getResources().getString(R.string.change_locateHere))
                //.snippet("innoaus.")
                .position(loc);

        marker = mGoogleMap.addMarker(markerOptions);

        mGoogleMap.setOnMapClickListener(this);


        //maker click event;
        final String finalDialogMsg = mContext.getResources().getString(R.string.a_u_change_locate);
        final String finalMakerMsg = mContext.getResources().getString(R.string.click_the_locate);
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (maker_change == false) {
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(mContext);
                    alt_bld.setMessage(finalDialogMsg).setCancelable(
                            false).setPositiveButton(mContext.getResources().getString(R.string.yesMsg),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    maker_change = true;
                                    mGoogleMap.getUiSettings().setScrollGesturesEnabled(true);// 화면 이동
                                    tv_gps.setText(finalMakerMsg);
                                    //Toast.makeText(mContext, "변경 할 위치를 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                                }
                            }).setNegativeButton(mContext.getResources().getString(R.string.noMsg),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    // Title for AlertDialog
                    alert.setTitle("maker change");
                    // Icon for AlertDialog

                    alert.show();
                }
                return false;
            }
        });


    }

    //현재 날짜 출력;
    private String today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-
        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String result = year + "_" + month + "_" + day;

        return result;
    }

    private String save_today() {
        final Calendar today = Calendar.getInstance(); //오늘날짜 셋팅-

        String year = String.valueOf(today.get(Calendar.YEAR));
        if (year.length() == 1) {
            year = "0" + year;
        }
        String month = String.valueOf(today.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }

        String day = String.valueOf(today.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }

        String hour = String.valueOf(today.get(Calendar.HOUR_OF_DAY));
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        String minute = String.valueOf(today.get(Calendar.MINUTE));
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        String second = String.valueOf(today.get(Calendar.SECOND));
        if (second.length() == 1) {
            second = "0" + second;
        }
        String result = year + "-" + month + "-"
                + day + " " + hour + ":" + minute + ":" + second;

        return result;
    }


    //파일매니저 사용했을 시
 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_RESULT && resultCode == RESULT_OK) {
            String file_name = data.getExtras().getString("file_name");
            String file_path = data.getExtras().getString("file_path");

            try {
                // csv 데이타 파일

                if (file_path.contains("csv")) {
                    File csv = new File(file_path);

                    ArrayList<String> singleBssidList = new ArrayList<>();

                    BufferedReader br = new BufferedReader(new FileReader(csv));

                    String line = "";
                    while ((line = br.readLine()) != null) {
                        // -1 옵션은 마지막 "," 이후 빈 공백도 읽기 위한 옵션
                        String[] token = line.split(",", -1);

                        for (String output : token) {
                            Log.e("data list ", output);
                            singleBssidList.add(output);
                        }
                    }
                    br.close();
                    //데이터 넣기 인가 single

                    // atEarDB.SINGLE_BSSID_TABLE_INSERT(singleBssidList);

                    tv_local.setText("- " + file_name);
                } else {
                    Toast.makeText(mContext, "csv파일 형태가 아닙니다.", Toast.LENGTH_SHORT).show();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/


//    private void CsvReader(String file_path, String file_name) {
//        try {
//            // csv 데이타 파일
//            if (file_path.contains("csv")) {
//                String line;
//                File csv = new File(file_path);
//
//                ArrayList<String> singleBssidList = new ArrayList<>();
//
//                BufferedReader br = new BufferedReader(new FileReader(csv));
//
//                while ((line = br.readLine()) != null) {
//                    // -1 옵션은 마지막 "," 이후 빈 공백도 읽기 위한 옵션
//                    String[] token = line.split(",", -1);
//
//                    for (String output : token) {
//                        Log.e("data list ", output);
//                        if (output.length() == 17) {
//                            //mac 만 가능 17글자.
//                            singleBssidList.add(output.toUpperCase());
//                        }
//                    }
//                }
//                br.close();
//
//
//                if (singleBssidList.size() != 0) {
//                    //데이터 넣기 인가 single
//                    atEarDB.SINGLE_BSSID_TABLE_INSERT(singleBssidList);
//
//                    tv_local.setText("- " + file_name);
//                } else {
//                    Toast.makeText(mContext, mContext.getResources().getString(R.string.not_csv), Toast.LENGTH_SHORT).show();
//                }
//            } else {
//                Toast.makeText(mContext, mContext.getResources().getString(R.string.rebuild_csv), Toast.LENGTH_SHORT).show();
//
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    //인터넷 연결 확인.
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    //핫스팟
    public static boolean isHotSpot(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        } catch (Throwable ignored) {
        }
        return false;
    }

    //gps check
    public boolean checkGPS() {

        int versionSDK = Build.VERSION.SDK_INT;

        if (versionSDK >= 23) {

            LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPS) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static boolean isStringDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isStringTo(String s) {
        if (s.contains("{") || s.contains("}") || s.contains("[") || s.contains("]") || s.contains("/") ||
                s.contains("(") || s.contains(")") || s.contains("'") || s.contains("'") || s.contains("\"") ||
                s.contains("-") || s.contains("\\") || s.contains(" ")) {
            return false;
        }
        return true;
    }


}
