package com.norma.atear_mobile.project.wifiPopup;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.data.Ap_info;
import com.norma.atear_mobile.data.DictionaryList;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.service.ARPThread;
import com.norma.atear_mobile.project.service.DNSThread;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hyojin on 7/27/16.
 */
public class ApInfoFragment extends Fragment implements View.OnClickListener {

    private View v;

    public boolean APINFO_VIEW = false;

    public boolean wifiConnect = false;
    // public boolean ActivitryCancel = false;
    private TextView tv_alert;
    private TextView tv_ssid;
    private TextView tv_bssid;
    private TextView tv_apType;
    private TextView tv_oui;
    private TextView tv_enc;
    private TextView tv_channel;
    private TextView tv_ax_text;

    private TextView tv_securiy_check;

    private Button btn_connect;
    private Button btn_dictionary;
    private Button btn_Authorized;
    private Button btn_security_check;
    private Button btn_security_change;
    private Button btn_disconnect;

    private EditText edt_password;
    private LinearLayout linear_login;
    private LinearLayout linear_no_login;

    private String ssid;
    private String bssid;
    private String location;

    private AtEarDB atEarDB;

    public static Context mContext;

    private Ap_info ap_info;

    private int risk;
    private int tempNetId;

    private ProgressDialog progressDialog;


    private WifiManager wifiManager;
    private WifiConfiguration wifiConfiguration;

    private StringBuffer sb_apCheck;

    private boolean isOpenDns1 = true;
    private boolean isOpenDns2 = true;

    private String geteway = "";
    private String dns1 = "";
    private String dns2 = "";

    private int securiyBtnOption = 0;

    SharedPref sharedPref;
    int language = 0;

    ARPThread arpThread;
    DNSThread dnsThread;

    String attackSuccessMsg = "";
    String apChangMsg = "";

    PenTestingTask penTestingTask;

    DictionaryList dictionaryList;
    ArrayList<String> passwordList;
    //WifiReceiver wifiReceiver;

    boolean isApConnect = false;


    public ApInfoFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        v = inflater.inflate(R.layout.fragment_apinfo, container, false);

        findView();

        APINFO_VIEW = true;
        Intent intent = getActivity().getIntent();

        bssid = intent.getStringExtra("bssid");
        ssid = intent.getStringExtra("ssid");

        location = sharedPref.getValue("locationName", "return");

        ap_info = atEarDB.PACKET_TABLE_SELECT(bssid, ssid);

        if (ap_info.encryption != null && ap_info.encryption.equals("WPA/WPA2")) {
            ap_info.encryption = "WPA/WPA2";
        }
        Log.e("sss", bssid + "/" + ssid + "/" + ap_info.encryption + "/" + ap_info.channel);

        if (ap_info == null) {
            Log.e("apinfo_null", "null##$!$!@$!@$!$");
        }

        if (ap_info.encryption == null) {
            //   ap_info.encryption = "null";
        }
        tv_alert.setText(ap_info.alert);
        tv_ssid.setText(ap_info.ssid);
        tv_bssid.setText(ap_info.bssid);
        tv_apType.setText(ap_info.ap_type);
        tv_oui.setText(ap_info.oui);
        tv_enc.setText(ap_info.encryption);
        tv_channel.setText(String.valueOf(ap_info.channel));
        tv_ax_text.setText(ap_info.is11axAp?"O":"X");

        // risk check
        // 중복 mac    10 0x02 2

        // 정책 위험   100 0x04 4
        // 정책 중간  1000 0x08 8
        // 정책 안전 10000 0x10 16

        // softMac  100000 0x20 32
        // 접속 기록 1000000 0x40 64

        risk = ap_info.risk;
        if ((risk & 0x40) == 0x40) {
            //    Log.e("risk test", "0x40");
        } else if ((risk & 0x04) == 0x04) {
            //   Log.e("risk test", "0x04");
        } else if ((risk & 0x08) == 0x08) {
            //  Log.e("risk test", "0x08");
        } else if ((risk & 0x10) == 0x10) {
            //     Log.e("risk test", "0x10");
        } else {
            // Log.e("risk test", String.valueOf(risk));
        }

        //연결된  wifi 확인
        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String wifissid = wifiInfo.getSSID();
        String wifibssid = wifiInfo.getBSSID();
        String gatewayIp = Formatter.formatIpAddress(wifiInfo.getIpAddress());

        tempNetId = -1;

        if (ap_info.encryption != null && ap_info.encryption.equals("OPEN")) {

            edt_password.setVisibility(View.GONE);
            btn_dictionary.setVisibility(View.GONE);
        }

        String ssid = ap_info.ssid;
        try {
            if (wifissid != null && wifibssid != null) { // current ap
                if (wifissid.contains(ssid) && wifibssid.toUpperCase().contains(bssid) && gatewayIp.equals("0.0.0.0")) {
                    linear_login.setVisibility(View.GONE);
                    linear_no_login.setVisibility(View.VISIBLE);
                } else if (wifissid.contains(ssid) && wifibssid.toUpperCase().contains(bssid)) {

                    wifiConnect = true;
                    linear_login.setVisibility(View.VISIBLE);
                    linear_no_login.setVisibility(View.GONE);
                    btn_connect.setText(mContext.getResources().getString(R.string.DisConnectMsg));

                    Log.e("wifi status : ", "true");
                } else {
                    //eap
                    if ((risk & 0x64) == 0x64) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.eapMsg), Toast.LENGTH_LONG).show();
                        linear_no_login.setVisibility(View.GONE);
                    }
                }
            } else {
                //eap
                if ((risk & 0x64) == 0x64) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.eapMsg), Toast.LENGTH_LONG).show();
                    linear_no_login.setVisibility(View.GONE);
                } else {
                    linear_login.setVisibility(View.GONE);
                    linear_no_login.setVisibility(View.VISIBLE);
                }
            }

            if (ssid.equals("(Hidden)")) {
                linear_no_login.setVisibility(View.GONE);
            }

            for (WifiConfiguration wifiConfiguration : wifiManager.getConfiguredNetworks()) {
                if (wifiConfiguration.SSID.toUpperCase().equals("\"" + ap_info.ssid.toUpperCase() + "\"")
                        && bssid.toUpperCase().equals(ap_info.bssid.toUpperCase())) {
                    //   Log.e("wifiCOnfigurations", String.valueOf(wifiConfiguration));
                    // atear 를 제외한 다른 앱에서 wifi 연결했을 경우
                    if (!wifiConfiguration.toString().contains("com.norma.atear_mobile") && wifiConnect == false) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.systemWIFIMsg), Toast.LENGTH_LONG).show();
                        btn_dictionary.setVisibility(View.GONE);
                    }

                    if (tempNetId != -1) {
                        //   Log.e("wifiCOnfigurations", String.valueOf(wifiConfiguration));
                        edt_password.setVisibility(View.GONE);
                        btn_dictionary.setVisibility(View.GONE);
                    } else {
                        if (wifiConnect == false) {
                            wifiManager.removeNetwork(wifiConfiguration.networkId);
                            wifiManager.saveConfiguration();

                        }
                    }
                    break;
                }
            }

        } catch (Exception e) {
            getActivity().finish();
        }

        edt_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_password.getWindowToken(), 0);
                }
                return true;
            }
        });

        return v;
    }


    private void findView() {

        mContext = getContext();

        tv_alert = (TextView) v.findViewById(R.id.tv_alert);
        tv_ssid = (TextView) v.findViewById(R.id.tv_ssid);
        tv_bssid = (TextView) v.findViewById(R.id.tv_bssid);
        tv_apType = (TextView) v.findViewById(R.id.tv_apType);
        tv_oui = (TextView) v.findViewById(R.id.tv_oui);
        tv_enc = (TextView) v.findViewById(R.id.tv_enc);
        tv_channel = (TextView) v.findViewById(R.id.tv_channel);
        tv_ax_text = (TextView) v.findViewById(R.id.tv_ax_text);
        tv_securiy_check = (TextView) v.findViewById(R.id.tv_securiy_check);

        edt_password = (EditText) v.findViewById(R.id.edt_password);

        //연결됏을때
        btn_Authorized = (Button) v.findViewById(R.id.btn_Authorized);
        btn_connect = (Button) v.findViewById(R.id.btn_connect);
        btn_dictionary = (Button) v.findViewById(R.id.btn_dictionary);

        //연결 안됐을때
        btn_security_check = (Button) v.findViewById(R.id.btn_security_check);
        btn_security_change = (Button) v.findViewById(R.id.btn_security_change);
        btn_disconnect = (Button) v.findViewById(R.id.btn_disconnect);

        linear_login = (LinearLayout) v.findViewById(R.id.linear_login);
        linear_no_login = (LinearLayout) v.findViewById(R.id.linear_no_login);

        btn_Authorized.setOnClickListener(this);
        btn_connect.setOnClickListener(this);
        btn_dictionary.setOnClickListener(this);
        btn_security_check.setOnClickListener(this);
        btn_security_change.setOnClickListener(this);
        btn_disconnect.setOnClickListener(this);

        atEarDB = new AtEarDB(mContext);
        sharedPref = new SharedPref(mContext);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Authorized:

                final Dialog dialog = new Dialog(mContext);
                // Setting dialogview
                dialog.getWindow().setGravity(Gravity.CENTER);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.btnAuthMsg));
                tv_text.setText(mContext.getResources().getString(R.string.info_authMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        atEarDB.BSSID_TABLE_INSERT(location, ap_info.bssid, ap_info.ssid, mContext);
                        // atEarDB.PACKET_TABLE_UPDATE_AUTHORIZED(ap_info);
                        dialog.dismiss();
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                break;

            case R.id.btn_connect:
                wifiConnecting();
                break;

            case R.id.btn_dictionary:
                /**
                 * 딕셔너리
                 */
                //무작위 String 담기
                dictionaryList = new DictionaryList(ap_info.ssid, ap_info.encryption);
                passwordList = dictionaryList.dictionaryList;
                //pentesting
                PenTestingTask penTestingTask = new PenTestingTask(passwordList);
                penTestingTask.execute();
                break;

            case R.id.btn_security_check:
                if(Build.VERSION.SDK_INT>28){
                    Toast.makeText(getContext(),getContext().getResources().getString(R.string.deprecatedFunction),Toast.LENGTH_SHORT).show();
                    return;
                }
                Checking();
                break;

            case R.id.btn_security_change:

                if (securiyBtnOption == 0) { //기록정보 등록
                    atEarDB.CONNECT_AP_TABLE_UPDATE_IP(bssid, geteway, dns1, dns2, 0);

                    switch (language) {
                        case 0: //영어
                            apChangMsg = "Recorded information has changed.";
                            break;
                        case 1: //한국어
                            apChangMsg = "기록 된 정보가 변경되었습니다.";
                            break;
                        case 2:  //일본어
                            apChangMsg = "記録された情報が変更されました。";
                            break;
                        case 3:  //중국어
                            apChangMsg = "存储信息已变更";
                            break;
                    }
                    btn_security_change.setVisibility(View.GONE);
                    tv_securiy_check.setText(apChangMsg);
                    tv_securiy_check.setTextColor(0xFFFFFFFF);

                } else {
                    // OPEN DNS 인가로 등록
                    if (isOpenDns1 == false && isOpenDns2 == false) { //dns1, dns2 둘다 위험
                        atEarDB.OPEN_DNS_TABLE_INSERT(dns1);
                        atEarDB.OPEN_DNS_TABLE_INSERT(dns2);
                        isOpenDns1 = true;
                        isOpenDns2 = true;
                    } else if (isOpenDns1 == false && isOpenDns2 == true) {  //dns1 위험
                        atEarDB.OPEN_DNS_TABLE_INSERT(dns1);
                        isOpenDns1 = true;
                    } else if (isOpenDns1 == true && isOpenDns2 == false) {  //dns2 위험
                        atEarDB.OPEN_DNS_TABLE_INSERT(dns2);//insert
                        isOpenDns2 = true;
                    }
                    //dns등록과 동시에 접속기록 같이 변경
                    atEarDB.CONNECT_AP_TABLE_UPDATE_IP(bssid, geteway, dns1, dns2, 0);

                    switch (language) {
                        case 0: //영어
                            apChangMsg = "Saved the current DNS.";
                            break;
                        case 1: //한국어
                            apChangMsg = "DNS정보를 등록하였습니다.";
                            break;
                        case 2:  //일본어
                            apChangMsg = "現在のDNS保存しました。";
                        case 3:  //중국어
                            apChangMsg = "已注册DNS信息";
                            break;
                    }
                }
                btn_security_change.setVisibility(View.GONE);
                tv_securiy_check.setText(apChangMsg);
                tv_securiy_check.setTextColor(0xFFFFFFFF);
                break;

            case R.id.btn_disconnect:

                wifiManager.disconnect();
                wifiManager.removeNetwork(wifiManager.getConnectionInfo().getNetworkId());
                //   Log.e("netWORK_ID", String.valueOf((wifiManager.getConnectionInfo().getNetworkId());
                // wifiManager.saveConfiguration();  //저장

                getActivity().finish();
                break;
        }
    }

    private void Checking() {

        arpThread = new ARPThread(mContext, bssid);
        dnsThread = new DNSThread(mContext, bssid);

        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {

        }
        //    try {
 /*           getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {*/
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(mContext.getResources().getString(R.string.info_progressMsg));
        progressDialog.setCancelable(false);
        progressDialog.show();
        mHandler.sendEmptyMessageDelayed(1, 100);
        //     }
        //     });
      /*  } catch (Exception e) {

        }*/
        DhcpInfo dhcp = wifiManager.getDhcpInfo();
        geteway = intToIp(dhcp.gateway);
        dns1 = intToIp(dhcp.dns1);
        dns2 = intToIp(dhcp.dns2);

        //openDNS
        if (atEarDB.OPEN_DNS_TABLE_SELECT(dns1) == false) {
            //안전하지 않은 DNS
            isOpenDns1 = false;
        }
        if (atEarDB.OPEN_DNS_TABLE_SELECT(dns2) == false) {
            //안전하지 않은 DNS
            isOpenDns2 = false;
        }

        //change dns
        atEarDB.CONNECT_AP_TABLE_SELECT(bssid, ap_info.ssid, geteway,
                dns1, dns2);

        sb_apCheck = new StringBuffer();

        if (isOpenDns1 == false && isOpenDns2 == false) { //dns1, dns2 둘다 위험
            sb_apCheck.append(dns1 + "\n" + dns2 + "\n" + mContext.getResources().getString(R.string.info_notSafeMsg));
            tv_securiy_check.setTextColor(Color.RED);

            btn_security_change.setVisibility(View.VISIBLE);
            btn_security_change.setText(mContext.getResources().getString(R.string.btnInsertMsg));
            tv_securiy_check.setText(sb_apCheck);
            securiyBtnOption = 1;
        } else if (isOpenDns1 == false && isOpenDns2 == true) {  //dns1 위험
            sb_apCheck.append(dns1 + "\n" + mContext.getResources().getString(R.string.info_notSafeMsg));
            tv_securiy_check.setTextColor(Color.RED);
            btn_security_change.setVisibility(View.VISIBLE);
            btn_security_change.setText(mContext.getResources().getString(R.string.btnInsertMsg));
            tv_securiy_check.setText(sb_apCheck);
            securiyBtnOption = 1;
        } else if (isOpenDns1 == true && isOpenDns2 == false) {  //dns2 위험
            sb_apCheck.append(dns2 + "\n" + mContext.getResources().getString(R.string.info_notSafeMsg));
            tv_securiy_check.setTextColor(Color.RED);
            btn_security_change.setVisibility(View.VISIBLE);
            btn_security_change.setText(mContext.getResources().getString(R.string.btnInsertMsg));
            tv_securiy_check.setText(sb_apCheck);
            securiyBtnOption = 1;
        } else {
            //안전한 dns
            if (!arpThread.isAlive()) {
                arpThread.start();
                Log.e("arpThread", "start");
            }
            if (!dnsThread.isAlive()) {
                dnsThread.start();
            }
        }


    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    try {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {

                    }
                    break;
                case 1:
                    try {
                        //openDNS
                        if (isOpenDns1 == false || isOpenDns2 == false) {
                            try {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            } catch (Exception e) {

                            }
                        } else if (arpThread.getState() != Thread.State.RUNNABLE && arpThread.checkARPAsync.getStatus() == AsyncTask.Status.FINISHED && dnsThread.getState() != Thread.State.RUNNABLE) {
                            //공격탐지 thread end
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            sb_apCheck = new StringBuffer();
                            String dataChangeMsg = "";
                            int connect_risk = atEarDB.CONNECT_AP_TABLE_SELECT_SPOOFING(bssid);
                            Log.e("Risk_Type", String.valueOf(connect_risk));

                            sb_apCheck = new StringBuffer();


                            //  0 = null, 1 = arp,  2 = dns , 3= ip,   4= 기록검증
                            switch (connect_risk) {
                                case 0:

                                    sb_apCheck.append(mContext.getResources().getString(R.string.apCheckMsg));
                                    sb_apCheck.append(mContext.getResources().getString(R.string.SpoofingMsg));
                                    tv_securiy_check.setTextColor(Color.WHITE);
                                    btn_security_change.setVisibility(View.GONE);

                                    break;
                                case 1:
                                    sb_apCheck.append(mContext.getResources().getString(R.string.arpCheckMsg));
                                    tv_securiy_check.setTextColor(Color.RED);
                                    break;

                                case 2:
                                    sb_apCheck.append(mContext.getResources().getString(R.string.dnsCheckMsg));
                                    tv_securiy_check.setTextColor(Color.RED);

                                    break;

                                case 3:
                                    sb_apCheck.append(mContext.getResources().getString(R.string.ipCheckMsg));
                                    tv_securiy_check.setTextColor(Color.RED);
                                    break;
                                case 4:

                                    sb_apCheck = new StringBuffer();
                                    // sb_apCheck.append(dataChangeMsg);
                                    btn_security_change.setText(mContext.getResources().getString(R.string.btnChangeMsg));
                                    tv_securiy_check.setTextColor(Color.RED);
                                    sb_apCheck.append(mContext.getResources().getString(R.string.A_changed_inform_rewrite));
                                    btn_security_change.setVisibility(View.VISIBLE);
                                    btn_security_change.setBackgroundResource(R.drawable.btn_pressed);
                                    break;

                            }
                            tv_securiy_check.setText(sb_apCheck);
                        } else {
                            mHandler.sendEmptyMessageDelayed(1, 1000);
                            Log.e("replay", "aa");
                        }
                    } catch (Exception e) {

                    }
                    break;
                case 2:
                    try {
                        if (penTestingTask.getStatus() == AsyncTask.Status.FINISHED) {

                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        } else {
                            mHandler.sendEmptyMessageDelayed(2, 1000);
                        }
                    } catch (Exception e) {

                    }
                    break;
                default:


                    break;
            }
        }
    };


    private void wifiConnecting() {

        int netID = -1;

        if (tempNetId != -1 && wifiConnect != true) {
            netID = tempNetId;
            wifiManager.enableNetwork(tempNetId, true);
            Toast.makeText(mContext, ap_info.ssid + "  " + mContext.getResources().getString(R.string.connectingMsg), Toast.LENGTH_SHORT).show();

            ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo != null) {
                if (networkInfo.isConnected()) {
                    Toast.makeText(mContext, ap_info.ssid + "  " + mContext.getResources().getString(R.string.connectingMsg), Toast.LENGTH_SHORT).show();
                } else {
                    wifiManager.removeNetwork(netID);
                    wifiManager.saveConfiguration();
                    Log.e("removeNetID ", String.valueOf(netID));
                }
            } else {
                wifiManager.removeNetwork(netID);
                wifiManager.saveConfiguration();
                Log.e("removeNetID ", String.valueOf(netID));

            }


            //   Toast.makeText(mContext, ap_info.ssid + "  " + connectMsg, Toast.LENGTH_SHORT).show();

        } else if (wifiConnect == true) {
            // setResult(RESULT_FIRST_USER, i);
            wifiManager.disconnect();

        } else {
            String password = edt_password.getText().toString();

            wifiConfiguration = new WifiConfiguration();
            // wifi connect setting
            wifiConfiguration.SSID = "\"".concat(ap_info.ssid).concat("\"");
            wifiConfiguration.status = WifiConfiguration.Status.DISABLED;
            wifiConfiguration.priority = 40;

            if (ap_info.encryption.equals("OPEN")) {

                // OPEN
                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                wifiConfiguration.allowedAuthAlgorithms.clear();
                wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            } else if (ap_info.encryption.equals("WEP")) {

                // WEP
                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                wifiConfiguration.wepKeys[0] = "\"".concat(password).concat("\"");
                wifiConfiguration.wepTxKeyIndex = 0;
            } else if (ap_info.encryption.contains("WPA")) {
                // WPA, WPA2

                wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                wifiConfiguration.preSharedKey = "\"".concat(password).concat("\"");
            } else {

            }

            int networkId = wifiManager.addNetwork(wifiConfiguration);
            netID = networkId;
            wifiManager.enableNetwork(networkId, true);
            Log.e("networkID ::", String.valueOf(networkId));


            if (networkId != -1) {
                //    wifiManager.enableNetwork(networkId, true);
                // Log.e("network id", networkId + "");
                Toast.makeText(mContext, ap_info.ssid + "  " + mContext.getResources().getString(R.string.connectingMsg), Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.connectFailedMsg), Toast.LENGTH_SHORT).show();

                wifiManager.removeNetwork(netID);
                wifiManager.saveConfiguration();

            }


        }

        getActivity().finish();
    }


    private class PenTestingTask extends AsyncTask<Void, Void, Boolean> {

        int passwordSize = 0;

        String password = "";
        String successPassword = "";

        ArrayList<String> pwdList;
        boolean isConnect = false;

        HashMap<Integer, String> netIdHashMap;
        ProgressDialog stopProgressDialog;

        public PenTestingTask(ArrayList<String> passwordList) {
            this.pwdList = passwordList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            netIdHashMap = new HashMap<>();
            passwordSize = pwdList.size();

            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = new ProgressDialog(mContext);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.setMessage(mContext.getResources().getString(R.string.info_progressMsg));
                        progressDialog.setCancelable(false);
                        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                passwordSize = 1;
                                //   isApConnect = true;

                                stopProgressDialog = new ProgressDialog(mContext);
                                stopProgressDialog.setMessage(mContext.getResources().getString(R.string.station_progressMsg));
                                stopProgressDialog.setCancelable(false);
                                stopProgressDialog.show();
                            }
                        });
                        progressDialog.show();
                        progressDialog.setProgress(0);
                        progressDialog.setMax(passwordSize);
                        progressDialog.setProgressNumberFormat(null);

                        mHandler.sendEmptyMessageDelayed(2, 1300);

                    }
                });
            } catch (Exception e) {

            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            int netID = 0;
            int progressCount = 0;

            while (passwordSize > 0) {
                if (APINFO_VIEW == false) {
                    break;
                }
                progressDialog.setProgress(progressCount);

                password = pwdList.get(passwordSize - 1);

                wifiConfiguration = new WifiConfiguration();
                // wifi connect setting
                wifiConfiguration.SSID = "\"".concat(ap_info.ssid).concat("\"");
                wifiConfiguration.status = WifiConfiguration.Status.DISABLED;
                wifiConfiguration.priority = 40;
                if (ap_info.encryption.equals("WEP")) {
                    // WEP
                    wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                    wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

                    wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                    wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                    wifiConfiguration.wepKeys[0] = "\"".concat(password).concat("\"");
                    wifiConfiguration.wepTxKeyIndex = 0;
                } else if (ap_info.encryption.contains("WPA")) {
                    // WPA, WPA2
                    wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                    wifiConfiguration.preSharedKey = "\"".concat(password).concat("\"");
                } else {

                }
                isConnect = false;

                wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

                int networkId = wifiManager.addNetwork(wifiConfiguration);
                boolean connecting = wifiManager.enableNetwork(networkId, true);

            /*    try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                Log.e("networkID ::", String.valueOf(networkId));
                if (networkId != -1) {
                    Log.e("pentesting", "connect :" + password);
                    //  passwordSize = 1;
                } else {
                    Log.e("pentesting", "failed :" + password);
                    break;
                }

                if (networkId == -1) {
                    //시스템 네크워크
                } else {
                    netIdHashMap.put(networkId, password);

                    if (connecting == true) {
                        try {
                            Thread.sleep(7000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.e("wifiInfo_", "connecting ");
                        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                        //   while (networkInfo.isAvailable()) {
                        if (networkInfo.isConnected()) {
                            Log.e("wifiInfo_", " network connecting ");
                            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                            if (wifiInfo.getBSSID() != null && (wifiInfo.getBSSID().toUpperCase().toString().equals(bssid.toUpperCase()))) {
                                //연결
                                isApConnect = true;
                                isConnect = true;  //성공
                                Log.e("wifiInfo_", wifiInfo.getBSSID() + " / " + wifiInfo.getSSID());

                                successPassword = netIdHashMap.get(wifiManager.getConnectionInfo().getNetworkId());

                                passwordSize = 1;
                            }
                        }
                    }
                }
                passwordSize--;
                progressCount++;
            }
            return isConnect;
        }

        @Override
        protected void onPostExecute(Boolean isConnect) {
            super.onPostExecute(isConnect);
            RemoveWifiNetworks();

            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }

            try {
                if (stopProgressDialog.isShowing()) {
                    stopProgressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            if (APINFO_VIEW == true) {
                if (isConnect == true) {//성공
                    atEarDB.PACKET_TABLE_UPDATE_PANTEST(ap_info, 2, successPassword);
                    Log.e("Attack", "success");

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                    Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                    switch (language) {
                        case 0: //영어
                            attackSuccessMsg = "Attack Success.\nThe password is " + successPassword;
                            break;

                        case 1: //한국어
                            attackSuccessMsg = "공격에 성공했습니다.\n패스워드는 " + successPassword + " 입니다.";
                            break;

                        case 2:  //일본어
                            attackSuccessMsg = "成功しました\nキー「" + successPassword + "」を取得しました。";
                            break;

                        case 3:  //중국어
                            attackSuccessMsg = "攻击成功。密码为 " + successPassword;
                            break;

                    }

                    tv_title.setText(mContext.getResources().getString(R.string.pentestmsg));
                    tv_text.setText(attackSuccessMsg);
                    btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                    btn_no.setVisibility(View.GONE);
                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });
                    dialog.show();

                } else { //실패
                    atEarDB.PACKET_TABLE_UPDATE_PANTEST(ap_info, 1, "none");
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.toastConnectFailedMsg), Toast.LENGTH_SHORT).show();

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                    Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                    tv_title.setText(mContext.getResources().getString(R.string.pentestmsg));
                    tv_text.setText(mContext.getResources().getString(R.string.toastConnectFailedMsg));
                    btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                    btn_no.setVisibility(View.GONE);
                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });

                    dialog.show();

                    WifiManager connect = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

                    WifiInfo wifiInfo = connect.getConnectionInfo();
                    if (bssid != null && wifiInfo.getBSSID() != null) {
                        if (!bssid.toUpperCase().equals(wifiInfo.getBSSID().toUpperCase())) {
                            wifiManager.removeNetwork(tempNetId);
                            wifiManager.saveConfiguration();
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        APINFO_VIEW = false;
    }

    //네트워크 아이디 삭제
    private void RemoveWifiNetworks() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            wifiManager.removeNetwork(i.networkId);  //원하는 네트워크 ID 삭제
            wifiManager.saveConfiguration();
        }
    }

    //wifi 연결 여부
    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifi.isConnected())
            return true;

        return false;
    }


    //네트워크 이벤트
    private boolean netWorkConnecting() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //  NetworkInfo mobile = connectivityManager.getActiveNetworkInfo();
        NetworkInfo.State wifi = connectivityManager.getNetworkInfo(1).getState();

        Log.e("networkInfo.getState()", String.valueOf(wifi));

        if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.DISCONNECTED) {
            return true;
        } else {
            return false;
        }
    }


    private String intToIp(int i) {
        return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
    }

    //ping 8.8.8.8
    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
