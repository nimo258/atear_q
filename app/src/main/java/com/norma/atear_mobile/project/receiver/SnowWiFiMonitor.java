package com.norma.atear_mobile.project.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by 효진 on 2016-09-09.
 */
public class SnowWiFiMonitor extends BroadcastReceiver {
    public final static int WIFI_STATE_DISABLED = 0x00;
    public final static int WIFI_STATE_DISABLING = WIFI_STATE_DISABLED + 1;
    public final static int WIFI_STATE_ENABLED = WIFI_STATE_DISABLING + 1;
    public final static int WIFI_STATE_ENABLING = WIFI_STATE_ENABLED + 1;
    public final static int WIFI_STATE_UNKNOWN = WIFI_STATE_ENABLING + 1;
    public final static int NETWORK_STATE_CONNECTED = WIFI_STATE_UNKNOWN + 1;
    public final static int NETWORK_STATE_CONNECTING = NETWORK_STATE_CONNECTED + 1;
    public final static int NETWORK_STATE_DISCONNECTED = NETWORK_STATE_CONNECTING + 1;
    public final static int NETWORK_STATE_DISCONNECTING = NETWORK_STATE_DISCONNECTED + 1;
    public final static int NETWORK_STATE_SUSPENDED = NETWORK_STATE_DISCONNECTING + 1;
    public final static int NETWORK_STATE_UNKNOWN = NETWORK_STATE_SUSPENDED + 1;

    public final static int FOUR_WAY_HANSHAKE = NETWORK_STATE_UNKNOWN + 1;
    public final static int ASSOCIATED = FOUR_WAY_HANSHAKE + 1;
    public final static int ASSOCIATING = ASSOCIATED + 1;
    public final static int COMPLETED = ASSOCIATING + 1;
    public final static int DISCONNECTED = COMPLETED + 1;
    public final static int DORMANT = DISCONNECTED + 1;
    public final static int GROUP_HANDSHAKE = DORMANT + 1;
    public final static int INACTIVE = GROUP_HANDSHAKE + 1;
    public final static int INVALID = INACTIVE + 1;
    public final static int SCANNING = INVALID + 1;
    public final static int UNINITIALIZED = SCANNING + 1;

    public final static int SCAN_RESULTS = -1;

    public interface OnChangeNetworkStatusListener {
        public void OnChange(int status);
    }

    private WifiManager mWiFiManager = null;
    private ConnectivityManager mConnManager = null;
    private OnChangeNetworkStatusListener mOnChangeNetworkStatusListener = null;

    public void unregisterReceiver(Activity activity, BroadcastReceiver receiver) {
        try {
            if (receiver != null) {
                activity.unregisterReceiver(receiver);
                Log.d("receiver", "unregisted BroadcastReceiver");
            }
        } catch (IllegalArgumentException e) {
            Log.d("receiver", "failed to unregister BroadcastReceiver");
        }
    }

    public SnowWiFiMonitor(Context context) {
        mWiFiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        mConnManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // mSupplicant = (WifiManager) context.getSystemService(Context.);
    }

    public void setOnChangeNetworkStatusListener(
            OnChangeNetworkStatusListener listener) {
        mOnChangeNetworkStatusListener = listener;
    }

    public SnowWiFiMonitor() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("SnowWiFiMonitor", "onReceive:" + intent);
        if (mOnChangeNetworkStatusListener == null)
            return;

        String strAction = intent.getAction();
        Log.d("SnowWiFiMonitor", "onReceive intent:" + strAction);

        // if (strAction.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
        // mOnChangeNetworkStatusListener.OnChange(SCAN_RESULTS);
        // // }else
        // if(strAction.equals(mWiFiManager.NETWORK_STATE_CHANGED_ACTION)){
        // //// sendBroadcast(new Intent("wifi.ON_NETWORK_STATE_CHANGED"));
        //
        // }else
        if (strAction.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
            Log.d("SnowWiFiMonitor",
                    "onReceive wifi:" + mWiFiManager.getWifiState());
            switch (mWiFiManager.getWifiState()) {
                case WifiManager.WIFI_STATE_DISABLED:
                    Log.e("snow disabled", "a");
                    mOnChangeNetworkStatusListener.OnChange(WIFI_STATE_DISABLED);
                 //   context.sendBroadcast(intent);
                    break;
                case WifiManager.WIFI_STATE_DISABLING:
                    Log.e("snow disabling", "a");
                    mOnChangeNetworkStatusListener.OnChange(WIFI_STATE_DISABLING);
                 //   context.sendBroadcast(intent);
                    break;
                case WifiManager.WIFI_STATE_ENABLED:
                    Log.e("snow enabled", "a");
                    mOnChangeNetworkStatusListener.OnChange(WIFI_STATE_ENABLED);
                 //   context.sendBroadcast(intent);
                    break;
                case WifiManager.WIFI_STATE_ENABLING:
                    Log.e("snow enabling", "a");
                    mOnChangeNetworkStatusListener.OnChange(WIFI_STATE_ENABLING);
                  //  context.sendBroadcast(intent);
                    break;
                case WifiManager.WIFI_STATE_UNKNOWN:
                    mOnChangeNetworkStatusListener.OnChange(WIFI_STATE_UNKNOWN);
                   // context.sendBroadcast(intent);
                    break;
            }
        } else if (strAction.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            Log.d("SnowWiFiMonitor", "ConnectivityManager conn");
            // try {
            NetworkInfo networkInfoWifi = mConnManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            Log.d("SnowWiFiMonitor", "onReceive conn:" + networkInfoWifi);

            if (networkInfoWifi != null
                    && networkInfoWifi.isAvailable() == true) {
                if (networkInfoWifi.getState() == NetworkInfo.State.CONNECTED) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_CONNECTED);
            //        context.sendBroadcast(intent);
                } else if (networkInfoWifi.getState() == NetworkInfo.State.CONNECTING) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_CONNECTING);
              //      context.sendBroadcast(intent);
                } else if (networkInfoWifi.getState() == NetworkInfo.State.DISCONNECTED) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_DISCONNECTED);
                //    context.sendBroadcast(intent);
                } else if (networkInfoWifi.getState() == NetworkInfo.State.DISCONNECTING) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_DISCONNECTING);
               //     context.sendBroadcast(intent);
                } else if (networkInfoWifi.getState() == NetworkInfo.State.SUSPENDED) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_SUSPENDED);
                 //   context.sendBroadcast(intent);
                } else if (networkInfoWifi.getState() == NetworkInfo.State.UNKNOWN) {
                    mOnChangeNetworkStatusListener
                            .OnChange(NETWORK_STATE_UNKNOWN);
              //      context.sendBroadcast(intent);
                }
            }
            // } catch (Exception e) {
            // Log.d("SnowWiFiMonitor", "onReceive e:"+ e);
            // }
            // }else
            // if(strAction.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)){
            // // Log.d("", "");
            // /*handleSupplicantStateChanged(
            // (SupplicantState)
            // intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE),
            // intent.hasExtra(WifiManager.EXTRA_SUPPLICANT_ERROR),
            // intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, 0));*/
        } else if (strAction
                .equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {
            Log.d("SnowWiFiMonitor",
                    "onReceive wifi:" + mWiFiManager.getWifiState());
            WifiInfo info = mWiFiManager.getConnectionInfo();
            SupplicantState sup = info.getSupplicantState();

            if (SupplicantState.FOUR_WAY_HANDSHAKE.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(FOUR_WAY_HANSHAKE);

            } else if (SupplicantState.ASSOCIATED.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(ASSOCIATED);

            } else if (SupplicantState.ASSOCIATING.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(ASSOCIATING);

            } else if (SupplicantState.COMPLETED.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(COMPLETED);

            } else if (SupplicantState.DISCONNECTED.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(DISCONNECTED);

            } else if (SupplicantState.DORMANT.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(DORMANT);

            } else if (SupplicantState.GROUP_HANDSHAKE.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(GROUP_HANDSHAKE);

            } else if (SupplicantState.INACTIVE.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(INACTIVE);

            } else if (SupplicantState.INVALID.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(INVALID);

            } else if (SupplicantState.SCANNING.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(SCANNING);

            } else if (SupplicantState.UNINITIALIZED.equals(sup)) {
                mOnChangeNetworkStatusListener.OnChange(UNINITIALIZED);
            }
        }
    }

	/*
	 * private void handleSupplicantStateChanged(SupplicantState state, boolean
	 * hasError, int error) { if (hasError) {
	 * mSupplicantState.setText("ERROR AUTHENTICATING"); Toast.makeText(get,
	 * text, duration) } else { setSupplicantStateText(state); } }
	 */
}
