package com.norma.atear_mobile.project.comparator;

import com.norma.atear_mobile.data.Ap_info;

import java.util.Comparator;


/**
 * Created by hyojin on 7/8/16.
 */
public class AlertComparator implements Comparator<Ap_info> {

    int sort = 0;  // 0 위험순 ,  1 안전순

    @Override
    public int compare(Ap_info a1, Ap_info a2) {
        if (sort == 1) {
            return a1.alert.compareToIgnoreCase(a2.alert);  //안전순
        } else {
            return a2.alert.compareToIgnoreCase(a1.alert);  //위험순
            //  return a2.risk < a1.risk ? -1 : a2.risk > a1.risk ? 1 : 0;
        }
    }

    public void sortType(int type) {
        sort = type;
    }
}