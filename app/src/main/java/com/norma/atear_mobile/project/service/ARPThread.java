package com.norma.atear_mobile.project.service;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.text.format.Formatter;
import android.util.Log;

import com.norma.atear_mobile.db.AtEarDB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
/**
 * Created by hyojin on 7/14/16.
 */


public class ARPThread extends Thread {

    public  boolean arpToggle = false;

    private String gateIp;
    private String gateSsid;
    private String ipTemp;
    private String gateMac;
    private BufferedReader br = null;
    private WifiInfo info = null;
    private String pollutionGateTemp;
    private WifiManager wifiManager;
    public CheckARPAsync checkARPAsync;
    private ArrayList<String> reachIpList;

    private AtEarDB atEarDB;

    private String bssid;

    DhcpInfo dhcp;
    String tempGateIp;
    String tempGateMac;
    String tempGateSsid;

    Context mContext;


    public ARPThread(Context context ,String bssid) {
        this.mContext = context;
        arpToggle = true;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        info = wifiManager.getConnectionInfo();
        gateSsid = null;
        gateIp = null;
        ipTemp = null;

        this.bssid = bssid;
        atEarDB = new AtEarDB(context);

        atEarDB.CONNECT_AP_TABLE_UPDATE_SPOOFING(bssid, 0, "null","null", "null");
    }

    public void start() {
        super.start();
    }


    @Override
    public void run() {

                int num = 0;
                try {
                    checkARPAsync = new CheckARPAsync();

                    // br = new BufferedReader(new FileReader("/proc/net/arp"));
                    // String line;
                    // String ssid = info.getSSID();
                    // String ip =
                    // Formatter.formatIpAddress(info.getIpAddress());
                    // while ((line = br.readLine()) != null) {
                    // Log.d("ARP", "GateTemp : " + gateIp);
                    // Log.d("ARP", "SsidTemp : " + gateSsid);
                    // Log.e("ARP", "ssid : " + ssid);
                    // Log.e("ARP", "line :" + line + ", num : " + num);
                    //
                    // if (num >= 1) {
                    // String abc3 = line.substring(41, 58); //mac
                    // String apip = line.split(" ")[0]; //ip
                    //
                    // DhcpInfo dhcp = wifiManager.getDhcpInfo();
                    // String gateway = Formatter.formatIpAddress(dhcp.gateway);
                    // Log.e("ARP", "gate 1 line : " + abc3);
                    // Log.d("ARP", "apip : " + apip);
                    // Log.e("ARP", "gateway : " + gateway);
                    // Log.d("ARP", "ipTemp : " + ipTemp);
                    // Log.d("ARP", "ip : " + ip);
                    // Log.e("ARP", "GateTemp : " + gateIp);
                    // Log.e("ARP", "SsidTemp : " + gateSsid);
                    // if (gateIp != null && !gateIp.equals("00:00:00:00:00:00")
                    // &&
                    // !abc3.equals("00:00:00:00:00:00")) {
                    // if (!gateIp.equals(abc3) && apip.equals(gateway) &&
                    // ssid.equals(gateSsid) && ip.equals(ipTemp) &&
                    // !gateIp.substring(0, 8).equals(abc3.substring(0, 8))) {
                    //
                    // int macCut = Integer.parseInt((gateIp.substring(16, 17)),
                    // 16);
                    // int abcCut = Integer.parseInt(abc3.substring(16, 17),
                    // 16);
                    //
                    // Log.e("ARP", "macCut : " + macCut + ", abcCut : " +
                    // abcCut);
                    //
                    // if (gateIp.substring(1, 16).equals(abc3.substring(1, 16))
                    // &&
                    // Math.abs((abcCut - macCut)) == 1) {
                    // Log.d("ARP", "���� AP��");
                    // gateIp = abc3;
                    // gateSsid = ssid;
                    // } else {
                    //
                    // Log.d("ARP", "case5 Danger!!");
                    // Log.d("DARP", "case5 Danger!!");
                    // Log.e("DARP", "gatewayTemp : " + apip);
                    // Log.e("DARP", "gateway : " + gateway);
                    // Log.d("DARP", "ipTemp : " + ipTemp);
                    // Log.d("DARP", "ip : " + ip);
                    // Log.e("DARP", "MacTemp : " + gateIp);
                    // Log.d("DARP", "Mac : " + abc3);
                    // Log.e("DARP", "SsidTemp : " + gateSsid);
                    // Log.e("DARP", "ssid : " + ssid);
                    // Log.e("DARP", "line :" + line + ", num : "
                    // + num);
                    // while ((line = br.readLine()) != null) {
                    // Log.d("DARP", "line :" + line
                    // + ", num : " + ++num);
                    // }
                    // pollutionGateTemp = abc3;
                    // mHandler.sendEmptyMessage(6);
                    // mQuit = false;
                    // }
                    // break;
                    // }
                    // }
                    // if ((!ssid.equals(gateSsid) || !ip.equals(ipTemp)) &&
                    // gateway.equals(apip)) {
                    // gateIp = abc3;
                    // gateSsid = ssid;
                    // ipTemp = ip;
                    // }
                    // if ((gateIp == null || gateIp.equals("")) && (gateSsid ==
                    // null || gateSsid.equals("")) && (ipTemp == null ||
                    // ipTemp.equals("")) && gateway.equals(apip)) {
                    // gateIp = abc3;
                    // gateSsid = ssid;
                    // ipTemp = ip;
                    // }
                    // if (abc3 == null || abc3.equals("")) {
                    // mQuit = false;
                    // }
                    // }
                    // num++;
                    // }
                    //Log.e("wifi arp service on", "yes!");
                    dhcp = wifiManager.getDhcpInfo();
                    tempGateIp = Formatter.formatIpAddress(dhcp.gateway);
                    tempGateMac = getMacFromArpCache(tempGateIp);
                    tempGateSsid = info.getSSID();


                    if (gateIp == null || gateMac == null || gateSsid == null) { // connecting
                        gateIp = tempGateIp;
                        gateMac = tempGateMac;
                        gateSsid = tempGateSsid;

                        checkARPAsync.execute();
                    } else {
                        if (gateIp.equals(tempGateIp) && gateMac.equals(tempGateMac)) { // ip
                            // same,
                            // mac
                            // same
                            gateIp = tempGateIp;
                            gateMac = tempGateMac;
                            gateSsid = tempGateSsid;
                            arpToggle =false;
                        } else if (gateIp.equals(tempGateIp) && !gateMac.equals(tempGateMac)) { // ip
                            // same,
                            // mac
                            // dif
                            checkARPAsync.execute();

                        } else if (!gateIp.equals(tempGateIp) && !gateMac.equals(tempGateMac)) { // ip
                            // dif,
                            // mac
                            // dif
                            if (tempGateIp == null || tempGateMac == null || tempGateSsid == null) { // temporal
                                // disconnect
                                arpToggle = false;
                            } else { // wifi handoff
                                gateIp = tempGateIp;
                                gateMac = tempGateMac;
                                gateSsid = tempGateSsid;
                                arpToggle = false;
                            }
                        } else { // ip dif, mac same
                            arpToggle = false;
                        }
                    }

                } catch (Exception e1) {
                    // Log.e("ARP Thread", "thread exception", e1);
                    arpToggle = false;
                }

                // finally {
                // if (br != null) {
                // try {
                // br.close();
                // } catch (IOException e) {
                // e.printStackTrace();
                // }
                // }
                // }




    }

    private void readArpCache() {
        BufferedReader br = null;
        String page = "";
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;

            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && !splitted[3].equals("00:00:00:00:00:00")) {
                    String ip = splitted[0];
                    reachIpList.add(ip);
                    page += line;
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        reachIpList.remove(0);
        reachIpList.remove(gateIp);
    }

    private String getMacFromArpCache(String ip) {
        if (ip == null)
            return null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        if (mac.matches("00:00:00:00:00:00")) {
                            break;
                        } else {
                            return mac;
                        }
                    } else {
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public class CheckARPAsync extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            // pollutionGateTemp = tempGateMac;

            // Send Ping
            InetAddress ia;

            String endIp = tempGateIp;
            try {
                endIp = endIp.substring(0, endIp.lastIndexOf("."));
                reachIpList = new ArrayList<String>();

                for (int i = 0; i < 255; i++) {
                    String Ip = endIp + "." + String.valueOf(i + 1);

                    // PingArguments arguments = new PingArguments.Builder()
                    // .url(Ip).timeout(5000).bytes(32).build();
                    //
                    // PingResult results = Ping.ping(arguments, Backend.UNIX);
                    //
                    try {

                        ia = InetAddress.getByName(Ip);
                        // netIntfc = NetworkInterface.getByInetAddress(ia);
                        if (ia.isReachable(15) == true) {
                            // Log.e("reach", Ip);
                        }
                        if (i == 254) {
                            // Log.d("ipscan", "finish");
                        }

                    } catch (Exception e) {
                        // Log.e("ping exception", String.valueOf(e));
                        arpToggle = false;
                    }

                    // try {
                    // if (ping(Ip) == true) {
                    // reachIpList.add(Ip);
                    // Log.e("Yes",Ip);
                    // }
                    // } catch (IOException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // } catch (InterruptedException e) {
                    // // TODO Auto-generated catch block
                    // e.printStackTrace();
                    // }

                }
                readArpCache();
            } catch (Exception e) {
                Log.e("ARPThread_Exception", "wifi is off");
                arpToggle = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (reachIpList.size() > 0) {
                    String mac = new String();
                    for (final String reachIp : reachIpList) {
                        mac = getMacFromArpCache(reachIp);
                        if (mac.equals(tempGateMac)) {
                            Log.e("arp spoofing detected", tempGateMac + " / " + mac);
                            gateIp = null;
                            tempGateIp = null;
                            tempGateMac = null;

                            // arp spoofing
                            final String finalMac = mac;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    arpPopup(reachIp, finalMac);
                                }
                            }).start();
                            //arpPopup(reachIp, mac);
                            break;
                        }
                    }

                    int isDuplicate = 0;
                    String ipAddr = "";
                    String ipMac = "";
                    for (int i = 0; i < reachIpList.size(); i++) {
                        for (int j = i + 1; j < reachIpList.size(); j++) {
                            if (reachIpList.get(i).equals(reachIpList.get(j))) { // duplicate
                                // IP
                                ipAddr = reachIpList.get(j);
                                ipMac = getMacFromArpCache(reachIpList.get(j));
                                isDuplicate = 1;
                                break;
                            } else {
                                continue;
                            }
                        }

                        if (isDuplicate == 1) {
                            // ip jamming
                            // ip popup
                            final String finalIpAddr = ipAddr;
                            final String finalMac1 = mac;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    ipPopup(finalIpAddr, finalMac1);
                                }
                            }).start();

                            //ipPopup(ipAddr, mac);
                            gateIp = null;
                            tempGateIp = null;
                            tempGateMac = null;
                            break;
                        } else {
                            continue;
                        }
                    }

                    // roaming
                }else{
                    arpToggle = false;
                }
            } catch (Exception e) {
                // Log.e("arpthread", "onPostException", e);
                arpToggle = false;
            }

        }
    }

    public void arpPopup(String reachIp, String mac) {

        long now = System.currentTimeMillis();

        Date date = new Date(now);

        SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일");
        SimpleDateFormat CurTimeFormat = new SimpleDateFormat("HH시 mm분 ss초");

        String strCurDate = CurDateFormat.format(date);
        String strCurTime = CurTimeFormat.format(date);
        String strNow = strCurDate + " " + strCurTime;
        Log.e("spoofing","arp ok");
        atEarDB.CONNECT_AP_TABLE_UPDATE_SPOOFING(bssid, 1, reachIp, mac,strNow);
        arpToggle = false;

    }

    public void ipPopup(String reachIp, String mac) {

        long now = System.currentTimeMillis();

        Date date = new Date(now);

        SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일");
        SimpleDateFormat CurTimeFormat = new SimpleDateFormat("HH시 mm분");

        String strCurDate = CurDateFormat.format(date);
        String strCurTime = CurTimeFormat.format(date);
        String strNow = strCurDate + " " + strCurTime;

        atEarDB.CONNECT_AP_TABLE_UPDATE_SPOOFING(bssid, 3, reachIp,mac, strNow);
        arpToggle =false;

    }


}
