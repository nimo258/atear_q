package com.norma.atear_mobile.project.device;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.norma.atear_mobile.R;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/28/16.
 */
public class DeviceAdapter extends BaseAdapter {

    private ArrayList<DeviceInfo> deviceList;
    private ViewHolder holder;
    private Context mContext;

    public DeviceAdapter(Context mContext, ArrayList<DeviceInfo> deviceList) {
        super();
        this.deviceList = deviceList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return deviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return deviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            holder = new ViewHolder();


            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.device_item, null);

            holder.tv_ip = (TextView) convertView.findViewById(R.id.tv_ip);
            holder.tv_bssid = (TextView) convertView.findViewById(R.id.tv_bssid);
            holder.tv_deviceName = (TextView) convertView.findViewById(R.id.tv_deviceName);
            holder.tv_vendor = (TextView) convertView.findViewById(R.id.tv_vendor);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DeviceInfo data = deviceList.get(position);

        holder.tv_ip.setText(data.ip);

        holder.tv_bssid.setText(data.mac);

        holder.tv_deviceName.setText(data.deviceName);


        if (data.vendorName.equals("")) {
            holder.tv_vendor.setText("none");
        } else {
            holder.tv_vendor.setText(data.vendorName);
        }
        return convertView;
    }


    class ViewHolder {

        public TextView tv_ip;

        public TextView tv_bssid;

        public TextView tv_deviceName;

        public TextView tv_vendor;


    }

}
