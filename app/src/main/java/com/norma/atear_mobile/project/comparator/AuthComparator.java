package com.norma.atear_mobile.project.comparator;

import com.norma.atear_mobile.data.Ap_info;

import java.util.Comparator;

/**
 * Created by hyojin on 2017-08-10.
 */

public class AuthComparator implements Comparator<Ap_info> {
    @Override
    public int compare(Ap_info a1, Ap_info a2) {
        return a2.alert.compareToIgnoreCase(a1.alert);
    }


}