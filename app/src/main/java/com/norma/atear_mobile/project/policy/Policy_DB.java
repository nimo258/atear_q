package com.norma.atear_mobile.project.policy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hyojin on 7/11/16.
 *
 * 사용 안함.
 *
 */
public class Policy_DB extends SQLiteOpenHelper {

	private static final String DB_NAME = "Policy_DB";
	//table
	private static final  String SSID_TABLE = "ssidtable";
	private static final  String BSSID_TABLE = "bssidtable";


	private static final String SSID = "ssid";
	private static final String BSSID = "bssid";

	private static final String SSID_TABLE_CREATE = "create table "
			+ SSID_TABLE + "(" + SSID + " TEXT)";

	private static final String BSSID_TABLE_CREATE = "create table "
			+ BSSID_TABLE + "(" + BSSID + " TEXT)";

	public Policy_DB(Context context) {
		super(context, DB_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SSID_TABLE_CREATE);
		db.execSQL(BSSID_TABLE_CREATE);

		/**
		 * 예시
		 * Norma_2.4G , Norma_5G , Norma_Phone , Norma_test
		 */

	//	db.execSQL("insert into " + SSID_TABLE + " values('Norma_2.4G');");
	//	db.execSQL("insert into " + SSID_TABLE + " values('Norma_5G');");
	//	db.execSQL("insert into " + SSID_TABLE + " values('Norma_Phone');");
	//	db.execSQL("insert into " + SSID_TABLE + " values('Norma_test');");

	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists " + SSID_TABLE_CREATE);
		db.execSQL("drop table if exists " + BSSID_TABLE_CREATE);
	}

/*	public boolean SSID_TABLE_SELECT(String data) {

		SQLiteDatabase db = getWritableDatabase();

		String sql = "select " + SSID + " from " + SSID_TABLE + " where " + SSID + " = '" + data + "'";

		Cursor c = db.rawQuery(sql, null);

		return c.getCount() > 0;

	}

	public boolean BSSID_TABLE_SELECT(String data) {

		SQLiteDatabase db = getWritableDatabase();

		String sql = "select " + BSSID + " from " + BSSID_TABLE + " where " + BSSID + " = '" + data + "'";

		Cursor c = db.rawQuery(sql, null);

		return c.getCount() > 0;

	}*/

}
