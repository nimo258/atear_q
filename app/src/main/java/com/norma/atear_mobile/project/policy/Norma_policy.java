package com.norma.atear_mobile.project.policy;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Created by hyojin on 7/11/16.
 */
public class Norma_policy {


    private  Policy_HashMap policy_hashMap;
    public int POLICY = 0;

    private final static String PACKAGE_NAME = "com.norma.atear_mobile";

    public Norma_policy() {
        policy_hashMap = new Policy_HashMap();
    }

    public void non_policy() {


        policy_hashMap.clear();

        POLICY = 0;
    }


    public void policy_isms_auth() {


        File json_file = new File("/data/data/"+PACKAGE_NAME+"/Policy/policy_isms_auth.txt");

        if (json_file.exists()) {

            StringBuilder sb = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(json_file), "UTF-8"));
                String line = "";

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    //	Log.e("line", line);
                }

                br.close();
            } catch (Exception e) {
                //Log.e("json parsing exception", String.valueOf(e));
            }

            String total_json = sb.toString();
            JSONArray total_array = null;

            try {
                total_array = new JSONArray(total_json);

                policy_hashMap.clear();
                for (int i = 0; i < total_array.length(); i++) {

                    JSONObject json_object = total_array.getJSONObject(i);
                    String comment = json_object.getString("comment");
                    String enc = json_object.getString("enc");
                    String ssid = json_object.getString("ssid");
                    //String wps = json_object.getString("wps");
                    String authorized = json_object.getString("authorized");
                    String alert = json_object.getString("alert");
                    String type = json_object.getString("type");


                    Policy_info policy_info = new Policy_info();
                    Policy_Result policy_result = new Policy_Result();

                    policy_info.ssid = ssid;
                    policy_info.enc = enc;
                    policy_info.type = type;
                    policy_info.authorized = authorized;

                    policy_result.comment = comment;
                    policy_result.alert = alert;

                    policy_hashMap.put(policy_info, policy_result);
          //          Log.e("Norma_Policy", "data : " + policy_info.ssid + " , " + policy_info.type + " , " + policy_info.authorized + " , " + policy_info.enc + " , " +comment+" , "+alert);
                }

            } catch (Exception e) {
                Log.e("Norma_policy_TAG", "json error");
            }
        } else {
            Log.e("Norma_policy_TAG", "not file");
        }
        POLICY = 1;
    }

    public void policy_iso_auth() {


        File json_file = new File( "/data/data/"+PACKAGE_NAME+"/Policy/policy_iso_auth.txt");

        if (json_file.exists()) {

            StringBuilder sb = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(json_file),  "UTF-8"));
                String line = "";

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    //	Log.e("line", line);
                }

                br.close();
            } catch (Exception e) {
                //Log.e("json parsing exception", String.valueOf(e));
            }

            String total_json = sb.toString();
            JSONArray total_array = null;

            try {
                total_array = new JSONArray(total_json);

                policy_hashMap.clear();
                for (int i = 0; i < total_array.length(); i++) {

                    JSONObject json_object = total_array.getJSONObject(i);
                    String comment = json_object.getString("comment");
                    String enc = json_object.getString("enc");
                    String ssid = json_object.getString("ssid");
                    //String wps = json_object.getString("wps");
                    String authorized = json_object.getString("authorized");
                    String alert = json_object.getString("alert");
                    String type = json_object.getString("type");

                    Policy_info policy_info = new Policy_info();
                    Policy_Result result_info = new Policy_Result();

                    policy_info.ssid = ssid;
                    policy_info.enc = enc;
                    policy_info.type = type;
                    policy_info.authorized = authorized;

                    result_info.comment = comment;
                    result_info.alert = alert;


                    policy_hashMap.put(policy_info, result_info);

                }

            } catch (Exception e) {
                Log.e("Norma_policy_TAG", "json error");
            }
        } else {
            Log.e("Norma_policy_TAG", "not file");
        }
        POLICY = 2;
    }

    public void policy_norma_auth() {


        File json_file = new File( "/data/data/"+PACKAGE_NAME+"/Policy/policy_norma_auth.txt");

        if (json_file.exists()) {

            StringBuilder sb = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(json_file),  "UTF-8"));
                String line = "";

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    //	Log.e("line", line);
                }

                br.close();
            } catch (Exception e) {
                //Log.e("json parsing exception", String.valueOf(e));
            }

            String total_json = sb.toString();
            JSONArray total_array = null;

            try {
                total_array = new JSONArray(total_json);

                policy_hashMap.clear();
                for (int i = 0; i < total_array.length(); i++) {


                    JSONObject json_object = total_array.getJSONObject(i);
                    String comment = json_object.getString("comment");
                    String enc = json_object.getString("enc");
                    String ssid = json_object.getString("ssid");
                    String wps = json_object.getString("wps");
                    String authorized = json_object.getString("authorized");
                    String alert = json_object.getString("alert");
                    String type = json_object.getString("type");

                    Policy_info policy_info = new Policy_info();
                    Policy_Result result_info = new Policy_Result();

                    //hash map key
                    policy_info.ssid = ssid;
                    policy_info.enc = enc;
                    policy_info.type = type;
                    policy_info.authorized = authorized;
                    policy_info.wps = wps;

                    //hash map value
                    result_info.comment = comment;
                    result_info.alert = alert;


                    policy_hashMap.put(policy_info, result_info);

                }

            } catch (Exception e) {
                Log.e("Norma_policy_TAG", "json error");
            }
        } else {
            Log.e("Norma_policy_TAG", "not file");
        }
        POLICY = 3;
    }

    public Policy_Result resultData(Policy_info info) {

//        Log.e("testest", policy_hashMap.get(info).alert);

        return policy_hashMap.get(info);
    }

    public Policy_Result nonData() {

        Policy_Result policy_result = new Policy_Result();

        policy_result.comment = "none";
        policy_result.alert = "none";
        return policy_result;
    }
}
