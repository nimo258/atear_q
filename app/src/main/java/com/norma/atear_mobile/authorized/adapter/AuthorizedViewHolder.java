package com.norma.atear_mobile.authorized.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.norma.atear_mobile.R;

/**
 * Created by hyojin on 2018-03-22.
 */

public class AuthorizedViewHolder extends RecyclerView.ViewHolder {

    TextView tv_location;

    TextView tv_bssid;

    TextView tv_ssid;

    CheckBox cb_policy;

    public AuthorizedViewHolder(View itemView) {
        super(itemView);
        tv_location = (TextView) itemView.findViewById(R.id.tv_location);
        tv_ssid = (TextView) itemView.findViewById(R.id.tv_ssid);
        tv_bssid = (TextView) itemView.findViewById(R.id.tv_bssid);
        cb_policy = (CheckBox) itemView.findViewById(R.id.cb_policy);
    }
}
