package com.norma.atear_mobile.authorized.adapter;

import android.content.Context;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.authorized.AuthorizedActivity;
import com.norma.atear_mobile.authorized.data.AuthorizedInfo;

import java.util.ArrayList;

/**
 * Created by hyojin on 2018-03-23.
 */

public class AuthorizedAdapter extends RecyclerView.Adapter {

    ArrayList<AuthorizedInfo> authorisedList;
    boolean isDelete;
    Context mContext;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private OnLoadMoreListener onLoadMoreListener;

    private boolean isMoreLoading = true;

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public AuthorizedAdapter(Context mContext, ArrayList<AuthorizedInfo> authorisedList, boolean isDelete, OnLoadMoreListener onLoadMoreListener) {
        super();
        this.mContext = mContext;
        this.authorisedList = authorisedList;
        this.isDelete = isDelete;
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            return new AuthorizedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.authorized_item, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AuthorizedViewHolder) {
            final AuthorizedInfo data = authorisedList.get(position);

            ((AuthorizedViewHolder) holder).tv_location.setText(data.locationName);
            ((AuthorizedViewHolder) holder).tv_bssid.setText(data.bssid);
            ((AuthorizedViewHolder) holder).tv_ssid.setText(data.ssid);
            //체크 박스 보이게

            if (isDelete == true) {
                ((AuthorizedViewHolder) holder).cb_policy.setVisibility(View.VISIBLE);
            } else {
                ((AuthorizedViewHolder) holder).cb_policy.setVisibility(View.GONE);
            }

            if (authorisedList.get(position).del) {
                ((AuthorizedViewHolder) holder).cb_policy.setChecked(true);
            } else {
                ((AuthorizedViewHolder) holder).cb_policy.setChecked(false);

            }

            ((AuthorizedViewHolder) holder).cb_policy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!authorisedList.get(position).del) {
                        authorisedList.get(position).del = true;
                        Log.e("authorized ssid : ", authorisedList.get(position).ssid);
                    } else {
                        authorisedList.get(position).del = false;
                        if (((AuthorizedActivity) AuthorizedActivity.mContext).cb_all.isChecked() == true)
                            ((AuthorizedActivity) AuthorizedActivity.mContext).cb_all.setChecked(false);
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return authorisedList.size();
    }

    public void isDelete(boolean isDelete) {
        this.isDelete = isDelete;
        //notifyDataSetChanged();
    }

    public void setData(ArrayList<AuthorizedInfo> authorisedList) {
        this.authorisedList = authorisedList;
    }

    //리스트뷰 마지막 progress 변형
    @Override
    public int getItemViewType(int position) {
        return authorisedList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void addItemMore(ArrayList<AuthorizedInfo> lst) {
        int sizeInit = authorisedList.size();
        authorisedList.addAll(lst);
        notifyItemRangeChanged(sizeInit, authorisedList.size());
    }

    public void showLoading() {
        Log.e("abdfd", "Lodding");
        if (isMoreLoading && authorisedList != null && onLoadMoreListener != null) {
            isMoreLoading = false;
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    authorisedList.add(null);
                    Log.e("abdfd", "Lodding333");
                    notifyItemInserted(authorisedList.size() - 1);
                    onLoadMoreListener.onLoadMore();
                }
            });
        }
    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public void dismissLoading() {
        if (authorisedList != null && authorisedList.size() > 0) {
            authorisedList.remove(authorisedList.size() - 1);
            notifyItemRemoved(authorisedList.size());
        }
    }

    static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pBar;

        public ProgressViewHolder(View v) {
            super(v);
            pBar = (ProgressBar) v.findViewById(R.id.pBar);
        }
    }
}

