package com.norma.atear_mobile.authorized;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.R;
import com.norma.atear_mobile.authorized.adapter.AuthorizedAdapter;
import com.norma.atear_mobile.authorized.data.AuthorizedInfo;
import com.norma.atear_mobile.csv.CSVImport;
import com.norma.atear_mobile.db.AtEarDB;
import com.norma.atear_mobile.dialog.FileImportDialog;
import com.norma.atear_mobile.project.SharedPref;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by hyojin on 8/10/16.
 */
public class AuthorizedActivity extends Activity implements AuthorizedAdapter.OnLoadMoreListener {

    // private Button btn_delete;
    private TextView tv_title;
    private Button btn_save;
    private LinearLayout btn_back;
    private Button btn_isdel;
    private ImageView btn_import;
    private Button btn_search;
    private EditText edt_search;
    private RecyclerView lv_authorized;
    public CheckBox cb_all;

    private AtEarDB atEarDB;
    public static Context mContext;

    private boolean isDelete = false;

    private ArrayList<AuthorizedInfo> authorizedList;
    private ArrayList<AuthorizedInfo> authorizedTempList;

    private AuthorizedAdapter authorzedAdapter;

    private LinearLayout.LayoutParams layoutControl;
    LinearLayoutManager mLayoutManager;

    SharedPref sharedPref;

    ListResetAsync listResetAsync;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorized);

        findView();

        edt_search.setHint(mContext.getResources().getString(R.string.find_authorized_mac));

        setListView();

        lv_authorized.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (dy > 0 && llManager.findLastCompletelyVisibleItemPosition() == (authorzedAdapter.getItemCount() - 2)) {
                    authorzedAdapter.showLoading();
                }
            }
        });


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = edt_search.getText().toString();
                if (!text.equals("")) {
                    setListView(text);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                } else {
                    setListView();
                }
            }
        });


        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);

                    String text = edt_search.getText().toString();
                    if (!text.equals("")) {
                        setListView(text);
                    } else {
                        setListView();
                    }

                }

                return true;
            }
        });

        btn_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FileImportDialog fileImportDialog = new FileImportDialog(mContext);
                fileImportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                fileImportDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                fileImportDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface $dialog) {
                        FileImportDialog dialog = (FileImportDialog) $dialog;

                        final String fileName = dialog.getFileName();
                        final String filePath = dialog.getFilePath();
                        Log.e("csvFile", filePath + "," + fileName);
                        if (!filePath.equals("none")) {
                            //  CSVImport csvImport = new CSVImport(mContext, atEarDB, filePath, fileName);
                            // csvImport.execute();
                            new CSVImport(mContext, atEarDB, filePath, fileName, new CSVImport.OnChangeText() {
                                @Override
                                public void resultString(String text) {
                                    setListView();
                                }

                                @Override
                                public void postExecuteString(String text) {
                                    Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();

                                    //setListView();
                                }
                            }).execute();

                        } else {
                            //atEarDB.SINGLE_BSSID_TABLE_DROP();
                        }
                    }
                });

                fileImportDialog.show();
            }
        });


        cb_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_all.isChecked()) {
                    for (int i = 0; i < authorizedList.size(); i++) {
                        authorizedList.get(i).del = true;
                    }
                } else {
                    for (int i = 0; i < authorizedList.size(); i++) {
                        authorizedList.get(i).del = false;
                    }
                }
                authorzedAdapter.notifyDataSetChanged();
            }
        });

        btn_isdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDelete == true) {
                    //삭제모드
                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                    Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                    tv_title.setText(mContext.getResources().getString(R.string.delDialogTitleMsg));
                    tv_text.setText(mContext.getResources().getString(R.string.isMacMsg));
                    btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                    btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ArrayList<String> bssidList = new ArrayList<String>();
                            for (AuthorizedInfo info : authorizedList) {
                                if (info.del == true) {
                                    bssidList.add(info.bssid);
                                }
                            }
                            if (bssidList.size() != 0) {
                                new AsyncTask<Void, Void, Void>() {
                                    private ProgressDialog progressDialog;

                                    @Override
                                    protected void onPreExecute() {
                                        super.onPreExecute();
                                        progressDialog = new ProgressDialog(mContext);
                                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                                        progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                                        progressDialog.setMax(bssidList.size());
                                        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getResources().getString(R.string.btnCancelMsg), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                        progressDialog.show();
                                    }

                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        atEarDB.setLoadingProgress(mContext, progressDialog, bssidList.size(), mContext.getResources().getString(R.string.DB_delete));
                                        atEarDB.BSSID_TABLE_DELETE(bssidList);  //리스트 삭제

                                        //  authorizedList = atEarDB.BSSID_TABLE_SELECT();  //다시 조회
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        super.onPostExecute(aVoid);
                                        if (progressDialog.isShowing())
                                            progressDialog.dismiss();

                                        cb_all.setChecked(false);

                                        listResetAsync = new ListResetAsync(edt_search.getText().toString());
                                        listResetAsync.execute();

                                        // authorzedAdapter.notifyDataSetChanged();  //리스트 갱신
                                    }
                                }.execute();

                                //   atEarDB.BSSID_TABLE_DELETE(bssidList);  //리스트 삭제

                                //   authorisedList = atEarDB.BSSID_TABLE_SELECT();  //다시 조회

                                // authorzedAdapter.notifyDataSetChanged();  //리스트 갱신
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.noDelMsg), Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });

                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {

                    isDelete = true;
                    authorzedAdapter.isDelete(isDelete);
                    cb_all.setVisibility(View.VISIBLE);


                    btn_isdel.setBackgroundResource(R.drawable.btn_pressed);
                    btn_isdel.setText(mContext.getResources().getString(R.string.btnDelMsg));
                    btn_isdel.setTextColor(0xFFFFFFFF);
                    btn_save.setBackgroundResource(R.drawable.btn_pressed);
                    btn_save.setText(mContext.getResources().getString(R.string.btnBackMsg));
                    btn_save.setTextColor(0xFFFFFFFF);
                }
                authorzedAdapter.notifyDataSetChanged();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //파일저장
                if (isDelete == true) {
                    isDelete = false;
                    authorzedAdapter.isDelete(isDelete);

                    cb_all.setChecked(false);
                    cb_all.setVisibility(View.GONE);
                    btn_isdel.setBackgroundResource(R.drawable.btn_pressed);
                    btn_isdel.setText(mContext.getResources().getString(R.string.btnEditMsg));
                    btn_save.setBackgroundResource(R.drawable.btn_pressed);
                    btn_save.setText(mContext.getResources().getString(R.string.saveMsg));
                    authorzedAdapter.notifyDataSetChanged();
                } else {
                    //save csv
                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_custom);

                    TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
                    final EditText edt_text = (EditText) dialog.findViewById(R.id.edt_text);
                    Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                    Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                    tv_title.setText(mContext.getResources().getString(R.string.saveDialogTitleMsg));
                    tv_text.setText(mContext.getResources().getString(R.string.inPutMsg));
                    btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                    btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                    edt_text.setVisibility(View.VISIBLE);
                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String value = edt_text.getText().toString();

                            if (value.equals("")) {
                                //  value = "none";
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.inPutMsg), Toast.LENGTH_SHORT).show();
                            } else {
                                CsvFileSave(value);
                            }
                            dialog.dismiss();
                        }
                    });

                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                // CsvFileSave("norma");
                //Toast.makeText(mContext, "파일 저장", Toast.LENGTH_SHORT).show();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void findView() {
        mContext = this;

        tv_title = (TextView) findViewById(R.id.tv_title);
        btn_back = (LinearLayout) findViewById(R.id.btn_back);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_isdel = (Button) findViewById(R.id.btn_isdel);
        lv_authorized = (RecyclerView) findViewById(R.id.lv_authorized);
        cb_all = (CheckBox) findViewById(R.id.cb_all);
        btn_import = (ImageView) findViewById(R.id.btn_import);
        btn_search = (Button) findViewById(R.id.btn_search);
        edt_search = (EditText) findViewById(R.id.edt_search);
        edt_search.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        atEarDB = new AtEarDB(mContext);

        layoutControl = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutControl.leftMargin = 20;
        layoutControl.rightMargin = 20;

        mLayoutManager = new LinearLayoutManager(mContext);

        lv_authorized.setHasFixedSize(true);
        lv_authorized.setLayoutManager(mLayoutManager);
        //  lv_authorized.setItemAnimator(new DefaultItemAnimator());
        //  lv_authorized.set
        sharedPref = new SharedPref(mContext);
    }

    private void setListView() {
        listResetAsync = new ListResetAsync();
        listResetAsync.execute();
    }

    private void setListView(String text) {

        if (text.equals("")) {
            text = null;
            Log.e("texts", "null");
        } else {
            Log.e("texts", text);
        }
        listResetAsync = new ListResetAsync(text.toUpperCase());
        listResetAsync.execute();
    }

    @Override
    public void onLoadMore() {
        new AsyncTask<Void, Void, ArrayList<AuthorizedInfo>>() {
            @Override
            protected ArrayList<AuthorizedInfo> doInBackground(Void... voids) {
                /**
                 *    Delete everything what is below // and place your code logic
                 */
                ArrayList<AuthorizedInfo> list = new ArrayList<>();
                ///////////////////////////////////////////
                if (authorizedTempList.size() <= authorizedTempList.size()) {
                    int start = authorizedTempList.size();
                    int end = start + 500;
                    if (end > authorizedList.size()) {
                        end = authorizedList.size();
                    }


                    for (int i = start; i < end; i++) {
                        list.add(authorizedList.get(i));
                    }
                }
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /////////////////////////////////////////////////
                return list;

            }

            @Override
            protected void onPostExecute(ArrayList<AuthorizedInfo> items) {
                super.onPostExecute(items);
                authorzedAdapter.dismissLoading();
                authorzedAdapter.addItemMore(items);
                authorzedAdapter.setMore(true);
            }
        }.execute();
    }

    //listview 갱신
    class ListResetAsync extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;
        String text = null;

        public ListResetAsync() {
            super();
            text = null;
        }

        public ListResetAsync(String text) {
            super();
            this.text = text;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (text == null)
                authorizedList = atEarDB.BSSID_TABLE_SELECT();
            else
                authorizedList = atEarDB.BSSID_TABLE_SELECT_LIKE(text);


            authorizedTempList = new ArrayList<>();

            if (authorizedList.size() > 500) {
                for (int i = 0; i < 500; i++) {
                    authorizedTempList.add(authorizedList.get(i));
                }
            } else {
                authorizedTempList = authorizedList;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            //    authorzedAdapter = new AuthorizedAdapter(mContext, authorisedList, isDelete);
            setAdapter();
        }

    }

    private void setAdapter() {
        if (authorzedAdapter == null) {
            authorzedAdapter = new AuthorizedAdapter(mContext, authorizedTempList, isDelete, this);

            lv_authorized.setAdapter(authorzedAdapter);
        } else {
            authorzedAdapter.setData(authorizedTempList);

            authorzedAdapter.notifyDataSetChanged();
        }
    }

    private void CsvFileSave(final String file_name) {

        String filePath = "/sdcard/Norma/Authorized";

        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        final String path = filePath + "/" + file_name + ".csv";
        final File file = new File(path);

        if (file.exists()) {
            // Toast.makeText(mContext,"같은 이름에 파일이 존재합니다. 덮어 쓰시겠습니까?",Toast.LENGTH_SHORT).show();
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_custom);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);
            Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

            tv_title.setText("Authorized CSV");
            tv_text.setText(mContext.getResources().getString(R.string.samFileMsg));

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncTask<Void, Void, Void>() {
                        ProgressDialog progressDialog;

                        @Override

                        protected void onPreExecute() {
                            super.onPreExecute();
                            progressDialog = new ProgressDialog(mContext);
                            progressDialog.setCancelable(true);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setMessage(mContext.getResources().getString(R.string.progressMsg));
                            progressDialog.show();
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            file.delete();

                            if (!file_name.equals("null")) {
                                try {
                                    BufferedWriter bw = new BufferedWriter(
                                            new OutputStreamWriter(new FileOutputStream(path), "MS949"));

                                    for (AuthorizedInfo s : authorizedList) {
                                        bw.write(s.bssid + "\r\n");
                                    }
                                    bw.close();


                                } catch (Exception e) {

                                }
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            dialog.dismiss();
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.fileSavedMsg), Toast.LENGTH_SHORT).show();
                        }
                    }.execute();
                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();

        } else {
            if (!file_name.equals("null")) {
                try {
                    BufferedWriter bw = new BufferedWriter(
                            new OutputStreamWriter(new FileOutputStream(path), "MS949"));

                    for (AuthorizedInfo s : authorizedList) {
                        bw.write(s.bssid + "\r\n");
                    }
                    bw.close();

                    Toast.makeText(mContext, mContext.getResources().getString(R.string.fileSavedMsg), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {

                }
            }
        }
    }
}
