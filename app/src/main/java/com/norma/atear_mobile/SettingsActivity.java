package com.norma.atear_mobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.norma.atear_mobile.component.LocaleChanger;
import com.norma.atear_mobile.component.SHA256Hash;
import com.norma.atear_mobile.project.SharedPref;
import com.norma.atear_mobile.project.policy.Policy_setting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by hyojin on 7/8/16.
 */
public class SettingsActivity extends Activity {

    private final static String reset_Url = "https://211.110.140.144/reset_license";

    private int data;

    private final static String RESULT = "result";
    private final static String IS_SUCCESS = "success";

    private RadioButton rb_isms;
    private RadioButton rb_iso;
    private RadioButton rb_norma;
    private RadioButton rb_none;

    private Button btn_language;
    private Button btn_policy;
    private Button btn_app;
    private Button btn_codeReset;

    private TextView tv_title;
    private TextView tv_language;
    private TextView tv_languageTitle;
    private TextView tv_codeTitle;
    private TextView tv_codeResetTitle;
    private TextView tv_policyTitle;
    private TextView tv_resetPolicy;
    private TextView tv_resetSettings;
    private TextView tv_none;
    private TextView tv_resetTitle;


    private LinearLayout btn_back;
    private Context mContext;
    private SHA256Hash sha256Hash;

    private String license = "";

    private SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        findView();

        SetLanguage();

        license = sharedPref.getValue("LINCESE", "fail");

        MainActivity.norma_policy.POLICY = sharedPref.getValue("policyType", 0);
        switch (MainActivity.norma_policy.POLICY) {
            case 0:
                rb_none.setChecked(true);
                data = 0;
                break;
            case 1:
                rb_isms.setChecked(true);
                data = 1;
                break;

            case 2:
                rb_iso.setChecked(true);
                data = 2;
                break;

            case 3:
                rb_norma.setChecked(true);
                data = 3;
                break;
        }

        rb_none.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    rb_isms.setChecked(false);
                    rb_iso.setChecked(false);
                    rb_norma.setChecked(false);

                    data = 0;

                    SharedPref sharedPref = new SharedPref(mContext);
                    sharedPref.put("policyType", data);
                    MainActivity.norma_policy.non_policy();
                }
            }
        });

        rb_isms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_none.setChecked(false);
                    rb_iso.setChecked(false);
                    rb_norma.setChecked(false);

                    data = 1;
                    SharedPref sharedPref = new SharedPref(mContext);
                    sharedPref.put("policyType", data);
                    MainActivity.norma_policy.policy_isms_auth();
                }
            }
        });

        rb_iso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_none.setChecked(false);
                    rb_isms.setChecked(false);
                    rb_norma.setChecked(false);


                    data = 2;
                    SharedPref sharedPref = new SharedPref(mContext);
                    sharedPref.put("policyType", data);
                    MainActivity.norma_policy.policy_iso_auth();
                }
            }
        });

        rb_norma.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_none.setChecked(false);
                    rb_iso.setChecked(false);
                    rb_isms.setChecked(false);


                    data = 3;
                    SharedPref sharedPref = new SharedPref(mContext);
                    sharedPref.put("policyType", data);
                    MainActivity.norma_policy.policy_norma_auth();
                }
            }
        });


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_language);

                final RadioButton rb_english = (RadioButton) dialog.findViewById(R.id.rb_english);
                final RadioButton rb_korean = (RadioButton) dialog.findViewById(R.id.rb_korean);
                final RadioButton rb_japen = (RadioButton) dialog.findViewById(R.id.rb_japen);
                final RadioButton rb_china = (RadioButton) dialog.findViewById(R.id.rb_china);
                final Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
                final TextView tv_china = (TextView) dialog.findViewById(R.id.tv_china);
                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);

                tv_china.setText("中文");
                tv_title.setText(mContext.getResources().getString(R.string.langDialogTitleMsg));
                btn_submit.setText(mContext.getResources().getString(R.string.submitMsg));

                int language = sharedPref.getValue("language", 0);
                switch (language) {
                    case 0: //영어
                        rb_english.setChecked(true);
                        break;

                    case 1: //한국어
                        rb_korean.setChecked(true);
                        break;

                    case 2:  //일본어
                        rb_japen.setChecked(true);
                        break;

                    case 3: // 중국어
                        rb_china.setChecked(true);
                        break;
                }


                rb_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_korean.setChecked(false);
                            rb_japen.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_korean.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_japen.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_japen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_korean.setChecked(false);
                            rb_china.setChecked(false);
                        }
                    }
                });

                rb_china.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            rb_english.setChecked(false);
                            rb_korean.setChecked(false);
                            rb_japen.setChecked(false);
                        }
                    }
                });


                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rb_english.isChecked() == true) {
                            sharedPref.put("language", 0);

                        }
                        if (rb_korean.isChecked() == true) {
                     //        Toast.makeText(getApplicationContext(), "This Korean is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 1);
                        }
                        if (rb_japen.isChecked() == true) {
                       //       Toast.makeText(getApplicationContext(), "This 日本語 is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 2);
                        }
                        if (rb_china.isChecked() == true) {
                      //        Toast.makeText(getApplicationContext(), "This 中文 is coming soon.", Toast.LENGTH_SHORT).show();
                            sharedPref.put("language", 3);
                        }
                        LocaleChanger.setLang(mContext, sharedPref.getValue("language",0));
                        dialog.dismiss();
                        Intent intent = new Intent(mContext, SettingsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();


            }
        });

        btn_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);

                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.string_policy));
                tv_text.setText(mContext.getResources().getString(R.string.policyMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Policy_setting policy_setting = new Policy_setting(mContext);
                        policy_setting.start();
                        dialog.dismiss();
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);

                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.policyDialogTitleMsg));
                tv_text.setText(mContext.getResources().getString(R.string.appMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        MainActivity.norma_policy.POLICY = 0;
                        SharedPref sharedPref = new SharedPref(mContext);
                        sharedPref.delete("policyType");
                        //project 셋팅
                        sharedPref.delete("projectName");
                        sharedPref.delete("locationName");
                        sharedPref.delete("today");
                        sharedPref.delete("saveTime");
                        sharedPref.delete("gps");
                        sharedPref.delete("sampleMAC");

                        Intent intent = new Intent(mContext, SettingsActivity.class);
                        startActivity(intent);
                        finish();
                        dialog.dismiss();
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        });

        btn_codeReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_custom);

                TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                TextView tv_text = (TextView) dialog.findViewById(R.id.tv_text);

                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

                tv_title.setText(mContext.getResources().getString(R.string.codeTitleMsg));
                tv_text.setText(mContext.getResources().getString(R.string.codeResetDialogMsg));
                btn_yes.setText(mContext.getResources().getString(R.string.yesMsg));
                btn_no.setText(mContext.getResources().getString(R.string.noMsg));

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isNetworkConnected() == true) {
                            ResetLicense resetLicense = new ResetLicense(mContext, license, sha256Hash.getHashHexString(DeviceSerial()));
                            resetLicense.execute();
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.netWorkCheckMsg), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        });
    }

    private void findView() {
        mContext = this;

        btn_back = (LinearLayout) findViewById(R.id.btn_back);

        rb_isms = (RadioButton) findViewById(R.id.rb_isms);
        rb_iso = (RadioButton) findViewById(R.id.rb_iso);
        rb_norma = (RadioButton) findViewById(R.id.rb_norma);
        rb_none = (RadioButton) findViewById(R.id.rb_none);

        btn_language = (Button) findViewById(R.id.btn_language);
        btn_policy = (Button) findViewById(R.id.btn_policy);
        btn_app = (Button) findViewById(R.id.btn_app);
        btn_codeReset = (Button) findViewById(R.id.btn_codeReset);


        tv_title = (TextView) findViewById( R.id.tv_title);
        tv_language = (TextView) findViewById(R.id.tv_language);
        tv_languageTitle = (TextView) findViewById(R.id.tv_languageTitle);
        tv_resetTitle = (TextView) findViewById(R.id.tv_resetTitle);
        tv_policyTitle = (TextView) findViewById(R.id.tv_policyTitle);
        tv_resetPolicy = (TextView) findViewById(R.id.tv_resetPolicy);
        tv_resetSettings = (TextView) findViewById(R.id.tv_resetSettings);
        tv_codeResetTitle = (TextView) findViewById(R.id.tv_code_reset_title);
        tv_codeTitle = (TextView) findViewById(R.id.tv_codeTitle);
        tv_none = (TextView) findViewById(R.id.tv_none);

        sharedPref = new SharedPref(mContext);
        sha256Hash = new SHA256Hash();
    }

    private void SetLanguage() {
        tv_language.setText(mContext.getResources().getString(R.string.langDef));
        tv_title.setText(mContext.getResources().getString(R.string.settingsMsg));
        tv_languageTitle.setText(mContext.getResources().getString(R.string.langTitleMsg));
        tv_resetTitle.setText(mContext.getResources().getString(R.string.resetMsg));
        tv_resetPolicy.setText("- " + mContext.getResources().getString(R.string.resetPolicyMsg));
        tv_resetSettings.setText("- " + mContext.getResources().getString(R.string.resetSettingMsg));
        tv_policyTitle.setText(mContext.getResources().getString(R.string.g_titleMsg));
        tv_none.setText(mContext.getResources().getString(R.string.noneMsg));
        tv_codeResetTitle.setText(mContext.getResources().getString(R.string.codeTitleMsg));
        tv_codeTitle.setText("- " + mContext.getResources().getString(R.string.codeMsg));
        btn_language.setText(mContext.getResources().getString(R.string.selectMsg));
        btn_policy.setText(mContext.getResources().getString(R.string.resetMsg));
        btn_app.setText(mContext.getResources().getString(R.string.resetMsg));
        btn_codeReset.setText(mContext.getResources().getString(R.string.btnChangeMsg));

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private class ResetLicense extends AsyncTask<Void, Void, Void> {

        private StringBuilder sb;

        private String license_number = "null";
        private String client_hash = "null";

        private int responseCode = 0;


        private Context mContext;

        private ResetLicense(Context mContext, String license_number, String client_hash) {

            this.mContext = mContext;
            this.license_number = license_number;
            this.client_hash = client_hash;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                Thread.sleep(500);

                URL url = new URL(reset_Url);

                //https
                TrustAllHosts();
                HttpsURLConnection httpURLCon = (HttpsURLConnection) url.openConnection();
                httpURLCon.setHostnameVerifier(DO_NOT_VERIFY);
                httpURLCon.setConnectTimeout(3000);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                httpURLCon.setRequestProperty("Content-Type", "application/json");
                httpURLCon.setRequestMethod("POST");

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("license_number", license_number);
                jsonObject.put("client_hash", client_hash);
                // jsonObject.put("version", version);

                OutputStream os = httpURLCon.getOutputStream();
                os.write(jsonObject.toString().getBytes());
                os.flush();
                responseCode = httpURLCon.getResponseCode();

                BufferedReader bufferedReader = null;

                sb = new StringBuilder();
                Log.e("httpCode_Tag", String.valueOf(responseCode));


                if (responseCode == 200) {
                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json);
                    }


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                responseCode = 510;

            } catch (JSONException e) {
                e.printStackTrace();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (responseCode == 200) {

                Log.e("JsonData", sb.toString());
                JSONArray total_array = null;

                try {
                    JSONObject json_object = new JSONObject(sb.toString());
                    String result = json_object.getString(RESULT);

                    if (result.equals(IS_SUCCESS)) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.codeChangeSuccessMsg), Toast.LENGTH_LONG).show();
                        sharedPref.put("LINCESE_DATE", "fail");
                        sharedPref.put("LINCESE", "fail");

                        Intent intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                        MainActivity.MainAct.finish();
                        finish();

                    } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.netWorkCheckMsg), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {

                }

            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.netWorkCheckMsg), Toast.LENGTH_LONG).show();
            }
        }


    }

    private static void TrustAllHosts() {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static String DeviceSerial() {
        try {
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception ignored) {
            return null;
        }

    }
}
