package com.norma.atear_mobile.utils

import android.content.Context
import android.provider.Settings
import com.norma.atear_mobile.utils.Logger.info

/**
 * Created by dev.oni on 5/28/20.
 * Copyright or OutSourcing Source
 * certificate dev.oni
 */
object Throttle {
    const val ERROR = -1
    const val ENABLED = 1
    const val DISABLED = 0

    fun isEnabled(ctx: Context):Int = try {
        //wifi 쓰로틀링 상태 확인
        val throttle = Settings.Global.getInt(ctx.contentResolver, "wifi_scan_throttle_enabled")

        // wifi 쓰로틀링 해제
        // Permission denial: writing to settings requires:android.permission.WRITE_SECURE_SETTINGS
//        Settings.Global.putInt(ctx.contentResolver, "wifi_scan_throttle_enabled",0)

//        info("throttle", "$throttle")
        throttle
    }catch (  e: Settings.SettingNotFoundException) {
        e.printStackTrace()
        ERROR
    }
}