package com.norma.atear_mobile.utils;

import android.net.wifi.ScanResult;
import android.util.Log;

import java.lang.reflect.Field;

public class Dot11axInfo {
    public static final String LOGNAME="dot11axinfo";
    public static final String SR_MEMBER_IES="informationElements";
    public static final String SR_IE_MEMBER_ID="id";
    public static final String SR_IE_MEMBER_BYTES="bytes";
    public static final int IE_ID_EXTENSION = 255;
    public static final int EID_EXT_HE_CAPABILITY=35;
    public static final int EID_EXT_HE_OPERATION=36;

    public static boolean is11ax(ScanResult result) throws NoSuchFieldException, IllegalAccessException {
        Field field;
        Object[] ieArray;
        boolean has_he_cap = false;
        boolean has_he_op = false;

        field = result.getClass().getDeclaredField(SR_MEMBER_IES);
        field.setAccessible(true);
        ieArray = (Object[])field.get(result);

        if (ieArray != null) {
            for (Object obj : ieArray) {
                int id;
                byte[] bytes;

                /* acquire IE id */
                field = obj.getClass().getDeclaredField(SR_IE_MEMBER_ID);
                field.setAccessible(true);
                id = (int) field.get(obj);

                /* acquire IE bytes */
                field = obj.getClass().getDeclaredField(SR_IE_MEMBER_BYTES);
                bytes = (byte[]) field.get(obj);

                if (id == IE_ID_EXTENSION) {
                    int eid_ext_type = bytes[0];
    //                Log.i("Dot11axInfo", "Found Extenstion IE : type=" + String.valueOf(eid_ext_type));
                    switch (eid_ext_type) {
                        case EID_EXT_HE_CAPABILITY:
                            has_he_cap = true;
                            break;
                        case EID_EXT_HE_OPERATION:
                            has_he_op = true;
                            break;
                    }
                }
            }
        }

        return has_he_cap || has_he_op;
    }
}
