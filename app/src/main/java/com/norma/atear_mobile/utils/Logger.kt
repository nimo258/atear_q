package com.norma.atear_mobile.utils

import android.util.Log
/**
 * Created by dev.oni on 2020-05-26.
 * Copyright or OutSourcing Source
 * certificate dev.oni
 */
object Logger {
    private const val LOG_DEPTH = 8 // Method Stack 4 ~ 8 까지만 탐색
    private const val THIS_DEBUG = true // true = debug, false = release

    @JvmStatic
    fun error(msg:String?=null,e:Exception){
        buildLogException(Log.ERROR,msg?:"",e)
    }
    @JvmStatic
    fun error(msg:String?=null,e:Throwable){
        buildLogException(Log.ERROR,msg?:"",e)
    }
    @JvmStatic
    fun info(msg:String){
        buildLogMsg(Log.ERROR,msg)
    }
    @JvmStatic
    fun info(title:String,msg:String){
        buildLogMsg(Log.ERROR,"[$title] $msg")
    }

    private fun buildLogMsg(priority:Int,msg:String) {
            Log.println(
                priority,
                Thread.currentThread().stackTrace[4].fileName!!.replace(".java",""),
                StringBuilder().apply {
                    append("$msg\n")
                    for(i in 4 until if(Thread.currentThread().stackTrace.size>LOG_DEPTH) LOG_DEPTH else Thread.currentThread().stackTrace.size){
                        append("DEPTH ${i-3} => ")
                        append("${Thread.currentThread().stackTrace[i].className}.${Thread.currentThread().stackTrace[i].methodName}() ")
                        append("(${Thread.currentThread().stackTrace[i].fileName}:${Thread.currentThread().stackTrace[i].lineNumber})\n")
                    }
                }.toString()
            )
    }
    private fun buildLogException(priority:Int,msg:String,ex:Exception){
            Log.println(
                priority,
                Thread.currentThread().stackTrace[4].fileName!!.replace(".java",""),
                StringBuilder().apply {
                    append("$msg\n")
                    for(i in 4 until if(Thread.currentThread().stackTrace.size>LOG_DEPTH) LOG_DEPTH else Thread.currentThread().stackTrace.size){
                        append("DEPTH ${i-3} => ")
                        append("${Thread.currentThread().stackTrace[i].className}.${Thread.currentThread().stackTrace[i].methodName}() ")
                        append("(${Thread.currentThread().stackTrace[i].fileName}:${Thread.currentThread().stackTrace[i].lineNumber})\n")
                    }
                    append("=== === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === ===\n")
                    append("=== === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === ===\n")
                    append(ex.printStackTrace())
                }.toString()
            )
    }
    private fun buildLogException(priority:Int,msg:String,ex:Throwable){
        if(THIS_DEBUG){
            Log.println(
                priority,
                Thread.currentThread().stackTrace[4].fileName!!.replace(".java",""),
                StringBuilder().apply {
                    append("$msg\n")
                    for(i in 4 until if(Thread.currentThread().stackTrace.size>LOG_DEPTH) LOG_DEPTH else Thread.currentThread().stackTrace.size){
                        append("DEPTH ${i-3} => ")
                        append("${Thread.currentThread().stackTrace[i].className}.${Thread.currentThread().stackTrace[i].methodName}() ")
                        append("(${Thread.currentThread().stackTrace[i].fileName}:${Thread.currentThread().stackTrace[i].lineNumber})\n")
                    }
                    append("=== === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === ===\n")
                    append("=== === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === === CAUSE === === === === === === === === === ===\n")
                    append(ex.printStackTrace())
                }.toString()
            )
        }
    }
}