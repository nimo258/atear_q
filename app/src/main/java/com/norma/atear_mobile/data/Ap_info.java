package com.norma.atear_mobile.data;

import java.util.ArrayList;

/**
 * Created by hyojin on 7/8/16.
 */
public class Ap_info {

    public String projectName;
    public String locationName;
    public String save_time;
    public String gps;

    public String bssid;
    public String ssid;
    public int signal;  //signal
    public int channel;
    public String encryption;  //open, wep, wps, wps2  , WPA_s =(wpa,wpa2)
    public String cipher;  // ccmp,tkip
    public String auth;    // psk, mgt
    public String scan_time;  //save time
    public String ap_type;  //soft , hd hoc  //검출 못함  빼자........
    public String oui;
    //public String model;    //iptime
    public String comment; //정책;
    public String alert;  // 위험도.

    public int risk; // 정책 외 위험한것들.(중복mac, ip검증,dns검증);

    public int wps;
    public int isAuthorized;    //인가 여부  1 = 인가 , 0 인가아님
    public int ssid_filter;
    public int bssid_filter;

    public int policyType;  //0 = none  , 1 = isms , 2 = iso  ,  3 = norma

    public String picturePath;
    public String pictureBase64;
    public String memo;
    public int isPentest;  // 0 = none  , 1 = failed  , 2 success
    public String pentest_password;

    public boolean isGrayAp;
    public boolean is11axAp;

    //접속기록 검증 용

    public String geteway; //getway
    public String dns1;
    public String dns2;
    //spoofing
    public int attck_type;    // 0 = null, 1 = arp,  2 = dns , 3= ip  ,4 = ip검증
    public String acttck_ip;   //공경자 ip
    public String attack_mac;// 공격자 mac
    public String attck_time;  //공격 시간


    // risk
    // 중복 mac    10 0x02 2

    // 정책 위험   100 0x04 4
    // 정책 중간  1000 0x08 8
    // 정책 안전 10000 0x10 16

    // softMac  100000 0x20 32
    // 접속 기록 1000000 0x40 64

    public static ArrayList<Ap_info> viewApList; //service에 static하게 저장해두었다가 view에 뿌려주기 위한 list
}
