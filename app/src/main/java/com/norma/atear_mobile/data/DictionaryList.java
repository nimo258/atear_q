package com.norma.atear_mobile.data;

import java.util.ArrayList;

/**
 * Created by hyojin on 8/17/16.
 */
public class DictionaryList {

    private String[] WPA_Password = {"sktelecom", "00000000", "11111111", "12341234"
            /*"12345678"*/, "123456789", "123456789a", "123456789b", "123456789c", "1234567890", "0123456789a",
            "a123456789", "1q2w3e4r", "1q2w3e4r!", "1q2w3e4r5", "1q2w3e4r5", "1q2w3e4r5t", "1qaz2wsx",
            "1234qwer", "77777777", "789456123", "moda16273", "88888888", "987654321a", "987654321b", "534f4b4354",
            "asdfasdf", "computer", "dhkdlvkdl", "iloveyou", "password", "passw0rd", "P@ssw0rd", "password1",
            "Password1", "password4", "Password", "password12", "password123", "q1w2e3r4", "qwertyui", "qlalfqjsgh", "bean12345678", "2676674a", "2555571a",
            "xxxxxxxx", "2681815a", "myLGNetfe07", "SHOW3382", "0432961111",/*"norma./12345678./",*/ "2648509a", "12345678","lguplus002", "lguplus100", "show3382"};

    private String[] WEP_Password = {"00000", "11111", "12345", "55555", "77777", "hello"};

    private String[] ssid_random = {"0", "1", "00", "01", "a", "b", "aa", "11", "bb", "xx", "q", "w", "qw", "qwer",
            "q1", "q2", "qw12", "77", "7", "as", "asd", "asdf", "1234", "0000", "000", "ccc", "c", "aaaa", "qqq",
            "qqq", "abcd", "!", "@", "!@", "#", "##", "$", "!@#", "!!"};

    public ArrayList<String> dictionaryList;

    public DictionaryList(String ssid, String enc) {
        super();
        dictionaryList = new ArrayList<>();
        if (enc.equals("WEP")) {
            for (int i = 0; i < WEP_Password.length; i++)
                dictionaryList.add(WEP_Password[i]);
        } else {
            //SSID rnadom list
            for (int i = 0; i < ssid_random.length; i++) {
                String pwd = ssid + ssid_random[i];

                if (pwd.length() >= 8)
                    dictionaryList.add(pwd);
            }
            // WPA LIST
            for (int i = 0; i < WPA_Password.length; i++)
                dictionaryList.add(WPA_Password[i]);

        }
    }
}
