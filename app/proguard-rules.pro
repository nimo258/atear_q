# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/hyojin/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep public class *{public protected *;}
-keep class com.itextpdf.text.**{*;}

-dontwarn jcifs.http.**
-dontwarn org.xbill.**
-dontwarn com.itextpdf.text.**
-dontwarn org.apache.**
-dontwarn com.microsoft.schemas.**
-dontwarn org.etsi.uri.x01903.**
-dontwarn org.openxmlformats.schemas.**
-dontwarn org.etsi.uri.x01903.v13.**
-dontwarn org.openxmlformats.schemas.**
-dontwarn schemasMicrosoftComOfficeExcel.**
-dontwarn schemasMicrosoftComVml.**
-dontwarn org.w3.x2000.x09.xmldsig.**
-dontwarn schemasMicrosoftComOfficeOffice.**
-dontwarn com.norma.*

# Excel NoClassDefFoundError [attached by dev.oni]
-keep class aavax.xml.stream.** {*;}
-keep interface aavax.xml.stream.** {*;}
-keep class aavax.xml.stream.FactoryFinder {*;}
-keep class org.apache.poi.** {*;}
-keep interface org.apache.poi.** {*;}
-keep class org.apache.poi.openxml4j.opc.** {*;}
-keep interface org.apache.poi.openxml4j.opc.** {*;}
-keep class org.apache.xmlbeans.impl.store.** {*;}
# ---------------------------
